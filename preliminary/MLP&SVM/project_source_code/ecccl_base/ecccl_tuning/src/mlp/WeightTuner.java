package mlp;

import main.Tuning;
import opencl.Kernel;
import opencl.OCLUtil;
import opencl.PlatformUtil;
import opencl.params_dynamic.ParamsL1L2L3;
import org.lwjgl.opencl.CL10;
import executor.training.WeightUpdateExecutor;
import util.JSONUtil;

public class WeightTuner {
    private final static int WARM_UP_RUNS = 3;
    private final static int RUNS = 5;

    public static void main(String[] args) {
        Tuning.initPlatform(args[0], args[1], args[2]);
        boolean err = false;

        // parse args
        int chainCount = Integer.parseInt(args[3]);
        String[] layersStr = args[4].split(";");
        String tpsJSON = args[5];

        int[] layers = new int[layersStr.length];
        for(int i = 0; i < layersStr.length; i++) {
            layers[i] = Integer.parseInt(layersStr[i].trim());
        }

        try {
            // init tuning parameter
            ParamsL1L2L3 params = tpsJSON.equals("") ? new ParamsL1L2L3(ParamsL1L2L3.TUNING_FILE_WEIGHT_UPDATE)
                    : new ParamsL1L2L3(JSONUtil.readArgs(tpsJSON));

            WeightUpdateExecutor[] weightExecutor = new WeightUpdateExecutor[layers.length - 1];
            for(int i = 0; i < layers.length - 1; i++) {
                weightExecutor[i] = new WeightUpdateExecutor(chainCount, layers[i], layers[i + 1], params);
            }

            long runtime = 0;

            // warm up
            for(int i = 0; i < WARM_UP_RUNS; i++) getRuntime(chainCount, layers, weightExecutor);

            // profile
            for(int i = 0; i < RUNS; i++) runtime += getRuntime(chainCount, layers, weightExecutor);
            runtime /= RUNS;

            // release executors
            for(int i = 0; i < layers.length - 1; i++) {
                weightExecutor[i].release();
            }

            System.out.println(runtime);
        } catch (Exception e) {
            err = true;
            e.printStackTrace();
        } finally {
            PlatformUtil.requestShutdown();
            if(err) System.exit(1);
        }
    }

    private static long getRuntime(int chainCount, int[] layers, WeightUpdateExecutor[] exec) {
        long runtime = 0;

        for(int i = 0; i < layers.length - 1; i++) {

            // init memory on device
            long Z = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, chainCount * layers[i] * 4);
            long D = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, chainCount * layers[i + 1] * 4);
            long weights = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, chainCount * layers[i] * layers[i + 1] * 4);

            long prevGrad = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, chainCount * layers[i] * layers[i + 1] * 4);
            long prevGradTmp = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, chainCount * layers[i] * layers[i + 1] * 4);

            // profile
            runtime += exec[i].profile(Z, D, weights, prevGrad, prevGradTmp, 0.3f, 0.2f);

            // release memory
            CL10.clReleaseMemObject(Z);
            CL10.clReleaseMemObject(D);
            CL10.clReleaseMemObject(weights);
            CL10.clReleaseMemObject(prevGrad);
            CL10.clReleaseMemObject(prevGradTmp);
        }
        return runtime;
    }
}
