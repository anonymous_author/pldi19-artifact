package mlp;

import main.Tuning;
import opencl.Kernel;
import opencl.OCLUtil;
import opencl.PlatformUtil;
import opencl.params_dynamic.ParamsL1L2R1;
import org.lwjgl.opencl.CL10;
import executor.training.ForwardExecutor;
import util.JSONUtil;

/**
 * Tuner for tuning the forward steps without the last step.
 */
public class ForwardTuner {
    private final static int WARM_UP_RUNS = 3;
    private final static int RUNS = 5;

    public static void main(String[] args) {
        Tuning.initPlatform(args[0], args[1], args[2]);
        boolean err = false;

        // parse args
        int chainCount = Integer.parseInt(args[3]);
        String[] layersStr = args[4].split(";");
        String tpsJSON = args[5];

        int[] layers = new int[layersStr.length];
        for(int i = 0; i < layersStr.length; i++) layers[i] = Integer.parseInt(layersStr[i].trim());

        try {
            // init tuning parameter
            ParamsL1L2R1 params = tpsJSON.equals("") ? new ParamsL1L2R1(ParamsL1L2R1.TUNING_FILE_FORWARD_GEMM)
                    : new ParamsL1L2R1(JSONUtil.readArgs(tpsJSON));

            // init executor
            ForwardExecutor[] exec = new ForwardExecutor[layers.length - 2];
            for(int i = 0; i < layers.length - 2; i++) {
                exec[i] = new ForwardExecutor(chainCount, layers[i + 1], layers[i], params);
            }

            long runtime = 0;

            // warm up
            for(int i = 0; i < WARM_UP_RUNS; i++) getRuntime(chainCount, layers, exec);

            // profile
            for(int i = 0; i < RUNS; i++) runtime += getRuntime(chainCount, layers, exec);
            runtime /= RUNS;

            System.out.println(runtime);

            for(int i = 0; i < layers.length - 2; i++) {
                exec[i].release();
            }
        } catch (Exception e) {
            err = true;
        } finally {
            PlatformUtil.requestShutdown();
            if(err) System.exit(1);
        }
    }

    private static long getRuntime(int chainCount, int[] layers, ForwardExecutor[] exec) {
        long runtime = 0;

        // profile forward step
        for(int i = 0; i < layers.length - 2; i++) {

            // init memory on device
            long A = OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, chainCount * layers[i] * 4);
            long B = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, chainCount * layers[i] * layers[i + 1] * 4);
            long S = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, chainCount * layers[i + 1] * 4);
            long nextZ = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, chainCount * layers[i + 1] * 4);

            long Bias = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, chainCount * layers[i + 1] * 4);

            // profile
            runtime += exec[i].profile(A, B, S, nextZ, Bias);

            // release memory
            CL10.clReleaseMemObject(A);
            CL10.clReleaseMemObject(B);
            CL10.clReleaseMemObject(S);
            CL10.clReleaseMemObject(nextZ);
            CL10.clReleaseMemObject(Bias);
        }

        return runtime;
    }
}
