package svm;

import com.ecccl.svm.executor.training.ErrorUpdateExecutor;
import main.Tuning;
import opencl.Kernel;
import opencl.OCLUtil;
import opencl.PlatformUtil;
import opencl.params.ParamsL1;
import util.JSONUtil;

public class ErrorUpdateTuning {
    private final static int WARM_UP_RUNS = 10;
    private final static int RUNS = 25;

    public static void main(String[] args) {
        Tuning.initPlatform(args[0], args[1], args[2]);
        boolean err = false;

        // read Args
        int numInstances = Integer.parseInt(args[3]);
        String tpsJSON = args[4];

        try {
            // init TPs
            ParamsL1 errorParams = tpsJSON.equals("") ? new ParamsL1("/tuning_results/svm/error_update_kernel_tp.json")
                    : new ParamsL1(JSONUtil.readArgs(tpsJSON));

            // init memory on device
            long cache = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, numInstances * 4 * 2);
            long error = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, numInstances * 4);
            long list = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, numInstances);

            // init executor
            ErrorUpdateExecutor exec = ErrorUpdateExecutor.getInstance(errorParams, numInstances, cache, error, list);

            // warm up
            for(int i = 0; i < WARM_UP_RUNS; i++) exec.profile(0, 1, 0, 0, 0, 1, (byte) 1, (byte) 2);

            // profile
            long runtime = 0;
            for(int i = 0; i < RUNS; i++) runtime += exec.profile(0, 1, 0, 0,0, 1, (byte) 1, (byte) 2);
            runtime /= RUNS;

            // release memory
            OCLUtil.releaseMem(cache);
            OCLUtil.releaseMem(error);
            OCLUtil.releaseMem(list);

            System.out.println(runtime);
        } catch (Exception e) {
            err = true;
        } finally {
            PlatformUtil.requestShutdown();
            if(err) System.exit(1);
        }
    }
}
