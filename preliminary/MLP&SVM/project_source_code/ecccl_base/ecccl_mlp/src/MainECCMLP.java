import classifier.EccMLP;
import evaluator.Evaluator;
import model.InputData;
import model.LabelResults;
import model.ModelEccMLP;
import opencl.PlatformUtil;
import tester.EccMLPTester;
import util.*;

public class MainECCMLP {
    public static void main(String[] args) throws Exception {
        InputData train = WekaLoader.load("./data/ecccl/emotions-train-norm", 6);
        InputData test = WekaLoader.load("./data/ecccl/emotions-test-norm", 6);

        PlatformUtil.requestInit(Config.PLATFORM_ID, Config.DEVICE_ID, Config.DEVICE_TYPE);

        // Parameter
        int[] layerSizes = new int[]{-1};
        float learningRate = 0.3f;
        float momentum = 0.2f;
        int epoch = 10;

        int chainCount = 10;
        int seed = 0;
        int subsetPercentage = 100;

        int batchSize = 256;
        float thresholdMLP = 0.5f;
        float thresholdECC = 0.5f;

        // initialize tester and classifier
        EccMLP eccMLP = new EccMLP.Builder(layerSizes, epoch)
                .setChainCount(chainCount)
                .setSubsetPercentage(subsetPercentage)
                .setSeed(seed)
                .setLearningRate(learningRate)
                .setMomentum(momentum)
                .build();

        EccMLPTester tester = new EccMLPTester.Builder()
                .setBatchSize(batchSize)
                .setThresholdMLP(thresholdMLP)
                .setThresholdECC(thresholdECC)
                .setUsePadding(true)
                .build();

        // training
        long startTrain = System.currentTimeMillis();
        ModelEccMLP model = eccMLP.train(train);
        long endTrain = System.currentTimeMillis();

        ModelIOUtil.storeModel(model, "./models/result.model");

        //ModelEccMLP model = ModelIOUtil.restoreModel("./models/result.model");

        // testing
        long startClassBatch = System.currentTimeMillis();
        LabelResults resultsBatch = tester.classify(test, model);
        long endClassBatch = System.currentTimeMillis();

        Evaluator.evaluate(test, resultsBatch, "./outputs/");

        System.out.println("\nRuntime Training: " + (endTrain - startTrain) + " ms");
        System.out.println("Runtime Training Kernel: " + MLPBenchmark.getRunTimeTraining() + " ms");
        System.out.println("Runtime Training (ohne Bauen): " + (endTrain - startTrain - MLPBenchmark.getBuildTimeTraining()) + " ms");

        System.out.println("\nRuntime Batch Classifying: " + (endClassBatch - startClassBatch) + " ms");
        System.out.println("Runtime Batch Classifying Kernel: " + MLPBenchmark.getRunTimeTesting() + " ms");
        System.out.println("Runtime Batch Classifying (ohne Bauen): " + (endClassBatch - startClassBatch - MLPBenchmark.getBuildTimeTesting()) + " ms");

        PlatformUtil.requestShutdown();
    }
}
