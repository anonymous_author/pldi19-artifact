package tasks.routines;

import executor.training.BackwardExecutor;
import tasks.training.GpuTrainingPtr;

public class Backward {
    private int[] layer;

    private BackwardExecutor[] backwardExecutor;

    private GpuTrainingPtr gpuPtr;

    public Backward(int chainCount, int[] layer, GpuTrainingPtr gpuPtr) {
        this.layer = layer;
        this.gpuPtr = gpuPtr;

        backwardExecutor = new BackwardExecutor[layer.length - 2];

        for(int i = layer.length - 1; i >= 2; i--) {
            backwardExecutor[i - 2] = new BackwardExecutor(chainCount, layer[i - 1], layer[i]);
        }
    }

    public void execute() {

        for(int i = layer.length - 1; i >= 2; i--) {

            // compute matrix D (delta) of previous layer
            backwardExecutor[i - 2].execute(gpuPtr.D(i - 1), gpuPtr.weights(i - 1), gpuPtr.S(i - 2),
                    gpuPtr.B(i - 2), gpuPtr.D(i - 2));

        }
    }

    public void finish() {
        for(int i = layer.length - 1; i >= 2; i--) {
            backwardExecutor[i - 2].release();
        }
    }
}
