package tasks.classification;

import opencl.OCLUtil;

public class GpuClassificationPtr {

    private long[] weights;
    private long[] B;
    private long[] Z;

    public GpuClassificationPtr(int[] layer) {
        this.weights = new long[layer.length - 1];
        this.Z = new long[layer.length - 1];
        this.B = new long[layer.length - 1];
    }

    public void release() {
        OCLUtil.releaseMem(weights);
        OCLUtil.releaseMem(Z);
        OCLUtil.releaseMem(B);
    }

    public long[] weights() {
        return weights;
    }

    public long weights(int index) {
        return weights[index];
    }

    public void setWeights(int index, long ptr) {
        weights[index] = ptr;
    }

    public long[] B() {
        return B;
    }

    public long B(int index) {
        return B[index];
    }

    public void setB(int index, long ptr) {
        B[index] = ptr;
    }

    public long[] Z() {
        return Z;
    }

    public long Z(int index) {
        return Z[index];
    }

    public void setZ(int index, long ptr) {
        Z[index] = ptr;
    }
}
