package tasks.training;

import opencl.OCLUtil;

public class GpuTrainingPtr {

    // gpu pointer: weights of each layer
    private long[] weights;

    // gpu pointer: input and output of neurons in each layer
    private long[] S;
    private long[] Z;

    // gpu pointer: delta values and delta values of prev step
    private long[] D;
    private long[] oldD;

    // gpu pointer: bias values of each layer
    private long[] B;

    public GpuTrainingPtr(int[] layer) {
        weights = new long[layer.length - 1];

        S = new long[layer.length - 1];
        Z = new long[layer.length - 1];

        D = new long[layer.length - 1];
        oldD = new long[layer.length - 1];

        B = new long[layer.length - 1];
    }

    public void release() {
        OCLUtil.releaseMem(weights);
        OCLUtil.releaseMem(S);
        OCLUtil.releaseMem(Z);
        OCLUtil.releaseMem(D);
        OCLUtil.releaseMem(oldD);
        OCLUtil.releaseMem(B);
    }

    public void swapDelta() {
        long[] tempD = oldD;
        oldD = D;
        D = tempD;
    }

    public long[] weights() {
        return weights;
    }

    public long weights(int index) {
        return weights[index];
    }

    public void setWeights(int index, long ptr) {
        weights[index] = ptr;
    }

    public long[] S() {
        return S;
    }

    public long S(int index) {
        return S[index];
    }

    public void setS(int index, long ptr) {
        S[index] = ptr;
    }

    public long[] Z() {
        return Z;
    }

    public long Z(int index) {
        return Z[index];
    }

    public void setZ(int index, long ptr) {
        Z[index] = ptr;
    }

    public long[] D() {
        return D;
    }

    public long D(int index) {
        return D[index];
    }

    public void setD(int index, long ptr) {
        D[index] = ptr;
    }

    public long[] oldD() {
        return oldD;
    }

    public long oldD(int index) {
        return oldD[index];
    }

    public void setOldD(int index, long ptr) {
        oldD[index] = ptr;
    }

    public long[] B() {
        return B;
    }

    public long B(int index) {
        return B[index];
    }

    public void setB(int index, long ptr) {
        B[index] = ptr;
    }
}
