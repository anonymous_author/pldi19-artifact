package executor.training;

import opencl.Kernel;
import opencl.PlatformUtil;
import opencl.params_dynamic.ParamsL1L2;
import org.lwjgl.BufferUtils;
import util.MLPBenchmark;

import java.nio.FloatBuffer;

public class BiasExecutor {
    private ParamsL1L2 params;
    private Kernel kernel;

    // buffer for learning rate and momentum
    private FloatBuffer learningRateBuffer, momentumBuffer;

    public interface ARGS {
        int SIZE = 5;
        int B = 0, D = 1, oldD = 2, LEARNING_RATE = 3, MOMENTUM = 4;
    }

    private BiasExecutor(int L1, int L2, ParamsL1L2 params, boolean useCache) {
        this.params = params;

        long start = System.nanoTime();
        buildKernel(L1, L2, useCache);
        long end = System.nanoTime();
        MLPBenchmark.weightUpdateBuild += end - start;

        initArgs();
    }

    public BiasExecutor(int L1, int L2) {
        this(L1, L2, new ParamsL1L2(ParamsL1L2.TUNING_FILE_WEIGHT_BIAS), true);
    }

    public BiasExecutor(int L1, int L2, ParamsL1L2 params) {
        this(L1, L2, params, false);
    }

    private void buildKernel(int L1, int L2, boolean useCache) {
        long program = PlatformUtil.buildProgram("ocl_kernel/mlp/mlp_weight_bias", params.getOptions(L1, L2), useCache);

        kernel = new Kernel(program, "mlp_weight_bias_1");
        kernel.setLocalSize(params.LOCAL_SIZE_DIM_0, params.LOCAL_SIZE_DIM_1);
        kernel.setGlobalSize(params.GLOBAL_SIZE_DIM_0, params.GLOBAL_SIZE_DIM_1);
        kernel.setWorkDim(params.WORK_DIM);
    }

    private void initArgs() {
        kernel.initArgs(ARGS.SIZE);
        learningRateBuffer = BufferUtils.createFloatBuffer(1);
        momentumBuffer = BufferUtils.createFloatBuffer(1);
        kernel.setScalarArg(ARGS.LEARNING_RATE, learningRateBuffer);
        kernel.setScalarArg(ARGS.MOMENTUM, momentumBuffer);
    }

    private void updateArguments(long B, long D, long oldD, float learningRate, float momentum) {
        learningRateBuffer.put(learningRate).flip();
        momentumBuffer.put(momentum).flip();

        kernel.setArg(ARGS.B, B, Kernel.TypeFlag.SHARED);
        kernel.setArg(ARGS.D, D, Kernel.TypeFlag.SHARED);
        kernel.setArg(ARGS.oldD, oldD, Kernel.TypeFlag.SHARED);
        kernel.setArgs();
    }

    public long execute(long B, long D, long oldD, float learningRate, float momentum) {
        updateArguments(B, D, oldD, learningRate, momentum);

        if(PlatformUtil.PROFILING_ENABLED) {
            MLPBenchmark.weightUpdateKernelTime += kernel.executeKernelProfile();
        } else {
            kernel.executeKernel();
        }

        return B;
    }

    public long profile(long B, long D, long oldD, float learningRate, float momentum) {
        updateArguments(B, D, oldD, learningRate, momentum);

        return kernel.executeKernelProfile();
    }

    public void release() {
        if (kernel != null) {
            kernel.releaseArgs();
            kernel.releaseKernel();
        }
    }

}
