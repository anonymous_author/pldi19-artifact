package util;

import opencl.params_dynamic.ParamsL1L2L3R1;

import java.nio.FloatBuffer;

public class PaddingUtil {

    public static int getPadding(int layerId, int[] layer) {
        ParamsL1L2L3R1 paramsFwBatch = ParamsL1L2L3R1.getParamsForwardBatch(layer[layerId]);
        int padding;

        if(layerId == 0) {
            padding = (int) Math.ceil((double) layer[layerId] / paramsFwBatch.L_CB_SIZE_R_1) * paramsFwBatch.L_CB_SIZE_R_1;
        } else if (layerId == layer.length - 1) {
            padding = (int) Math.ceil((double) layer[layerId] / paramsFwBatch.L_CB_SIZE_L_3) * paramsFwBatch.L_CB_SIZE_L_3;
        } else {

            // get biggest L_CB_SIZE for next layer and current layer
            int biggestSize = paramsFwBatch.L_CB_SIZE_L_3 > paramsFwBatch.L_CB_SIZE_R_1
                    ? paramsFwBatch.L_CB_SIZE_L_3 : paramsFwBatch.L_CB_SIZE_R_1;

            padding = (int) Math.ceil((double) layer[layerId] / biggestSize) * biggestSize;
        }

        return padding;
    }

    public static void fillPaddingNegInf(FloatBuffer buffer, int layerSize, int layerPadSize) {
        if(layerSize < layerPadSize) {
            for(int i = 0; i < layerPadSize - layerSize; i++) {
                buffer.put(Float.NEGATIVE_INFINITY);
            }
        }
    }


}
