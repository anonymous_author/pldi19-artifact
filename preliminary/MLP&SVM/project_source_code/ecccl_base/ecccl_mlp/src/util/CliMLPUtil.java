package util;

import classifier.EccMLP;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import tester.EccMLPTester;

public abstract class CliMLPUtil extends CliUtil {
    private static final float DEFAULT_LEARNING_RATE = 0.3f;
    private static final float DEFAULT_MOMENTUM = 0.2f;
    private static final int DEFAULT_EPOCH = 50;
    private static final float DEFAULT_THRESHOLD_MLP = 0.5f;

    private static final int DEFAULT_BATCH_SIZE = 32;
    private static final int DEFAULT_NUM_CC = 10;
    private static final int DEFAULT_BAG_SIZE = 100;
    private static final int DEFAULT_SEED = 0;
    private static final float DEFAULT_THRESHOLD_ECC = 0.5f;

    public static Options getMLPOptions() {
        Options optionsParameter = new Options();
        optionsParameter.addOption("layer",true, "hidden layer sizes: \"n1,n2,n3,...\" (default: \"a\" = half of input layer)\n ");
        optionsParameter.addOption("e", "epoch", true, "number of epochs (default " + DEFAULT_EPOCH + ")");
        optionsParameter.addOption("r", "rate", true, "learning rate (default " + DEFAULT_LEARNING_RATE + ")");
        optionsParameter.addOption("m", "momentum", true, "momentum (default " + DEFAULT_MOMENTUM + ")");

        optionsParameter.addOption("b", "bag", true, "size of samples in each chain in percentage (default " + DEFAULT_BAG_SIZE + ")");
        optionsParameter.addOption("n", "num", true, "ensemble size: number of classifier chain models (default " + DEFAULT_NUM_CC + ")");
        optionsParameter.addOption("s", "seed", true, "seeding of random number generator (default " + DEFAULT_SEED + ")");
        return optionsParameter;
    }

    public static EccMLP getMLPFromOptions(CommandLine cmd) {
        String[] layerStr = parseString("layer", cmd, "a").split(",");

        // parse layer
        int[] layer = new int[layerStr.length];
        for(int i = 0; i < layerStr.length; i++) {
            if(layerStr[i].equals("a")) layer[i] = -1;
            else layer[i] = Integer.parseInt(layerStr[i]);
        }

        // parse SVM parameter
        int epoch = parseInt("e", cmd, DEFAULT_EPOCH);
        float learningRate = parseFloat("r", cmd, DEFAULT_LEARNING_RATE);
        float momentum = parseFloat("m", cmd, DEFAULT_MOMENTUM);

        // parse ECC parameter
        int bagSize = parseInt("b", cmd, DEFAULT_BAG_SIZE);
        int numCC = parseInt("n", cmd, DEFAULT_NUM_CC);
        int seed = parseInt("s", cmd, DEFAULT_SEED);

        return new EccMLP.Builder(layer, epoch)
                .setLearningRate(learningRate)
                .setMomentum(momentum)
                .setSubsetPercentage(bagSize)
                .setChainCount(numCC)
                .setSeed(seed)
                .build();
    }

    public static Options getMLPEvalOptions() {
        Options optionsParameter = getMLPOptions();
        optionsParameter.addOption("t", "threshold", true,
                "threshold for deciding class in MLP (default " + DEFAULT_THRESHOLD_MLP + ")");
        optionsParameter.addOption("tecc", "thresholdECC", true,
                "threshold for deciding class in ECC (default " + DEFAULT_THRESHOLD_ECC + ")");
        optionsParameter.addOption("bs", "batch" ,true,
                "size of batch when classifying (default " + DEFAULT_BATCH_SIZE + ")");
        optionsParameter.addOption("p", "padding", false,
                "use padding [may be required] (default false)");

        return optionsParameter;
    }

    public static Options getMLPClassifyOptions() {
        Options optionsParameter = new Options();
        optionsParameter.addOption("t", "threshold", true,
                "threshold for deciding class in MLP (default " + DEFAULT_THRESHOLD_MLP + ")");
        optionsParameter.addOption("tecc", "thresholdECC", true,
                "threshold for deciding class in ECC (default " + DEFAULT_THRESHOLD_ECC + ")");
        optionsParameter.addOption("bs", "batch" ,true,
                "size of batch when classifying (default " + DEFAULT_BATCH_SIZE + ")");
        optionsParameter.addOption("p", "padding", false,
                "use padding [may be required] (default false)");
        return optionsParameter;
    }

    public static EccMLPTester getMLPTesterFromOptions(CommandLine cmd) {
        int batchSize = parseInt("bs", cmd, DEFAULT_BATCH_SIZE);
        float thresholdMLP = parseFloat("t", cmd, DEFAULT_THRESHOLD_MLP);
        float thresholdECC = parseFloat("tecc", cmd, DEFAULT_THRESHOLD_ECC);
        boolean usePadding = cmd.hasOption("p");

        return new EccMLPTester.Builder()
                .setBatchSize(batchSize)
                .setThresholdMLP(thresholdMLP)
                .setThresholdECC(thresholdECC)
                .setUsePadding(usePadding)
                .build();
    }

    public static OptionComparator<Option> getTrainComparator() {
        return new OptionComparator<>("lermbns");
    }

    public static OptionComparator<Option>  getEvalComparator() {
        return new OptionComparator<>("lermtbns");
    }

    public static OptionComparator<Option> getClassifyComparator() {
        return new OptionComparator<>("tbp");
    }
}
