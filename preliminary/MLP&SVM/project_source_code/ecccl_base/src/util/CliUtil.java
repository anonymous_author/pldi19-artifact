package util;

import opencl.PlatformUtil;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import util.Config;

import java.util.Comparator;

public abstract class CliUtil {

    private static final int DEFAULT_PLATFORM_ID = 0;
    private static final int DEFAULT_DEVICE_ID = 0;
    private static final String DEFAULT_DEVICE_TYPE = "gpu";

    public static Options getPlatformOptions() {
        Options optionsPlatform = new Options();
        optionsPlatform.addOption(new Option("pid", true, "platform id (default 0)"));
        optionsPlatform.addOption(new Option("did", true, "device id (default 0)"));
        optionsPlatform.addOption(new Option("dtype", true, "device type: gpu | cpu | acc (default gpu)"));
        return optionsPlatform;
    }

    public static void parsePlatformOptions(CommandLine cmd) {
        Config.PLATFORM_ID = parseInt("pid", cmd, DEFAULT_PLATFORM_ID);
        Config.DEVICE_ID = parseInt("did", cmd, DEFAULT_DEVICE_ID);
        String deviceType = parseString("dtype", cmd, DEFAULT_DEVICE_TYPE);

        switch (deviceType) {
            case "gpu":
                Config.DEVICE_TYPE = PlatformUtil.DeviceType.GPU;
                break;
            case "cpu":
                Config.DEVICE_TYPE = PlatformUtil.DeviceType.CPU;
                break;
            case "acc":
                Config.DEVICE_TYPE = PlatformUtil.DeviceType.ACC;
                break;
        }

    }

    public static void formatHelp(Options optionsIO, Options optionsPlatform, Options optionsParameter,
                                  OptionComparator<Option> order, String cmdLineSyntax) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.setOptionComparator(new OptionComparator<>("tloq"));
        formatter.printHelp( cmdLineSyntax, "Input and Output", optionsIO, "");
        formatter.setSyntaxPrefix("");
        formatter.setOptionComparator(order);
        formatter.printHelp("Platform", optionsPlatform);
        formatter.printHelp( "Parameter", optionsParameter);
    }

    public static void formatHelp(Options optionsIO, Options optionsPlatform, Options optionsParameter,
                                  String orderInput, String order, String cmdLineSyntax) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.setOptionComparator(new OptionComparator<>(orderInput));
        formatter.printHelp( cmdLineSyntax, "Input and Output", optionsIO, "");
        formatter.setSyntaxPrefix("");
        formatter.setOptionComparator(new OptionComparator<>(order));
        formatter.printHelp("Platform", optionsPlatform);
        formatter.printHelp( "Parameter", optionsParameter);
    }

    protected static class OptionComparator<T extends Option> implements Comparator<T> {
        private String order; // short option names

        public OptionComparator(String order) {
            this.order = order;
        }

        public int compare(T o1, T o2) {
            return order.indexOf(o1.getOpt()) - order.indexOf(o2.getOpt());
        }
    }

    public static String parseString(String option, CommandLine cmd, String defaultVal) {
        return cmd.hasOption(option) ? cmd.getOptionValue(option) : defaultVal;
    }

    public static float parseFloat(String option, CommandLine cmd, float defaultVal) {
        return cmd.hasOption(option) ? Float.parseFloat(cmd.getOptionValue(option)) : defaultVal;
    }

    public static int parseInt(String option, CommandLine cmd, int defaultVal) {
        return cmd.hasOption(option) ? Integer.parseInt(cmd.getOptionValue(option)) : defaultVal;
    }
}
