package opencl.params;

import org.json.simple.JSONObject;
import util.JSONUtil;

public class ParamsL1L2R1 {
    public static final String TUNING_FILE_RBF = "/tuning_results/svm/eval_rbf_kernel_tp.json";
    public static final String TUNING_FILE_FORWARD_GEMM = "/tuning_results/mlp_old/gemm_kernel_tp.json";

    private static ParamsL1L2R1 PARAMS_RBF;

    public static ParamsL1L2R1 getParamsRbf() {
        if(PARAMS_RBF == null) {
            PARAMS_RBF = new ParamsL1L2R1(TUNING_FILE_RBF);
        }
        return PARAMS_RBF;
    }

    public int RANGE;

    public int CACHE_L_CB;
    public int CACHE_P_CB;

    public int G_CB_RES_DEST_LEVEL;
    public int L_CB_RES_DEST_LEVEL;
    public int P_CB_RES_DEST_LEVEL;

    // L1 Dimension
    // G_CB_SIZE_L_1 = RANGE;
    public int L_CB_SIZE_L_1;
    public int P_CB_SIZE_L_1;

    public int NUM_WG_L_1;
    public int NUM_WI_L_1;
    public int OCL_DIM_L_1;

    // L2 Dimension
    // G_CB_SIZE_L_2 = NUM_INPUT_SV;
    public int L_CB_SIZE_L_2;
    public int P_CB_SIZE_L_2;

    public int NUM_WG_L_2;
    public int NUM_WI_L_2;
    public int OCL_DIM_L_2;

    // R1 Dimension
    // G_CB_SIZE_R_1 = NUM_ATTRIBUTES;
    public int L_CB_SIZE_R_1;
    public int P_CB_SIZE_R_1;

    public int NUM_WG_R_1;
    public int NUM_WI_R_1;
    public int OCL_DIM_R_1;

    public int LOCAL_SIZE_DIM_0;
    public int GLOBAL_SIZE_DIM_0;

    public int LOCAL_SIZE_DIM_1;
    public int GLOBAL_SIZE_DIM_1;

    public int LOCAL_SIZE_DIM_2;
    public int GLOBAL_SIZE_DIM_2;

    public int K2_LOCAL_SIZE_DIM_0;
    public int K2_GLOBAL_SIZE_DIM_0;

    public int K2_LOCAL_SIZE_DIM_1;
    public int K2_GLOBAL_SIZE_DIM_1;

    public int K2_LOCAL_SIZE_DIM_2;
    public int K2_GLOBAL_SIZE_DIM_2;

    // Work dimension
    public int WORK_DIM = 3;

    public ParamsL1L2R1(String path) {
        JSONObject tps = JSONUtil.readFileArgs(path);
        setVars(tps);
    }

    public ParamsL1L2R1(JSONObject tps) {
        setVars(tps);
    }

    private void setVars(JSONObject tps) {
        Object range = tps.get("RANGE");
        if(range != null) RANGE = ((Long) range).intValue();

        CACHE_L_CB = ((Long) tps.get("CACHE_L_CB")).intValue();
        CACHE_P_CB = ((Long) tps.get("CACHE_P_CB")).intValue();
        G_CB_RES_DEST_LEVEL = ((Long) tps.get("G_CB_RES_DEST_LEVEL")).intValue();
        L_CB_RES_DEST_LEVEL = ((Long) tps.get("L_CB_RES_DEST_LEVEL")).intValue();
        P_CB_RES_DEST_LEVEL = ((Long) tps.get("P_CB_RES_DEST_LEVEL")).intValue();

        L_CB_SIZE_L_1 = ((Long) tps.get("L_CB_SIZE_L_1")).intValue();
        P_CB_SIZE_L_1 = ((Long) tps.get("P_CB_SIZE_L_1")).intValue();
        NUM_WG_L_1 = ((Long) tps.get("NUM_WG_L_1")).intValue();
        NUM_WI_L_1 = ((Long) tps.get("NUM_WI_L_1")).intValue();
        OCL_DIM_L_1 = ((Long) tps.get("OCL_DIM_L_1")).intValue();

        L_CB_SIZE_L_2 = ((Long) tps.get("L_CB_SIZE_L_2")).intValue();
        P_CB_SIZE_L_2 = ((Long) tps.get("P_CB_SIZE_L_2")).intValue();
        NUM_WG_L_2 = ((Long) tps.get("NUM_WG_L_2")).intValue();
        NUM_WI_L_2 = ((Long) tps.get("NUM_WI_L_2")).intValue();
        OCL_DIM_L_2 = ((Long) tps.get("OCL_DIM_L_2")).intValue();

        L_CB_SIZE_R_1 = ((Long) tps.get("L_CB_SIZE_R_1")).intValue();
        P_CB_SIZE_R_1 = ((Long) tps.get("P_CB_SIZE_R_1")).intValue();
        NUM_WG_R_1 = ((Long) tps.get("NUM_WG_R_1")).intValue();
        NUM_WI_R_1 = ((Long) tps.get("NUM_WI_R_1")).intValue();
        OCL_DIM_R_1 = ((Long) tps.get("OCL_DIM_R_1")).intValue();

        if(OCL_DIM_L_1 == 0) {
            LOCAL_SIZE_DIM_0 = K2_LOCAL_SIZE_DIM_0 = NUM_WI_L_1;
            GLOBAL_SIZE_DIM_0 = K2_GLOBAL_SIZE_DIM_0 = NUM_WG_L_1 * NUM_WI_L_1;
        } else if (OCL_DIM_L_1 == 1) {
            LOCAL_SIZE_DIM_1 = K2_LOCAL_SIZE_DIM_1 = NUM_WI_L_1;
            GLOBAL_SIZE_DIM_1 = K2_GLOBAL_SIZE_DIM_1 = NUM_WG_L_1 * NUM_WI_L_1;
        } else if (OCL_DIM_L_1 == 2) {
            LOCAL_SIZE_DIM_2 = K2_LOCAL_SIZE_DIM_2 = NUM_WI_L_1;
            GLOBAL_SIZE_DIM_2 = K2_GLOBAL_SIZE_DIM_2 = NUM_WG_L_1 * NUM_WI_L_1;
        }

        if(OCL_DIM_L_2 == 0) {
            LOCAL_SIZE_DIM_0 = K2_LOCAL_SIZE_DIM_0 = NUM_WI_L_2;
            GLOBAL_SIZE_DIM_0 = K2_GLOBAL_SIZE_DIM_0 = NUM_WG_L_2 * NUM_WI_L_2;
        } else if (OCL_DIM_L_2 == 1) {
            LOCAL_SIZE_DIM_1 = K2_LOCAL_SIZE_DIM_1 = NUM_WI_L_2;
            GLOBAL_SIZE_DIM_1 = K2_GLOBAL_SIZE_DIM_1 = NUM_WG_L_2 * NUM_WI_L_2;
        } else if (OCL_DIM_L_2 == 2) {
            LOCAL_SIZE_DIM_2 = K2_LOCAL_SIZE_DIM_2 = NUM_WI_L_2;
            GLOBAL_SIZE_DIM_2 = K2_GLOBAL_SIZE_DIM_2 = NUM_WG_L_2 * NUM_WI_L_2;
        }

        if(OCL_DIM_R_1 == 0) {
            LOCAL_SIZE_DIM_0 = NUM_WI_R_1;
            GLOBAL_SIZE_DIM_0 = NUM_WG_R_1 * NUM_WI_R_1;
            K2_LOCAL_SIZE_DIM_0 = getLocalSizeSecondKernelStatic();
            K2_GLOBAL_SIZE_DIM_0 = getLocalSizeSecondKernelStatic();
        } else if (OCL_DIM_R_1 == 1) {
            LOCAL_SIZE_DIM_1 = NUM_WI_R_1;
            GLOBAL_SIZE_DIM_1 = NUM_WG_R_1 * NUM_WI_R_1;
            K2_LOCAL_SIZE_DIM_1 = getLocalSizeSecondKernelStatic();
            K2_GLOBAL_SIZE_DIM_1 = getLocalSizeSecondKernelStatic();
        } else if (OCL_DIM_R_1 == 2) {
            LOCAL_SIZE_DIM_2 = NUM_WI_R_1;
            GLOBAL_SIZE_DIM_2 = NUM_WG_R_1 * NUM_WI_R_1;
            K2_LOCAL_SIZE_DIM_2 = getLocalSizeSecondKernelStatic();
            K2_GLOBAL_SIZE_DIM_2 = getLocalSizeSecondKernelStatic();
        }
    }

    public String getOptions(int L1, int L2, int R1){
        return "-D CACHE_L_CB=" + CACHE_L_CB +
                " -D CACHE_P_CB=" + CACHE_P_CB +
                " -D G_CB_RES_DEST_LEVEL=" + G_CB_RES_DEST_LEVEL +
                " -D L_CB_RES_DEST_LEVEL=" + L_CB_RES_DEST_LEVEL +
                " -D P_CB_RES_DEST_LEVEL=" + P_CB_RES_DEST_LEVEL +
                " -D G_CB_SIZE_L_1=" + L1 +
                " -D L_CB_SIZE_L_1=" + L_CB_SIZE_L_1 +
                " -D P_CB_SIZE_L_1=" + P_CB_SIZE_L_1 +
                " -D NUM_WG_L_1=" + NUM_WG_L_1 +
                " -D NUM_WI_L_1=" + NUM_WI_L_1 +
                " -D OCL_DIM_L_1=" + OCL_DIM_L_1 +
                " -D G_CB_SIZE_L_2=" + L2 +
                " -D L_CB_SIZE_L_2=" + L_CB_SIZE_L_2 +
                " -D P_CB_SIZE_L_2=" + P_CB_SIZE_L_2 +
                " -D NUM_WG_L_2=" + NUM_WG_L_2 +
                " -D NUM_WI_L_2=" + NUM_WI_L_2 +
                " -D OCL_DIM_L_2=" + OCL_DIM_L_2 +
                " -D G_CB_SIZE_R_1=" + R1 +
                " -D L_CB_SIZE_R_1=" + L_CB_SIZE_R_1 +
                " -D P_CB_SIZE_R_1=" + P_CB_SIZE_R_1 +
                " -D NUM_WG_R_1=" + NUM_WG_R_1 +
                " -D NUM_WI_R_1=" + NUM_WI_R_1 +
                " -D OCL_DIM_R_1=" + OCL_DIM_R_1;
    }

    public String getOptions(){
        return "-D CACHE_L_CB=" + CACHE_L_CB +
                " -D CACHE_P_CB=" + CACHE_P_CB +
                " -D G_CB_RES_DEST_LEVEL=" + G_CB_RES_DEST_LEVEL +
                " -D L_CB_RES_DEST_LEVEL=" + L_CB_RES_DEST_LEVEL +
                " -D P_CB_RES_DEST_LEVEL=" + P_CB_RES_DEST_LEVEL +
                " -D L_CB_SIZE_L_1=" + L_CB_SIZE_L_1 +
                " -D P_CB_SIZE_L_1=" + P_CB_SIZE_L_1 +
                " -D NUM_WG_L_1=" + NUM_WG_L_1 +
                " -D NUM_WI_L_1=" + NUM_WI_L_1 +
                " -D OCL_DIM_L_1=" + OCL_DIM_L_1 +
                " -D L_CB_SIZE_L_2=" + L_CB_SIZE_L_2 +
                " -D P_CB_SIZE_L_2=" + P_CB_SIZE_L_2 +
                " -D NUM_WG_L_2=" + NUM_WG_L_2 +
                " -D NUM_WI_L_2=" + NUM_WI_L_2 +
                " -D OCL_DIM_L_2=" + OCL_DIM_L_2 +
                " -D L_CB_SIZE_R_1=" + L_CB_SIZE_R_1 +
                " -D P_CB_SIZE_R_1=" + P_CB_SIZE_R_1 +
                " -D NUM_WG_R_1=" + NUM_WG_R_1 +
                " -D NUM_WI_R_1=" + NUM_WI_R_1 +
                " -D OCL_DIM_R_1=" + OCL_DIM_R_1;
    }

    private int getLocalSizeSecondKernelStatic() {
        return Math.min(NUM_WI_R_1, ceil(L_CB_SIZE_R_1, P_CB_SIZE_R_1));
    }

    public int K2_G_CB_SIZE_R_1(int G_CB_SIZE_R_1) {
        return Math.min(NUM_WG_R_1, ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1));
    }

    private int ceil(int x, int y){
        return (x + y - 1) / y;
    }
}
