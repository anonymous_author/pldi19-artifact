package com.ecccl.svm.steps;

import com.ecccl.svm.executor.training.RBFKernelExecutor;
import model.ChainData;
import com.ecccl.svm.executor.training.AbstractKernelExecutor;
import com.ecccl.svm.executor.training.LinearKernelExecutor;
import com.ecccl.svm.model.kernel.LocalKernel;
import com.ecccl.svm.model.kernel.RBFKernel;
import com.ecccl.svm.tasks.util.CacheLRU;
import opencl.Kernel;
import opencl.OCLUtil;
import org.lwjgl.BufferUtils;
import org.lwjgl.opencl.CL10;

import java.nio.FloatBuffer;

public class KernelEvaluator {
    private AbstractKernelExecutor kernelExecutor;

    private long input;
    private long cache;
    private CacheLRU cacheLRU;

    private LocalKernel kernel;

    private int numInstances;
    public KernelEvaluator(LocalKernel kernel, int numInstances, int sizeCacheMB) {
        this.kernel = kernel;
        this.numInstances = numInstances;

        // initialize cache memory on device
        long cacheLines = initCacheOnDevice(numInstances, sizeCacheMB);

        // initialize LRU cache for managing kernel rows from the host
        this.cacheLRU = new CacheLRU(cacheLines);
    }

    public void updateData(ChainData data) {

        // load input to device
        loadInputToDevice(data);

        // reset LRU cache
        this.cacheLRU.reset();

        // initialize executor
        switch(kernel.getKernelVersion()) {
            case LINEAR:
                kernelExecutor = LinearKernelExecutor.getInstance(data.length(), data.getNumAttributes(), input, cache);
                break;
            case RBF:
                float gamma = ((RBFKernel) kernel).getGamma();
                kernelExecutor = RBFKernelExecutor.getInstance(data.length(), data.getNumAttributes(), gamma, input, cache);
        }
    }

    public long getCache() {
        return cache;
    }

    public int checkKernelRow(int id, int mateId) {
        if (!cacheLRU.contains(id)) {
            // make sure mate stays in cache
            cacheLRU.get(mateId);

            int index = cacheLRU.put(id);
            kernelExecutor.execute(id, index);

            return index;
        }
        return cacheLRU.get(id);
    }

    public void finish() {
        CL10.clReleaseMemObject(cache);
    }

    public void release() {
        CL10.clReleaseMemObject(input);
    }

    private void loadInputToDevice(ChainData data) {
        FloatBuffer inputBuffer = BufferUtils.createFloatBuffer(data.length() * data.getNumAttributes());

        for(int instanceId = 0; instanceId < data.length(); ++instanceId){

            // load attributes to buffer
            inputBuffer.put(data.getAttributes(instanceId),0, data.getInitNumAttributes());

            // load labels as attributes to buffer
            if(data.getLabelAsAttrCount() > 0)
                inputBuffer.put(data.getLabel(instanceId), 0, data.getLabelAsAttrCount());
        }

        inputBuffer.flip();
        input = OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, OCLUtil.MemType.COPY, inputBuffer);
    }

    private long initCacheOnDevice(int numInstances, int sizeCacheMB) {
        long cacheLines;
        if((long) numInstances * numInstances * 4 > (long) sizeCacheMB * 1000 * 1000) {
            long sizeBytes = (long) sizeCacheMB * 1000 * 1000;
            cacheLines = sizeBytes / (numInstances * 4);
        } else {
            cacheLines = numInstances;
        }

        if(cacheLines < 2) {
            try {
                throw new IllegalArgumentException("Error: Size of cache is too small.");
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }

        cache = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, cacheLines * numInstances * 4);
        return cacheLines;
    }
}
