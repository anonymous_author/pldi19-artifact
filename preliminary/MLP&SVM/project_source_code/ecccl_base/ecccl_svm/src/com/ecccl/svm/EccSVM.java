package com.ecccl.svm;

import model.InputData;
import model.ChainData;
import com.ecccl.svm.model.kernel.LocalKernel;
import com.ecccl.svm.model.ModelSVM;
import com.ecccl.svm.model.kernel.RBFKernel;
import com.ecccl.svm.tasks.Training;
import util.RandomUtil;
import com.ecccl.svm.util.SVMBenchmark;
import opencl.PlatformUtil;
import util.Config;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * This class implements Ensemble of Classifier Chains with a Support Vector Machine
 * as the basis classifier of the classifier chains.
 *
 * The Support Vector Machine is implemented with the sequential minimal optimization
 * algorithm (SMO) of John Platt with the Working Set Selection of Sathiya Keerthi.
 *
 * The attributes of the input data set should be in a normalized range of [0-1] and
 * the classes have to be either -1/+1 or 0/+1.
 *
 * It uses OpenCL to parallelize the compute intensive tasks.
 *
 * Reference SMO:
 * J. Platt: Fast Training of Support Vector Machines using Sequential Minimal Optimization.
 * In B. Schoelkopf and C. Burges and A. Smola, editors, Advances in Kernel Methods - Support Vector Learning, 1998.
 *
 * Reference Keerthi's Working Set Selection:
 * S.S. Keerthi, S.K. Shevade, C. Bhattacharyya, K.R.K. Murthy (2001).
 * Improvements to Platt's SMO Algorithm for SVM Classifier Design. Neural Computation. 13(3):637-649.
 *
 * @author
 * @version 1.0
 */
public class EccSVM {
    private Random rand;

    // SVM parameters
    private float C;
    private double tol;
    private LocalKernel kernel;
    private int sizeCacheMB;
    private float eps;

    // ECC parameter
    private int chainCount;
    private float subsetPercentage;

    private EccSVM(Builder builder) {
        this.C = builder.C;
        this.tol = builder.tol;
        this.kernel = builder.kernel;
        this.sizeCacheMB = builder.sizeCacheMB;
        this.eps = 0.00001f;

        this.chainCount = builder.chainCount;
        this.subsetPercentage = builder.subsetPercentage / 100;
        this.rand = new Random(builder.seed);
    }

    public List<ModelSVM> train(InputData input) {
        if(Config.PRINT_LEVEL > 0) System.out.print("\nTraining: ");

        // initialize handler for training
        int numInstancesSub = (int) Math.floor(subsetPercentage * input.length());
        Training training = new Training(numInstancesSub, C, eps, tol, sizeCacheMB, kernel);;

        List<ModelSVM> models = new ArrayList<>();

        // train each chain on handler
        for (int chainId = 0; chainId < chainCount; chainId++) {
            if(subsetPercentage < 1.0f && subsetPercentage > 0f) {
                InputData subset = getSubset(input, subsetPercentage);

                trainChain(training, subset, models);
            } else {

                trainChain(training, input, models);
            }
        }
        if(Config.PRINT_LEVEL > 0) System.out.println();

        // finish training handler
        training.finish();

        if(PlatformUtil.PROFILING_ENABLED) {
            System.out.println("\nBias Kernel Time: " + SVMBenchmark.runBias / 1000000.0 + " ms");
            System.out.println("Bias Read Time: " + SVMBenchmark.readBias / 1000000.0 + " ms");
            System.out.println("\nError Kernel Time: " + SVMBenchmark.runError / 1000000.0 + " ms");
            System.out.println("\nKernel Time: " + SVMBenchmark.runKernel / 1000000.0 + " ms");
            System.out.println("Kernel Map Time: " + SVMBenchmark.runKernelMap / 1000000.0 + " ms");
            System.out.println("\nProfiled Time: " + SVMBenchmark.getRunTimeTraining() + " ms");
        }

        finish();

        return models;
    }

    private void finish() {
        Training.releaseKernel();
    }

    private void trainChain(Training training, InputData input, List<ModelSVM> models) {

        // get random order of labels for current chain and prepare input for classifier chain
        int[] labelOrder = RandomUtil.getShuffledRange(input.getNumLabels(), rand);
        ChainData chainData = new ChainData(input, labelOrder);

        for (int j = 0; j < input.getNumLabels(); j++) {
            // update handler with current chain data
            training.updateData(chainData);

            // classify and store resulting model
            models.add(training.train());

            // release resources of current iteration
            training.release();

            // add next label for next classifier
            chainData.addNextLabel();

            if(Config.PRINT_LEVEL > 0) System.out.print(".");
        }
    }

    private InputData getSubset(InputData input, float percent) {
        // get count of subset
        int count = (int) Math.floor(percent * input.length());

        // get random numbers in range of input length and limit it to the count
        final int[] randoms = rand.ints(0, input.length()).distinct().limit(count).toArray();

        // set up the new subset
        float[][] instances = new float[count][input.getNumAttributes() + input.getNumLabels()];
        for(int i = 0; i < count; i++) {
            instances[i] = input.getInstance(randoms[i]);
        }

        return new InputData(instances, input.getNumAttributes(), input.getNumLabels(), count);
    }

    public static class Builder {
        private int seed = 0;

        private LocalKernel kernel = new RBFKernel(0.01f);
        private double tol = 0.001f;
        private float C = 1;
        private int sizeCacheMB = 1024;

        private int chainCount = 10;
        private float subsetPercentage = 100;

        public Builder() { }

        public EccSVM build() {
            return new EccSVM(this);
        }

        public Builder setSeed(int seed) {
            this.seed = seed;
            return this;
        }

        public Builder setKernel(LocalKernel kernel) {
            this.kernel = kernel;
            return this;
        }

        public Builder setTol(double tol) {
            this.tol = tol;
            return this;
        }

        public Builder setC(float c) {
            C = c;
            return this;
        }

        public Builder setSizeCacheMB(int sizeCacheMB) {
            this.sizeCacheMB = sizeCacheMB;
            return this;
        }

        public Builder setChainCount(int chainCount) {
            this.chainCount = chainCount;
            return this;
        }

        public Builder setSubsetPercentage(float subsetPercentage) {
            this.subsetPercentage = subsetPercentage;
            return this;
        }
    }
}
