package com.ecccl.svm.model.kernel;

/**
 * This enum class describes the version of the used kernel.
 *
 * 0 = Linear
 * 1 = RBF
 */
public enum KernelVersion {
    LINEAR(0), RBF(1);

    private final int id;

    KernelVersion(int id){
        this.id = id;
    }

    public int getId(){
        return id;
    }
}
