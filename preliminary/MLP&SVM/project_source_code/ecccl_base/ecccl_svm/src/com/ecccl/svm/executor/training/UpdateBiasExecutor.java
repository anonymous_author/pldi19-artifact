package com.ecccl.svm.executor.training;

import com.ecccl.svm.tasks.util.BiasResult;
import com.ecccl.svm.util.SVMBenchmark;
import opencl.Kernel;
import opencl.PlatformUtil;
import opencl.params.ParamsR1;
import org.lwjgl.BufferUtils;

import java.nio.ByteBuffer;

/**
 * This class manages the OpenCL kernel execution of updating
 * the bias values at the end of every iteration.
 *
 * @author
 */
public class UpdateBiasExecutor {
    private static UpdateBiasExecutor instance = null;
    private static Kernel kernel;

    private ParamsR1 params;
    private ByteBuffer resultBuffer;

    public interface ARGS {
        int SIZE = 4;
        int ERROR = 0, LIST = 1;
        int RES_G = 2, RESULT = 3;
    }

    public static UpdateBiasExecutor getInstance(int numInstances, long list, long error) {
        if (instance == null) {
            ParamsR1 params = new ParamsR1(ParamsR1.TUNING_FILE_BIAS);
            instance = new UpdateBiasExecutor(params, numInstances, list, error, false);
        }
        instance.updateArgs(list, error);
        return instance;
    }

    public static UpdateBiasExecutor getInstance(ParamsR1 params, int numInstances, long list, long error) {
        if (instance == null) {
            instance = new UpdateBiasExecutor(params, numInstances, list, error, false);
        }
        instance.updateArgs(list, error);
        return instance;
    }

    private UpdateBiasExecutor(ParamsR1 params, int numInstances, long list, long error, boolean useBuildCache) {
        this.params = params;

        long start = System.nanoTime();
        buildKernel(numInstances, useBuildCache);
        long end = System.nanoTime();
        SVMBenchmark.buildBias += end - start;

        initArgs();
    }

    private void buildKernel(int numInstances, boolean useBuildCache) {
        long program = PlatformUtil.buildProgram("ocl_kernel/svm/svm_working_set_selection",
                params.getOptions(numInstances), useBuildCache);;

        kernel = new Kernel(program, "svm_update_bias");
        kernel.setLocalSize(params.LOCAL_SIZE_DIM_0);
        kernel.setGlobalSize(params.GLOBAL_SIZE_DIM_0);
        kernel.setWorkDim(params.WORK_DIM);
    }

    private void updateArgs(long list, long error) {
        kernel.setArg(ARGS.ERROR, error, Kernel.TypeFlag.SHARED);
        kernel.setArg(ARGS.LIST, list, Kernel.TypeFlag.SHARED);
        kernel.setArgs();
    }

    private void initArgs() {
        resultBuffer = BufferUtils.createByteBuffer(16);

        kernel.initArgs(ARGS.SIZE);
        kernel.setInputOutputArg(ARGS.RES_G, 1, Kernel.TypeFlag.CACHE);
        kernel.setOutputArg(ARGS.RESULT, resultBuffer, Kernel.TypeFlag.READ);
        kernel.setArgs();
    }

    public BiasResult execute(){
        if(PlatformUtil.PROFILING_ENABLED) {
            SVMBenchmark.runBias += kernel.executeKernelProfile();
            SVMBenchmark.readBias += kernel.readArgsProfile();
        } else {
            kernel.executeKernel();
            kernel.readArgs();
        }

        return new BiasResult(resultBuffer.getFloat(0), resultBuffer.getInt(4),
                resultBuffer.getFloat(8), resultBuffer.getInt(12));
    }

    public long profile() {
        return kernel.executeKernelProfile();
    }

    public static void releaseKernel() {
        if(kernel != null)  {
            kernel.releaseArgs();
            kernel.releaseKernel();
        }
    }
}
