#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" &&

DEFAULTVALUE="inputs.json"
INPUT=${1:-$DEFAULTVALUE}

DEFAULT_OUTPUT="."
OUTPUT_PATH=${2:-$DEFAULT_OUTPUT}

$DIR/scripts/02_train_linear.sh $INPUT $OUTPUT_PATH &&
$DIR/scripts/03_train_error_update.sh $INPUT $OUTPUT_PATH &&
$DIR/scripts/04_train_update_bias.sh $INPUT $OUTPUT_PATH
