#include "../../md_hom_generator.hpp"

/**
 * L1: Number of instances
 * R1: Number of attributes
 */
int main(){
    // attributes of the instances of the input data set
    auto input_data = md_hom::input_buffer("input_data", {md_hom::L(1), md_hom::R(1)});

    // attributes of instance for kernel evaluations
    auto input_line = md_hom::input_buffer("input_line", {md_hom::R(1)});

    // gamma for rbf kernel
    auto gamma = md_hom::input_scalar("gamma", true);

    // result of kernel evaluations
    auto result = md_hom::result_buffer("res", {md_hom::L(1)});

    // scalar function for computing quadratic euclid: -||x - y||^2
    auto f = md_hom::scalar_function("return 2 * input_data_val * input_line_val - input_data_val * input_data_val - input_line_val * input_line_val;");

    // scalar function for computing rbf: exp(gamma * res)
    auto g = md_hom::scalar_function("return exp(gamma_val * res);");

    auto md_hom_rbf = md_hom::md_hom<1, 1>("compute_rbf_kernel", md_hom::inputs(input_data, input_line, gamma), f, g, result, true);

    auto generator = md_hom::generator::ocl_generator(md_hom_rbf);

    std::ofstream kernel_file;
    kernel_file.open("svm_rbf_kernel.cl", std::fstream::out | std::fstream::trunc);
    kernel_file << generator.kernel_1();
    kernel_file.close();
    kernel_file.open("svm_rbf_kernel_2.cl", std::fstream::out | std::fstream::trunc);
    kernel_file << generator.kernel_2();
    kernel_file.close();
}
