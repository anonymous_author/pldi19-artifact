#include "../../md_hom_generator.hpp"

/**
 * L1: Number of chains
 * L2: Size of layer
 */
int main(){
    // B of current layer
    auto B = md_hom::input_buffer("B", {md_hom::L(1), md_hom::L(2)});

    // D of current layer
    auto D = md_hom::input_buffer("D", {md_hom::L(1), md_hom::L(2)});

    // D of current layer of previous Iteration
    auto prevD = md_hom::input_buffer("prevD", {md_hom::L(1), md_hom::L(2)});

    // learning rate and momentum
    auto learning_rate = md_hom::input_scalar("learning_rate");
    auto momentum = md_hom::input_scalar("momentum");

    // new B of current layer
    // TODO: replace by old B
    auto result = md_hom::result_buffer("result", {md_hom::L(1), md_hom::L(2)});

    // scalar function to calculate new B
    auto f = md_hom::scalar_function("return B_val + learning_rate_val * D_val + momentum_val * prevD_val;");

    auto md_hom_map = md_hom::md_hom<2, 0>("mlp_weight_bias", md_hom::inputs(B, D, prevD, learning_rate, momentum),
            f, md_hom::scalar_function(""), result);

    auto generator = md_hom::generator::ocl_generator(md_hom_map);
    std::ofstream kernel_file;
    kernel_file.open("mlp_weight_bias.cl", std::fstream::out | std::fstream::trunc);
    kernel_file << generator.kernel_1();
    kernel_file.close();
}
