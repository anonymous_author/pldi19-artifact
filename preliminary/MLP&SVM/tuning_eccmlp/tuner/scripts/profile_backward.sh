#!/bin/bash
cd ./profiler

MAX_HEAP=`grep -oP "\"java_max_heap_mb\"[ ]*:[ ]*\K[^ ,;\n]+" ../../settings.json`
PLATFORM_ID=`grep -oP "\"platform_id\"[ ]*:[ ]*\K[^ ,;\n]+" ../../settings.json`
DEVICE_ID=`grep -oP "\"device_id\"[ ]*:[ ]*\K[^ ,;\n]+" ../../settings.json`
DEVICE_TYPE=`grep -oP "\"device_type\"[ ]*:[ ]*\"\K[^ ,;\n\"]+" ../../settings.json`

# run kernel
OUTPUT=$(java -Xmx"$MAX_HEAP"m -jar ./profile_backward.jar $PLATFORM_ID $DEVICE_ID $DEVICE_TYPE $1 $2 "{
  \"CACHE_L_CB\": $CACHE_L_CB,
  \"CACHE_P_CB\": $CACHE_P_CB,
  \"G_CB_RES_DEST_LEVEL\": $G_CB_RES_DEST_LEVEL,
  \"L_CB_RES_DEST_LEVEL\": $L_CB_RES_DEST_LEVEL,
  \"P_CB_RES_DEST_LEVEL\": $P_CB_RES_DEST_LEVEL,
  \"L_CB_SIZE_L_1\": $L_CB_SIZE_L_1,
  \"P_CB_SIZE_L_1\": $P_CB_SIZE_L_1,
  \"NUM_WG_L_1\": $NUM_WG_L_1,
  \"NUM_WI_L_1\": $NUM_WI_L_1,
  \"OCL_DIM_L_1\": $OCL_DIM_L_1,
  \"L_CB_SIZE_L_2\": $L_CB_SIZE_L_2,
  \"P_CB_SIZE_L_2\": $P_CB_SIZE_L_2,
  \"NUM_WG_L_2\": $NUM_WG_L_2,
  \"NUM_WI_L_2\": $NUM_WI_L_2,
  \"OCL_DIM_L_2\": $OCL_DIM_L_2,
  \"L_CB_SIZE_R_1\": $L_CB_SIZE_R_1,
  \"P_CB_SIZE_R_1\": $P_CB_SIZE_R_1,
  \"NUM_WG_R_1\": $NUM_WG_R_1,
  \"NUM_WI_R_1\": $NUM_WI_R_1,
  \"OCL_DIM_R_1\": $OCL_DIM_R_1
}" )
#2> /dev/null)

# read status and runtime
STATUS=$?
COST=$(echo $OUTPUT | grep -Eo '[0-9]+$')

cd ..

# write cost of configuration into cost file
if [ "$STATUS" -eq "0" ]; then
    echo $COST > runtime
else
    exit 1
fi
