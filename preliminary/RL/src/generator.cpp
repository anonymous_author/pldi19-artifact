//
// Created by  on 30.07.18.
//
#include <md_hom_generator.hpp>

using md_hom::L;
using md_hom::R;

int main() {
    auto blockrun = md_hom::input_scalar("blockrun");
    auto probM = md_hom::input_scalar("probM");

    auto n_id = md_hom::input_buffer("n_id", {L(1)});
    auto n_lastname1 = md_hom::input_buffer("n_lastname1", {L(1)});
    auto n_lastname2 = md_hom::input_buffer("n_lastname2", {L(1)});
    auto n_lastname3 = md_hom::input_buffer("n_lastname3", {L(1)});
    auto n_firstname1 = md_hom::input_buffer("n_firstname1", {L(1)});
    auto n_firstname2 = md_hom::input_buffer("n_firstname2", {L(1)});
    auto n_firstname3 = md_hom::input_buffer("n_firstname3", {L(1)});
    auto n_birthname1 = md_hom::input_buffer("n_birthname1", {L(1)});
    auto n_birthname2 = md_hom::input_buffer("n_birthname2", {L(1)});
    auto n_birthname3 = md_hom::input_buffer("n_birthname3", {L(1)});
    auto n_birthday = md_hom::input_buffer("n_birthday", {L(1)});
    auto n_same_firstname_group = md_hom::input_buffer("n_same_firstname_group", {L(1)});
    auto n_gender = md_hom::input_buffer("n_gender", {L(1)});
    auto n_birthmonth = md_hom::input_buffer("n_birthmonth", {L(1)});
    auto n_birthyear = md_hom::input_buffer("n_birthyear", {L(1)});
    auto n_cin = md_hom::input_buffer("n_cin", {L(1)});
    auto n_prob_lastname1 = md_hom::input_buffer("n_prob_lastname1", {L(1)});
    auto n_prob_lastname2 = md_hom::input_buffer("n_prob_lastname2", {L(1)});
    auto n_prob_lastname3 = md_hom::input_buffer("n_prob_lastname3", {L(1)});
    auto n_prob_firstname1 = md_hom::input_buffer("n_prob_firstname1", {L(1)});
    auto n_prob_firstname2 = md_hom::input_buffer("n_prob_firstname2", {L(1)});
    auto n_prob_firstname3 = md_hom::input_buffer("n_prob_firstname3", {L(1)});
    auto n_prob_birthname1 = md_hom::input_buffer("n_prob_birthname1", {L(1)});
    auto n_prob_birthname2 = md_hom::input_buffer("n_prob_birthname2", {L(1)});
    auto n_prob_birthname3 = md_hom::input_buffer("n_prob_birthname3", {L(1)});
    auto n_prob_birthday = md_hom::input_buffer("n_prob_birthday", {L(1)});
    auto n_prob_gender = md_hom::input_buffer("n_prob_gender", {L(1)});
    auto n_prob_birthmonth = md_hom::input_buffer("n_prob_birthmonth", {L(1)});
    auto n_prob_birthyear = md_hom::input_buffer("n_prob_birthyear", {L(1)});
    auto n_prob_cin = md_hom::input_buffer("n_prob_cin", {L(1)});

    auto i_id = md_hom::input_buffer("i_id", {L(2)});
    auto i_lastname1 = md_hom::input_buffer("i_lastname1", {L(2)});
    auto i_lastname2 = md_hom::input_buffer("i_lastname2", {L(2)});
    auto i_lastname3 = md_hom::input_buffer("i_lastname3", {L(2)});
    auto i_firstname1 = md_hom::input_buffer("i_firstname1", {L(2)});
    auto i_firstname2 = md_hom::input_buffer("i_firstname2", {L(2)});
    auto i_firstname3 = md_hom::input_buffer("i_firstname3", {L(2)});
    auto i_birthname1 = md_hom::input_buffer("i_birthname1", {L(2)});
    auto i_birthname2 = md_hom::input_buffer("i_birthname2", {L(2)});
    auto i_birthname3 = md_hom::input_buffer("i_birthname3", {L(2)});
    auto i_birthday = md_hom::input_buffer("i_birthday", {L(2)});
    auto i_same_firstname_group = md_hom::input_buffer("i_same_firstname_group", {L(2)});
    auto i_gender = md_hom::input_buffer("i_gender", {L(2)});
    auto i_birthmonth = md_hom::input_buffer("i_birthmonth", {L(2)});
    auto i_birthyear = md_hom::input_buffer("i_birthyear", {L(2)});
    auto i_cin = md_hom::input_buffer("i_cin", {L(2)});
    auto i_prob_lastname1 = md_hom::input_buffer("i_prob_lastname1", {L(2)});
    auto i_prob_lastname2 = md_hom::input_buffer("i_prob_lastname2", {L(2)});
    auto i_prob_lastname3 = md_hom::input_buffer("i_prob_lastname3", {L(2)});
    auto i_prob_firstname1 = md_hom::input_buffer("i_prob_firstname1", {L(2)});
    auto i_prob_firstname2 = md_hom::input_buffer("i_prob_firstname2", {L(2)});
    auto i_prob_firstname3 = md_hom::input_buffer("i_prob_firstname3", {L(2)});
    auto i_prob_birthname1 = md_hom::input_buffer("i_prob_birthname1", {L(2)});
    auto i_prob_birthname2 = md_hom::input_buffer("i_prob_birthname2", {L(2)});
    auto i_prob_birthname3 = md_hom::input_buffer("i_prob_birthname3", {L(2)});
    auto i_prob_birthday = md_hom::input_buffer("i_prob_birthday", {L(2)});
    auto i_prob_gender = md_hom::input_buffer("i_prob_gender", {L(2)});
    auto i_prob_birthmonth = md_hom::input_buffer("i_prob_birthmonth", {L(2)});
    auto i_prob_birthyear = md_hom::input_buffer("i_prob_birthyear", {L(2)});
    auto i_prob_cin = md_hom::input_buffer("i_prob_cin", {L(2)});

    auto result = md_hom::result_buffer("result", {L(1), L(2)});
    auto md_hom = md_hom::md_hom<2, 0>(
            "rl",
            md_hom::inputs(blockrun, probM,
                           n_id, n_lastname1, n_lastname2, n_lastname3, n_firstname1, n_firstname2, n_firstname3, n_birthname1, n_birthname2, n_birthname3, n_birthday, n_same_firstname_group, n_gender, n_birthmonth, n_birthyear, n_cin, n_prob_lastname1, n_prob_lastname2, n_prob_lastname3, n_prob_firstname1, n_prob_firstname2, n_prob_firstname3, n_prob_birthname1, n_prob_birthname2, n_prob_birthname3, n_prob_birthday, n_prob_gender, n_prob_birthmonth, n_prob_birthyear, n_prob_cin,
                           i_id, i_lastname1, i_lastname2, i_lastname3, i_firstname1, i_firstname2, i_firstname3, i_birthname1, i_birthname2, i_birthname3, i_birthday, i_same_firstname_group, i_gender, i_birthmonth, i_birthyear, i_cin, i_prob_lastname1, i_prob_lastname2, i_prob_lastname3, i_prob_firstname1, i_prob_firstname2, i_prob_firstname3, i_prob_birthname1, i_prob_birthname2, i_prob_birthname3, i_prob_birthday, i_prob_gender, i_prob_birthmonth, i_prob_birthyear, i_prob_cin),
            md_hom::scalar_function(""),
            result
    );

    auto generator = md_hom::generator::ocl_generator(md_hom);
    std::string kernel_source_1 = generator.kernel_1();
    std::ofstream file_1("rl.cl");
    file_1 << kernel_source_1;
    file_1.close();
}