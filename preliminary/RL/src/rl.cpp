//
// Created by  on 28.01.18.
//

// TODO: OpenCL Platform & Anzahl Meldungen festlegen
#define PLATFORM_ID             0
#define DEVICE_ID               0
#define NUM_NEUE_MELDUNGEN      500
#define NUM_BESTANDS_MELDUNGEN  4400000
#define BLOCKRUN                1

// TODO: Anpassen
//#define TUNE
#define CHECK_CORRECTNESS

#ifdef TUNE
#include "../libraries/ATF/atf.h"
#else
#include <CL/cl.hpp>
#endif

#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <chrono>

typedef struct {
    char str[46];
} str46;
typedef struct {
    char str[9];
} str9;
typedef struct {
    char values[2];
} chr2;
typedef struct {
    double values[8];
} dbl8;

void check_error(cl_int err) {
    if (err != CL_SUCCESS) {
        printf("Error with errorcode: %d\n", err);
        exit(EXIT_FAILURE);
    }
}

int main(int argc, char *argv[]) {
    // TODO: eigene Daten einlesen
    const int blockrun = BLOCKRUN;
    const dbl8 probM = {{0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1}};

    std::vector<long long> n_id(NUM_NEUE_MELDUNGEN);
    std::vector<str46>     n_lastname1(NUM_NEUE_MELDUNGEN);
    std::vector<str46>     n_lastname2(NUM_NEUE_MELDUNGEN);
    std::vector<str46>     n_lastname3(NUM_NEUE_MELDUNGEN);
    std::vector<str46>     n_firstname1(NUM_NEUE_MELDUNGEN);
    std::vector<str46>     n_firstname2(NUM_NEUE_MELDUNGEN);
    std::vector<str46>     n_firstname3(NUM_NEUE_MELDUNGEN);
    std::vector<str46>     n_birthname1(NUM_NEUE_MELDUNGEN);
    std::vector<str46>     n_birthname2(NUM_NEUE_MELDUNGEN);
    std::vector<str46>     n_birthname3(NUM_NEUE_MELDUNGEN);
    std::vector<str46>     n_birthday(NUM_NEUE_MELDUNGEN);
    std::vector<str9>      n_same_firstname_group(NUM_NEUE_MELDUNGEN);
    std::vector<chr2>      n_gender(NUM_NEUE_MELDUNGEN);
    std::vector<int>       n_birthmonth(NUM_NEUE_MELDUNGEN);
    std::vector<int>       n_birthyear(NUM_NEUE_MELDUNGEN);
    std::vector<int>       n_cin(NUM_NEUE_MELDUNGEN);
    std::vector<double>    n_prob_lastname1(NUM_NEUE_MELDUNGEN);
    std::vector<double>    n_prob_lastname2(NUM_NEUE_MELDUNGEN);
    std::vector<double>    n_prob_lastname3(NUM_NEUE_MELDUNGEN);
    std::vector<double>    n_prob_firstname1(NUM_NEUE_MELDUNGEN);
    std::vector<double>    n_prob_firstname2(NUM_NEUE_MELDUNGEN);
    std::vector<double>    n_prob_firstname3(NUM_NEUE_MELDUNGEN);
    std::vector<double>    n_prob_birthname1(NUM_NEUE_MELDUNGEN);
    std::vector<double>    n_prob_birthname2(NUM_NEUE_MELDUNGEN);
    std::vector<double>    n_prob_birthname3(NUM_NEUE_MELDUNGEN);
    std::vector<double>    n_prob_birthday(NUM_NEUE_MELDUNGEN);
    std::vector<double>    n_prob_gender(NUM_NEUE_MELDUNGEN);
    std::vector<double>    n_prob_birthmonth(NUM_NEUE_MELDUNGEN);
    std::vector<double>    n_prob_birthyear(NUM_NEUE_MELDUNGEN);
    std::vector<double>    n_prob_cin(NUM_NEUE_MELDUNGEN);

    std::vector<long long> i_id(NUM_BESTANDS_MELDUNGEN);
    std::vector<str46>     i_lastname1(NUM_BESTANDS_MELDUNGEN);
    std::vector<str46>     i_lastname2(NUM_BESTANDS_MELDUNGEN);
    std::vector<str46>     i_lastname3(NUM_BESTANDS_MELDUNGEN);
    std::vector<str46>     i_firstname1(NUM_BESTANDS_MELDUNGEN);
    std::vector<str46>     i_firstname2(NUM_BESTANDS_MELDUNGEN);
    std::vector<str46>     i_firstname3(NUM_BESTANDS_MELDUNGEN);
    std::vector<str46>     i_birthname1(NUM_BESTANDS_MELDUNGEN);
    std::vector<str46>     i_birthname2(NUM_BESTANDS_MELDUNGEN);
    std::vector<str46>     i_birthname3(NUM_BESTANDS_MELDUNGEN);
    std::vector<str46>     i_birthday(NUM_BESTANDS_MELDUNGEN);
    std::vector<str9>      i_same_firstname_group(NUM_BESTANDS_MELDUNGEN);
    std::vector<chr2>      i_gender(NUM_BESTANDS_MELDUNGEN);
    std::vector<int>       i_birthmonth(NUM_BESTANDS_MELDUNGEN);
    std::vector<int>       i_birthyear(NUM_BESTANDS_MELDUNGEN);
    std::vector<int>       i_cin(NUM_BESTANDS_MELDUNGEN);
    std::vector<double>    i_prob_lastname1(NUM_BESTANDS_MELDUNGEN);
    std::vector<double>    i_prob_lastname2(NUM_BESTANDS_MELDUNGEN);
    std::vector<double>    i_prob_lastname3(NUM_BESTANDS_MELDUNGEN);
    std::vector<double>    i_prob_firstname1(NUM_BESTANDS_MELDUNGEN);
    std::vector<double>    i_prob_firstname2(NUM_BESTANDS_MELDUNGEN);
    std::vector<double>    i_prob_firstname3(NUM_BESTANDS_MELDUNGEN);
    std::vector<double>    i_prob_birthname1(NUM_BESTANDS_MELDUNGEN);
    std::vector<double>    i_prob_birthname2(NUM_BESTANDS_MELDUNGEN);
    std::vector<double>    i_prob_birthname3(NUM_BESTANDS_MELDUNGEN);
    std::vector<double>    i_prob_birthday(NUM_BESTANDS_MELDUNGEN);
    std::vector<double>    i_prob_gender(NUM_BESTANDS_MELDUNGEN);
    std::vector<double>    i_prob_birthmonth(NUM_BESTANDS_MELDUNGEN);
    std::vector<double>    i_prob_birthyear(NUM_BESTANDS_MELDUNGEN);
    std::vector<double>    i_prob_cin(NUM_BESTANDS_MELDUNGEN);

#ifdef CHECK_CORRECTNESS
    std::vector<char> expected_result(static_cast<size_t>(NUM_NEUE_MELDUNGEN) * static_cast<size_t>(NUM_BESTANDS_MELDUNGEN));
    for (int i = 0; i < static_cast<size_t>(NUM_NEUE_MELDUNGEN) * static_cast<size_t>(NUM_BESTANDS_MELDUNGEN); ++i) expected_result[i] = 0;
#endif

#ifdef TUNE
    auto tuner = atf::open_tuner(new atf::cond::duration<std::chrono::minutes>(1));
#else
    // configuration
    std::map<std::string, int> configuration;
    configuration["CACHE_L_CB"] = 0;
    configuration["CACHE_P_CB"] = 0;
    configuration["G_CB_RES_DEST_LEVEL"] = 2;
    configuration["L_CB_RES_DEST_LEVEL"] = 2;
    configuration["P_CB_RES_DEST_LEVEL"] = 2;
    configuration["G_CB_SIZE_L_1"] = NUM_NEUE_MELDUNGEN;
    configuration["L_CB_SIZE_L_1"] = 1;
    configuration["P_CB_SIZE_L_1"] = 1;
    configuration["NUM_WG_L_1"] = 1;
    configuration["NUM_WI_L_1"] = 1;
    configuration["OCL_DIM_L_1"] = 1;
    configuration["G_CB_SIZE_L_2"] = NUM_BESTANDS_MELDUNGEN;
    configuration["L_CB_SIZE_L_2"] = 32;
    configuration["P_CB_SIZE_L_2"] = 1;
    configuration["NUM_WG_L_2"] = 1;
    configuration["NUM_WI_L_2"] = 32;
    configuration["OCL_DIM_L_2"] = 0;
#endif

#ifdef TUNE
    auto device = atf::cf::device_info(PLATFORM_ID, DEVICE_ID);
    device.initialize();

    // get device specific limits
    std::vector<size_t> max_wi_size;
    size_t max_wg_size;
    cl_ulong max_local_mem_size;
    cl_uint dims;
    atf::cf::check_error(device.device().getInfo(CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, &dims));
    max_wi_size = std::vector<size_t>(dims);
    atf::cf::check_error(device.device().getInfo(CL_DEVICE_MAX_WORK_ITEM_SIZES, &max_wi_size));
    atf::cf::check_error(device.device().getInfo(CL_DEVICE_MAX_WORK_GROUP_SIZE, &max_wg_size));
    atf::cf::check_error(device.device().getInfo(CL_DEVICE_LOCAL_MEM_SIZE, &max_local_mem_size));
    const int combined_max_wi_size = *std::max_element(max_wi_size.begin(), max_wi_size.end());

    auto TP_CACHE_L_CB          = atf::tp("CACHE_L_CB",          {0, 1});
    auto TP_CACHE_P_CB          = atf::tp("CACHE_P_CB",          {0, 1});
    auto TP_G_CB_RES_DEST_LEVEL = atf::tp("G_CB_RES_DEST_LEVEL", {2});
    auto TP_L_CB_RES_DEST_LEVEL = atf::tp("L_CB_RES_DEST_LEVEL", {0, 1, 2}, [&](auto TP_L_CB_RES_DEST_LEVEL) { return TP_L_CB_RES_DEST_LEVEL <= TP_G_CB_RES_DEST_LEVEL; });
    auto TP_P_CB_RES_DEST_LEVEL = atf::tp("P_CB_RES_DEST_LEVEL", {0, 1, 2}, [&](auto TP_P_CB_RES_DEST_LEVEL) { return TP_P_CB_RES_DEST_LEVEL <= TP_L_CB_RES_DEST_LEVEL; });
    auto TP_G_CB_SIZE_L_1       = atf::tp("G_CB_SIZE_L_1",       {NUM_NEUE_MELDUNGEN});
    auto TP_L_CB_SIZE_L_1       = atf::tp("L_CB_SIZE_L_1",       atf::interval(0, (int)ceil(log2(NUM_NEUE_MELDUNGEN)), atf::pow_2), [&](auto TP_L_CB_SIZE_L_1) { return TP_G_CB_SIZE_L_1 >= TP_L_CB_SIZE_L_1; });
    auto TP_P_CB_SIZE_L_1       = atf::tp("P_CB_SIZE_L_1",       atf::interval(0, (int)ceil(log2(NUM_NEUE_MELDUNGEN)), atf::pow_2), [&](auto TP_P_CB_SIZE_L_1) { return TP_L_CB_SIZE_L_1 >= TP_P_CB_SIZE_L_1; });
    auto TP_G_CB_SIZE_L_2       = atf::tp("G_CB_SIZE_L_2",       {NUM_BESTANDS_MELDUNGEN});
    auto TP_L_CB_SIZE_L_2       = atf::tp("L_CB_SIZE_L_2",       atf::interval(0, (int)ceil(log2(NUM_BESTANDS_MELDUNGEN)), atf::pow_2), [&](auto TP_L_CB_SIZE_L_2) { return TP_G_CB_SIZE_L_2 >= TP_L_CB_SIZE_L_2; });
    auto TP_P_CB_SIZE_L_2       = atf::tp("P_CB_SIZE_L_2",       atf::interval(0, (int)ceil(log2(NUM_BESTANDS_MELDUNGEN)), atf::pow_2), [&](auto TP_P_CB_SIZE_L_2) { return TP_L_CB_SIZE_L_2 >= TP_P_CB_SIZE_L_2; });
    auto TP_NUM_WG_L_1          = atf::tp("NUM_WG_L_1",          atf::interval(0, (int)ceil(log2(NUM_NEUE_MELDUNGEN)), atf::pow_2));
    auto TP_NUM_WG_L_2          = atf::tp("NUM_WG_L_2",          atf::interval(0, (int)ceil(log2(NUM_BESTANDS_MELDUNGEN)), atf::pow_2));
    auto TP_OCL_DIM_L_1         = atf::tp("OCL_DIM_L_1",         {0, 1});
    auto TP_OCL_DIM_L_2         = atf::tp("OCL_DIM_L_2",         {0, 1}, atf::unequal(TP_OCL_DIM_L_1));
    auto TP_NUM_WI_L_1          = atf::tp("NUM_WI_L_1",          atf::interval(0, (int)ceil(log2(std::min(int(NUM_NEUE_MELDUNGEN), combined_max_wi_size))), atf::pow_2), [&](auto TP_NUM_WI_L_1) { return TP_OCL_DIM_L_1 == 0 ? (TP_NUM_WI_L_1 <= max_wi_size[0]) : (TP_OCL_DIM_L_1 == 1 ? (TP_NUM_WI_L_1 <= max_wi_size[1]) : (TP_NUM_WI_L_1 <= max_wi_size[2])); });
    auto TP_NUM_WI_L_2          = atf::tp("NUM_WI_L_2",          atf::interval(0, (int)ceil(log2(std::min(int(NUM_BESTANDS_MELDUNGEN), combined_max_wi_size))), atf::pow_2), [&](auto TP_NUM_WI_L_2) { return TP_OCL_DIM_L_2 == 0 ? (TP_NUM_WI_L_2 <= max_wi_size[0]) : (TP_OCL_DIM_L_2 == 1 ? (TP_NUM_WI_L_2 <= max_wi_size[1]) : (TP_NUM_WI_L_2 <= max_wi_size[2])) && TP_NUM_WI_L_1 * TP_NUM_WI_L_2 <= max_wg_size; });
    tuner(TP_CACHE_L_CB);
    tuner(TP_CACHE_P_CB);
    tuner(TP_G_CB_RES_DEST_LEVEL, TP_L_CB_RES_DEST_LEVEL, TP_P_CB_RES_DEST_LEVEL);
    tuner(TP_G_CB_SIZE_L_1, TP_L_CB_SIZE_L_1, TP_P_CB_SIZE_L_1);
    tuner(TP_G_CB_SIZE_L_2, TP_L_CB_SIZE_L_2, TP_P_CB_SIZE_L_2);
    tuner(TP_NUM_WG_L_1);
    tuner(TP_NUM_WG_L_2);
    tuner(TP_OCL_DIM_L_1, TP_OCL_DIM_L_2,
          TP_NUM_WI_L_1, TP_NUM_WI_L_2);

    // create kernel wrapper
    std::vector<char> int_res(NUM_NEUE_MELDUNGEN * NUM_BESTANDS_MELDUNGEN);
    auto res_g_size = [&](unsigned int kernel, atf::configuration &config) -> size_t {
        size_t size = NUM_NEUE_MELDUNGEN * NUM_BESTANDS_MELDUNGEN * sizeof(char);
        return size;
    };
    auto int_res_size = [&](atf::configuration &config) -> size_t {
        size_t size = NUM_NEUE_MELDUNGEN * NUM_BESTANDS_MELDUNGEN * sizeof(char);
        return size;
    };
    auto needs_second_kernel = [&](atf::configuration &config) -> bool {
        return false;
    };
    auto is_valid = [&](atf::configuration &config) -> bool {
        return true;
    };
    atf::cf::process_wrapper_info process_wrapper_info = {
            atf::cf::NONE,
            "", "", ""
    };
    atf::cf::timeout warm_up_timeout = {atf::cf::ABSOLUTE, {.absolute = 0}};
    atf::cf::timeout evaluation_timeout = {atf::cf::ABSOLUTE, {.absolute = 0}};
    auto wrapper = atf::cf::ocl_md_hom(
            device,
            {atf::cf::kernel_info::FILENAME, "rl.cl", "test_1", ""},
            atf::inputs(atf::buffer(a), atf::buffer(b)),
            atf::cf::GS(
                    atf::max((TP_OCL_DIM_L_2 == 0) * (TP_NUM_WG_L_2 * TP_NUM_WI_L_2)
                    + (TP_OCL_DIM_L_1 == 0) * (TP_NUM_WG_L_1 * TP_NUM_WI_L_1), 1),

                    atf::max((TP_OCL_DIM_L_2 == 1) * (TP_NUM_WG_L_2 * TP_NUM_WI_L_2)
                    + (TP_OCL_DIM_L_1 == 1) * (TP_NUM_WG_L_1 * TP_NUM_WI_L_1), 1),

                    atf::max((TP_OCL_DIM_L_2 == 2) * (TP_NUM_WG_L_2 * TP_NUM_WI_L_2)
                    + (TP_OCL_DIM_L_1 == 2) * (TP_NUM_WG_L_1 * TP_NUM_WI_L_1), 1)
            ),
            atf::cf::LS(
                    atf::max((TP_OCL_DIM_L_2 == 0) * (TP_NUM_WI_L_2)
                    + (TP_OCL_DIM_L_1 == 0) * (TP_NUM_WI_L_1), 1),

                    atf::max((TP_OCL_DIM_L_2 == 1) * (TP_NUM_WI_L_2)
                    + (TP_OCL_DIM_L_1 == 1) * (TP_NUM_WI_L_1), 1),

                    atf::max((TP_OCL_DIM_L_2 == 2) * (TP_NUM_WI_L_2)
                    + (TP_OCL_DIM_L_1 == 2) * (TP_NUM_WI_L_1), 1)
            ),
            {atf::cf::kernel_info::CODE, "", "", ""},
            atf::inputs(),
            atf::cf::GS(1, 1, 1),
            atf::cf::LS(1, 1, 1),
            res_g_size,
            atf::buffer(int_res),
            int_res_size,
            needs_second_kernel,
            is_valid,
            5, 3,
            false,
            process_wrapper_info,
            warm_up_timeout,
            evaluation_timeout
    );

    // start tuning
    auto best_configuration = tuner(wrapper);
    std::map<std::string, int> configuration;
    for (auto val : best_configuration) {
        configuration[val.first] = val.second.value();
    }
#endif

    // read kernel code
    std::string kernel_source_1;
    std::ifstream file_1("rl.cl");
    kernel_source_1 = std::string((std::istreambuf_iterator<char>(file_1)),
                                  std::istreambuf_iterator<char>());
    file_1.close();

    // get platform
    cl_int error;
    std::vector<cl::Platform> platforms;
    error = cl::Platform::get(&platforms); check_error(error);
    if (PLATFORM_ID >= platforms.size()) exit(EXIT_FAILURE);
    cl::Platform _platform = platforms[PLATFORM_ID];
    std::string platform_name, platform_vendor;
    error = _platform.getInfo(CL_PLATFORM_NAME, &platform_name); check_error(error);
    error = _platform.getInfo(CL_PLATFORM_VENDOR, &platform_vendor); check_error(error);
    std::cout << "Using platform \"" << platform_name << "\" of vendor \"" << platform_vendor << "\"" << std::endl;

    // get device
    std::vector<cl::Device> devices;
    error = _platform.getDevices(CL_DEVICE_TYPE_ALL, &devices); check_error(error);
    if (DEVICE_ID >= devices.size()) exit(EXIT_FAILURE);
    cl::Device _device = devices[DEVICE_ID];
    std::string device_name;
    error = _device.getInfo(CL_DEVICE_NAME, &device_name); check_error(error);
    std::cout << "Using device \"" << device_name << "\"" << std::endl;

    // create context and command queue
    cl_context_properties props[] = {CL_CONTEXT_PLATFORM,
                                     reinterpret_cast<cl_context_properties>( _platform()),
                                     0
    };
    cl::Context _context(VECTOR_CLASS<cl::Device>(1, _device), props);
    cl::CommandQueue _command_queue(_context, _device, CL_QUEUE_PROFILING_ENABLE);

    std::string kernel_name_1 = "rl_1";

    std::vector<char> res(static_cast<size_t>(NUM_NEUE_MELDUNGEN) * static_cast<size_t>(NUM_BESTANDS_MELDUNGEN));

    // copy buffer
    cl::Event event;
    cl_ulong start, end;

    cl::Buffer n_id_buf(_context, CL_MEM_READ_ONLY, n_id.size() * sizeof(long long), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_id_buf, CL_TRUE, 0, n_id.size() * sizeof(long long), n_id.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_id_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_lastname1_buf(_context, CL_MEM_READ_ONLY, n_lastname1.size() * sizeof(str46), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_lastname1_buf, CL_TRUE, 0, n_lastname1.size() * sizeof(str46), n_lastname1.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_lastname1_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_lastname2_buf(_context, CL_MEM_READ_ONLY, n_lastname2.size() * sizeof(str46), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_lastname2_buf, CL_TRUE, 0, n_lastname2.size() * sizeof(str46), n_lastname2.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_lastname2_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_lastname3_buf(_context, CL_MEM_READ_ONLY, n_lastname3.size() * sizeof(str46), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_lastname3_buf, CL_TRUE, 0, n_lastname3.size() * sizeof(str46), n_lastname3.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_lastname3_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_firstname1_buf(_context, CL_MEM_READ_ONLY, n_firstname1.size() * sizeof(str46), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_firstname1_buf, CL_TRUE, 0, n_firstname1.size() * sizeof(str46), n_firstname1.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_firstname1_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_firstname2_buf(_context, CL_MEM_READ_ONLY, n_firstname2.size() * sizeof(str46), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_firstname2_buf, CL_TRUE, 0, n_firstname2.size() * sizeof(str46), n_firstname2.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_firstname2_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_firstname3_buf(_context, CL_MEM_READ_ONLY, n_firstname3.size() * sizeof(str46), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_firstname3_buf, CL_TRUE, 0, n_firstname3.size() * sizeof(str46), n_firstname3.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_firstname3_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_birthname1_buf(_context, CL_MEM_READ_ONLY, n_birthname1.size() * sizeof(str46), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_birthname1_buf, CL_TRUE, 0, n_birthname1.size() * sizeof(str46), n_birthname1.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_birthname1_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_birthname2_buf(_context, CL_MEM_READ_ONLY, n_birthname2.size() * sizeof(str46), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_birthname2_buf, CL_TRUE, 0, n_birthname2.size() * sizeof(str46), n_birthname2.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_birthname2_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_birthname3_buf(_context, CL_MEM_READ_ONLY, n_birthname3.size() * sizeof(str46), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_birthname3_buf, CL_TRUE, 0, n_birthname3.size() * sizeof(str46), n_birthname3.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_birthname3_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_birthday_buf(_context, CL_MEM_READ_ONLY, n_birthday.size() * sizeof(str46), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_birthday_buf, CL_TRUE, 0, n_birthday.size() * sizeof(str46), n_birthday.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_birthday_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_same_firstname_group_buf(_context, CL_MEM_READ_ONLY, n_same_firstname_group.size() * sizeof(str9), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_same_firstname_group_buf, CL_TRUE, 0, n_same_firstname_group.size() * sizeof(str9), n_same_firstname_group.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_same_firstname_group_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_gender_buf(_context, CL_MEM_READ_ONLY, n_gender.size() * sizeof(chr2), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_gender_buf, CL_TRUE, 0, n_gender.size() * sizeof(chr2), n_gender.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_gender_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_birthmonth_buf(_context, CL_MEM_READ_ONLY, n_birthmonth.size() * sizeof(int), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_birthmonth_buf, CL_TRUE, 0, n_birthmonth.size() * sizeof(int), n_birthmonth.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_birthmonth_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_birthyear_buf(_context, CL_MEM_READ_ONLY, n_birthyear.size() * sizeof(int), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_birthyear_buf, CL_TRUE, 0, n_birthyear.size() * sizeof(int), n_birthyear.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_birthyear_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_cin_buf(_context, CL_MEM_READ_ONLY, n_cin.size() * sizeof(int), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_cin_buf, CL_TRUE, 0, n_cin.size() * sizeof(int), n_cin.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_cin_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_prob_lastname1_buf(_context, CL_MEM_READ_ONLY, n_prob_lastname1.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_prob_lastname1_buf, CL_TRUE, 0, n_prob_lastname1.size() * sizeof(double), n_prob_lastname1.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_prob_lastname1_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_prob_lastname2_buf(_context, CL_MEM_READ_ONLY, n_prob_lastname2.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_prob_lastname2_buf, CL_TRUE, 0, n_prob_lastname2.size() * sizeof(double), n_prob_lastname2.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_prob_lastname2_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_prob_lastname3_buf(_context, CL_MEM_READ_ONLY, n_prob_lastname3.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_prob_lastname3_buf, CL_TRUE, 0, n_prob_lastname3.size() * sizeof(double), n_prob_lastname3.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_prob_lastname3_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_prob_firstname1_buf(_context, CL_MEM_READ_ONLY, n_prob_firstname1.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_prob_firstname1_buf, CL_TRUE, 0, n_prob_firstname1.size() * sizeof(double), n_prob_firstname1.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_prob_firstname1_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_prob_firstname2_buf(_context, CL_MEM_READ_ONLY, n_prob_firstname2.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_prob_firstname2_buf, CL_TRUE, 0, n_prob_firstname2.size() * sizeof(double), n_prob_firstname2.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_prob_firstname2_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_prob_firstname3_buf(_context, CL_MEM_READ_ONLY, n_prob_firstname3.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_prob_firstname3_buf, CL_TRUE, 0, n_prob_firstname3.size() * sizeof(double), n_prob_firstname3.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_prob_firstname3_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_prob_birthname1_buf(_context, CL_MEM_READ_ONLY, n_prob_birthname1.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_prob_birthname1_buf, CL_TRUE, 0, n_prob_birthname1.size() * sizeof(double), n_prob_birthname1.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_prob_birthname1_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_prob_birthname2_buf(_context, CL_MEM_READ_ONLY, n_prob_birthname2.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_prob_birthname2_buf, CL_TRUE, 0, n_prob_birthname2.size() * sizeof(double), n_prob_birthname2.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_prob_birthname2_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_prob_birthname3_buf(_context, CL_MEM_READ_ONLY, n_prob_birthname3.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_prob_birthname3_buf, CL_TRUE, 0, n_prob_birthname3.size() * sizeof(double), n_prob_birthname3.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_prob_birthname3_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_prob_birthday_buf(_context, CL_MEM_READ_ONLY, n_prob_birthday.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_prob_birthday_buf, CL_TRUE, 0, n_prob_birthday.size() * sizeof(double), n_prob_birthday.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_prob_birthday_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_prob_gender_buf(_context, CL_MEM_READ_ONLY, n_prob_gender.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_prob_gender_buf, CL_TRUE, 0, n_prob_gender.size() * sizeof(double), n_prob_gender.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_prob_gender_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_prob_birthmonth_buf(_context, CL_MEM_READ_ONLY, n_prob_birthmonth.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_prob_birthmonth_buf, CL_TRUE, 0, n_prob_birthmonth.size() * sizeof(double), n_prob_birthmonth.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_prob_birthmonth_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_prob_birthyear_buf(_context, CL_MEM_READ_ONLY, n_prob_birthyear.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_prob_birthyear_buf, CL_TRUE, 0, n_prob_birthyear.size() * sizeof(double), n_prob_birthyear.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_prob_birthyear_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer n_prob_cin_buf(_context, CL_MEM_READ_ONLY, n_prob_cin.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(n_prob_cin_buf, CL_TRUE, 0, n_prob_cin.size() * sizeof(double), n_prob_cin.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (n_prob_cin_buf): " << (end - start) / 1000000 << "ms" <<std::endl;

    cl::Buffer i_id_buf(_context, CL_MEM_READ_ONLY, i_id.size() * sizeof(long long), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_id_buf, CL_TRUE, 0, i_id.size() * sizeof(long long), i_id.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_id_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_lastname1_buf(_context, CL_MEM_READ_ONLY, i_lastname1.size() * sizeof(str46), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_lastname1_buf, CL_TRUE, 0, i_lastname1.size() * sizeof(str46), i_lastname1.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_lastname1_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_lastname2_buf(_context, CL_MEM_READ_ONLY, i_lastname2.size() * sizeof(str46), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_lastname2_buf, CL_TRUE, 0, i_lastname2.size() * sizeof(str46), i_lastname2.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_lastname2_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_lastname3_buf(_context, CL_MEM_READ_ONLY, i_lastname3.size() * sizeof(str46), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_lastname3_buf, CL_TRUE, 0, i_lastname3.size() * sizeof(str46), i_lastname3.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_lastname3_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_firstname1_buf(_context, CL_MEM_READ_ONLY, i_firstname1.size() * sizeof(str46), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_firstname1_buf, CL_TRUE, 0, i_firstname1.size() * sizeof(str46), i_firstname1.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_firstname1_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_firstname2_buf(_context, CL_MEM_READ_ONLY, i_firstname2.size() * sizeof(str46), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_firstname2_buf, CL_TRUE, 0, i_firstname2.size() * sizeof(str46), i_firstname2.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_firstname2_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_firstname3_buf(_context, CL_MEM_READ_ONLY, i_firstname3.size() * sizeof(str46), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_firstname3_buf, CL_TRUE, 0, i_firstname3.size() * sizeof(str46), i_firstname3.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_firstname3_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_birthname1_buf(_context, CL_MEM_READ_ONLY, i_birthname1.size() * sizeof(str46), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_birthname1_buf, CL_TRUE, 0, i_birthname1.size() * sizeof(str46), i_birthname1.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_birthname1_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_birthname2_buf(_context, CL_MEM_READ_ONLY, i_birthname2.size() * sizeof(str46), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_birthname2_buf, CL_TRUE, 0, i_birthname2.size() * sizeof(str46), i_birthname2.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_birthname2_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_birthname3_buf(_context, CL_MEM_READ_ONLY, i_birthname3.size() * sizeof(str46), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_birthname3_buf, CL_TRUE, 0, i_birthname3.size() * sizeof(str46), i_birthname3.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_birthname3_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_birthday_buf(_context, CL_MEM_READ_ONLY, i_birthday.size() * sizeof(str46), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_birthday_buf, CL_TRUE, 0, i_birthday.size() * sizeof(str46), i_birthday.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_birthday_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_same_firstname_group_buf(_context, CL_MEM_READ_ONLY, i_same_firstname_group.size() * sizeof(str9), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_same_firstname_group_buf, CL_TRUE, 0, i_same_firstname_group.size() * sizeof(str9), i_same_firstname_group.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_same_firstname_group_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_gender_buf(_context, CL_MEM_READ_ONLY, i_gender.size() * sizeof(chr2), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_gender_buf, CL_TRUE, 0, i_gender.size() * sizeof(chr2), i_gender.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_gender_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_birthmonth_buf(_context, CL_MEM_READ_ONLY, i_birthmonth.size() * sizeof(int), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_birthmonth_buf, CL_TRUE, 0, i_birthmonth.size() * sizeof(int), i_birthmonth.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_birthmonth_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_birthyear_buf(_context, CL_MEM_READ_ONLY, i_birthyear.size() * sizeof(int), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_birthyear_buf, CL_TRUE, 0, i_birthyear.size() * sizeof(int), i_birthyear.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_birthyear_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_cin_buf(_context, CL_MEM_READ_ONLY, i_cin.size() * sizeof(int), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_cin_buf, CL_TRUE, 0, i_cin.size() * sizeof(int), i_cin.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_cin_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_prob_lastname1_buf(_context, CL_MEM_READ_ONLY, i_prob_lastname1.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_prob_lastname1_buf, CL_TRUE, 0, i_prob_lastname1.size() * sizeof(double), i_prob_lastname1.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_prob_lastname1_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_prob_lastname2_buf(_context, CL_MEM_READ_ONLY, i_prob_lastname2.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_prob_lastname2_buf, CL_TRUE, 0, i_prob_lastname2.size() * sizeof(double), i_prob_lastname2.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_prob_lastname2_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_prob_lastname3_buf(_context, CL_MEM_READ_ONLY, i_prob_lastname3.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_prob_lastname3_buf, CL_TRUE, 0, i_prob_lastname3.size() * sizeof(double), i_prob_lastname3.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_prob_lastname3_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_prob_firstname1_buf(_context, CL_MEM_READ_ONLY, i_prob_firstname1.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_prob_firstname1_buf, CL_TRUE, 0, i_prob_firstname1.size() * sizeof(double), i_prob_firstname1.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_prob_firstname1_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_prob_firstname2_buf(_context, CL_MEM_READ_ONLY, i_prob_firstname2.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_prob_firstname2_buf, CL_TRUE, 0, i_prob_firstname2.size() * sizeof(double), i_prob_firstname2.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_prob_firstname2_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_prob_firstname3_buf(_context, CL_MEM_READ_ONLY, i_prob_firstname3.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_prob_firstname3_buf, CL_TRUE, 0, i_prob_firstname3.size() * sizeof(double), i_prob_firstname3.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_prob_firstname3_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_prob_birthname1_buf(_context, CL_MEM_READ_ONLY, i_prob_birthname1.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_prob_birthname1_buf, CL_TRUE, 0, i_prob_birthname1.size() * sizeof(double), i_prob_birthname1.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_prob_birthname1_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_prob_birthname2_buf(_context, CL_MEM_READ_ONLY, i_prob_birthname2.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_prob_birthname2_buf, CL_TRUE, 0, i_prob_birthname2.size() * sizeof(double), i_prob_birthname2.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_prob_birthname2_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_prob_birthname3_buf(_context, CL_MEM_READ_ONLY, i_prob_birthname3.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_prob_birthname3_buf, CL_TRUE, 0, i_prob_birthname3.size() * sizeof(double), i_prob_birthname3.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_prob_birthname3_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_prob_birthday_buf(_context, CL_MEM_READ_ONLY, i_prob_birthday.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_prob_birthday_buf, CL_TRUE, 0, i_prob_birthday.size() * sizeof(double), i_prob_birthday.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_prob_birthday_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_prob_gender_buf(_context, CL_MEM_READ_ONLY, i_prob_gender.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_prob_gender_buf, CL_TRUE, 0, i_prob_gender.size() * sizeof(double), i_prob_gender.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_prob_gender_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_prob_birthmonth_buf(_context, CL_MEM_READ_ONLY, i_prob_birthmonth.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_prob_birthmonth_buf, CL_TRUE, 0, i_prob_birthmonth.size() * sizeof(double), i_prob_birthmonth.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_prob_birthmonth_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_prob_birthyear_buf(_context, CL_MEM_READ_ONLY, i_prob_birthyear.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_prob_birthyear_buf, CL_TRUE, 0, i_prob_birthyear.size() * sizeof(double), i_prob_birthyear.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_prob_birthyear_buf): " << (end - start) / 1000000 << "ms" <<std::endl;
    cl::Buffer i_prob_cin_buf(_context, CL_MEM_READ_ONLY, i_prob_cin.size() * sizeof(double), nullptr, &error); check_error(error);
    error = _command_queue.enqueueWriteBuffer(i_prob_cin_buf, CL_TRUE, 0, i_prob_cin.size() * sizeof(double), i_prob_cin.data(), nullptr, &event); check_error(error);
    check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); std::cout << "host->dev (i_prob_cin_buf): " << (end - start) / 1000000 << "ms" <<std::endl;

    cl::Buffer res_buf(_context, CL_MEM_READ_WRITE, res.size() * sizeof(char), nullptr, &error); check_error(error);

    // set kernel arguments
    size_t gs_0_1 = 1, gs_1_1 = 1, gs_2_1 = 1;
    size_t ls_0_1 = 1, ls_1_1 = 1, ls_2_1 = 1;
    if (configuration.find("OCL_DIM_L_1") != configuration.end()) {
        int tmp_gs = configuration["NUM_WG_L_1"] * configuration["NUM_WI_L_1"];
        int tmp_ls = configuration["NUM_WI_L_1"];
        if (configuration["OCL_DIM_L_1"] == 0) {
            gs_0_1 = tmp_gs;
            ls_0_1 = tmp_ls;
        } else if (configuration["OCL_DIM_L_1"] == 1) {
            gs_1_1 = tmp_gs;
            ls_1_1 = tmp_ls;
        } else if (configuration["OCL_DIM_L_1"] == 2) {
            gs_2_1 = tmp_gs;
            ls_2_1 = tmp_ls;
        }
    }
    if (configuration.find("OCL_DIM_L_2") != configuration.end()) {
        int tmp_gs = configuration["NUM_WG_L_2"] * configuration["NUM_WI_L_2"];
        int tmp_ls = configuration["NUM_WI_L_2"];
        if (configuration["OCL_DIM_L_2"] == 0) {
            gs_0_1 = tmp_gs;
            ls_0_1 = tmp_ls;
        } else if (configuration["OCL_DIM_L_2"] == 1) {
            gs_1_1 = tmp_gs;
            ls_1_1 = tmp_ls;
        } else if (configuration["OCL_DIM_L_2"] == 2) {
            gs_2_1 = tmp_gs;
            ls_2_1 = tmp_ls;
        }
    }
    cl::NDRange global_size_1(gs_0_1, gs_1_1, gs_2_1);
    cl::NDRange local_size_1(ls_0_1, ls_1_1, ls_2_1);
    auto set_kernel_arguments_1 = [&](cl::Kernel &kernel) {
        kernel.setArg( 0, blockrun);
        kernel.setArg( 1, probM);

        kernel.setArg( 2, n_id_buf);
        kernel.setArg( 3, n_lastname1_buf);
        kernel.setArg( 4, n_lastname2_buf);
        kernel.setArg( 5, n_lastname3_buf);
        kernel.setArg( 6, n_firstname1_buf);
        kernel.setArg( 7, n_firstname2_buf);
        kernel.setArg( 8, n_firstname3_buf);
        kernel.setArg( 9, n_birthname1_buf);
        kernel.setArg(10, n_birthname2_buf);
        kernel.setArg(11, n_birthname3_buf);
        kernel.setArg(12, n_birthday_buf);
        kernel.setArg(13, n_same_firstname_group_buf);
        kernel.setArg(14, n_gender_buf);
        kernel.setArg(15, n_birthmonth_buf);
        kernel.setArg(16, n_birthyear_buf);
        kernel.setArg(17, n_cin_buf);
        kernel.setArg(18, n_prob_lastname1_buf);
        kernel.setArg(19, n_prob_lastname2_buf);
        kernel.setArg(20, n_prob_lastname3_buf);
        kernel.setArg(21, n_prob_firstname1_buf);
        kernel.setArg(22, n_prob_firstname2_buf);
        kernel.setArg(23, n_prob_firstname3_buf);
        kernel.setArg(24, n_prob_birthname1_buf);
        kernel.setArg(25, n_prob_birthname2_buf);
        kernel.setArg(26, n_prob_birthname3_buf);
        kernel.setArg(27, n_prob_birthday_buf);
        kernel.setArg(28, n_prob_gender_buf);
        kernel.setArg(29, n_prob_birthmonth_buf);
        kernel.setArg(30, n_prob_birthyear_buf);
        kernel.setArg(31, n_prob_cin_buf);

        kernel.setArg(32, i_id_buf);
        kernel.setArg(33, i_lastname1_buf);
        kernel.setArg(34, i_lastname2_buf);
        kernel.setArg(35, i_lastname3_buf);
        kernel.setArg(36, i_firstname1_buf);
        kernel.setArg(37, i_firstname2_buf);
        kernel.setArg(38, i_firstname3_buf);
        kernel.setArg(39, i_birthname1_buf);
        kernel.setArg(40, i_birthname2_buf);
        kernel.setArg(41, i_birthname3_buf);
        kernel.setArg(42, i_birthday_buf);
        kernel.setArg(43, i_same_firstname_group_buf);
        kernel.setArg(44, i_gender_buf);
        kernel.setArg(45, i_birthmonth_buf);
        kernel.setArg(46, i_birthyear_buf);
        kernel.setArg(47, i_cin_buf);
        kernel.setArg(48, i_prob_lastname1_buf);
        kernel.setArg(49, i_prob_lastname2_buf);
        kernel.setArg(50, i_prob_lastname3_buf);
        kernel.setArg(51, i_prob_firstname1_buf);
        kernel.setArg(52, i_prob_firstname2_buf);
        kernel.setArg(53, i_prob_firstname3_buf);
        kernel.setArg(54, i_prob_birthname1_buf);
        kernel.setArg(55, i_prob_birthname2_buf);
        kernel.setArg(56, i_prob_birthname3_buf);
        kernel.setArg(57, i_prob_birthday_buf);
        kernel.setArg(58, i_prob_gender_buf);
        kernel.setArg(59, i_prob_birthmonth_buf);
        kernel.setArg(60, i_prob_birthyear_buf);
        kernel.setArg(61, i_prob_cin_buf);

        kernel.setArg(62, res_buf); // res_g not used -> pass res_buf twice
        kernel.setArg(63, res_buf);
    };

    // create programs
    cl::Program _program_1(_context,
                           cl::Program::Sources(1, std::make_pair(kernel_source_1.c_str(),
                                                                  kernel_source_1.length()))
    );

    // build kernel
    std::stringstream flags;
    for (const auto tp : configuration) {
        flags << " -D" << tp.first << "=" << tp.second;
    }
    auto chrono_start = std::chrono::system_clock::now();
    error = _program_1.build(std::vector<cl::Device>(1, _device), flags.str().c_str());
    if (error != CL_SUCCESS) {
        std::cout << flags.str() << std::endl;
        std::cout << error << std::endl;
        if (error == CL_BUILD_PROGRAM_FAILURE) {
            auto buildLog = _program_1.getBuildInfo<CL_PROGRAM_BUILD_LOG>(_device, &error); check_error(error);
            std::cout << std::endl << "Build of first kernel failed! Log:" << std::endl << buildLog << std::endl;
        }
        exit(EXIT_FAILURE);
    }
    auto chrono_end = std::chrono::system_clock::now();
    auto runtime_in_sec = std::chrono::duration_cast<std::chrono::milliseconds>(chrono_end - chrono_start).count();
    std::cout << "compilation time for first kernel: " << runtime_in_sec << "ms" << std::endl;
    auto buildLog = _program_1.getBuildInfo<CL_PROGRAM_BUILD_LOG>(_device, &error); check_error(error);
    std::cout << std::endl << "Log:" << std::endl << buildLog << std::endl;

    // launch first kernel
    auto kernel_1 = cl::Kernel(_program_1, kernel_name_1.c_str(), &error); check_error(error);
    set_kernel_arguments_1(kernel_1);
    cl::Event event_1;
    error = _command_queue.enqueueNDRangeKernel(kernel_1, cl::NullRange, global_size_1, local_size_1, NULL, &event_1); check_error(error);
    check_error(event_1.wait());
    check_error(event_1.getProfilingInfo(CL_PROFILING_COMMAND_START, &start));
    check_error(event_1.getProfilingInfo(CL_PROFILING_COMMAND_END, &end));
    std::cout << "kernel: " << (end - start) / 1000000 << "ms" << std::endl;

    #ifdef CHECK_CORRECTNESS
    char *result = new char[expected_result.size()];
    char *expected_result_ptr = new char[expected_result.size()];
    for (int i = 0; i < expected_result.size(); ++i) {
        expected_result_ptr[i] = expected_result[i];
    }
    error = _command_queue.enqueueReadBuffer(res_buf, CL_TRUE, 0, expected_result.size() * sizeof(char), result, nullptr, &event); check_error(error);
    check_error(event.wait());
    check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start));
    check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end));
    std::cout << "dev->host: " << (end - start) / 1000000 << "ms" << std::endl;
    if (memcmp(result, expected_result_ptr, expected_result.size() * sizeof(char)) != 0) {
        std::ofstream log_file;
        log_file.open("errors.txt", std::ofstream::out | std::ofstream::trunc);
        log_file << "!!! ERROR IN RESULT !!!" << std::endl;
        log_file << "configuration: " << std::endl;
        for (const auto &val : configuration) {
            log_file << "#define " << val.first << " " << val.second << std::endl;
        }
        log_file << "result buffer: " << std::endl;
        for (int i = 0; i < expected_result.size(); ++i) {
            log_file << result[i] << "\t";
        }
        log_file << std::endl << "gold buffer: " << std::endl;
        for (const auto val : expected_result) {
            log_file << val << "\t";
        }
        log_file << std::endl;
        log_file.close();
        std::cout << "result is incorrect" << std::endl;
        exit(1);
    } else {
        std::cout << "result is correct" << std::endl;
    }
#endif

    return 0;
}