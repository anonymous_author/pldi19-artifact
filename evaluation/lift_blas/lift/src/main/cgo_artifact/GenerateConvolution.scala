package cgo_artifact

import apart.arithmetic.PerformSimplification
import ir._
import ir.ast._
import opencl.executor.Compile
import opencl.generator.{PerformBarrierElimination, PerformLoopOptimisation}
import opencl.ir._
import opencl.ir.pattern._
import rewriting.utils.Utils

object GenerateConvolution {

  def main(args: Array[String]): Unit = {

    val inputSize_small = 4096
    val inputSize_large = 8192

    def blurXTiled2D(): Lambda = {
      fun(
        //ArrayType(ArrayType(Float, Var("N", StartFromRange(100))), Var("M", StartFromRange(100))),
        ArrayType(ArrayType(Float, inputSize_small), inputSize_small),
        ArrayType(Float, 17),
        (matrix, weights) => {
          Untile() o MapWrg(1)(MapWrg(0)(fun( tile =>

            MapLcl(1)(MapLcl(0)(
              // stencil computation
              fun(elem => {
                toGlobal(MapSeqUnroll(id)) o
                  ReduceSeqUnroll(fun((acc, pair) => {
                    val pixel = Get(pair, 0)
                    val weight = Get(pair, 1)
                    multAndSumUp.apply(acc, pixel, weight)
                  }), 0.0f) $ Zip(Join() $ elem, weights)
              })
              // create neighbourhoods in tiles
            )) o Slide2D(1,1, 17,1) o
              // load to local memory
              toLocal(MapLcl(1)(MapLcl(0)(id))) $ tile
          ))) o
            // tiling
            Slide2D(4,4, 144,128) o
            Pad2D(0,0, 8,8, Pad.Boundary.Clamp) $ matrix
        }
      )
    }

    def blurYTiled2DTiledLoadingTransposed(): Lambda = {
      val copy = UserFun("copy", "x", "{ return x; }", Float, Float).
        setScalaFun(x => x.head)

      fun(
        //ArrayType(ArrayType(Float, Var("N", StartFromRange(100))), Var("M", StartFromRange(100))),
        ArrayType(ArrayType(Float, inputSize_small), inputSize_small),
        ArrayType(Float, 17),
        (matrix, weights) => {
          Untile() o MapWrg(1)(MapWrg(0)(fun( tile =>

            MapLcl(1)(MapLcl(0)(
              // stencil computation
              fun(elem => {
                toGlobal(MapSeqUnroll(id)) o
                  ReduceSeqUnroll(fun((acc, pair) => {
                    val pixel = Get(pair, 0)
                    val weight = Get(pair, 1)
                    multAndSumUp.apply(acc, pixel, weight)
                  }), 0.0f) $ Zip(Join() $ elem, weights)
              })
              // create neighbourhoods in tiles
            )) o UnPadEmpty(0,1) o Slide2D(17,1, 1,1) o
              // transposed load
              Transpose() o
              Map(Join()) o
              // tiled loading
              toLocal(MapLcl(0)(MapSeqUnroll(MapLcl(1)(copy)))) o
              // split tile into chunks
              Map(Split(8)) o
              Map(PadEmpty(0,1)) o
              Transpose() $ tile
          ))) o
            // tiling
            Slide2D(80,64, 16,16) o
            Pad2D(8,8, 0,0, Pad.Boundary.Clamp) $ matrix
        }
      )
    }

    def blurXTiled2D_large(): Lambda = {
      fun(
        //ArrayType(ArrayType(Float, Var("N", StartFromRange(100))), Var("M", StartFromRange(100))),
        ArrayType(ArrayType(Float, inputSize_large), inputSize_large),
        ArrayType(Float, 17),
        (matrix, weights) => {
          Untile() o MapWrg(1)(MapWrg(0)(fun( tile =>

            MapLcl(1)(MapLcl(0)(
              // stencil computation
              fun(elem => {
                toGlobal(MapSeqUnroll(id)) o
                  ReduceSeqUnroll(fun((acc, pair) => {
                    val pixel = Get(pair, 0)
                    val weight = Get(pair, 1)
                    multAndSumUp.apply(acc, pixel, weight)
                  }), 0.0f) $ Zip(Join() $ elem, weights)
              })
              // create neighbourhoods in tiles
            )) o Slide2D(1,1, 17,1) o
              // load to local memory
              toLocal(MapLcl(1)(MapLcl(0)(id))) $ tile
          ))) o
            // tiling
            Slide2D(4,4, 144,128) o
            Pad2D(0,0, 8,8, Pad.Boundary.Clamp) $ matrix
        }
      )
    }


    def blurYTiled2DTiledLoadingTransposed_large(): Lambda = {
      val copy = UserFun("copy", "x", "{ return x; }", Float, Float).
        setScalaFun(x => x.head)

      fun(
        //ArrayType(ArrayType(Float, Var("N", StartFromRange(100))), Var("M", StartFromRange(100))),
        ArrayType(ArrayType(Float, inputSize_large), inputSize_large),
        ArrayType(Float, 17),
        (matrix, weights) => {
          Untile() o MapWrg(1)(MapWrg(0)(fun( tile =>

            MapLcl(1)(MapLcl(0)(
              // stencil computation
              fun(elem => {
                toGlobal(MapSeqUnroll(id)) o
                  ReduceSeqUnroll(fun((acc, pair) => {
                    val pixel = Get(pair, 0)
                    val weight = Get(pair, 1)
                    multAndSumUp.apply(acc, pixel, weight)
                  }), 0.0f) $ Zip(Join() $ elem, weights)
              })
              // create neighbourhoods in tiles
            )) o UnPadEmpty(0,1) o Slide2D(17,1, 1,1) o
              // transposed load
              Transpose() o
              Map(Join()) o
              // tiled loading
              toLocal(MapLcl(0)(MapSeqUnroll(MapLcl(1)(copy)))) o
              // split tile into chunks
              Map(Split(8)) o
              Map(PadEmpty(0,1)) o
              Transpose() $ tile
          ))) o
            // tiling
            Slide2D(80,64, 16,16) o
            Pad2D(8,8, 0,0, Pad.Boundary.Clamp) $ matrix
        }
      )
    }

    val code_x_small = Compile(blurXTiled2D(), 16, 4, 1, 512, 4096, 1, collection.immutable.Map())
    val code_y_small = Compile(blurYTiled2DTiledLoadingTransposed(), 16, 8, 1, 4096, 512, 1, collection.immutable.Map())
    val code_x_large = Compile(blurXTiled2D_large(), 16, 4, 1, 1024, 8192, 1, collection.immutable.Map())
    val code_y_large = Compile(blurYTiled2DTiledLoadingTransposed_large(), 16, 8, 1, 8192, 1024, 1, collection.immutable.Map())

    val extension =
      if (PerformSimplification())
        ".cl"
      else if (!PerformLoopOptimisation() && !PerformBarrierElimination())
        "_no_opt.cl"
      else
        "_no_simpl.cl"

    val folder = "generated_kernels/convolution/"

    Utils.dumpToFile(code_x_small, "convolution_row_small" + extension, folder)
    Utils.dumpToFile(code_y_small, "convolution_column_small" + extension, folder)
    Utils.dumpToFile(code_x_large, "convolution_row_large" + extension, folder)
    Utils.dumpToFile(code_y_large, "convolution_column_large" + extension, folder)
  }

}
