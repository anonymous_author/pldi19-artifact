package cgo_artifact

import apart.arithmetic.{PerformSimplification, SizeVar}
import ir._
import ir.ast._
import opencl.executor.Compile
import opencl.generator.{PerformBarrierElimination, PerformLoopOptimisation}
import opencl.ir._
import opencl.ir.pattern._
import rewriting.utils.Utils

object GenerateMRIQ {

  def main(args: Array[String]): Unit = {
    val phiMag = UserFun("phiMag",
      Array("phiR", "phiI"),
      "{ return phiR * phiR + phiI * phiI; }",
      Seq(Float, Float),
      Float)

    val qFun = UserFun("computeQ",
      Array("sX", "sY", "sZ", "Kx", "Ky", "Kz", "PhiMag", "acc"),
      """{
        |    #define PIx2 6.2831853071795864769252867665590058f
        |    float expArg = PIx2 * (Kx * sX + Ky * sY + Kz * sZ);
        |    acc._0 = acc._0 + PhiMag * cos(expArg);
        |    acc._1 = acc._1 + PhiMag * sin(expArg);
        |
        |    return acc;
        |}""".stripMargin,
      Seq(Float, Float, Float, Float, Float, Float, Float, TupleType(Float, Float)),
      TupleType(Float, Float))


    val pair = UserFun("pair", Array("x", "y"), "{ Tuple t = {x, y}; return t; }",
      Seq(Float, Float), TupleType(Float, Float))

    val k = SizeVar("K")

    val computePhiMag = fun(
      ArrayType(Float, k),
      ArrayType(Float, k),
      (phiR, phiI) => MapGlb(phiMag) $ Zip(phiR, phiI)
    )

    val x = SizeVar("X")

    val computeQ = fun(
      ArrayType(Float, x),
      ArrayType(Float, x),
      ArrayType(Float, x),
      ArrayType(Float, x),
      ArrayType(Float, x),
      ArrayType(TupleType(Float, Float, Float, Float), k),
      (x, y, z, Qr, Qi, kvalues) =>
        Zip(x, y, z, Qr, Qi) :>>
          MapGlb(\(t =>
            t._0 :>> toPrivate(id) :>> Let(sX =>
              t._1 :>> toPrivate(id) :>> Let(sY =>
                t._2 :>> toPrivate(id) :>> Let(sZ =>
                  kvalues :>>
                    ReduceSeq(\((acc, p) =>
                      qFun(sX, sY, sZ, p._0, p._1, p._2, p._3, acc)
                    ), toPrivate(fun(x => pair(x._0, x._1))) $ Tuple(t._3, t._4)) :>>
                    toGlobal(MapSeq(idFF))
                )
              )
            )
          ))
    )

    val extension =
      if (PerformSimplification())
        ".cl"
      else if (!PerformLoopOptimisation() && !PerformBarrierElimination())
        "_no_opt.cl"
      else
        "_no_simpl.cl"

    val codePhiMag = Compile(computePhiMag, 1, 1, 1, k, 1, 1, collection.immutable.Map())
    val codeComputeQ = Compile(computeQ, 1, 1, 1,x,1,1,collection.immutable.Map())

    val folder = "generated_kernels/mriq/"

    val phiMagFilename = "compute_phi_mag" + extension
    val computeQFilename = "compute_q" + extension

    Utils.dumpToFile(codePhiMag, phiMagFilename, folder)
    Utils.dumpToFile(codeComputeQ, computeQFilename, folder)

  }

}
