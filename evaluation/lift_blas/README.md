# Artifact for CGO 2017

This repository is the artifact for the paper ["_Lift_: A Functional Data-Parallel IR for High-Performance GPU Code Generation"](https://github.com/michel-steuwer/publications/raw/master/2017/CGO-2017.pdf) accepted for publication at the [International Symposium on
Code Generationand Optimization 2017](http://cgo.org/cgo2017/).

This artifact consists of:
 - the [paper](https://github.com/michel-steuwer/publications/raw/master/2017/CGO-2017.pdf);
 - the _Lift_ compiler implementation;
 - the _lift_ IR expressions from which the OpenCL kernels used in the evaluation are generated;
 - the automatically generated OpenCL kernels used in the evaluation;
 - reference implementations of the benchmarks used for comparison.

## Overview
 - [Installation](#installation) using [Docker](#installation-docker), [Nvidia-Docker](#installation-nvidia-docker) or [directly on a Linux machine](#installation-linux)
 - [Building and getting started](#getting-started)
 - [Experiment Workflow](#evaluation)

---

# <a name="installation"></a>Installation

## Git clone
  - Due to large files in the artifact we require that you have [`git-lfs`](https://git-lfs.github.com/) installed on your machine besides `git`.
    Deb and RPM packages for `git-lfs` can be found here: https://github.com/git-lfs/git-lfs/releases/tag/v1.5.3

  - If you have `git` and `git-lfs` installed, you should start by cloning this repository. When being ask for a username and password you can provide empty once by hitting the enter key.
```
$ git version
git version 2.1.4
$ git lfs version
git-lfs/1.4.4 (GitHub; linux amd64; go 1.7.3)
$ git lfs install
Git LFS initialized.
$ git clone https://gitlab.com/michel-steuwer/cgo_2017_artifact.git
Cloning into 'cgo_2017_artifact'...
remote: Counting objects: 2572, done.
remote: Compressing objects: 100% (122/122), done.
remote: Total 2572 (delta 52), reused 0 (delta 0)
Receiving objects: 100% (2572/2572), 36.57 MiB | 3.65 MiB/s, done.
Resolving deltas: 100% (889/889), done.
Checking connectivity... done.
Downloading amd.tar.bz2 (52.49 MB)
Username for 'https://gitlab.com': 
Password for 'https://michel-steuwer@gitlab.com': 
Downloading nvidia.tar.bz2 (51.65 MB) 
Username for 'https://gitlab.com': 
Password for 'https://michel-steuwer@gitlab.com': 
Downloading rodinia_3.1.tar.bz2 (434.76 MB)
Username for 'https://gitlab.com': 
Password for 'https://michel-steuwer@gitlab.com': 
Checking out files: 100% (2109/2109), done.
```
  - and then change into the directory:
```
cd cgo_2017_artifact
```
    We will refer to this directory from now on with `$ROOT`.

## Docker / Nvidia-Docker
We have prepared `Dockerfile`s for easily getting a working software environment.

If you have [`docker`](https://www.docker.com/products/overview#/install_the_platform) installed you can use the container to explore the _Lift_ implementation and you can reproduce the OpenCL kernels used in the evaluation of the paper.
But you will not be able to perform performance measurements from inside the container.

If you have an Nvidia GPU and you have [`Nvidia docker`](https://github.com/NVIDIA/nvidia-docker/wiki#quick-start) installed you can also perform some performance measurements.

There are two separate `Dockerfiles` for `docker` and `nvidia-docker`.

### <a name="installation-docker"></a>Get started using Docker
 - If you want to use `docker` and have it [installed](https://www.docker.com/products/overview#/install_the_platform) you should run the following
    command from the `$ROOT` directory to create a docker image called `lift`: 

```
docker build -t lift docker
```

 - You can then run the image using the following command from the `$ROOT` directory
 
```
docker run -it --rm -v `pwd`:/lift lift
```

  This will make the `$ROOT` directory accessible in the container in the `/lift` directory. So inside the container change into it:
  
```
cd /lift
```
  
  You are now ready to start the [evaluation](#getting-started).

### <a name="installation-nvidia-docker"></a>Get started using Nvidia-Docker
  - If you want to use `nvidia-docker` and have it [installed](https://github.com/NVIDIA/nvidia-docker/wiki#quick-start) you should run the following
    command from the `$ROOT` directory to create a docker image called `lift`:
  
```
docker build -t lift nvidia-docker
```

  - You can then run the image using the following command from the `$ROOT` directory

```
nvidia-docker run -it --rm -v `pwd`:/lift lift
```

  This will make the `$ROOT` directory accessible in the container in the `/lift` directory.So inside the container change into it:
  
```
cd /lift
```

  You are now ready to start the [evaluation](#getting-started).



## <a name="installation-linux"></a>Installation without Docker on a Linux machine
The reviewers are very welcome to not use `docker` and install _Lift_ on their own Linux machines.

### Dependencies
The _Lift_ compiler is implemented in Scala and uses OpenCL to run the generated kernels on an OpenCL device, such as a GPU.
Currently, we expect a Linux environment and have the following software dependencies:
 - OpenCL
 - Java 8 SDK
 - CMake
 - G++
 - LLVM / Libclang
 - OpenSSL
 - [SBT](http://www.scala-sbt.org)

For executing scripts and plotting we require:
 - Z shell
 - Python
 - R with ggplot
 - dot

The reference implementations additionally require:
 - OpenGL (libmesa)
 - GLUT and GLEW
 
For an **Ubuntu** system these packages satisfy all dependencies (besides SBT):
 ```
sudo apt-get update && sudo apt-get install -y default-jdk cmake g++ wget unzip libclang-dev llvm libgl1-mesa-dev freeglut3-dev libglew-dev libz-dev libssl-dev opencl-headers zsh python finger r-base r-cran-ggplot2 apt-transport-https graphviz
```
These lines will install [SBT](http://www.scala-sbt.org):
 ```
echo "deb https://dl.bintray.com/sbt/debian /" | sudo tee -a /etc/apt/sources.list.d/sbt.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823
sudo apt-get update
sudo apt-get install -y sbt
```



# <a name="getting-started"></a>Building and getting Started

## Overview of folder structure
 - The `lift` directory contains the implementation of the _Lift_ compiler
 - The `kernels` directory contains the OpenCL kernels generated by the _Lift_ compiler we used during our evaluation
 - The `CLBlast`, `AMDAPP`, `NVIDIA_GPU_Computing_SDK`, `rodinia_3.1`, `gemm`, `gemv_N`, `gemv_T`, `parboil`, and `shoc` directories contain reference implementations used in the performance comparison


## <a name="buildin-lift"></a>Building _Lift_
 - To build lift change into the `lift` subdirectory (for the docker container this is `/lift/lift`) and execute `sbt compile`:

```
cd $ROOT/lift
sbt compile
```

This takes some time, as it first bootstraps Scala and the compiles the _Lift_ compiler.

## <a name="buildin-ref"></a>Building Reference Implementations
 - To build the reference implementations used in the experiments run:
 
```
cd $ROOT
./build.zsh
```

This will unpack and compile the following reference implementations:
 - CLBlast and three standalone applications using it (gemv_N, gemv_T, gemm)
 - NVIDIA SDK
 - AMD SDK
 - Rodinia
 - Parboil
 - SHOC

# <a name="evaluation"></a>Experiment Workflow

## Overview Experiment Workflow
To evaluate the artifact the reviewers are invited to:
 1. inspect the _Lift_ implementation and investigate how it matches the description in the paper;
 2. reproduce the main performance result from the paper (Figure 8);
 3. generate the OpenCL kernels used in the experimental evaluation.

## Inspect the _Lift_ Implementation
The _Lift_ implementation is in the `$ROOT/lift` folder.

### _Lift_ IR Example

The partial dot-product example discussed in Section 4.2 and show in Listing 1 can be found in `$ROOT/lift/src/main/cgo_artifact/Listing1.scala`.
Running `$ROOT/lift/scripts/Listing1` will generate the OpenCL code at `$ROOT/partialDot.cl` and a figure similar to Figure 3 at `$ROOT/partialDot.pdf`.

### Organization of classes

Most of the classes shown in the class diagram in Figure 2, like `Expr` and `FunDecl` can be found in `$ROOT/lift/src/main/ir/ast/Expr.scala` and `$ROOT/lift/src/main/ir/ast/Decl.scala`.
The implementations of more patterns discussed in the paper can be found in `$ROOT/lift/src/main/ir/ast/` and `$ROOT/lift/src/main/opencl/ir/pattern/`.


## Reproduce Results from Figure 8
 - To reproduce the performance results perform the following steps **after** performing the [build](#buildin-lift) [steps](#buildin-ref).

 - To select which OpenCL platform and device to use, edit the `PLATFORM` and `DEVICE` variables in the `$ROOT/run_baseline.zsh` and `$ROOT/run_lift.zsh` scripts. By default, platform 0 and device 0 are selected.
 
 - For running the reference implementations execute:
```
cd $ROOT
./run_baseline.zsh
```
The results will be recorded in the `baseline_results.csv` file

 - For running the _Lift_ implementations set the `ARTIFACT_ROOT` environment variable to point to the root directory and then execute:
```
export ARTIFACT_ROOT=$ROOT
cd $ARTIFACT_ROOT
./run_lift.zsh
./parse_lift.zsh
```
The results will be recorded in the `lift_results.csv` file

 - For plotting the results execute:
```
cd $ROOT
./plot.r
```
This will produce the `plot.pdf` file which contains a plot for the selected platform similar to Figure 8.


## Generating the Kernels

We provide the kernels we used in the experimental evaluation in the directory `$ROOT/kernels`.

 - To reproduce the kernels used in the evaluation, perform the following steps **after** performing the [build](#buildin-lift) [steps](#buildin-ref).
 
 - For generating the kernels execute:
```
cd $ROOT
./generate_lift.zsh
```
 The generated kernels can now be found in the `$ROOT/generated_kernels` directory.