cmake_minimum_required(VERSION 2.8.11)
project(cudnn)

set(CMAKE_CXX_STANDARD 14)

# CUDA + cuDNN
find_package(CUDA REQUIRED)
include_directories(${CUDA_INCLUDE_DIRS})
find_package(CUDNN REQUIRED)
include_directories(${CUDNN_INCLUDE_DIR})

add_executable(cudnn_gaussian gaussian.cpp)
target_include_directories(cudnn_gaussian PUBLIC ${ARGPARSE_INCLUDE_DIR})
target_link_libraries(cudnn_gaussian ${CUDA_CUDART_LIBRARY} ${CUDNN_LIBRARY})

add_executable(cudnn_multi_channel_convolution multi_channel_convolution.cpp)
target_include_directories(cudnn_multi_channel_convolution PUBLIC ${ARGPARSE_INCLUDE_DIR})
target_link_libraries(cudnn_multi_channel_convolution ${CUDA_CUDART_LIBRARY} ${CUDNN_LIBRARY})