#include <iostream>
#include <cstdlib>
#include <argparse.hpp>
#include <cuda_runtime.h>
#include <cudnn.h>
#include <fstream>

#define CUDA_SAFE_CALL(x)                                   \
do {                                                        \
    cudaError_t cuda_call_result = x;                       \
    if (cuda_call_result != cudaSuccess) {                  \
        std::cerr << "error: " #x " failed with error "     \
                  << cudaGetErrorString(cuda_call_result);  \
        exit(1);                                            \
    }                                                       \
} while(false)

#define CUDNN_SAFE_CALL(expression)                          \
  {                                                          \
    cudnnStatus_t status = (expression);                     \
    if (status != CUDNN_STATUS_SUCCESS) {                    \
      std::cerr << "Error on line " << __LINE__ << ": "      \
                << cudnnGetErrorString(status) << std::endl; \
      std::exit(EXIT_FAILURE);                               \
    }                                                        \
  }

int main(int argc, const char **argv) {
    // define and parse arguments
    ArgumentParser args;
    args.addArgument("--device-id",   1, false);
    args.addArgument("--input-size",  5, false);
    args.parse(static_cast<size_t>(argc), argv);
    const size_t device_id          = args.retrieve_size_t("device-id");
    std::vector<size_t> input_size  = args.retrieve_size_t_vector("input-size");
    const int N = input_size[0], K = input_size[1], P = input_size[2], Q = input_size[3], C = input_size[4];
    int R = 3;
    int S = 3;
    int H = P + R - 1;
    int W = Q + S - 1;

    // set device
    CUDA_SAFE_CALL(cudaSetDevice(device_id));

    // create cuDNN handle
    cudnnHandle_t handle;
    CUDNN_SAFE_CALL(cudnnCreate(&handle));

    // describe input tensor
    cudnnTensorDescriptor_t input_descriptor;
    CUDNN_SAFE_CALL(cudnnCreateTensorDescriptor(&input_descriptor));
    CUDNN_SAFE_CALL(cudnnSetTensor4dDescriptor(
            input_descriptor,
            CUDNN_TENSOR_NCHW,
            CUDNN_DATA_FLOAT,
            N,
            C,
            H,
            W
    ));

    // describe filter tensor
    cudnnFilterDescriptor_t filter_descriptor;
    CUDNN_SAFE_CALL(cudnnCreateFilterDescriptor(&filter_descriptor));
    CUDNN_SAFE_CALL(cudnnSetFilter4dDescriptor(
            filter_descriptor,
            CUDNN_DATA_FLOAT,
            CUDNN_TENSOR_NCHW,
            K,
            C,
            R,
            S
    ));

    // describe output tensor
    cudnnTensorDescriptor_t output_descriptor;
    CUDNN_SAFE_CALL(cudnnCreateTensorDescriptor(&output_descriptor));
    CUDNN_SAFE_CALL(cudnnSetTensor4dDescriptor(
            output_descriptor,
            CUDNN_TENSOR_NCHW,
            CUDNN_DATA_FLOAT,
            N,
            K,
            P,
            Q
    ));

    // describe convolution
    cudnnConvolutionDescriptor_t convolution_descriptor;
    CUDNN_SAFE_CALL(cudnnCreateConvolutionDescriptor(&convolution_descriptor));
    CUDNN_SAFE_CALL(cudnnSetConvolution2dDescriptor(
            convolution_descriptor,
            0,
            0,
            1,
            1,
            1,
            1,
            CUDNN_CROSS_CORRELATION,
            CUDNN_DATA_FLOAT
    ));
    cudnnConvolutionFwdAlgo_t convolution_algorithm;
    CUDNN_SAFE_CALL(cudnnGetConvolutionForwardAlgorithm(
            handle,
            input_descriptor,
            filter_descriptor,
            convolution_descriptor,
            output_descriptor,
            CUDNN_CONVOLUTION_FWD_PREFER_FASTEST,
            0,
            &convolution_algorithm
    ));

    // allocate memory
    size_t workspace_bytes = 0;
    CUDNN_SAFE_CALL(cudnnGetConvolutionForwardWorkspaceSize(
            handle,
            input_descriptor,
            filter_descriptor,
            convolution_descriptor,
            output_descriptor,
            convolution_algorithm,
            &workspace_bytes
    ));
    void* d_workspace{nullptr};
    CUDA_SAFE_CALL(cudaMalloc(&d_workspace, workspace_bytes));

    std::vector<float> h_filter(K * C * R * S);
    int i = 1;
    for (int k = 0; k < K; ++k) {
        for (int c = 0; c < C; ++c) {
            for (int r = 0; r < 3; ++r) {
                for (int s = 0; s < 3; ++s) {
                    h_filter[k * C * R * S + c * R * S + r * S + s] = i++;
                }
            }
        }
    }
    float* d_filter{nullptr};
    CUDA_SAFE_CALL(cudaMalloc(&d_filter, h_filter.size() * sizeof(float)));
    CUDA_SAFE_CALL(cudaMemcpy(d_filter, h_filter.data(), h_filter.size() * sizeof(float), cudaMemcpyHostToDevice));

    std::vector<float> image((N) * (K) * (P + 3 - 1) * (Q + 3 - 1) * (C)); for (int i = 0; i < image.size(); ++i) image[i] = (i % 100) + 1;
    float* d_input{nullptr};
    CUDA_SAFE_CALL(cudaMalloc(&d_input, image.size() * sizeof(float)));
    CUDA_SAFE_CALL(cudaMemcpy(d_input, image.data(), image.size() * sizeof(float), cudaMemcpyHostToDevice));
    float* d_output{nullptr};
    CUDA_SAFE_CALL(cudaMalloc(&d_output, N * K * P * Q * sizeof(float)));
    CUDA_SAFE_CALL(cudaMemset(d_output, 0, N * K * P * Q * sizeof(float)));

    // benchmark API call
    std::cout << std::endl << "benchmarking NVIDIA cuDNN..." << std::endl;
    for (int i = 0; i < 10; ++i) {
        const float alpha = 1, beta = 0;
        CUDNN_SAFE_CALL(cudnnConvolutionForward(
                handle,
                &alpha,
                input_descriptor,
                d_input,
                filter_descriptor,
                d_filter,
                convolution_descriptor,
                convolution_algorithm,
                d_workspace,
                workspace_bytes,
                &beta,
                output_descriptor,
                d_output
        ));
    }
    float runtime = std::numeric_limits<float>::max();
    for (int i = 0; i < 200; ++i) {
        cudaEvent_t start, stop;
        CUDA_SAFE_CALL(cudaEventCreate(&start));
        CUDA_SAFE_CALL(cudaEventCreate(&stop));
        CUDA_SAFE_CALL(cudaEventRecord(start));
        const float alpha = 1, beta = 0;
        CUDNN_SAFE_CALL(cudnnConvolutionForward(
                handle,
                &alpha,
                input_descriptor,
                d_input,
                filter_descriptor,
                d_filter,
                convolution_descriptor,
                convolution_algorithm,
                d_workspace,
                workspace_bytes,
                &beta,
                output_descriptor,
                d_output
        ));
        CUDA_SAFE_CALL(cudaEventRecord(stop));
        CUDA_SAFE_CALL(cudaEventSynchronize(stop));
        float tmp = 0;
        CUDA_SAFE_CALL(cudaEventElapsedTime(&tmp, start, stop));
        runtime = std::min(runtime, tmp);
        CUDA_SAFE_CALL(cudaEventDestroy(start));
        CUDA_SAFE_CALL(cudaEventDestroy(stop));
    }
    CUDA_SAFE_CALL(cudaFree(d_workspace));
    CUDA_SAFE_CALL(cudaFree(d_filter));
    CUDA_SAFE_CALL(cudaFree(d_input));
    CUDA_SAFE_CALL(cudaFree(d_output));

    // write runtime to file
    std::string runtime_file_name = std::getenv("ARTIFACT_ROOT");
    runtime_file_name.append("/results/gpu/cudnn/");
    runtime_file_name.append("multi_channel_convolution_");
    if (N == 16 && K == 64 && P == 12 && Q == 120 && C == 32) {
        runtime_file_name.append("small");
    } else if (N == 8 && K == 64 && P == 54 && Q == 54 && C == 64) {
        runtime_file_name.append("large");
    } else {
        runtime_file_name.append(std::to_string(N)).append("x")
                .append(std::to_string(K)).append("x")
                .append(std::to_string(P)).append("x")
                .append(std::to_string(Q)).append("x")
                .append(std::to_string(C));
    }
    runtime_file_name.append("_runtime");
    std::ofstream runtime_file(runtime_file_name, std::ios::out | std::ios::trunc);
    runtime_file << runtime;
    runtime_file.close();
}