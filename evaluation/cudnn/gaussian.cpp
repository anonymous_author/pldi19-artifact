#include <iostream>
#include <cstdlib>
#include <argparse.hpp>
#include <cuda_runtime.h>
#include <cudnn.h>
#include <fstream>

#define CUDA_SAFE_CALL(x)                                   \
do {                                                        \
    cudaError_t cuda_call_result = x;                       \
    if (cuda_call_result != cudaSuccess) {                  \
        std::cerr << "error: " #x " failed with error "     \
                  << cudaGetErrorString(cuda_call_result);  \
        exit(1);                                            \
    }                                                       \
} while(false)

#define CUDNN_SAFE_CALL(expression)                          \
  {                                                          \
    cudnnStatus_t status = (expression);                     \
    if (status != CUDNN_STATUS_SUCCESS) {                    \
      std::cerr << "Error on line " << __LINE__ << ": "      \
                << cudnnGetErrorString(status) << std::endl; \
      std::exit(EXIT_FAILURE);                               \
    }                                                        \
  }

int main(int argc, const char **argv) {
    // define and parse arguments
    ArgumentParser args;
    args.addArgument("--device-id",   1, false);
    args.addArgument("--input-size",  2, false);
    args.parse(static_cast<size_t>(argc), argv);
    const size_t device_id          = args.retrieve_size_t("device-id");
    std::vector<size_t> input_size  = args.retrieve_size_t_vector("input-size");
    const size_t H = input_size[0], W = input_size[1];

    // set device
    CUDA_SAFE_CALL(cudaSetDevice(device_id));

    // create cuDNN handle
    cudnnHandle_t handle;
    CUDNN_SAFE_CALL(cudnnCreate(&handle));

    // describe input tensor
    cudnnTensorDescriptor_t input_descriptor;
    CUDNN_SAFE_CALL(cudnnCreateTensorDescriptor(&input_descriptor));
    CUDNN_SAFE_CALL(cudnnSetTensor4dDescriptor(input_descriptor,
            /*format=*/CUDNN_TENSOR_NCHW,
            /*dataType=*/CUDNN_DATA_FLOAT,
            /*batch_size=*/1,
            /*channels=*/1,
            /*image_height=*/(2 + H + 2),
            /*image_width=*/(2 + W + 2)));

    // describe output tensor
    cudnnTensorDescriptor_t output_descriptor;
    CUDNN_SAFE_CALL(cudnnCreateTensorDescriptor(&output_descriptor));
    CUDNN_SAFE_CALL(cudnnSetTensor4dDescriptor(output_descriptor,
            /*format=*/CUDNN_TENSOR_NCHW,
            /*dataType=*/CUDNN_DATA_FLOAT,
            /*batch_size=*/1,
            /*channels=*/1,
            /*image_height=*/H,
            /*image_width=*/W));

    // describe kernel tensor
    cudnnFilterDescriptor_t kernel_descriptor;
    CUDNN_SAFE_CALL(cudnnCreateFilterDescriptor(&kernel_descriptor));
    CUDNN_SAFE_CALL(cudnnSetFilter4dDescriptor(kernel_descriptor,
            /*dataType=*/CUDNN_DATA_FLOAT,
            /*format=*/CUDNN_TENSOR_NCHW,
            /*out_channels=*/1,
            /*in_channels=*/1,
            /*kernel_height=*/5,
            /*kernel_width=*/5));

    // describe convolution
    cudnnConvolutionDescriptor_t convolution_descriptor;
    CUDNN_SAFE_CALL(cudnnCreateConvolutionDescriptor(&convolution_descriptor));
    CUDNN_SAFE_CALL(cudnnSetConvolution2dDescriptor(convolution_descriptor,
            /*pad_height=*/0,
            /*pad_width=*/0,
            /*vertical_stride=*/1,
            /*horizontal_stride=*/1,
            /*dilation_height=*/1,
            /*dilation_width=*/1,
            /*mode=*/CUDNN_CROSS_CORRELATION,
            /*computeType=*/CUDNN_DATA_FLOAT));
    cudnnConvolutionFwdAlgo_t convolution_algorithm;
    CUDNN_SAFE_CALL(cudnnGetConvolutionForwardAlgorithm(
            handle,
            input_descriptor,
            kernel_descriptor,
            convolution_descriptor,
            output_descriptor,
            CUDNN_CONVOLUTION_FWD_PREFER_FASTEST,
            /*memoryLimitInBytes=*/0,
            &convolution_algorithm));

    // allocate memory
    size_t workspace_bytes = 0;
    CUDNN_SAFE_CALL(cudnnGetConvolutionForwardWorkspaceSize(handle,
                                                            input_descriptor,
                                                            kernel_descriptor,
                                                            convolution_descriptor,
                                                            output_descriptor,
                                                            convolution_algorithm,
                                                            &workspace_bytes));
    void* d_workspace{nullptr};
    CUDA_SAFE_CALL(cudaMalloc(&d_workspace, workspace_bytes));

    float kernel_template[5][5] = {
            {2,  4,  5,  4, 2},
            {4,  9, 12,  9, 4},
            {5, 12, 15, 12, 5},
            {4,  9, 12,  9, 4},
            {2,  4,  5,  4, 2}
    };
    for (int i = 0; i < 5; ++i)
        for (int j = 0; j < 5; ++j)
            kernel_template[i][j] /= 159.0;
    float h_kernel[1][5][5][1];
    for (int kernel = 0; kernel < 1; ++kernel) {
        for (int channel = 0; channel < 1; ++channel) {
            for (int row = 0; row < 5; ++row) {
                for (int column = 0; column < 5; ++column) {
                    h_kernel[kernel][row][column][channel] = kernel_template[row][column];
                }
            }
        }
    }
    float* d_kernel{nullptr};
    CUDA_SAFE_CALL(cudaMalloc(&d_kernel, sizeof(h_kernel)));
    CUDA_SAFE_CALL(cudaMemcpy(d_kernel, h_kernel, sizeof(h_kernel), cudaMemcpyHostToDevice));

    std::vector<float> image((2 + H + 2) * (2 + W + 2)); for (int i = 0; i < image.size(); ++i) image[i] = (i % 100) + 1;
    float* d_input{nullptr};
    CUDA_SAFE_CALL(cudaMalloc(&d_input, image.size() * sizeof(float)));
    CUDA_SAFE_CALL(cudaMemcpy(d_input, image.data(), image.size() * sizeof(float), cudaMemcpyHostToDevice));
    float* d_output{nullptr};
    CUDA_SAFE_CALL(cudaMalloc(&d_output, H * W * sizeof(float)));
    CUDA_SAFE_CALL(cudaMemset(d_output, 0, H * W * sizeof(float)));

    // benchmark API call
    std::cout << std::endl << "benchmarking NVIDIA cuDNN..." << std::endl;
    for (int i = 0; i < 10; ++i) {
        const float alpha = 1, beta = 0;
        CUDNN_SAFE_CALL(cudnnConvolutionForward(handle,
                                                &alpha,
                                                input_descriptor,
                                                d_input,
                                                kernel_descriptor,
                                                d_kernel,
                                                convolution_descriptor,
                                                convolution_algorithm,
                                                d_workspace,
                                                workspace_bytes,
                                                &beta,
                                                output_descriptor,
                                                d_output));
    }
    float runtime = std::numeric_limits<float>::max();
    for (int i = 0; i < 200; ++i) {
        cudaEvent_t start, stop;
        CUDA_SAFE_CALL(cudaEventCreate(&start));
        CUDA_SAFE_CALL(cudaEventCreate(&stop));
        CUDA_SAFE_CALL(cudaEventRecord(start));
        const float alpha = 1, beta = 0;
        CUDNN_SAFE_CALL(cudnnConvolutionForward(handle,
                                                &alpha,
                                                input_descriptor,
                                                d_input,
                                                kernel_descriptor,
                                                d_kernel,
                                                convolution_descriptor,
                                                convolution_algorithm,
                                                d_workspace,
                                                workspace_bytes,
                                                &beta,
                                                output_descriptor,
                                                d_output));
        CUDA_SAFE_CALL(cudaEventRecord(stop));
        CUDA_SAFE_CALL(cudaEventSynchronize(stop));
        float tmp = 0;
        CUDA_SAFE_CALL(cudaEventElapsedTime(&tmp, start, stop));
        runtime = std::min(runtime, tmp);
        CUDA_SAFE_CALL(cudaEventDestroy(start));
        CUDA_SAFE_CALL(cudaEventDestroy(stop));
    }
    CUDA_SAFE_CALL(cudaFree(d_workspace));
    CUDA_SAFE_CALL(cudaFree(d_kernel));
    CUDA_SAFE_CALL(cudaFree(d_input));
    CUDA_SAFE_CALL(cudaFree(d_output));

    // write runtime to file
    std::string runtime_file_name = std::getenv("ARTIFACT_ROOT");
    runtime_file_name.append("/results/gpu/cudnn/");
    runtime_file_name.append("gaussian_");
    if (H == 220 && W == 220) {
        runtime_file_name.append("small");
    } else if (H == 4092 && W == 4092) {
        runtime_file_name.append("large");
    } else {
        runtime_file_name.append(std::to_string(H + 4)).append("x")
                .append(std::to_string(W + 4));
    }
    runtime_file_name.append("_runtime");
    std::ofstream runtime_file(runtime_file_name, std::ios::out | std::ios::trunc);
    runtime_file << runtime;
    runtime_file.close();
}