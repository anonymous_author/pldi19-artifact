// helper macros
#if   OCL_DIM_L_1 == 1
  #define CONCAT_IN_DESCENDING_OCL_ORDER_1(i, j) i

  #define CONCAT_IN_DESCENDING_OCL_ORDER_0(i, j) j
#elif OCL_DIM_R_1 == 1
  #define CONCAT_IN_DESCENDING_OCL_ORDER_1(i, j) j

  #define CONCAT_IN_DESCENDING_OCL_ORDER_0(i, j) i
#endif
#define CONCAT_IN_DESCENDING_OCL_ORDER(i, j) CONCAT_IN_DESCENDING_OCL_ORDER_1(i, j)CONCAT_IN_DESCENDING_OCL_ORDER_0(i, j)

#if   OCL_DIM_L_1 == 1
  #define FLAT_INDEX_IN_DESCENDING_OCL_ORDER_1(i_id, j_id, i_size, j_size) i_id

  #define FLAT_INDEX_IN_DESCENDING_OCL_ORDER_0(i_id, j_id, i_size, j_size) (FLAT_INDEX_IN_DESCENDING_OCL_ORDER_1(i_id, j_id, i_size, j_size)) * (j_size) + (j_id)
#elif OCL_DIM_R_1 == 1
  #define FLAT_INDEX_IN_DESCENDING_OCL_ORDER_1(i_id, j_id, i_size, j_size) j_id

  #define FLAT_INDEX_IN_DESCENDING_OCL_ORDER_0(i_id, j_id, i_size, j_size) (FLAT_INDEX_IN_DESCENDING_OCL_ORDER_1(i_id, j_id, i_size, j_size)) * (i_size) + (i_id)
#endif
#define FLAT_INDEX_IN_DESCENDING_OCL_ORDER(i_id, j_id, i_size, j_size) FLAT_INDEX_IN_DESCENDING_OCL_ORDER_0(i_id, j_id, i_size, j_size)

#define DESCENDING_L_DIMS_0(i) i

#define CEIL(x,y) (((x) + (y) - 1) / (y))


#define GET_GLOBAL_ID_L_1   get_global_id(OCL_DIM_L_1)
#define GET_LOCAL_ID_L_1    get_local_id(OCL_DIM_L_1)
#define GET_GROUP_ID_L_1    get_group_id(OCL_DIM_L_1)
#define GET_GLOBAL_SIZE_L_1 (NUM_WG_L_1 * NUM_WI_L_1)
#define GET_LOCAL_SIZE_L_1  NUM_WI_L_1
#define GET_GLOBAL_ID_R_1   get_global_id(OCL_DIM_R_1)
#define GET_LOCAL_ID_R_1    get_local_id(OCL_DIM_R_1)
#define GET_GROUP_ID_R_1    get_group_id(OCL_DIM_R_1)
#define GET_GLOBAL_SIZE_R_1 NUM_WI_R_1
#define GET_LOCAL_SIZE_R_1  NUM_WI_R_1

#define PRIVATE 0
#define LOCAL   1
#define GLOBAL  2

// =============== macro definitions per dimension ============================
// -------------------- L_1 --------------------

// functional unit ids
#define K2_G_FU_ID_L_1 i_wg_l_1
#define K2_L_FU_ID_L_1 i_wi_l_1
#define K2_P_FU_ID_L_1 0

// number of functional units
#define K2_G_NUM_FU_L_1 NUM_WG_L_1
#define K2_L_NUM_FU_L_1 NUM_WI_L_1
#define K2_P_NUM_FU_L_1 1

// cache block sizes
#define K2_G_CB_SIZE_L_1 INPUT_SIZE_L_1
#define K2_L_CB_SIZE_L_1 L_CB_SIZE_L_1
#define K2_P_CB_SIZE_L_1 P_CB_SIZE_L_1

// number of steps per FU
#define K2_G_NUM_STEPS_L_1 1
#define K2_L_NUM_STEPS_L_1 (K2_G_NUM_CACHED_ITERATIONS_L_1 / K2_L_NUM_CACHED_ITERATIONS_L_1)
#define K2_P_NUM_STEPS_L_1 (K2_L_NUM_CACHED_ITERATIONS_L_1 / K2_P_NUM_CACHED_ITERATIONS_L_1)

// number of cached iterations per FU
#define K2_G_NUM_CACHED_ITERATIONS_L_1 (K2_G_CB_SIZE_L_1 / GET_GLOBAL_SIZE_L_1)
#define K2_L_NUM_CACHED_ITERATIONS_L_1 (K2_L_CB_SIZE_L_1 / GET_LOCAL_SIZE_L_1)
#define K2_P_NUM_CACHED_ITERATIONS_L_1 (K2_P_CB_SIZE_L_1)

// number of extra cached iterations per FU
#define K2_G_NUM_EXTRA_CACHED_ITERATIONS_L_1 0
#define K2_L_NUM_EXTRA_CACHED_ITERATIONS_L_1 (K2_G_CB_SIZE_L_1 % (GET_GLOBAL_SIZE_L_1 * K2_L_NUM_CACHED_ITERATIONS_L_1) / GET_GLOBAL_SIZE_L_1)
#define K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 ((GET_LOCAL_SIZE_L_1 * K2_L_NUM_CACHED_ITERATIONS_L_1) % (GET_LOCAL_SIZE_L_1 * K2_P_NUM_CACHED_ITERATIONS_L_1) / GET_LOCAL_SIZE_L_1)

// number of extra elements
#define K2_G_NUM_EXTRA_ELEMENTS_L_1 0
#define K2_L_NUM_EXTRA_ELEMENTS_L_1 (K2_G_CB_SIZE_L_1 % (GET_GLOBAL_SIZE_L_1 * K2_L_NUM_CACHED_ITERATIONS_L_1) % GET_GLOBAL_SIZE_L_1)
#define K2_P_NUM_EXTRA_ELEMENTS_L_1 ((GET_LOCAL_SIZE_L_1 * K2_L_NUM_CACHED_ITERATIONS_L_1) % (GET_LOCAL_SIZE_L_1 * K2_P_NUM_CACHED_ITERATIONS_L_1) % GET_LOCAL_SIZE_L_1)

// number of processed elements
#define K2_G_NUM_PROCESSED_ELEMENTS_L_1 ((K2_L_NUM_STEPS_L_1 * K2_L_NUM_CACHED_ITERATIONS_L_1 + K2_L_NUM_EXTRA_CACHED_ITERATIONS_L_1) * GET_GLOBAL_SIZE_L_1 + K2_L_NUM_EXTRA_ELEMENTS_L_1)
#define K2_L_NUM_PROCESSED_ELEMENTS_L_1 ((K2_P_NUM_STEPS_L_1 * K2_P_NUM_CACHED_ITERATIONS_L_1 + K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1) * GET_LOCAL_SIZE_L_1 + K2_P_NUM_EXTRA_ELEMENTS_L_1)
#define K2_P_NUM_PROCESSED_ELEMENTS_L_1 K2_P_NUM_CACHED_ITERATIONS_L_1

// incomplete cache block sizes
#define K2_P_INCOMPLETE_CB_SIZE_IN_COMPLETE_L_CB_L_1 (K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1)
#define K2_P_INCOMPLETE_CB_SIZE_IN_COMPLETE_G_CB_L_1 (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1)
#define K2_L_INCOMPLETE_CB_SIZE_IN_COMPLETE_G_CB_L_1 (K2_L_NUM_EXTRA_CACHED_ITERATIONS_L_1 * GET_LOCAL_SIZE_L_1)

// number of steps per FU in incomplete cache blocks
#define K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 (K2_L_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_CACHED_ITERATIONS_L_1)

// number of cached iterations per FU in incomplete cache blocks
#define K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1 (K2_P_INCOMPLETE_CB_SIZE_IN_COMPLETE_L_CB_L_1)
#define K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 (K2_P_INCOMPLETE_CB_SIZE_IN_COMPLETE_G_CB_L_1)
#define K2_L_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 (K2_L_INCOMPLETE_CB_SIZE_IN_COMPLETE_G_CB_L_1 / GET_LOCAL_SIZE_L_1)

// number of extra cached iterations per FU in incomplete cache blocks
#if K2_P_NUM_CACHED_ITERATIONS_L_1 > 0
#define K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 ((GET_LOCAL_SIZE_L_1 * K2_L_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1) % (GET_LOCAL_SIZE_L_1 * K2_P_NUM_CACHED_ITERATIONS_L_1) / GET_LOCAL_SIZE_L_1)
#else
#define K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 0
#endif

// number of extra elements in incomplete cache blocks
#if K2_P_NUM_CACHED_ITERATIONS_L_1 > 0
#define K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 ((GET_LOCAL_SIZE_L_1 * K2_L_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1) % (GET_LOCAL_SIZE_L_1 * K2_P_NUM_CACHED_ITERATIONS_L_1) % GET_LOCAL_SIZE_L_1)
#else
#define K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 0
#endif

// number of processed elements in incomplete cache blocks
#define K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1
#define K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1
#define K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 ((K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 * K2_P_NUM_CACHED_ITERATIONS_L_1 + K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1) * GET_LOCAL_SIZE_L_1 + K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1)

// index conversion
#define K2_L_TO_G_INDEX_L_1(i) ((l_step_l_1 * K2_L_NUM_CACHED_ITERATIONS_L_1 + (i) / K2_L_NUM_FU_L_1) * GET_GLOBAL_SIZE_L_1 + K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1 + ((i) % K2_L_NUM_FU_L_1))
#define K2_P_TO_L_INDEX_L_1(i) ((p_step_l_1 * K2_P_NUM_CACHED_ITERATIONS_L_1 + (i) / K2_P_NUM_FU_L_1) * GET_LOCAL_SIZE_L_1 + K2_L_FU_ID_L_1 * K2_P_NUM_FU_L_1 + ((i) % K2_P_NUM_FU_L_1))

// index conversion in phase 3
#define K2_P3_L_TO_G_INDEX_L_1(i) (K2_G_CB_SIZE_L_1 / GET_GLOBAL_SIZE_L_1 * GET_GLOBAL_SIZE_L_1 + K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1 + (i))
#define K2_P3_P_TO_L_INDEX_L_1(i) (K2_L_CB_SIZE_L_1 / GET_LOCAL_SIZE_L_1 * GET_LOCAL_SIZE_L_1 + K2_L_FU_ID_L_1 * K2_P_NUM_FU_L_1 + (i))
#define K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(i) (K2_L_INCOMPLETE_CB_SIZE_IN_COMPLETE_G_CB_L_1 / GET_LOCAL_SIZE_L_1 * GET_LOCAL_SIZE_L_1 + K2_L_FU_ID_L_1 * K2_P_NUM_FU_L_1 + (i))
#define K2_PP3_P_TO_L_INDEX_L_1(i) (K2_L_FU_ID_L_1 * K2_P_NUM_FU_L_1 + (i))

// iteration to index conversion
#define K2_L_ITERATION_TO_L_INDEX_L_1(i) ((i) * K2_L_NUM_FU_L_1 + K2_L_FU_ID_L_1)
#define K2_P_ITERATION_TO_P_INDEX_L_1(i) ((i) * K2_P_NUM_FU_L_1 + K2_P_FU_ID_L_1)


// -------------------- R_1 --------------------

// functional unit ids
#define K2_G_FU_ID_R_1 i_wg_r_1
#define K2_L_FU_ID_R_1 i_wi_r_1
#define K2_P_FU_ID_R_1 0

// number of functional units
#define K2_G_NUM_FU_R_1 1
#define K2_L_NUM_FU_R_1 NUM_WI_R_1
#define K2_P_NUM_FU_R_1 1

// cache block sizes
#if NUM_WG_R_1 * NUM_WI_R_1 > INPUT_SIZE_R_1
#define K2_G_CB_SIZE_R_1 ((INPUT_SIZE_R_1 + NUM_WI_R_1 - 1) / NUM_WI_R_1)
#else
#define K2_G_CB_SIZE_R_1 NUM_WG_R_1
#endif
#define K2_L_CB_SIZE_R_1 L_CB_SIZE_R_1
#define K2_P_CB_SIZE_R_1 P_CB_SIZE_R_1

// number of steps per FU
#define K2_G_NUM_STEPS_R_1 1
#define K2_L_NUM_STEPS_R_1 (K2_G_NUM_CACHED_ITERATIONS_R_1 / K2_L_NUM_CACHED_ITERATIONS_R_1)
#define K2_P_NUM_STEPS_R_1 (K2_L_NUM_CACHED_ITERATIONS_R_1 / K2_P_NUM_CACHED_ITERATIONS_R_1)

// number of cached iterations per FU
#define K2_G_NUM_CACHED_ITERATIONS_R_1 (K2_G_CB_SIZE_R_1 / GET_GLOBAL_SIZE_R_1)
#define K2_L_NUM_CACHED_ITERATIONS_R_1 (K2_L_CB_SIZE_R_1 / GET_LOCAL_SIZE_R_1)
#define K2_P_NUM_CACHED_ITERATIONS_R_1 (K2_P_CB_SIZE_R_1)

// number of extra cached iterations per FU
#define K2_G_NUM_EXTRA_CACHED_ITERATIONS_R_1 0
#define K2_L_NUM_EXTRA_CACHED_ITERATIONS_R_1 (K2_G_CB_SIZE_R_1 % (GET_GLOBAL_SIZE_R_1 * K2_L_NUM_CACHED_ITERATIONS_R_1) / GET_GLOBAL_SIZE_R_1)
#define K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 ((GET_LOCAL_SIZE_R_1 * K2_L_NUM_CACHED_ITERATIONS_R_1) % (GET_LOCAL_SIZE_R_1 * K2_P_NUM_CACHED_ITERATIONS_R_1) / GET_LOCAL_SIZE_R_1)

// number of extra elements
#define K2_G_NUM_EXTRA_ELEMENTS_R_1 0
#define K2_L_NUM_EXTRA_ELEMENTS_R_1 (K2_G_CB_SIZE_R_1 % (GET_GLOBAL_SIZE_R_1 * K2_L_NUM_CACHED_ITERATIONS_R_1) % GET_GLOBAL_SIZE_R_1)
#define K2_P_NUM_EXTRA_ELEMENTS_R_1 ((GET_LOCAL_SIZE_R_1 * K2_L_NUM_CACHED_ITERATIONS_R_1) % (GET_LOCAL_SIZE_R_1 * K2_P_NUM_CACHED_ITERATIONS_R_1) % GET_LOCAL_SIZE_R_1)

// number of processed elements
#define K2_G_NUM_PROCESSED_ELEMENTS_R_1 ((K2_L_NUM_STEPS_R_1 * K2_L_NUM_CACHED_ITERATIONS_R_1 + K2_L_NUM_EXTRA_CACHED_ITERATIONS_R_1) * GET_GLOBAL_SIZE_R_1 + K2_L_NUM_EXTRA_ELEMENTS_R_1)
#define K2_L_NUM_PROCESSED_ELEMENTS_R_1 ((K2_P_NUM_STEPS_R_1 * K2_P_NUM_CACHED_ITERATIONS_R_1 + K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1) * GET_LOCAL_SIZE_R_1 + K2_P_NUM_EXTRA_ELEMENTS_R_1)
#define K2_P_NUM_PROCESSED_ELEMENTS_R_1 K2_P_NUM_CACHED_ITERATIONS_R_1

// incomplete cache block sizes
#define K2_P_INCOMPLETE_CB_SIZE_IN_COMPLETE_L_CB_R_1 (K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1)
#define K2_P_INCOMPLETE_CB_SIZE_IN_COMPLETE_G_CB_R_1 (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1)
#define K2_L_INCOMPLETE_CB_SIZE_IN_COMPLETE_G_CB_R_1 (K2_L_NUM_EXTRA_CACHED_ITERATIONS_R_1 * GET_LOCAL_SIZE_R_1)

// number of steps per FU in incomplete cache blocks
#define K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 (K2_L_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 / K2_P_NUM_CACHED_ITERATIONS_R_1)

// number of cached iterations per FU in incomplete cache blocks
#define K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1 (K2_P_INCOMPLETE_CB_SIZE_IN_COMPLETE_L_CB_R_1)
#define K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 (K2_P_INCOMPLETE_CB_SIZE_IN_COMPLETE_G_CB_R_1)
#define K2_L_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 (K2_L_INCOMPLETE_CB_SIZE_IN_COMPLETE_G_CB_R_1 / GET_LOCAL_SIZE_R_1)

// number of extra cached iterations per FU in incomplete cache blocks
#if K2_P_NUM_CACHED_ITERATIONS_R_1 > 0
#define K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 ((GET_LOCAL_SIZE_R_1 * K2_L_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1) % (GET_LOCAL_SIZE_R_1 * K2_P_NUM_CACHED_ITERATIONS_R_1) / GET_LOCAL_SIZE_R_1)
#else
#define K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 0
#endif

// number of extra elements in incomplete cache blocks
#if K2_P_NUM_CACHED_ITERATIONS_R_1 > 0
#define K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 ((GET_LOCAL_SIZE_R_1 * K2_L_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1) % (GET_LOCAL_SIZE_R_1 * K2_P_NUM_CACHED_ITERATIONS_R_1) % GET_LOCAL_SIZE_R_1)
#else
#define K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 0
#endif

// number of processed elements in incomplete cache blocks
#define K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1 K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1
#define K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1 K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1
#define K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1 ((K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 * K2_P_NUM_CACHED_ITERATIONS_R_1 + K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1) * GET_LOCAL_SIZE_R_1 + K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1)

// index conversion
#define K2_L_TO_G_INDEX_R_1(i) ((l_step_r_1 * K2_L_NUM_CACHED_ITERATIONS_R_1 + (i) / K2_L_NUM_FU_R_1) * GET_GLOBAL_SIZE_R_1 + K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1 + ((i) % K2_L_NUM_FU_R_1))
#define K2_P_TO_L_INDEX_R_1(i) ((p_step_r_1 * K2_P_NUM_CACHED_ITERATIONS_R_1 + (i) / K2_P_NUM_FU_R_1) * GET_LOCAL_SIZE_R_1 + K2_L_FU_ID_R_1 * K2_P_NUM_FU_R_1 + ((i) % K2_P_NUM_FU_R_1))

// index conversion in phase 3
#define K2_P3_L_TO_G_INDEX_R_1(i) (K2_G_CB_SIZE_R_1 / GET_GLOBAL_SIZE_R_1 * GET_GLOBAL_SIZE_R_1 + K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1 + (i))
#define K2_P3_P_TO_L_INDEX_R_1(i) (K2_L_CB_SIZE_R_1 / GET_LOCAL_SIZE_R_1 * GET_LOCAL_SIZE_R_1 + K2_L_FU_ID_R_1 * K2_P_NUM_FU_R_1 + (i))
#define K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(i) (K2_L_INCOMPLETE_CB_SIZE_IN_COMPLETE_G_CB_R_1 / GET_LOCAL_SIZE_R_1 * GET_LOCAL_SIZE_R_1 + K2_L_FU_ID_R_1 * K2_P_NUM_FU_R_1 + (i))
#define K2_PP3_P_TO_L_INDEX_R_1(i) (K2_L_FU_ID_R_1 * K2_P_NUM_FU_R_1 + (i))

// iteration to index conversion
#define K2_L_ITERATION_TO_L_INDEX_R_1(i) ((i) * K2_L_NUM_FU_R_1 + K2_L_FU_ID_R_1)
#define K2_P_ITERATION_TO_P_INDEX_R_1(i) ((i) * K2_P_NUM_FU_R_1 + K2_P_FU_ID_R_1)


// -------------------- combined over dimensions --------------------

// flat WI ids
#define K2_G_FLAT_WI_ID (FLAT_INDEX_IN_DESCENDING_OCL_ORDER(GET_GLOBAL_ID_L_1, GET_GLOBAL_ID_R_1, GET_GLOBAL_SIZE_L_1, GET_GLOBAL_SIZE_R_1))
#define K2_L_FLAT_WI_ID (FLAT_INDEX_IN_DESCENDING_OCL_ORDER(GET_LOCAL_ID_L_1, GET_LOCAL_ID_R_1, GET_LOCAL_SIZE_L_1, GET_LOCAL_SIZE_R_1))
#define K2_P_FLAT_WI_ID (0)

// flat number of WIs
#define K2_G_FLAT_NUM_WI (K2_G_NUM_FU_L_1 * K2_L_NUM_FU_L_1 * K2_G_NUM_FU_R_1 * K2_L_NUM_FU_R_1)
#define K2_L_FLAT_NUM_WI (K2_L_NUM_FU_L_1 * K2_L_NUM_FU_R_1)
#define K2_P_FLAT_NUM_WI (1)
// =============== end of macro definitions per dimension =====================

// =============== macro definitions per buffer ===============================
// -------------------- buffer INT_RES --------------------

// buffer abstraction
#if   OCL_DIM_L_1 == 1
  #define BUFFER_INT_RES_INDEX_1(i, j) i
  #define BUFFER_INT_RES_G_SIZE_1 K2_G_CB_SIZE_L_1
  #define BUFFER_INT_RES_L_SIZE_1 K2_L_CB_SIZE_L_1
  #define BUFFER_INT_RES_P_SIZE_1 K2_P_CB_SIZE_L_1

  #define BUFFER_INT_RES_INDEX_0(i, j) j
  #define BUFFER_INT_RES_G_SIZE_0 NUM_WG_R_1
  #define BUFFER_INT_RES_L_SIZE_0 K2_L_CB_SIZE_R_1
  #define BUFFER_INT_RES_P_SIZE_0 K2_P_CB_SIZE_R_1
#elif OCL_DIM_R_1 == 1
  #define BUFFER_INT_RES_INDEX_1(i, j) j
  #define BUFFER_INT_RES_G_SIZE_1 NUM_WG_R_1
  #define BUFFER_INT_RES_L_SIZE_1 K2_L_CB_SIZE_R_1
  #define BUFFER_INT_RES_P_SIZE_1 K2_P_CB_SIZE_R_1

  #define BUFFER_INT_RES_INDEX_0(i, j) i
  #define BUFFER_INT_RES_G_SIZE_0 K2_G_CB_SIZE_L_1
  #define BUFFER_INT_RES_L_SIZE_0 K2_L_CB_SIZE_L_1
  #define BUFFER_INT_RES_P_SIZE_0 K2_P_CB_SIZE_L_1
#endif
#define K2_G_BUFFER_INT_RES(i, j) int_res[(BUFFER_INT_RES_INDEX_1(i, j)) * BUFFER_INT_RES_G_SIZE_0 + (BUFFER_INT_RES_INDEX_0(i, j))]
#define K2_L_BUFFER_INT_RES(i, j) cb_l_int_res[(BUFFER_INT_RES_INDEX_1(i, j))][(BUFFER_INT_RES_INDEX_0(i, j))]
#define K2_P_BUFFER_INT_RES(i, j) cb_p_int_res[(BUFFER_INT_RES_INDEX_1(i, j))][(BUFFER_INT_RES_INDEX_0(i, j))]

// partitioning and cache usage
#define K2_G_MEM_INT_RES(i, j) K2_G_BUFFER_INT_RES(i, j)
#if CACHE_L_CB != 0
#define K2_L_MEM_INT_RES(i, j) K2_L_BUFFER_INT_RES(i, j)
#define K2_P3_R_1_L_MEM_INT_RES(i, j) K2_L_BUFFER_INT_RES(i, j)
#define K2_P3_L_1_L_MEM_INT_RES(i, j) K2_L_BUFFER_INT_RES(i, j)
#define K2_P3_L_1_P3_R_1_L_MEM_INT_RES(i, j) K2_L_BUFFER_INT_RES(i, j)
#else
#define K2_L_MEM_INT_RES(i, j) K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(i), K2_L_TO_G_INDEX_R_1(j))
#define K2_P3_R_1_L_MEM_INT_RES(i, j) K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(i), K2_P3_L_TO_G_INDEX_R_1(j))
#define K2_P3_L_1_L_MEM_INT_RES(i, j) K2_G_MEM_INT_RES(K2_P3_L_TO_G_INDEX_L_1(i), K2_L_TO_G_INDEX_R_1(j))
#define K2_P3_L_1_P3_R_1_L_MEM_INT_RES(i, j) K2_G_MEM_INT_RES(K2_P3_L_TO_G_INDEX_L_1(i), K2_P3_L_TO_G_INDEX_R_1(j))
#endif
#if CACHE_P_CB != 0
#define K2_P_MEM_INT_RES(i, j) K2_P_BUFFER_INT_RES(i, j)
#define K2_P3_R_1_P_MEM_INT_RES(i, j) K2_P_BUFFER_INT_RES(i, j)
#define K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(i, j) K2_P_BUFFER_INT_RES(i, j)
#define K2_PP3_R_1_P_MEM_INT_RES(i, j) K2_P_BUFFER_INT_RES(i, j)
#define K2_P3_L_1_P_MEM_INT_RES(i, j) K2_P_BUFFER_INT_RES(i, j)
#define K2_P3_L_1_P3_R_1_P_MEM_INT_RES(i, j) K2_P_BUFFER_INT_RES(i, j)
#define K2_P3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(i, j) K2_P_BUFFER_INT_RES(i, j)
#define K2_P3_L_1_PP3_R_1_P_MEM_INT_RES(i, j) K2_P_BUFFER_INT_RES(i, j)
#define K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(i, j) K2_P_BUFFER_INT_RES(i, j)
#define K2_P3_IN_COMPLETE_G_CB_L_1_P3_R_1_P_MEM_INT_RES(i, j) K2_P_BUFFER_INT_RES(i, j)
#define K2_P3_IN_COMPLETE_G_CB_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(i, j) K2_P_BUFFER_INT_RES(i, j)
#define K2_P3_IN_COMPLETE_G_CB_L_1_PP3_R_1_P_MEM_INT_RES(i, j) K2_P_BUFFER_INT_RES(i, j)
#define K2_PP3_L_1_P_MEM_INT_RES(i, j) K2_P_BUFFER_INT_RES(i, j)
#define K2_PP3_L_1_P3_R_1_P_MEM_INT_RES(i, j) K2_P_BUFFER_INT_RES(i, j)
#define K2_PP3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(i, j) K2_P_BUFFER_INT_RES(i, j)
#define K2_PP3_L_1_PP3_R_1_P_MEM_INT_RES(i, j) K2_P_BUFFER_INT_RES(i, j)
#else
#define K2_P_MEM_INT_RES(i, j) K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(i), K2_P_TO_L_INDEX_R_1(j))
#define K2_P3_R_1_P_MEM_INT_RES(i, j) K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(i), K2_P3_P_TO_L_INDEX_R_1(j))
#define K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(i, j) K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(i), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(j))
#define K2_PP3_R_1_P_MEM_INT_RES(i, j) K2_P3_R_1_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(i), K2_PP3_P_TO_L_INDEX_R_1(j))
#define K2_P3_L_1_P_MEM_INT_RES(i, j) K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(i), K2_P_TO_L_INDEX_R_1(j))
#define K2_P3_L_1_P3_R_1_P_MEM_INT_RES(i, j) K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(i), K2_P3_P_TO_L_INDEX_R_1(j))
#define K2_P3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(i, j) K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(i), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(j))
#define K2_P3_L_1_PP3_R_1_P_MEM_INT_RES(i, j) K2_P3_R_1_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(i), K2_PP3_P_TO_L_INDEX_R_1(j))
#define K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(i, j) K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(i), K2_P_TO_L_INDEX_R_1(j))
#define K2_P3_IN_COMPLETE_G_CB_L_1_P3_R_1_P_MEM_INT_RES(i, j) K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(i), K2_P3_P_TO_L_INDEX_R_1(j))
#define K2_P3_IN_COMPLETE_G_CB_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(i, j) K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(i), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(j))
#define K2_P3_IN_COMPLETE_G_CB_L_1_PP3_R_1_P_MEM_INT_RES(i, j) K2_P3_R_1_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(i), K2_PP3_P_TO_L_INDEX_R_1(j))
#define K2_PP3_L_1_P_MEM_INT_RES(i, j) K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(i), K2_P_TO_L_INDEX_R_1(j))
#define K2_PP3_L_1_P3_R_1_P_MEM_INT_RES(i, j) K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(i), K2_P3_P_TO_L_INDEX_R_1(j))
#define K2_PP3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(i, j) K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(i), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(j))
#define K2_PP3_L_1_PP3_R_1_P_MEM_INT_RES(i, j) K2_P3_L_1_P3_R_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(i), K2_PP3_P_TO_L_INDEX_R_1(j))
#endif



// -------------------- result buffer --------------------

// check which levels are used
#if G_CB_RES_DEST_LEVEL == PRIVATE || L_CB_RES_DEST_LEVEL == PRIVATE || P_CB_RES_DEST_LEVEL == PRIVATE
#define K2_P_LEVEL_HAS_RESULTS
#endif
#if G_CB_RES_DEST_LEVEL == LOCAL || L_CB_RES_DEST_LEVEL == LOCAL || P_CB_RES_DEST_LEVEL == LOCAL
#define K2_L_LEVEL_HAS_RESULTS
#endif
#if G_CB_RES_DEST_LEVEL == GLOBAL || L_CB_RES_DEST_LEVEL == GLOBAL || P_CB_RES_DEST_LEVEL == GLOBAL
#define K2_G_LEVEL_HAS_RESULTS
#endif

// ------ PRIVATE ------
#ifdef K2_P_LEVEL_HAS_RESULTS
// construct prefix for res_p
#if G_CB_RES_DEST_LEVEL == PRIVATE
#define K2_RES_P_BUFFER_G_PREFIX_L_1() [l_step_l_1]
#define K2_RES_P_BUFFER_DEF_G_PREFIX_L_1() [K2_L_NUM_STEPS_L_1 + (K2_L_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0) + (K2_L_NUM_EXTRA_ELEMENTS_L_1 > 0)]
#else
#define K2_RES_P_BUFFER_G_PREFIX_L_1()
#define K2_RES_P_BUFFER_DEF_G_PREFIX_L_1()
#endif
#if L_CB_RES_DEST_LEVEL == PRIVATE
#define K2_RES_P_BUFFER_L_PREFIX_L_1() [p_step_l_1]
#define K2_RES_P_BUFFER_DEF_L_PREFIX_L_1() [K2_P_NUM_STEPS_L_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0) + (K2_P_NUM_EXTRA_ELEMENTS_L_1 > 0)]
#else
#define K2_RES_P_BUFFER_L_PREFIX_L_1()
#define K2_RES_P_BUFFER_DEF_L_PREFIX_L_1()
#endif
// construct suffix for res_p
#if   P_CB_RES_DEST_LEVEL == PRIVATE
#define K2_RES_P_BUFFER_SUFFIX_R_1(i) [(i)]
#define K2_RES_P_BUFFER_DEF_SUFFIX_R_1 [K2_P_NUM_FU_R_1]
#endif
// buffer abstraction for res_p
#define K2_RES_P_BUFFER_NAME() res_p
#define K2_RES_P_BUFFER_DEF K2_RES_P_BUFFER_NAME()CONCAT_IN_DESCENDING_OCL_ORDER(K2_RES_P_BUFFER_DEF_G_PREFIX_L_1()K2_RES_P_BUFFER_DEF_L_PREFIX_L_1()[K2_P_CB_SIZE_L_1], K2_RES_P_BUFFER_DEF_SUFFIX_R_1)
#define K2_RES_P_BUFFER(i, j) K2_RES_P_BUFFER_NAME()CONCAT_IN_DESCENDING_OCL_ORDER(K2_RES_P_BUFFER_G_PREFIX_L_1()K2_RES_P_BUFFER_L_PREFIX_L_1()[i], K2_RES_P_BUFFER_SUFFIX_R_1(j))
#endif

// ------ LOCAL ------
#ifdef K2_L_LEVEL_HAS_RESULTS
// construct prefix for res_l
#if G_CB_RES_DEST_LEVEL == LOCAL
#define K2_RES_L_BUFFER_G_PREFIX_L_1() [l_step_l_1]
#define K2_RES_L_BUFFER_DEF_G_PREFIX_L_1() [K2_L_NUM_STEPS_L_1 + (K2_L_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0) + (K2_L_NUM_EXTRA_ELEMENTS_L_1 > 0)]
#else
#define K2_RES_L_BUFFER_G_PREFIX_L_1()
#define K2_RES_L_BUFFER_DEF_G_PREFIX_L_1()
#endif
// construct suffix for res_l
#if   P_CB_RES_DEST_LEVEL == LOCAL
#define K2_RES_L_BUFFER_SUFFIX_R_1(i, j) [(i)][(j)]
#define K2_RES_L_BUFFER_DEF_SUFFIX_R_1 [K2_L_NUM_FU_R_1][K2_P_NUM_FU_R_1]
#elif L_CB_RES_DEST_LEVEL == LOCAL
#define K2_RES_L_BUFFER_SUFFIX_R_1(i, j) [(i)]
#define K2_RES_L_BUFFER_DEF_SUFFIX_R_1 [K2_L_NUM_FU_R_1]
#endif
// buffer abstraction for res_l
#define K2_RES_L_BUFFER_NAME() res_l
#define K2_RES_L_BUFFER_DEF K2_RES_L_BUFFER_NAME()CONCAT_IN_DESCENDING_OCL_ORDER(K2_RES_L_BUFFER_DEF_G_PREFIX_L_1()[K2_L_CB_SIZE_L_1], K2_RES_L_BUFFER_DEF_SUFFIX_R_1)
#define K2_RES_L_BUFFER(i, j, k) K2_RES_L_BUFFER_NAME()CONCAT_IN_DESCENDING_OCL_ORDER(K2_RES_L_BUFFER_G_PREFIX_L_1()[i], K2_RES_L_BUFFER_SUFFIX_R_1(j, k))
#endif

// ------ GLOBAL ------
#ifdef K2_G_LEVEL_HAS_RESULTS
// construct suffix for res_g
#if   P_CB_RES_DEST_LEVEL == GLOBAL
#define K2_RES_G_BUFFER_SUFFIX_R_1(i, j, k) ((i)) * K2_L_NUM_FU_R_1 * K2_P_NUM_FU_R_1 + ((j)) * K2_P_NUM_FU_R_1 + ((k))
#define K2_RES_G_BUFFER_DEF_SUFFIX_R_1 K2_G_NUM_FU_R_1 * K2_L_NUM_FU_R_1 * K2_P_NUM_FU_R_1
#elif L_CB_RES_DEST_LEVEL == GLOBAL
#define K2_RES_G_BUFFER_SUFFIX_R_1(i, j, k) ((i)) * K2_L_NUM_FU_R_1 + ((j))
#define K2_RES_G_BUFFER_DEF_SUFFIX_R_1 K2_G_NUM_FU_R_1 * K2_L_NUM_FU_R_1
#elif G_CB_RES_DEST_LEVEL == GLOBAL
#define K2_RES_G_BUFFER_SUFFIX_R_1(i, j, k) ((i))
#define K2_RES_G_BUFFER_DEF_SUFFIX_R_1 K2_G_NUM_FU_R_1
#endif
// buffer abstraction for res_g
#if (L_CB_RES_DEST_LEVEL == GLOBAL && K2_L_NUM_FU_R_1 > 1) || (P_CB_RES_DEST_LEVEL == GLOBAL && K2_P_NUM_FU_R_1 > 1)
#define K2_RES_G_BUFFER_NAME() res_g
#define K2_RES_G_BUFFER(i, j, k, l) K2_RES_G_BUFFER_NAME()[FLAT_INDEX_IN_DESCENDING_OCL_ORDER(i, K2_RES_G_BUFFER_SUFFIX_R_1(j, k, l), K2_G_CB_SIZE_L_1, K2_RES_G_BUFFER_DEF_SUFFIX_R_1)]
#else
#define K2_RES_G_BUFFER_NAME() c
#define K2_RES_G_BUFFER(i, j, k, l) K2_RES_G_BUFFER_NAME()[(i)]
#endif
#endif

// determine memory destination for results
#if   P_CB_RES_DEST_LEVEL == PRIVATE
#define K2_P_CB_RES_DEST(i) K2_RES_P_BUFFER(i, K2_P_FU_ID_R_1)
#define K2_P3_L_1_P_CB_RES_DEST(i) K2_RES_P_BUFFER(i, K2_P_FU_ID_R_1)
#define K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(i) K2_RES_P_BUFFER(i, K2_P_FU_ID_R_1)
#define K2_PP3_L_1_P_CB_RES_DEST(i) K2_RES_P_BUFFER(i, K2_P_FU_ID_R_1)
#elif P_CB_RES_DEST_LEVEL == LOCAL
#define K2_P_CB_RES_DEST(i) K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(i), K2_L_FU_ID_R_1, K2_P_FU_ID_R_1)
#define K2_P3_L_1_P_CB_RES_DEST(i) K2_RES_L_BUFFER(K2_P3_P_TO_L_INDEX_L_1(i), K2_L_FU_ID_R_1, K2_P_FU_ID_R_1)
#define K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(i) K2_RES_L_BUFFER(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(i), K2_L_FU_ID_R_1, K2_P_FU_ID_R_1)
#define K2_PP3_L_1_P_CB_RES_DEST(i) K2_RES_L_BUFFER(K2_PP3_P_TO_L_INDEX_L_1(i), K2_L_FU_ID_R_1, K2_P_FU_ID_R_1)
#elif P_CB_RES_DEST_LEVEL == GLOBAL
#define K2_P_CB_RES_DEST(i) K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(i)), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1, K2_P_FU_ID_R_1)
#define K2_P3_L_1_P_CB_RES_DEST(i) K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P3_P_TO_L_INDEX_L_1(i)), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1, K2_P_FU_ID_R_1)
#define K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(i) K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(i)), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1, K2_P_FU_ID_R_1)
#define K2_PP3_L_1_P_CB_RES_DEST(i) K2_RES_G_BUFFER(K2_P3_L_TO_G_INDEX_L_1(K2_PP3_P_TO_L_INDEX_L_1(i)), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1, K2_P_FU_ID_R_1)
#endif

#if   L_CB_RES_DEST_LEVEL == PRIVATE
#define K2_L_CB_RES_DEST(i) K2_RES_P_BUFFER(i, K2_P_FU_ID_R_1)
#define K2_P3_L_1_L_CB_RES_DEST(i) K2_RES_P_BUFFER(i, K2_P_FU_ID_R_1)
#elif L_CB_RES_DEST_LEVEL == LOCAL
#define K2_L_CB_RES_DEST(i) K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(i), K2_L_FU_ID_R_1, K2_P_FU_ID_R_1)
#define K2_P3_L_1_L_CB_RES_DEST(i) K2_RES_L_BUFFER(K2_PP3_P_TO_L_INDEX_L_1(i), K2_L_FU_ID_R_1, K2_P_FU_ID_R_1)
#elif L_CB_RES_DEST_LEVEL == GLOBAL
#define K2_L_CB_RES_DEST(i) K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(i)), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1, K2_P_FU_ID_R_1)
#define K2_P3_L_1_L_CB_RES_DEST(i) K2_RES_G_BUFFER(K2_P3_L_TO_G_INDEX_L_1(K2_PP3_P_TO_L_INDEX_L_1(i)), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1, K2_P_FU_ID_R_1)
#endif

// check if seperate memory for reduction is needed
// will only be checked for local level, because of constraints for OpenCL
#if L_CB_RES_DEST_LEVEL < LOCAL
#define K2_L_REDUCTION_MEM_NAME() l_reduction_mem
#define K2_L_REDUCTION_MEM(i, j) K2_L_REDUCTION_MEM_NAME()CONCAT_IN_DESCENDING_OCL_ORDER([i], [(j)])
#endif

// buffer abstraction for kernel_res buffer
#define K2_KERNEL_RES_BUFFER(i, j) c[(i)]

#define K2_G_KERNEL_RES(i) K2_KERNEL_RES_BUFFER(i, K2_G_FU_ID_R_1)
#define K2_L_KERNEL_RES(i) K2_G_KERNEL_RES(K2_L_TO_G_INDEX_L_1(i))
#define K2_P3_L_1_L_KERNEL_RES(i) K2_G_KERNEL_RES(K2_P3_L_TO_G_INDEX_L_1(i))
#define K2_P_KERNEL_RES(i) K2_L_KERNEL_RES(K2_P_TO_L_INDEX_L_1(i))
#define K2_P3_L_1_P_KERNEL_RES(i) K2_L_KERNEL_RES(K2_P3_P_TO_L_INDEX_L_1(i))
#define K2_P3_IN_COMPLETE_G_CB_L_1_P_KERNEL_RES(i) K2_L_KERNEL_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(i))
#define K2_PP3_L_1_P_KERNEL_RES(i) K2_P3_L_1_L_KERNEL_RES(K2_PP3_P_TO_L_INDEX_L_1(i))
// =============== end of macro definitions per buffer ========================

// =============== kernel 2 ===================================================
__kernel void gemv_2(__global TYPE_T const * const restrict int_res, __global TYPE_TS * const restrict res_g, __global TYPE_TS * const restrict c) {
  // map md_hom dimensions to OpenCL dimensions
  const size_t i_wg_l_1 = GET_GROUP_ID_L_1;
  const size_t i_wi_l_1 = GET_LOCAL_ID_L_1;
  
  const size_t i_wg_r_1 = GET_GROUP_ID_R_1;
  const size_t i_wi_r_1 = GET_LOCAL_ID_R_1;
  
  // declare variables for caching inputs
  #if CACHE_L_CB != 0
  __local TYPE_T cb_l_int_res[BUFFER_INT_RES_L_SIZE_1][BUFFER_INT_RES_L_SIZE_0];
  #endif
  #if CACHE_P_CB != 0
  __private TYPE_T cb_p_int_res[BUFFER_INT_RES_P_SIZE_1][BUFFER_INT_RES_P_SIZE_0];
  #endif

  // declare variables for result memory
  // ------ LOCAL ------
  #ifdef K2_L_LEVEL_HAS_RESULTS
  __local TYPE_TS K2_RES_L_BUFFER_DEF;
  #endif
  // ------ PRIVATE ------
  #ifdef K2_P_LEVEL_HAS_RESULTS
  __private TYPE_TS K2_RES_P_BUFFER_DEF;
  #endif
  
  // declare variables for reduction memory (if needed)
  #if L_CB_RES_DEST_LEVEL < LOCAL && (K2_L_NUM_FU_R_1 > 1)
  __local TYPE_TS K2_L_REDUCTION_MEM_NAME()CONCAT_IN_DESCENDING_OCL_ORDER([K2_L_CB_SIZE_L_1], [K2_L_NUM_FU_R_1]);
  #endif

  // reset memory where reduction takes place for WIs that do not
  // calculate intermediate results, so that reduction can always
  // be done with all WIs
  #if K2_L_NUM_FU_R_1 > 1
  #if (K2_L_NUM_STEPS_R_1 > 0 && K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0) ||\
      (K2_L_NUM_STEPS_R_1 == 0 && K2_L_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0 && K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0) ||\
      (K2_L_NUM_STEPS_R_1 == 0 && K2_L_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0 && K2_L_NUM_EXTRA_ELEMENTS_R_1 > 0)
  #if K2_L_NUM_STEPS_R_1 == 0 && K2_L_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0 && K2_L_NUM_EXTRA_ELEMENTS_R_1 > 0
  if (K2_G_FU_ID_R_1 < (K2_L_NUM_EXTRA_ELEMENTS_R_1 + K2_L_NUM_FU_R_1 - 1) / K2_L_NUM_FU_R_1)
  #endif
  {
    #if K2_L_NUM_STEPS_R_1 > 0 && K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0
    const int num_active_wi_r_1 = K2_P_NUM_EXTRA_ELEMENTS_R_1;
    #elif K2_L_NUM_STEPS_R_1 == 0 && K2_L_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0 && K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0
    const int num_active_wi_r_1 = K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1;
    #elif K2_L_NUM_STEPS_R_1 == 0 && K2_L_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0 && K2_L_NUM_EXTRA_ELEMENTS_R_1 > 0
    const int num_active_wi_r_1 = K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1;
    #endif
    
    if (K2_L_FU_ID_R_1 >= num_active_wi_r_1) {
      // phase 1: process local cached iterations in dimension L_1 with all FUs
      #if K2_L_NUM_STEPS_L_1 > 0
      for (size_t l_step_l_1 = 0; l_step_l_1 < K2_L_NUM_STEPS_L_1; ++l_step_l_1) {
        
        // phase 1: process private cached iterations in dimension L_1 with all FUs
        #if K2_P_NUM_STEPS_L_1 > 0
        for (size_t p_step_l_1 = 0; p_step_l_1 < K2_P_NUM_STEPS_L_1; ++p_step_l_1) {
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            K2_L_CB_RES_DEST(p_iteration_l_1) = 0;
          
          }
        } // end of "p_step_l_1"-loop
        #endif
        // phase 2: process extra private iterations in dimension L_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0
        {  
          const size_t p_step_l_1 = K2_P_NUM_STEPS_L_1;
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            K2_L_CB_RES_DEST(p_iteration_l_1) = 0;
          
          }
        }
        #endif
        // phase 3: process extra private elements in dimension L_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_L_1 > 0
        if (K2_L_FU_ID_L_1 < (K2_P_NUM_EXTRA_ELEMENTS_L_1 + K2_P_NUM_FU_L_1 - 1) / K2_P_NUM_FU_L_1) {  
          // set step variable for access to res buffer
          const size_t p_step_l_1 = K2_P_NUM_STEPS_L_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0);
          {
            size_t p_iteration_l_1 = 0;
            K2_L_CB_RES_DEST(p_iteration_l_1) = 0;
          
          }
        }
        #endif
      
      } // end of "l_step_l_1"-loop
      #endif
      // phase 2: process extra local iterations in dimension L_1 with all FUs
      #if K2_L_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0
      {  
        const size_t l_step_l_1 = K2_L_NUM_STEPS_L_1;
        // phase 1: process private cached iterations in dimension L_1 with all FUs
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 > 0
        for (size_t p_step_l_1 = 0; p_step_l_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1; ++p_step_l_1) {
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            K2_L_CB_RES_DEST(p_iteration_l_1) = 0;
          
          }
        } // end of "p_step_l_1"-loop
        #endif
        // phase 2: process extra private iterations in dimension L_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 > 0
        {  
          const size_t p_step_l_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1;
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            K2_L_CB_RES_DEST(p_iteration_l_1) = 0;
          
          }
        }
        #endif
        // phase 3: process extra private elements in dimension L_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 > 0
        if (K2_L_FU_ID_L_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 + K2_P_NUM_FU_L_1 - 1) / K2_P_NUM_FU_L_1) {  
          // set step variable for access to res buffer
          const size_t p_step_l_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 > 0);
          {
            size_t p_iteration_l_1 = 0;
            K2_L_CB_RES_DEST(p_iteration_l_1) = 0;
          
          }
        }
        #endif
      }
      #endif
      // phase 3: process extra local elements in dimension L_1 with a subset of all FUs
      #if K2_L_NUM_EXTRA_ELEMENTS_L_1 > 0
      if (K2_G_FU_ID_L_1 < (K2_L_NUM_EXTRA_ELEMENTS_L_1 + K2_L_NUM_FU_L_1 - 1) / K2_L_NUM_FU_L_1) {  
        // set step variable for access to res buffer
        const size_t l_step_l_1 = K2_L_NUM_STEPS_L_1 + (K2_L_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0);
        // parent level in phase 3: process extra private elements in dimension L_1 with a subset of all FUs
        if (K2_L_FU_ID_L_1 < K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) {
          size_t p_step_l_1 = 0;
          {
            size_t p_iteration_l_1 = 0;
            K2_P3_L_1_L_CB_RES_DEST(p_iteration_l_1) = 0;
          
          }
        }
      
      }
      #endif
    }
  }
  #endif
  #endif

  // phase 1: process local cached iterations in dimension L_1 with all FUs
  #if K2_L_NUM_STEPS_L_1 > 0
  for (size_t l_step_l_1 = 0; l_step_l_1 < K2_L_NUM_STEPS_L_1; ++l_step_l_1) {
    // phase 1: process local cached iterations in dimension R_1 with all FUs
    #if K2_L_NUM_STEPS_R_1 > 0
    size_t l_step_r_1 = 0;
    // ---------- L caching --------------------
    #if CACHE_L_CB != 0
    #if (K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_FLAT_NUM_WI > 0
    for (size_t step = 0; step < (K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_FLAT_NUM_WI; ++step) {
      const size_t flat_index = K2_L_FLAT_WI_ID + step * K2_L_FLAT_NUM_WI;
      const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_R_1);
      const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_R_1;
      K2_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
    }
    #endif
    #if (K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) % K2_L_FLAT_NUM_WI > 0
    if (K2_L_FLAT_WI_ID < (K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) % K2_L_FLAT_NUM_WI) {
      const size_t flat_index = K2_L_FLAT_WI_ID + ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_FLAT_NUM_WI) * K2_L_FLAT_NUM_WI;
      const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_R_1);
      const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_R_1;
      K2_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
    }
    #endif
    barrier(CLK_LOCAL_MEM_FENCE);
    #endif
    // ---------- end of L caching -------------
    
    
    // phase 1: process private cached iterations in dimension L_1 with all FUs
    #if K2_P_NUM_STEPS_L_1 > 0
    for (size_t p_step_l_1 = 0; p_step_l_1 < K2_P_NUM_STEPS_L_1; ++p_step_l_1) {
      // phase 1: process private cached iterations in dimension R_1 with all FUs
      #if K2_P_NUM_STEPS_R_1 > 0
      size_t p_step_r_1 = 0;
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
        #if CACHE_L_CB != 0
        const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
        const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        #else
        const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
        #endif
        K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
      }
      #endif
      #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI > 0
      if (K2_P_FLAT_WI_ID < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI) {
        const size_t flat_index = K2_P_FLAT_WI_ID + ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
        #if CACHE_L_CB != 0
        const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
        const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        #else
        const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
        #endif
        K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
        size_t p_iteration_r_1 = 0;
        // process one mda element
        K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
        
        #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
        for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
        
        }
        #endif
        
      
      }
      #if K2_P_NUM_STEPS_R_1 > 1
      for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_R_1; ++p_step_r_1) {
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }
      } // end of "p_step_r_1"-loop
      #endif
      #endif
      // phase 2: process extra private iterations in dimension R_1 with all FUs
      #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0
      {  
        const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1;  
        #if K2_P_NUM_STEPS_R_1 == 0
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }  
        #else
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }  
        #endif
      }
      #endif
      // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
      #if K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
      if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
        // set step variable for access to res buffer
        const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0);  
        #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
        
        }  
        #else
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
          {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }  
        #endif
      }
      #endif
      
      // ---------- reduction --------------------
      // will never be necessary on this level
      // because K2_P_NUM_FU_R == 1 for OpenCL
      // ---------- end of reduction -------------
      
      // move results upwards in memory hierarchy
      #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
      #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
      if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
      #endif
      {
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1)
          K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
      }
      #endif
    
    } // end of "p_step_l_1"-loop
    #endif
    // phase 2: process extra private iterations in dimension L_1 with all FUs
    #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0
    {  
      const size_t p_step_l_1 = K2_P_NUM_STEPS_L_1;
      // phase 1: process private cached iterations in dimension R_1 with all FUs
      #if K2_P_NUM_STEPS_R_1 > 0
      size_t p_step_r_1 = 0;
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
        #if CACHE_L_CB != 0
        const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
        const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        #else
        const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
        #endif
        K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
      }
      #endif
      #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) % K2_P_FLAT_NUM_WI > 0
      if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) % K2_P_FLAT_NUM_WI) {
        const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
        #if CACHE_L_CB != 0
        const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
        const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        #else
        const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
        #endif
        K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
        size_t p_iteration_r_1 = 0;
        // process one mda element
        K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
        
        #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
        for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
        
        }
        #endif
        
      
      }
      #if K2_P_NUM_STEPS_R_1 > 1
      for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_R_1; ++p_step_r_1) {
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }
      } // end of "p_step_r_1"-loop
      #endif
      #endif
      // phase 2: process extra private iterations in dimension R_1 with all FUs
      #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0
      {  
        const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1;  
        #if K2_P_NUM_STEPS_R_1 == 0
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }  
        #else
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }  
        #endif
      }
      #endif
      // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
      #if K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
      if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
        // set step variable for access to res buffer
        const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0);  
        #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
        
        }  
        #else
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
          {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }  
        #endif
      }
      #endif
      
      // ---------- reduction --------------------
      // will never be necessary on this level
      // because K2_P_NUM_FU_R == 1 for OpenCL
      // ---------- end of reduction -------------
      
      // move results upwards in memory hierarchy
      #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
      #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
      if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
      #endif
      {
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1)
          K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
      }
      #endif
    
    }
    #endif
    // phase 3: process extra private elements in dimension L_1 with a subset of all FUs
    #if K2_P_NUM_EXTRA_ELEMENTS_L_1 > 0
    if (K2_L_FU_ID_L_1 < (K2_P_NUM_EXTRA_ELEMENTS_L_1 + K2_P_NUM_FU_L_1 - 1) / K2_P_NUM_FU_L_1) {  
      // set step variable for access to res buffer
      const size_t p_step_l_1 = K2_P_NUM_STEPS_L_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0);
      // phase 1: process private cached iterations in dimension R_1 with all FUs
      #if K2_P_NUM_STEPS_R_1 > 0
      size_t p_step_r_1 = 0;
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
        #if CACHE_L_CB != 0
        const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
        const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        #else
        const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
        #endif
        K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
      }
      if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
        const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
        #if CACHE_L_CB != 0
        const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
        const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        #else
        const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
        #endif
        K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
      }
      #endif
      // ---------- end of P caching -------------
      
      {
        size_t p_iteration_l_1 = 0;
        size_t p_iteration_r_1 = 0;
        // process one mda element
        K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
        
        #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
        for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
          // process one mda element
          K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
        
        }
        #endif
        
      
      }
      #if K2_P_NUM_STEPS_R_1 > 1
      for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_R_1; ++p_step_r_1) {
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        {
          size_t p_iteration_l_1 = 0;
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }
      } // end of "p_step_r_1"-loop
      #endif
      #endif
      // phase 2: process extra private iterations in dimension R_1 with all FUs
      #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0
      {  
        const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1;  
        #if K2_P_NUM_STEPS_R_1 == 0
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        {
          size_t p_iteration_l_1 = 0;
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }  
        #else
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        {
          size_t p_iteration_l_1 = 0;
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }  
        #endif
      }
      #endif
      // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
      #if K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
      if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
        // set step variable for access to res buffer
        const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0);  
        #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        {
          size_t p_iteration_l_1 = 0;
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_L_1_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
        
        }  
        #else
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        {
          size_t p_iteration_l_1 = 0;
          {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }  
        #endif
      }
      #endif
      
      // ---------- reduction --------------------
      // will never be necessary on this level
      // because K2_P_NUM_FU_R == 1 for OpenCL
      // ---------- end of reduction -------------
      
      // move results upwards in memory hierarchy
      #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
      #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
      if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
      #endif
      {
        
        {
          size_t p_iteration_l_1 = 0;
          K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
      }
      #endif
    
    }
    #endif
    
    
    // wait for all WIs to finish computation on local cache block
    #if CACHE_L_CB != 0
    barrier(CLK_LOCAL_MEM_FENCE);
    #endif
    #if K2_L_NUM_STEPS_R_1 > 1
    for (l_step_r_1 = 1; l_step_r_1 < K2_L_NUM_STEPS_R_1; ++l_step_r_1) {
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      #if (K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_L_FLAT_WI_ID + step * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_R_1);
        const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_R_1;
        K2_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      #endif
      #if (K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) % K2_L_FLAT_NUM_WI > 0
      if (K2_L_FLAT_WI_ID < (K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) % K2_L_FLAT_NUM_WI) {
        const size_t flat_index = K2_L_FLAT_WI_ID + ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_FLAT_NUM_WI) * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_R_1);
        const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_R_1;
        K2_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      #endif
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      
      // phase 1: process private cached iterations in dimension L_1 with all FUs
      #if K2_P_NUM_STEPS_L_1 > 0
      for (size_t p_step_l_1 = 0; p_step_l_1 < K2_P_NUM_STEPS_L_1; ++p_step_l_1) {
        // phase 1: process private cached iterations in dimension R_1 with all FUs
        #if K2_P_NUM_STEPS_R_1 > 0
        size_t p_step_r_1 = 0;
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }
        #else
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }
        #endif
        #if K2_P_NUM_STEPS_R_1 > 1
        for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_R_1; ++p_step_r_1) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
        } // end of "p_step_r_1"-loop
        #endif
        #endif
        // phase 2: process extra private iterations in dimension R_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0
        {  
          const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1;  
          #if K2_P_NUM_STEPS_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1 > 1
            for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
            #endif
            
          
          }
          #else
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
          // set step variable for access to res buffer
          const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0);  
          #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
          #else
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        
        // ---------- reduction --------------------
        // will never be necessary on this level
        // because K2_P_NUM_FU_R == 1 for OpenCL
        // ---------- end of reduction -------------
        
        // move results upwards in memory hierarchy
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
        #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
        #endif
        {
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1)
            K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
        #endif
      
      } // end of "p_step_l_1"-loop
      #endif
      // phase 2: process extra private iterations in dimension L_1 with all FUs
      #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0
      {  
        const size_t p_step_l_1 = K2_P_NUM_STEPS_L_1;
        // phase 1: process private cached iterations in dimension R_1 with all FUs
        #if K2_P_NUM_STEPS_R_1 > 0
        size_t p_step_r_1 = 0;
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }
        #else
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }
        #endif
        #if K2_P_NUM_STEPS_R_1 > 1
        for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_R_1; ++p_step_r_1) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
        } // end of "p_step_r_1"-loop
        #endif
        #endif
        // phase 2: process extra private iterations in dimension R_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0
        {  
          const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1;  
          #if K2_P_NUM_STEPS_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1 > 1
            for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
            #endif
            
          
          }
          #else
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
          // set step variable for access to res buffer
          const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0);  
          #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
          #else
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        
        // ---------- reduction --------------------
        // will never be necessary on this level
        // because K2_P_NUM_FU_R == 1 for OpenCL
        // ---------- end of reduction -------------
        
        // move results upwards in memory hierarchy
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
        #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
        #endif
        {
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1)
            K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
        #endif
      
      }
      #endif
      // phase 3: process extra private elements in dimension L_1 with a subset of all FUs
      #if K2_P_NUM_EXTRA_ELEMENTS_L_1 > 0
      if (K2_L_FU_ID_L_1 < (K2_P_NUM_EXTRA_ELEMENTS_L_1 + K2_P_NUM_FU_L_1 - 1) / K2_P_NUM_FU_L_1) {  
        // set step variable for access to res buffer
        const size_t p_step_l_1 = K2_P_NUM_STEPS_L_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0);
        // phase 1: process private cached iterations in dimension R_1 with all FUs
        #if K2_P_NUM_STEPS_R_1 > 0
        size_t p_step_r_1 = 0;
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
        {
          size_t p_iteration_l_1 = 0;
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }
        #else
        {
          size_t p_iteration_l_1 = 0;
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }
        #endif
        #if K2_P_NUM_STEPS_R_1 > 1
        for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_R_1; ++p_step_r_1) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
        } // end of "p_step_r_1"-loop
        #endif
        #endif
        // phase 2: process extra private iterations in dimension R_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0
        {  
          const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1;  
          #if K2_P_NUM_STEPS_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1 > 1
            for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
            #endif
            
          
          }
          #else
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
          // set step variable for access to res buffer
          const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0);  
          #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_L_1_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
          #else
          {
            size_t p_iteration_l_1 = 0;
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        
        // ---------- reduction --------------------
        // will never be necessary on this level
        // because K2_P_NUM_FU_R == 1 for OpenCL
        // ---------- end of reduction -------------
        
        // move results upwards in memory hierarchy
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
        #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
        #endif
        {
          
          {
            size_t p_iteration_l_1 = 0;
            K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
          }
        }
        #endif
      
      }
      #endif
      
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
    } // end of "l_step_r_1"-loop
    #endif
    #endif
    // phase 2: process extra local iterations in dimension R_1 with all FUs
    #if K2_L_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0
    {  
      const size_t l_step_r_1 = K2_L_NUM_STEPS_R_1;  
      #if K2_L_NUM_STEPS_R_1 == 0
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      #if ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_L_FLAT_WI_ID + step * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
        const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
        K2_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      #endif
      #if ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_L_FLAT_NUM_WI > 0
      if (K2_L_FLAT_WI_ID < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_L_FLAT_NUM_WI) {
        const size_t flat_index = K2_L_FLAT_WI_ID + (((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_L_FLAT_NUM_WI) * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
        const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
        K2_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      #endif
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      // phase 1: process private cached iterations in dimension L_1 with all FUs
      #if K2_P_NUM_STEPS_L_1 > 0
      for (size_t p_step_l_1 = 0; p_step_l_1 < K2_P_NUM_STEPS_L_1; ++p_step_l_1) {
        // phase 1: process private cached iterations in dimension R_1 with all FUs
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 0
        size_t p_step_r_1 = 0;
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 1
        for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1; ++p_step_r_1) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
        } // end of "p_step_r_1"-loop
        #endif
        #endif
        // phase 2: process extra private iterations in dimension R_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0
        {  
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1;  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 1
            for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
            #endif
            
          
          }  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
          // set step variable for access to res buffer
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0);  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        
        // ---------- reduction --------------------
        // will never be necessary on this level
        // because K2_P_NUM_FU_R == 1 for OpenCL
        // ---------- end of reduction -------------
        
        // move results upwards in memory hierarchy
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
        #endif
        {
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1)
            K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
        #endif
      
      } // end of "p_step_l_1"-loop
      #endif
      // phase 2: process extra private iterations in dimension L_1 with all FUs
      #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0
      {  
        const size_t p_step_l_1 = K2_P_NUM_STEPS_L_1;
        // phase 1: process private cached iterations in dimension R_1 with all FUs
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 0
        size_t p_step_r_1 = 0;
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 1
        for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1; ++p_step_r_1) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
        } // end of "p_step_r_1"-loop
        #endif
        #endif
        // phase 2: process extra private iterations in dimension R_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0
        {  
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1;  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 1
            for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
            #endif
            
          
          }  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
          // set step variable for access to res buffer
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0);  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        
        // ---------- reduction --------------------
        // will never be necessary on this level
        // because K2_P_NUM_FU_R == 1 for OpenCL
        // ---------- end of reduction -------------
        
        // move results upwards in memory hierarchy
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
        #endif
        {
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1)
            K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
        #endif
      
      }
      #endif
      // phase 3: process extra private elements in dimension L_1 with a subset of all FUs
      #if K2_P_NUM_EXTRA_ELEMENTS_L_1 > 0
      if (K2_L_FU_ID_L_1 < (K2_P_NUM_EXTRA_ELEMENTS_L_1 + K2_P_NUM_FU_L_1 - 1) / K2_P_NUM_FU_L_1) {  
        // set step variable for access to res buffer
        const size_t p_step_l_1 = K2_P_NUM_STEPS_L_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0);
        // phase 1: process private cached iterations in dimension R_1 with all FUs
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 0
        size_t p_step_r_1 = 0;
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        {
          size_t p_iteration_l_1 = 0;
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 1
        for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1; ++p_step_r_1) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
        } // end of "p_step_r_1"-loop
        #endif
        #endif
        // phase 2: process extra private iterations in dimension R_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0
        {  
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1;  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 1
            for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
            #endif
            
          
          }  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
          // set step variable for access to res buffer
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0);  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        
        // ---------- reduction --------------------
        // will never be necessary on this level
        // because K2_P_NUM_FU_R == 1 for OpenCL
        // ---------- end of reduction -------------
        
        // move results upwards in memory hierarchy
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
        #endif
        {
          
          {
            size_t p_iteration_l_1 = 0;
            K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
          }
        }
        #endif
      
      }
      #endif
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif  
      #else
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      #if ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_L_FLAT_WI_ID + step * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
        const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
        K2_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      #endif
      #if ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_L_FLAT_NUM_WI > 0
      if (K2_L_FLAT_WI_ID < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_L_FLAT_NUM_WI) {
        const size_t flat_index = K2_L_FLAT_WI_ID + (((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_L_FLAT_NUM_WI) * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
        const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
        K2_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      #endif
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      // phase 1: process private cached iterations in dimension L_1 with all FUs
      #if K2_P_NUM_STEPS_L_1 > 0
      for (size_t p_step_l_1 = 0; p_step_l_1 < K2_P_NUM_STEPS_L_1; ++p_step_l_1) {
        // phase 1: process private cached iterations in dimension R_1 with all FUs
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 0
        size_t p_step_r_1 = 0;
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }
        #else
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }
        #endif
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 1
        for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1; ++p_step_r_1) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
        } // end of "p_step_r_1"-loop
        #endif
        #endif
        // phase 2: process extra private iterations in dimension R_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0
        {  
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1;  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 1
            for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
            #endif
            
          
          }
          #else
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
          // set step variable for access to res buffer
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0);  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
          #else
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        
        // ---------- reduction --------------------
        // will never be necessary on this level
        // because K2_P_NUM_FU_R == 1 for OpenCL
        // ---------- end of reduction -------------
        
        // move results upwards in memory hierarchy
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
        #endif
        {
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1)
            K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
        #endif
      
      } // end of "p_step_l_1"-loop
      #endif
      // phase 2: process extra private iterations in dimension L_1 with all FUs
      #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0
      {  
        const size_t p_step_l_1 = K2_P_NUM_STEPS_L_1;
        // phase 1: process private cached iterations in dimension R_1 with all FUs
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 0
        size_t p_step_r_1 = 0;
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }
        #else
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }
        #endif
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 1
        for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1; ++p_step_r_1) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
        } // end of "p_step_r_1"-loop
        #endif
        #endif
        // phase 2: process extra private iterations in dimension R_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0
        {  
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1;  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 1
            for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
            #endif
            
          
          }
          #else
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
          // set step variable for access to res buffer
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0);  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
          #else
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        
        // ---------- reduction --------------------
        // will never be necessary on this level
        // because K2_P_NUM_FU_R == 1 for OpenCL
        // ---------- end of reduction -------------
        
        // move results upwards in memory hierarchy
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
        #endif
        {
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1)
            K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
        #endif
      
      }
      #endif
      // phase 3: process extra private elements in dimension L_1 with a subset of all FUs
      #if K2_P_NUM_EXTRA_ELEMENTS_L_1 > 0
      if (K2_L_FU_ID_L_1 < (K2_P_NUM_EXTRA_ELEMENTS_L_1 + K2_P_NUM_FU_L_1 - 1) / K2_P_NUM_FU_L_1) {  
        // set step variable for access to res buffer
        const size_t p_step_l_1 = K2_P_NUM_STEPS_L_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0);
        // phase 1: process private cached iterations in dimension R_1 with all FUs
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 0
        size_t p_step_r_1 = 0;
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
        {
          size_t p_iteration_l_1 = 0;
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }
        #else
        {
          size_t p_iteration_l_1 = 0;
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }
        #endif
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 1
        for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1; ++p_step_r_1) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
        } // end of "p_step_r_1"-loop
        #endif
        #endif
        // phase 2: process extra private iterations in dimension R_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0
        {  
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1;  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 1
            for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
            #endif
            
          
          }
          #else
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
          // set step variable for access to res buffer
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0);  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
          #else
          {
            size_t p_iteration_l_1 = 0;
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        
        // ---------- reduction --------------------
        // will never be necessary on this level
        // because K2_P_NUM_FU_R == 1 for OpenCL
        // ---------- end of reduction -------------
        
        // move results upwards in memory hierarchy
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
        #endif
        {
          
          {
            size_t p_iteration_l_1 = 0;
            K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
          }
        }
        #endif
      
      }
      #endif
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif  
      #endif
    }
    #endif
    // phase 3: process extra local elements in dimension R_1 with a subset of all FUs
    #if K2_L_NUM_EXTRA_ELEMENTS_R_1 > 0
    if (K2_G_FU_ID_R_1 < (K2_L_NUM_EXTRA_ELEMENTS_R_1 + K2_L_NUM_FU_R_1 - 1) / K2_L_NUM_FU_R_1) {  
      // set step variable for access to res buffer
      const size_t l_step_r_1 = K2_L_NUM_STEPS_R_1 + (K2_L_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0);  
      #if K2_L_NUM_STEPS_R_1 == 0 && K2_L_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      for (size_t step = 0; step < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1))) / K2_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_L_FLAT_WI_ID + step * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1)));
        const size_t l_index_r_1 = flat_index % ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1));
        K2_P3_R_1_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_P3_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      if (K2_L_FLAT_WI_ID < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1))) % K2_L_FLAT_NUM_WI) {
        const size_t flat_index = K2_L_FLAT_WI_ID + (((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1))) / K2_L_FLAT_NUM_WI) * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1)));
        const size_t l_index_r_1 = flat_index % ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1));
        K2_P3_R_1_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_P3_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      // phase 1: process private cached iterations in dimension L_1 with all FUs
      #if K2_P_NUM_STEPS_L_1 > 0
      for (size_t p_step_l_1 = 0; p_step_l_1 < K2_P_NUM_STEPS_L_1; ++p_step_l_1) {
        // parent level in phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        if (K2_L_FU_ID_R_1 < K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) {
          size_t p_step_r_1 = 0;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
        
          // ---------- reduction --------------------
          // will never be necessary on this level
          // because K2_P_NUM_FU_R == 1 for OpenCL
          // ---------- end of reduction -------------
        
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
          #if 1 == 0 && 0 == 0 && 0 > 0
          if (K2_L_FU_ID_R_1 < (0 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
          #endif
          {
            
            for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1)
              K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
          }
          #endif
        
        }
      
      } // end of "p_step_l_1"-loop
      #endif
      // phase 2: process extra private iterations in dimension L_1 with all FUs
      #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0
      {  
        const size_t p_step_l_1 = K2_P_NUM_STEPS_L_1;
        // parent level in phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        if (K2_L_FU_ID_R_1 < K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) {
          size_t p_step_r_1 = 0;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
        
          // ---------- reduction --------------------
          // will never be necessary on this level
          // because K2_P_NUM_FU_R == 1 for OpenCL
          // ---------- end of reduction -------------
        
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
          #if 1 == 0 && 0 == 0 && 0 > 0
          if (K2_L_FU_ID_R_1 < (0 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
          #endif
          {
            
            for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1)
              K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
          }
          #endif
        
        }
      
      }
      #endif
      // phase 3: process extra private elements in dimension L_1 with a subset of all FUs
      #if K2_P_NUM_EXTRA_ELEMENTS_L_1 > 0
      if (K2_L_FU_ID_L_1 < (K2_P_NUM_EXTRA_ELEMENTS_L_1 + K2_P_NUM_FU_L_1 - 1) / K2_P_NUM_FU_L_1) {  
        // set step variable for access to res buffer
        const size_t p_step_l_1 = K2_P_NUM_STEPS_L_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0);
        // parent level in phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        if (K2_L_FU_ID_R_1 < K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) {
          size_t p_step_r_1 = 0;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_L_1_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_L_1_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_L_1_PP3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
        
          // ---------- reduction --------------------
          // will never be necessary on this level
          // because K2_P_NUM_FU_R == 1 for OpenCL
          // ---------- end of reduction -------------
        
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
          #if 1 == 0 && 0 == 0 && 0 > 0
          if (K2_L_FU_ID_R_1 < (0 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
          #endif
          {
            
            {
              size_t p_iteration_l_1 = 0;
              K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
            }
          }
          #endif
        
        }
      
      }
      #endif
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif  
      #else
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      for (size_t step = 0; step < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1))) / K2_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_L_FLAT_WI_ID + step * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1)));
        const size_t l_index_r_1 = flat_index % ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1));
        K2_P3_R_1_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_P3_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      if (K2_L_FLAT_WI_ID < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1))) % K2_L_FLAT_NUM_WI) {
        const size_t flat_index = K2_L_FLAT_WI_ID + (((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1))) / K2_L_FLAT_NUM_WI) * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1)));
        const size_t l_index_r_1 = flat_index % ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1));
        K2_P3_R_1_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_P3_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      // phase 1: process private cached iterations in dimension L_1 with all FUs
      #if K2_P_NUM_STEPS_L_1 > 0
      for (size_t p_step_l_1 = 0; p_step_l_1 < K2_P_NUM_STEPS_L_1; ++p_step_l_1) {
        // parent level in phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        if (K2_L_FU_ID_R_1 < K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) {
          size_t p_step_r_1 = 0;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
          #else
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif
        
          // ---------- reduction --------------------
          // will never be necessary on this level
          // because K2_P_NUM_FU_R == 1 for OpenCL
          // ---------- end of reduction -------------
        
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
          #if 1 == 0 && 0 == 0 && 0 > 0
          if (K2_L_FU_ID_R_1 < (0 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
          #endif
          {
            
            for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1)
              K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
          }
          #endif
        
        }
      
      } // end of "p_step_l_1"-loop
      #endif
      // phase 2: process extra private iterations in dimension L_1 with all FUs
      #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0
      {  
        const size_t p_step_l_1 = K2_P_NUM_STEPS_L_1;
        // parent level in phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        if (K2_L_FU_ID_R_1 < K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) {
          size_t p_step_r_1 = 0;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
          #else
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif
        
          // ---------- reduction --------------------
          // will never be necessary on this level
          // because K2_P_NUM_FU_R == 1 for OpenCL
          // ---------- end of reduction -------------
        
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
          #if 1 == 0 && 0 == 0 && 0 > 0
          if (K2_L_FU_ID_R_1 < (0 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
          #endif
          {
            
            for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1)
              K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
          }
          #endif
        
        }
      
      }
      #endif
      // phase 3: process extra private elements in dimension L_1 with a subset of all FUs
      #if K2_P_NUM_EXTRA_ELEMENTS_L_1 > 0
      if (K2_L_FU_ID_L_1 < (K2_P_NUM_EXTRA_ELEMENTS_L_1 + K2_P_NUM_FU_L_1 - 1) / K2_P_NUM_FU_L_1) {  
        // set step variable for access to res buffer
        const size_t p_step_l_1 = K2_P_NUM_STEPS_L_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0);
        // parent level in phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        if (K2_L_FU_ID_R_1 < K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) {
          size_t p_step_r_1 = 0;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_L_1_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_L_1_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_L_1_PP3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
          #else
          {
            size_t p_iteration_l_1 = 0;
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_PP3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif
        
          // ---------- reduction --------------------
          // will never be necessary on this level
          // because K2_P_NUM_FU_R == 1 for OpenCL
          // ---------- end of reduction -------------
        
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
          #if 1 == 0 && 0 == 0 && 0 > 0
          if (K2_L_FU_ID_R_1 < (0 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
          #endif
          {
            
            {
              size_t p_iteration_l_1 = 0;
              K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
            }
          }
          #endif
        
        }
      
      }
      #endif
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif  
      #endif
    }
    #endif
    
    #if K2_L_NUM_FU_R_1 > 1
    #if K2_L_NUM_STEPS_R_1 == 0 && K2_L_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0 && K2_L_NUM_EXTRA_ELEMENTS_R_1 > 0
    if (K2_G_FU_ID_R_1 < (K2_L_NUM_EXTRA_ELEMENTS_R_1 + K2_L_NUM_FU_R_1 - 1) / K2_L_NUM_FU_R_1)
    #endif
    {
      // wait for all FUs to finish computation
      #if   L_CB_RES_DEST_LEVEL == LOCAL && CACHE_L_CB == 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #if L_CB_RES_DEST_LEVEL < LOCAL
      // phase 1: process private cached iterations in dimension L_1 with all FUs
      #if K2_P_NUM_STEPS_L_1 > 0
      for (size_t p_step_l_1 = 0; p_step_l_1 < K2_P_NUM_STEPS_L_1; ++p_step_l_1) {
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
          K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1) = K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
      } // end of "p_step_l_1"-loop
      #endif
      // phase 2: process extra private iterations in dimension L_1 with all FUs
      #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0
      {  
        const size_t p_step_l_1 = K2_P_NUM_STEPS_L_1;
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
          K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1) = K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
      }
      #endif
      // phase 3: process extra private elements in dimension L_1 with a subset of all FUs
      #if K2_P_NUM_EXTRA_ELEMENTS_L_1 > 0
      if (K2_L_FU_ID_L_1 < (K2_P_NUM_EXTRA_ELEMENTS_L_1 + K2_P_NUM_FU_L_1 - 1) / K2_P_NUM_FU_L_1) {  
        // set step variable for access to res buffer
        const size_t p_step_l_1 = K2_P_NUM_STEPS_L_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0);
        {
          size_t p_iteration_l_1 = 0;
          K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1) = K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
      }
      #endif
      // wait for all values to be copied into reduction memory
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      #if (K2_L_NUM_FU_R_1 & (K2_L_NUM_FU_R_1 - 1)) != 0
      // pre processing: reduce number of values to largest possible power of 2
      size_t stride = pow(2.0f, floor(log2((float) K2_L_NUM_FU_R_1)));
      if (K2_L_FU_ID_R_1 < stride && K2_L_FU_ID_R_1 + stride < K2_L_NUM_FU_R_1) {
        // phase 1: process private cached iterations in dimension L_1 with all FUs
        #if K2_P_NUM_STEPS_L_1 > 0
        for (size_t p_step_l_1 = 0; p_step_l_1 < K2_P_NUM_STEPS_L_1; ++p_step_l_1) {
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            #if L_CB_RES_DEST_LEVEL < LOCAL
            // reduction in separate memory location
            K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1) += K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride);
            #elif L_CB_RES_DEST_LEVEL == LOCAL
            K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
            #elif L_CB_RES_DEST_LEVEL == GLOBAL
            K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
            #endif
          
          }
        } // end of "p_step_l_1"-loop
        #endif
        // phase 2: process extra private iterations in dimension L_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0
        {  
          const size_t p_step_l_1 = K2_P_NUM_STEPS_L_1;
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
            #if L_CB_RES_DEST_LEVEL < LOCAL
            // reduction in separate memory location
            K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1) += K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride);
            #elif L_CB_RES_DEST_LEVEL == LOCAL
            K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
            #elif L_CB_RES_DEST_LEVEL == GLOBAL
            K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
            #endif
          
          }
        }
        #endif
        // phase 3: process extra private elements in dimension L_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_L_1 > 0
        if (K2_L_FU_ID_L_1 < (K2_P_NUM_EXTRA_ELEMENTS_L_1 + K2_P_NUM_FU_L_1 - 1) / K2_P_NUM_FU_L_1) {  
          // set step variable for access to res buffer
          const size_t p_step_l_1 = K2_P_NUM_STEPS_L_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0);
          {
            size_t p_iteration_l_1 = 0;
            #if L_CB_RES_DEST_LEVEL < LOCAL
            // reduction in separate memory location
            K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1) += K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride);
            #elif L_CB_RES_DEST_LEVEL == LOCAL
            K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
            #elif L_CB_RES_DEST_LEVEL == GLOBAL
            K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
            #endif
          
          }
        }
        #endif
      }
      stride /= 2;
      #if L_CB_RES_DEST_LEVEL <= LOCAL
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #else
      size_t stride = K2_L_NUM_FU_R_1 / 2;
      #endif
      // perform reduction
      for (; stride > 0; stride /= 2) {
        if (K2_L_FU_ID_R_1 < stride) {
          // phase 1: process private cached iterations in dimension L_1 with all FUs
          #if K2_P_NUM_STEPS_L_1 > 0
          for (size_t p_step_l_1 = 0; p_step_l_1 < K2_P_NUM_STEPS_L_1; ++p_step_l_1) {
            for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
              #if L_CB_RES_DEST_LEVEL < LOCAL
              // reduction in separate memory location
              K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1) += K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride);
              #elif L_CB_RES_DEST_LEVEL == LOCAL
              K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
              #elif L_CB_RES_DEST_LEVEL == GLOBAL
              K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
              #endif
            
            }
          } // end of "p_step_l_1"-loop
          #endif
          // phase 2: process extra private iterations in dimension L_1 with all FUs
          #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0
          {  
            const size_t p_step_l_1 = K2_P_NUM_STEPS_L_1;
            for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1) {
              #if L_CB_RES_DEST_LEVEL < LOCAL
              // reduction in separate memory location
              K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1) += K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride);
              #elif L_CB_RES_DEST_LEVEL == LOCAL
              K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
              #elif L_CB_RES_DEST_LEVEL == GLOBAL
              K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
              #endif
            
            }
          }
          #endif
          // phase 3: process extra private elements in dimension L_1 with a subset of all FUs
          #if K2_P_NUM_EXTRA_ELEMENTS_L_1 > 0
          if (K2_L_FU_ID_L_1 < (K2_P_NUM_EXTRA_ELEMENTS_L_1 + K2_P_NUM_FU_L_1 - 1) / K2_P_NUM_FU_L_1) {  
            // set step variable for access to res buffer
            const size_t p_step_l_1 = K2_P_NUM_STEPS_L_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0);
            {
              size_t p_iteration_l_1 = 0;
              #if L_CB_RES_DEST_LEVEL < LOCAL
              // reduction in separate memory location
              K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1) += K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride);
              #elif L_CB_RES_DEST_LEVEL == LOCAL
              K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
              #elif L_CB_RES_DEST_LEVEL == GLOBAL
              K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
              #endif
            
            }
          }
          #endif
        }
        #if L_CB_RES_DEST_LEVEL <= LOCAL
        barrier(CLK_LOCAL_MEM_FENCE);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
        barrier(CLK_GLOBAL_MEM_FENCE);
        #endif
      }
      #if L_CB_RES_DEST_LEVEL <= LOCAL
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
    }
    #endif
    
    // move results upwards in memory hierarchy
    #if G_CB_RES_DEST_LEVEL > L_CB_RES_DEST_LEVEL || K2_L_NUM_FU_R_1 > 1
    #if K2_L_NUM_STEPS_R_1 == 0 && K2_L_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0 && K2_L_NUM_EXTRA_ELEMENTS_R_1 > 0
    if (K2_G_FU_ID_R_1 < (K2_L_NUM_EXTRA_ELEMENTS_R_1 + K2_L_NUM_FU_R_1 - 1) / K2_L_NUM_FU_R_1)
    #endif
    {
      
      #if L_CB_RES_DEST_LEVEL < LOCAL && K2_L_NUM_FU_R_1 == 1
      if (K2_L_FU_ID_R_1 == 0) {
        // phase 1: process private cached iterations in dimension L_1 with all FUs
        #if K2_P_NUM_STEPS_L_1 > 0
        for (size_t p_step_l_1 = 0; p_step_l_1 < K2_P_NUM_STEPS_L_1; ++p_step_l_1) {
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1)
            K2_P_KERNEL_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        } // end of "p_step_l_1"-loop
        #endif
        // phase 2: process extra private iterations in dimension L_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0
        {  
          const size_t p_step_l_1 = K2_P_NUM_STEPS_L_1;
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_L_1; ++p_iteration_l_1)
            K2_P_KERNEL_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
        #endif
        // phase 3: process extra private elements in dimension L_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_L_1 > 0
        if (K2_L_FU_ID_L_1 < (K2_P_NUM_EXTRA_ELEMENTS_L_1 + K2_P_NUM_FU_L_1 - 1) / K2_P_NUM_FU_L_1) {  
          // set step variable for access to res buffer
          const size_t p_step_l_1 = K2_P_NUM_STEPS_L_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0);
          {
            size_t p_iteration_l_1 = 0;
            K2_P3_L_1_P_KERNEL_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
          }
        }
        #endif
      }
      #else
      #if K2_L_NUM_FU_R_1 == 1
        #if L_CB_RES_DEST_LEVEL <= LOCAL && CACHE_L_CB == 0
        barrier(CLK_LOCAL_MEM_FENCE);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
        barrier(CLK_GLOBAL_MEM_FENCE);
        #endif
      #endif
      #if (K2_L_NUM_PROCESSED_ELEMENTS_L_1) / K2_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K2_L_NUM_PROCESSED_ELEMENTS_L_1) / K2_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_L_FLAT_WI_ID + step * K2_L_FLAT_NUM_WI;
        #if K2_G_NUM_FU_R_1 == 1
        const size_t l_index_l_1 = flat_index;
        #else
        const size_t DESCENDING_L_DIMS_0(l_index_l_1) = flat_index;
        #endif
        K2_L_KERNEL_RES(l_index_l_1) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K2_L_REDUCTION_MEM(l_index_l_1, 0);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K2_RES_L_BUFFER(l_index_l_1, 0, 0);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(l_index_l_1), i_wg_r_1, 0, 0);
        #endif
      }
      #endif
      #if (K2_L_NUM_PROCESSED_ELEMENTS_L_1) % K2_L_FLAT_NUM_WI > 0
      if (K2_L_FLAT_WI_ID < (K2_L_NUM_PROCESSED_ELEMENTS_L_1) % K2_L_FLAT_NUM_WI) {
        const size_t flat_index = K2_L_FLAT_WI_ID + ((K2_L_NUM_PROCESSED_ELEMENTS_L_1) / K2_L_FLAT_NUM_WI) * K2_L_FLAT_NUM_WI;
        #if K2_G_NUM_FU_R_1 == 1
        const size_t l_index_l_1 = flat_index;
        #else
        const size_t DESCENDING_L_DIMS_0(l_index_l_1) = flat_index;
        #endif
        K2_L_KERNEL_RES(l_index_l_1) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K2_L_REDUCTION_MEM(l_index_l_1, 0);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K2_RES_L_BUFFER(l_index_l_1, 0, 0);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(l_index_l_1), i_wg_r_1, 0, 0);
        #endif
      }
      #endif
      #if L_CB_RES_DEST_LEVEL <= LOCAL
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #endif
    }
    #endif
  
  } // end of "l_step_l_1"-loop
  #endif
  // phase 2: process extra local iterations in dimension L_1 with all FUs
  #if K2_L_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0
  {  
    const size_t l_step_l_1 = K2_L_NUM_STEPS_L_1;
    // phase 1: process local cached iterations in dimension R_1 with all FUs
    #if K2_L_NUM_STEPS_R_1 > 0
    size_t l_step_r_1 = 0;
    // ---------- L caching --------------------
    #if CACHE_L_CB != 0
    #if ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_L_FLAT_NUM_WI > 0
    for (size_t step = 0; step < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_L_FLAT_NUM_WI; ++step) {
      const size_t flat_index = K2_L_FLAT_WI_ID + step * K2_L_FLAT_NUM_WI;
      const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_R_1);
      const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_R_1;
      K2_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
    }
    #endif
    #if ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) % K2_L_FLAT_NUM_WI > 0
    if (K2_L_FLAT_WI_ID < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) % K2_L_FLAT_NUM_WI) {
      const size_t flat_index = K2_L_FLAT_WI_ID + (((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_L_FLAT_NUM_WI) * K2_L_FLAT_NUM_WI;
      const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_R_1);
      const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_R_1;
      K2_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
    }
    #endif
    barrier(CLK_LOCAL_MEM_FENCE);
    #endif
    // ---------- end of L caching -------------
    
    
    // phase 1: process private cached iterations in dimension L_1 with all FUs
    #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 > 0
    for (size_t p_step_l_1 = 0; p_step_l_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1; ++p_step_l_1) {
      // phase 1: process private cached iterations in dimension R_1 with all FUs
      #if K2_P_NUM_STEPS_R_1 > 0
      size_t p_step_r_1 = 0;
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
        #if CACHE_L_CB != 0
        const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
        const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        #else
        const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
        #endif
        K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
      }
      #endif
      #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI > 0
      if (K2_P_FLAT_WI_ID < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI) {
        const size_t flat_index = K2_P_FLAT_WI_ID + ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
        #if CACHE_L_CB != 0
        const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
        const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        #else
        const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
        #endif
        K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
        size_t p_iteration_r_1 = 0;
        // process one mda element
        K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
        
        #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
        for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
        
        }
        #endif
        
      
      }
      #if K2_P_NUM_STEPS_R_1 > 1
      for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_R_1; ++p_step_r_1) {
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }
      } // end of "p_step_r_1"-loop
      #endif
      #endif
      // phase 2: process extra private iterations in dimension R_1 with all FUs
      #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0
      {  
        const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1;  
        #if K2_P_NUM_STEPS_R_1 == 0
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }  
        #else
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }  
        #endif
      }
      #endif
      // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
      #if K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
      if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
        // set step variable for access to res buffer
        const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0);  
        #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
        
        }  
        #else
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
          {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }  
        #endif
      }
      #endif
      
      // ---------- reduction --------------------
      // will never be necessary on this level
      // because K2_P_NUM_FU_R == 1 for OpenCL
      // ---------- end of reduction -------------
      
      // move results upwards in memory hierarchy
      #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
      #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
      if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
      #endif
      {
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1)
          K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
      }
      #endif
    
    } // end of "p_step_l_1"-loop
    #endif
    // phase 2: process extra private iterations in dimension L_1 with all FUs
    #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 > 0
    {  
      const size_t p_step_l_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1;
      // phase 1: process private cached iterations in dimension R_1 with all FUs
      #if K2_P_NUM_STEPS_R_1 > 0
      size_t p_step_r_1 = 0;
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
        #if CACHE_L_CB != 0
        const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
        const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        #else
        const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
        #endif
        K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
      }
      #endif
      #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) % K2_P_FLAT_NUM_WI > 0
      if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) % K2_P_FLAT_NUM_WI) {
        const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
        #if CACHE_L_CB != 0
        const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
        const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        #else
        const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
        #endif
        K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
        size_t p_iteration_r_1 = 0;
        // process one mda element
        K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
        
        #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
        for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
        
        }
        #endif
        
      
      }
      #if K2_P_NUM_STEPS_R_1 > 1
      for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_R_1; ++p_step_r_1) {
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }
      } // end of "p_step_r_1"-loop
      #endif
      #endif
      // phase 2: process extra private iterations in dimension R_1 with all FUs
      #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0
      {  
        const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1;  
        #if K2_P_NUM_STEPS_R_1 == 0
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }  
        #else
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }  
        #endif
      }
      #endif
      // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
      #if K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
      if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
        // set step variable for access to res buffer
        const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0);  
        #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
        
        }  
        #else
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
          {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }  
        #endif
      }
      #endif
      
      // ---------- reduction --------------------
      // will never be necessary on this level
      // because K2_P_NUM_FU_R == 1 for OpenCL
      // ---------- end of reduction -------------
      
      // move results upwards in memory hierarchy
      #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
      #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
      if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
      #endif
      {
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1)
          K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
      }
      #endif
    
    }
    #endif
    // phase 3: process extra private elements in dimension L_1 with a subset of all FUs
    #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 > 0
    if (K2_L_FU_ID_L_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 + K2_P_NUM_FU_L_1 - 1) / K2_P_NUM_FU_L_1) {  
      // set step variable for access to res buffer
      const size_t p_step_l_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 > 0);
      // phase 1: process private cached iterations in dimension R_1 with all FUs
      #if K2_P_NUM_STEPS_R_1 > 0
      size_t p_step_r_1 = 0;
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
        #if CACHE_L_CB != 0
        const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
        const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        #else
        const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
        #endif
        K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
      }
      if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
        const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
        #if CACHE_L_CB != 0
        const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
        const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        #else
        const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
        #endif
        K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
      }
      #endif
      // ---------- end of P caching -------------
      
      {
        size_t p_iteration_l_1 = 0;
        size_t p_iteration_r_1 = 0;
        // process one mda element
        K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
        
        #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
        for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
          // process one mda element
          K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
        
        }
        #endif
        
      
      }
      #if K2_P_NUM_STEPS_R_1 > 1
      for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_R_1; ++p_step_r_1) {
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        {
          size_t p_iteration_l_1 = 0;
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }
      } // end of "p_step_r_1"-loop
      #endif
      #endif
      // phase 2: process extra private iterations in dimension R_1 with all FUs
      #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0
      {  
        const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1;  
        #if K2_P_NUM_STEPS_R_1 == 0
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        {
          size_t p_iteration_l_1 = 0;
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }  
        #else
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        {
          size_t p_iteration_l_1 = 0;
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }  
        #endif
      }
      #endif
      // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
      #if K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
      if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
        // set step variable for access to res buffer
        const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0);  
        #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_IN_COMPLETE_G_CB_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_IN_COMPLETE_G_CB_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        {
          size_t p_iteration_l_1 = 0;
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_L_1_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
        
        }  
        #else
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_IN_COMPLETE_G_CB_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_P3_IN_COMPLETE_G_CB_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        {
          size_t p_iteration_l_1 = 0;
          {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }  
        #endif
      }
      #endif
      
      // ---------- reduction --------------------
      // will never be necessary on this level
      // because K2_P_NUM_FU_R == 1 for OpenCL
      // ---------- end of reduction -------------
      
      // move results upwards in memory hierarchy
      #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
      #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
      if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
      #endif
      {
        
        {
          size_t p_iteration_l_1 = 0;
          K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
      }
      #endif
    
    }
    #endif
    
    
    // wait for all WIs to finish computation on local cache block
    #if CACHE_L_CB != 0
    barrier(CLK_LOCAL_MEM_FENCE);
    #endif
    #if K2_L_NUM_STEPS_R_1 > 1
    for (l_step_r_1 = 1; l_step_r_1 < K2_L_NUM_STEPS_R_1; ++l_step_r_1) {
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      #if ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_L_FLAT_WI_ID + step * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_R_1);
        const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_R_1;
        K2_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      #endif
      #if ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) % K2_L_FLAT_NUM_WI > 0
      if (K2_L_FLAT_WI_ID < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) % K2_L_FLAT_NUM_WI) {
        const size_t flat_index = K2_L_FLAT_WI_ID + (((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_L_FLAT_NUM_WI) * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_R_1);
        const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_R_1;
        K2_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      #endif
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      
      // phase 1: process private cached iterations in dimension L_1 with all FUs
      #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 > 0
      for (size_t p_step_l_1 = 0; p_step_l_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1; ++p_step_l_1) {
        // phase 1: process private cached iterations in dimension R_1 with all FUs
        #if K2_P_NUM_STEPS_R_1 > 0
        size_t p_step_r_1 = 0;
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }
        #else
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }
        #endif
        #if K2_P_NUM_STEPS_R_1 > 1
        for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_R_1; ++p_step_r_1) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
        } // end of "p_step_r_1"-loop
        #endif
        #endif
        // phase 2: process extra private iterations in dimension R_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0
        {  
          const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1;  
          #if K2_P_NUM_STEPS_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1 > 1
            for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
            #endif
            
          
          }
          #else
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
          // set step variable for access to res buffer
          const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0);  
          #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
          #else
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        
        // ---------- reduction --------------------
        // will never be necessary on this level
        // because K2_P_NUM_FU_R == 1 for OpenCL
        // ---------- end of reduction -------------
        
        // move results upwards in memory hierarchy
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
        #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
        #endif
        {
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1)
            K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
        #endif
      
      } // end of "p_step_l_1"-loop
      #endif
      // phase 2: process extra private iterations in dimension L_1 with all FUs
      #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 > 0
      {  
        const size_t p_step_l_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1;
        // phase 1: process private cached iterations in dimension R_1 with all FUs
        #if K2_P_NUM_STEPS_R_1 > 0
        size_t p_step_r_1 = 0;
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }
        #else
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }
        #endif
        #if K2_P_NUM_STEPS_R_1 > 1
        for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_R_1; ++p_step_r_1) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
        } // end of "p_step_r_1"-loop
        #endif
        #endif
        // phase 2: process extra private iterations in dimension R_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0
        {  
          const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1;  
          #if K2_P_NUM_STEPS_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1 > 1
            for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
            #endif
            
          
          }
          #else
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
          // set step variable for access to res buffer
          const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0);  
          #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
          #else
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        
        // ---------- reduction --------------------
        // will never be necessary on this level
        // because K2_P_NUM_FU_R == 1 for OpenCL
        // ---------- end of reduction -------------
        
        // move results upwards in memory hierarchy
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
        #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
        #endif
        {
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1)
            K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
        #endif
      
      }
      #endif
      // phase 3: process extra private elements in dimension L_1 with a subset of all FUs
      #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 > 0
      if (K2_L_FU_ID_L_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 + K2_P_NUM_FU_L_1 - 1) / K2_P_NUM_FU_L_1) {  
        // set step variable for access to res buffer
        const size_t p_step_l_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 > 0);
        // phase 1: process private cached iterations in dimension R_1 with all FUs
        #if K2_P_NUM_STEPS_R_1 > 0
        size_t p_step_r_1 = 0;
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
        {
          size_t p_iteration_l_1 = 0;
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }
        #else
        {
          size_t p_iteration_l_1 = 0;
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }
        #endif
        #if K2_P_NUM_STEPS_R_1 > 1
        for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_R_1; ++p_step_r_1) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
        } // end of "p_step_r_1"-loop
        #endif
        #endif
        // phase 2: process extra private iterations in dimension R_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0
        {  
          const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1;  
          #if K2_P_NUM_STEPS_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1 > 1
            for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
            #endif
            
          
          }
          #else
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
          // set step variable for access to res buffer
          const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0);  
          #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_L_1_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
          #else
          {
            size_t p_iteration_l_1 = 0;
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        
        // ---------- reduction --------------------
        // will never be necessary on this level
        // because K2_P_NUM_FU_R == 1 for OpenCL
        // ---------- end of reduction -------------
        
        // move results upwards in memory hierarchy
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
        #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
        #endif
        {
          
          {
            size_t p_iteration_l_1 = 0;
            K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
          }
        }
        #endif
      
      }
      #endif
      
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
    } // end of "l_step_r_1"-loop
    #endif
    #endif
    // phase 2: process extra local iterations in dimension R_1 with all FUs
    #if K2_L_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0
    {  
      const size_t l_step_r_1 = K2_L_NUM_STEPS_R_1;  
      #if K2_L_NUM_STEPS_R_1 == 0
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      #if ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_L_FLAT_WI_ID + step * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
        const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
        K2_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      #endif
      #if ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_L_FLAT_NUM_WI > 0
      if (K2_L_FLAT_WI_ID < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_L_FLAT_NUM_WI) {
        const size_t flat_index = K2_L_FLAT_WI_ID + (((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_L_FLAT_NUM_WI) * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
        const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
        K2_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      #endif
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      // phase 1: process private cached iterations in dimension L_1 with all FUs
      #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 > 0
      for (size_t p_step_l_1 = 0; p_step_l_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1; ++p_step_l_1) {
        // phase 1: process private cached iterations in dimension R_1 with all FUs
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 0
        size_t p_step_r_1 = 0;
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 1
        for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1; ++p_step_r_1) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
        } // end of "p_step_r_1"-loop
        #endif
        #endif
        // phase 2: process extra private iterations in dimension R_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0
        {  
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1;  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 1
            for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
            #endif
            
          
          }  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
          // set step variable for access to res buffer
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0);  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        
        // ---------- reduction --------------------
        // will never be necessary on this level
        // because K2_P_NUM_FU_R == 1 for OpenCL
        // ---------- end of reduction -------------
        
        // move results upwards in memory hierarchy
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
        #endif
        {
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1)
            K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
        #endif
      
      } // end of "p_step_l_1"-loop
      #endif
      // phase 2: process extra private iterations in dimension L_1 with all FUs
      #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 > 0
      {  
        const size_t p_step_l_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1;
        // phase 1: process private cached iterations in dimension R_1 with all FUs
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 0
        size_t p_step_r_1 = 0;
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 1
        for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1; ++p_step_r_1) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
        } // end of "p_step_r_1"-loop
        #endif
        #endif
        // phase 2: process extra private iterations in dimension R_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0
        {  
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1;  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 1
            for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
            #endif
            
          
          }  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
          // set step variable for access to res buffer
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0);  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        
        // ---------- reduction --------------------
        // will never be necessary on this level
        // because K2_P_NUM_FU_R == 1 for OpenCL
        // ---------- end of reduction -------------
        
        // move results upwards in memory hierarchy
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
        #endif
        {
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1)
            K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
        #endif
      
      }
      #endif
      // phase 3: process extra private elements in dimension L_1 with a subset of all FUs
      #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 > 0
      if (K2_L_FU_ID_L_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 + K2_P_NUM_FU_L_1 - 1) / K2_P_NUM_FU_L_1) {  
        // set step variable for access to res buffer
        const size_t p_step_l_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 > 0);
        // phase 1: process private cached iterations in dimension R_1 with all FUs
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 0
        size_t p_step_r_1 = 0;
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        {
          size_t p_iteration_l_1 = 0;
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 1
        for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1; ++p_step_r_1) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
        } // end of "p_step_r_1"-loop
        #endif
        #endif
        // phase 2: process extra private iterations in dimension R_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0
        {  
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1;  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 1
            for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
            #endif
            
          
          }  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
          // set step variable for access to res buffer
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0);  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        
        // ---------- reduction --------------------
        // will never be necessary on this level
        // because K2_P_NUM_FU_R == 1 for OpenCL
        // ---------- end of reduction -------------
        
        // move results upwards in memory hierarchy
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
        #endif
        {
          
          {
            size_t p_iteration_l_1 = 0;
            K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
          }
        }
        #endif
      
      }
      #endif
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif  
      #else
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      #if ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_L_FLAT_WI_ID + step * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
        const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
        K2_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      #endif
      #if ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_L_FLAT_NUM_WI > 0
      if (K2_L_FLAT_WI_ID < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_L_FLAT_NUM_WI) {
        const size_t flat_index = K2_L_FLAT_WI_ID + (((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_L_FLAT_NUM_WI) * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
        const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
        K2_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      #endif
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      // phase 1: process private cached iterations in dimension L_1 with all FUs
      #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 > 0
      for (size_t p_step_l_1 = 0; p_step_l_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1; ++p_step_l_1) {
        // phase 1: process private cached iterations in dimension R_1 with all FUs
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 0
        size_t p_step_r_1 = 0;
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }
        #else
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }
        #endif
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 1
        for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1; ++p_step_r_1) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < (K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
        } // end of "p_step_r_1"-loop
        #endif
        #endif
        // phase 2: process extra private iterations in dimension R_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0
        {  
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1;  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 1
            for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
            #endif
            
          
          }
          #else
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
          // set step variable for access to res buffer
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0);  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
          #else
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        
        // ---------- reduction --------------------
        // will never be necessary on this level
        // because K2_P_NUM_FU_R == 1 for OpenCL
        // ---------- end of reduction -------------
        
        // move results upwards in memory hierarchy
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
        #endif
        {
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1)
            K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
        #endif
      
      } // end of "p_step_l_1"-loop
      #endif
      // phase 2: process extra private iterations in dimension L_1 with all FUs
      #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 > 0
      {  
        const size_t p_step_l_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1;
        // phase 1: process private cached iterations in dimension R_1 with all FUs
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 0
        size_t p_step_r_1 = 0;
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }
        #else
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }
        #endif
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 1
        for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1; ++p_step_r_1) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
        } // end of "p_step_r_1"-loop
        #endif
        #endif
        // phase 2: process extra private iterations in dimension R_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0
        {  
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1;  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 1
            for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
            #endif
            
          
          }
          #else
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
          // set step variable for access to res buffer
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0);  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
          #else
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        
        // ---------- reduction --------------------
        // will never be necessary on this level
        // because K2_P_NUM_FU_R == 1 for OpenCL
        // ---------- end of reduction -------------
        
        // move results upwards in memory hierarchy
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
        #endif
        {
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1)
            K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
        #endif
      
      }
      #endif
      // phase 3: process extra private elements in dimension L_1 with a subset of all FUs
      #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 > 0
      if (K2_L_FU_ID_L_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 + K2_P_NUM_FU_L_1 - 1) / K2_P_NUM_FU_L_1) {  
        // set step variable for access to res buffer
        const size_t p_step_l_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 > 0);
        // phase 1: process private cached iterations in dimension R_1 with all FUs
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 0
        size_t p_step_r_1 = 0;
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
        {
          size_t p_iteration_l_1 = 0;
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }
        #else
        {
          size_t p_iteration_l_1 = 0;
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }
        #endif
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 1
        for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1; ++p_step_r_1) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
        } // end of "p_step_r_1"-loop
        #endif
        #endif
        // phase 2: process extra private iterations in dimension R_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0
        {  
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1;  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 1
            for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
            #endif
            
          
          }
          #else
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
          // set step variable for access to res buffer
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0);  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
          #else
          {
            size_t p_iteration_l_1 = 0;
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        
        // ---------- reduction --------------------
        // will never be necessary on this level
        // because K2_P_NUM_FU_R == 1 for OpenCL
        // ---------- end of reduction -------------
        
        // move results upwards in memory hierarchy
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
        #endif
        {
          
          {
            size_t p_iteration_l_1 = 0;
            K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
          }
        }
        #endif
      
      }
      #endif
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif  
      #endif
    }
    #endif
    // phase 3: process extra local elements in dimension R_1 with a subset of all FUs
    #if K2_L_NUM_EXTRA_ELEMENTS_R_1 > 0
    if (K2_G_FU_ID_R_1 < (K2_L_NUM_EXTRA_ELEMENTS_R_1 + K2_L_NUM_FU_R_1 - 1) / K2_L_NUM_FU_R_1) {  
      // set step variable for access to res buffer
      const size_t l_step_r_1 = K2_L_NUM_STEPS_R_1 + (K2_L_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0);  
      #if K2_L_NUM_STEPS_R_1 == 0 && K2_L_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      for (size_t step = 0; step < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1))) / K2_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_L_FLAT_WI_ID + step * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1)));
        const size_t l_index_r_1 = flat_index % ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1));
        K2_P3_R_1_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_P3_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      if (K2_L_FLAT_WI_ID < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1))) % K2_L_FLAT_NUM_WI) {
        const size_t flat_index = K2_L_FLAT_WI_ID + (((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1))) / K2_L_FLAT_NUM_WI) * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1)));
        const size_t l_index_r_1 = flat_index % ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1));
        K2_P3_R_1_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_P3_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      // phase 1: process private cached iterations in dimension L_1 with all FUs
      #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 > 0
      for (size_t p_step_l_1 = 0; p_step_l_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1; ++p_step_l_1) {
        // parent level in phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        if (K2_L_FU_ID_R_1 < K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) {
          size_t p_step_r_1 = 0;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
        
          // ---------- reduction --------------------
          // will never be necessary on this level
          // because K2_P_NUM_FU_R == 1 for OpenCL
          // ---------- end of reduction -------------
        
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
          #if 1 == 0 && 0 == 0 && 0 > 0
          if (K2_L_FU_ID_R_1 < (0 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
          #endif
          {
            
            for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1)
              K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
          }
          #endif
        
        }
      
      } // end of "p_step_l_1"-loop
      #endif
      // phase 2: process extra private iterations in dimension L_1 with all FUs
      #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 > 0
      {  
        const size_t p_step_l_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1;
        // parent level in phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        if (K2_L_FU_ID_R_1 < K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) {
          size_t p_step_r_1 = 0;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
        
          // ---------- reduction --------------------
          // will never be necessary on this level
          // because K2_P_NUM_FU_R == 1 for OpenCL
          // ---------- end of reduction -------------
        
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
          #if 1 == 0 && 0 == 0 && 0 > 0
          if (K2_L_FU_ID_R_1 < (0 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
          #endif
          {
            
            for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1)
              K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
          }
          #endif
        
        }
      
      }
      #endif
      // phase 3: process extra private elements in dimension L_1 with a subset of all FUs
      #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 > 0
      if (K2_L_FU_ID_L_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 + K2_P_NUM_FU_L_1 - 1) / K2_P_NUM_FU_L_1) {  
        // set step variable for access to res buffer
        const size_t p_step_l_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 > 0);
        // parent level in phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        if (K2_L_FU_ID_R_1 < K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) {
          size_t p_step_r_1 = 0;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_L_1_PP3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
        
          // ---------- reduction --------------------
          // will never be necessary on this level
          // because K2_P_NUM_FU_R == 1 for OpenCL
          // ---------- end of reduction -------------
        
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
          #if 1 == 0 && 0 == 0 && 0 > 0
          if (K2_L_FU_ID_R_1 < (0 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
          #endif
          {
            
            {
              size_t p_iteration_l_1 = 0;
              K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
            }
          }
          #endif
        
        }
      
      }
      #endif
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif  
      #else
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      for (size_t step = 0; step < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1))) / K2_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_L_FLAT_WI_ID + step * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1)));
        const size_t l_index_r_1 = flat_index % ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1));
        K2_P3_R_1_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_P3_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      if (K2_L_FLAT_WI_ID < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1))) % K2_L_FLAT_NUM_WI) {
        const size_t flat_index = K2_L_FLAT_WI_ID + (((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1))) / K2_L_FLAT_NUM_WI) * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1)));
        const size_t l_index_r_1 = flat_index % ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1));
        K2_P3_R_1_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_L_TO_G_INDEX_L_1(l_index_l_1), K2_P3_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      // phase 1: process private cached iterations in dimension L_1 with all FUs
      #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 > 0
      for (size_t p_step_l_1 = 0; p_step_l_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1; ++p_step_l_1) {
        // parent level in phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        if (K2_L_FU_ID_R_1 < K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) {
          size_t p_step_r_1 = 0;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
          #else
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif
        
          // ---------- reduction --------------------
          // will never be necessary on this level
          // because K2_P_NUM_FU_R == 1 for OpenCL
          // ---------- end of reduction -------------
        
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
          #if 1 == 0 && 0 == 0 && 0 > 0
          if (K2_L_FU_ID_R_1 < (0 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
          #endif
          {
            
            for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1)
              K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
          }
          #endif
        
        }
      
      } // end of "p_step_l_1"-loop
      #endif
      // phase 2: process extra private iterations in dimension L_1 with all FUs
      #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 > 0
      {  
        const size_t p_step_l_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1;
        // parent level in phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        if (K2_L_FU_ID_R_1 < K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) {
          size_t p_step_r_1 = 0;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
          #else
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif
        
          // ---------- reduction --------------------
          // will never be necessary on this level
          // because K2_P_NUM_FU_R == 1 for OpenCL
          // ---------- end of reduction -------------
        
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
          #if 1 == 0 && 0 == 0 && 0 > 0
          if (K2_L_FU_ID_R_1 < (0 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
          #endif
          {
            
            for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1)
              K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
          }
          #endif
        
        }
      
      }
      #endif
      // phase 3: process extra private elements in dimension L_1 with a subset of all FUs
      #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 > 0
      if (K2_L_FU_ID_L_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 + K2_P_NUM_FU_L_1 - 1) / K2_P_NUM_FU_L_1) {  
        // set step variable for access to res buffer
        const size_t p_step_l_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 > 0);
        // parent level in phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        if (K2_L_FU_ID_R_1 < K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) {
          size_t p_step_r_1 = 0;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_P3_IN_COMPLETE_G_CB_L_1_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_R_1_L_MEM_INT_RES(K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_IN_COMPLETE_G_CB_L_1_PP3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
          #else
          {
            size_t p_iteration_l_1 = 0;
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_PP3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif
        
          // ---------- reduction --------------------
          // will never be necessary on this level
          // because K2_P_NUM_FU_R == 1 for OpenCL
          // ---------- end of reduction -------------
        
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
          #if 1 == 0 && 0 == 0 && 0 > 0
          if (K2_L_FU_ID_R_1 < (0 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
          #endif
          {
            
            {
              size_t p_iteration_l_1 = 0;
              K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_P3_IN_COMPLETE_G_CB_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
            }
          }
          #endif
        
        }
      
      }
      #endif
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif  
      #endif
    }
    #endif
    
    #if K2_L_NUM_FU_R_1 > 1
    #if K2_L_NUM_STEPS_R_1 == 0 && K2_L_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0 && K2_L_NUM_EXTRA_ELEMENTS_R_1 > 0
    if (K2_G_FU_ID_R_1 < (K2_L_NUM_EXTRA_ELEMENTS_R_1 + K2_L_NUM_FU_R_1 - 1) / K2_L_NUM_FU_R_1)
    #endif
    {
      // wait for all FUs to finish computation
      #if   L_CB_RES_DEST_LEVEL == LOCAL && CACHE_L_CB == 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #if L_CB_RES_DEST_LEVEL < LOCAL
      // phase 1: process private cached iterations in dimension L_1 with all FUs
      #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 > 0
      for (size_t p_step_l_1 = 0; p_step_l_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1; ++p_step_l_1) {
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
          K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1) = K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
      } // end of "p_step_l_1"-loop
      #endif
      // phase 2: process extra private iterations in dimension L_1 with all FUs
      #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 > 0
      {  
        const size_t p_step_l_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1;
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
          K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1) = K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
      }
      #endif
      // phase 3: process extra private elements in dimension L_1 with a subset of all FUs
      #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 > 0
      if (K2_L_FU_ID_L_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 + K2_P_NUM_FU_L_1 - 1) / K2_P_NUM_FU_L_1) {  
        // set step variable for access to res buffer
        const size_t p_step_l_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 > 0);
        {
          size_t p_iteration_l_1 = 0;
          K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1) = K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
      }
      #endif
      // wait for all values to be copied into reduction memory
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      #if (K2_L_NUM_FU_R_1 & (K2_L_NUM_FU_R_1 - 1)) != 0
      // pre processing: reduce number of values to largest possible power of 2
      size_t stride = pow(2.0f, floor(log2((float) K2_L_NUM_FU_R_1)));
      if (K2_L_FU_ID_R_1 < stride && K2_L_FU_ID_R_1 + stride < K2_L_NUM_FU_R_1) {
        // phase 1: process private cached iterations in dimension L_1 with all FUs
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 > 0
        for (size_t p_step_l_1 = 0; p_step_l_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1; ++p_step_l_1) {
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
            #if L_CB_RES_DEST_LEVEL < LOCAL
            // reduction in separate memory location
            K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1) += K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride);
            #elif L_CB_RES_DEST_LEVEL == LOCAL
            K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
            #elif L_CB_RES_DEST_LEVEL == GLOBAL
            K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
            #endif
          
          }
        } // end of "p_step_l_1"-loop
        #endif
        // phase 2: process extra private iterations in dimension L_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 > 0
        {  
          const size_t p_step_l_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1;
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
            #if L_CB_RES_DEST_LEVEL < LOCAL
            // reduction in separate memory location
            K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1) += K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride);
            #elif L_CB_RES_DEST_LEVEL == LOCAL
            K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
            #elif L_CB_RES_DEST_LEVEL == GLOBAL
            K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
            #endif
          
          }
        }
        #endif
        // phase 3: process extra private elements in dimension L_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 > 0
        if (K2_L_FU_ID_L_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 + K2_P_NUM_FU_L_1 - 1) / K2_P_NUM_FU_L_1) {  
          // set step variable for access to res buffer
          const size_t p_step_l_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 > 0);
          {
            size_t p_iteration_l_1 = 0;
            #if L_CB_RES_DEST_LEVEL < LOCAL
            // reduction in separate memory location
            K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1) += K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride);
            #elif L_CB_RES_DEST_LEVEL == LOCAL
            K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
            #elif L_CB_RES_DEST_LEVEL == GLOBAL
            K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
            #endif
          
          }
        }
        #endif
      }
      stride /= 2;
      #if L_CB_RES_DEST_LEVEL <= LOCAL
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #else
      size_t stride = K2_L_NUM_FU_R_1 / 2;
      #endif
      // perform reduction
      for (; stride > 0; stride /= 2) {
        if (K2_L_FU_ID_R_1 < stride) {
          // phase 1: process private cached iterations in dimension L_1 with all FUs
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 > 0
          for (size_t p_step_l_1 = 0; p_step_l_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1; ++p_step_l_1) {
            for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1) {
              #if L_CB_RES_DEST_LEVEL < LOCAL
              // reduction in separate memory location
              K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1) += K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride);
              #elif L_CB_RES_DEST_LEVEL == LOCAL
              K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
              #elif L_CB_RES_DEST_LEVEL == GLOBAL
              K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
              #endif
            
            }
          } // end of "p_step_l_1"-loop
          #endif
          // phase 2: process extra private iterations in dimension L_1 with all FUs
          #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 > 0
          {  
            const size_t p_step_l_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1;
            for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1) {
              #if L_CB_RES_DEST_LEVEL < LOCAL
              // reduction in separate memory location
              K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1) += K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride);
              #elif L_CB_RES_DEST_LEVEL == LOCAL
              K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
              #elif L_CB_RES_DEST_LEVEL == GLOBAL
              K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
              #endif
            
            }
          }
          #endif
          // phase 3: process extra private elements in dimension L_1 with a subset of all FUs
          #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 > 0
          if (K2_L_FU_ID_L_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 + K2_P_NUM_FU_L_1 - 1) / K2_P_NUM_FU_L_1) {  
            // set step variable for access to res buffer
            const size_t p_step_l_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 > 0);
            {
              size_t p_iteration_l_1 = 0;
              #if L_CB_RES_DEST_LEVEL < LOCAL
              // reduction in separate memory location
              K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1) += K2_L_REDUCTION_MEM(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride);
              #elif L_CB_RES_DEST_LEVEL == LOCAL
              K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_L_BUFFER(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
              #elif L_CB_RES_DEST_LEVEL == GLOBAL
              K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(K2_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
              #endif
            
            }
          }
          #endif
        }
        #if L_CB_RES_DEST_LEVEL <= LOCAL
        barrier(CLK_LOCAL_MEM_FENCE);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
        barrier(CLK_GLOBAL_MEM_FENCE);
        #endif
      }
      #if L_CB_RES_DEST_LEVEL <= LOCAL
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
    }
    #endif
    
    // move results upwards in memory hierarchy
    #if G_CB_RES_DEST_LEVEL > L_CB_RES_DEST_LEVEL || K2_L_NUM_FU_R_1 > 1
    #if K2_L_NUM_STEPS_R_1 == 0 && K2_L_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0 && K2_L_NUM_EXTRA_ELEMENTS_R_1 > 0
    if (K2_G_FU_ID_R_1 < (K2_L_NUM_EXTRA_ELEMENTS_R_1 + K2_L_NUM_FU_R_1 - 1) / K2_L_NUM_FU_R_1)
    #endif
    {
      
      #if L_CB_RES_DEST_LEVEL < LOCAL && K2_L_NUM_FU_R_1 == 1
      if (K2_L_FU_ID_R_1 == 0) {
        // phase 1: process private cached iterations in dimension L_1 with all FUs
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 > 0
        for (size_t p_step_l_1 = 0; p_step_l_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1; ++p_step_l_1) {
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_L_1; ++p_iteration_l_1)
            K2_P_KERNEL_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        } // end of "p_step_l_1"-loop
        #endif
        // phase 2: process extra private iterations in dimension L_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 > 0
        {  
          const size_t p_step_l_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1;
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1; ++p_iteration_l_1)
            K2_P_KERNEL_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
        #endif
        // phase 3: process extra private elements in dimension L_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 > 0
        if (K2_L_FU_ID_L_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_L_1 + K2_P_NUM_FU_L_1 - 1) / K2_P_NUM_FU_L_1) {  
          // set step variable for access to res buffer
          const size_t p_step_l_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_L_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_L_1 > 0);
          {
            size_t p_iteration_l_1 = 0;
            K2_P3_IN_COMPLETE_G_CB_L_1_P_KERNEL_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
          }
        }
        #endif
      }
      #else
      #if K2_L_NUM_FU_R_1 == 1
        #if L_CB_RES_DEST_LEVEL <= LOCAL && CACHE_L_CB == 0
        barrier(CLK_LOCAL_MEM_FENCE);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
        barrier(CLK_GLOBAL_MEM_FENCE);
        #endif
      #endif
      #if (K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_L_FLAT_WI_ID + step * K2_L_FLAT_NUM_WI;
        #if K2_G_NUM_FU_R_1 == 1
        const size_t l_index_l_1 = flat_index;
        #else
        const size_t DESCENDING_L_DIMS_0(l_index_l_1) = flat_index;
        #endif
        K2_L_KERNEL_RES(l_index_l_1) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K2_L_REDUCTION_MEM(l_index_l_1, 0);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K2_RES_L_BUFFER(l_index_l_1, 0, 0);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(l_index_l_1), i_wg_r_1, 0, 0);
        #endif
      }
      #endif
      #if (K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) % K2_L_FLAT_NUM_WI > 0
      if (K2_L_FLAT_WI_ID < (K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) % K2_L_FLAT_NUM_WI) {
        const size_t flat_index = K2_L_FLAT_WI_ID + ((K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_L_1) / K2_L_FLAT_NUM_WI) * K2_L_FLAT_NUM_WI;
        #if K2_G_NUM_FU_R_1 == 1
        const size_t l_index_l_1 = flat_index;
        #else
        const size_t DESCENDING_L_DIMS_0(l_index_l_1) = flat_index;
        #endif
        K2_L_KERNEL_RES(l_index_l_1) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K2_L_REDUCTION_MEM(l_index_l_1, 0);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K2_RES_L_BUFFER(l_index_l_1, 0, 0);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K2_RES_G_BUFFER(K2_L_TO_G_INDEX_L_1(l_index_l_1), i_wg_r_1, 0, 0);
        #endif
      }
      #endif
      #if L_CB_RES_DEST_LEVEL <= LOCAL
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #endif
    }
    #endif
  
  }
  #endif
  // phase 3: process extra local elements in dimension L_1 with a subset of all FUs
  #if K2_L_NUM_EXTRA_ELEMENTS_L_1 > 0
  if (K2_G_FU_ID_L_1 < (K2_L_NUM_EXTRA_ELEMENTS_L_1 + K2_L_NUM_FU_L_1 - 1) / K2_L_NUM_FU_L_1) {  
    // set step variable for access to res buffer
    const size_t l_step_l_1 = K2_L_NUM_STEPS_L_1 + (K2_L_NUM_EXTRA_CACHED_ITERATIONS_L_1 > 0);
    // phase 1: process local cached iterations in dimension R_1 with all FUs
    #if K2_L_NUM_STEPS_R_1 > 0
    size_t l_step_r_1 = 0;
    // ---------- L caching --------------------
    #if CACHE_L_CB != 0
    for (size_t step = 0; step < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= K2_L_NUM_FU_L_1) * (K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > K2_L_NUM_FU_L_1) * K2_L_NUM_FU_L_1))) / K2_L_FLAT_NUM_WI; ++step) {
      const size_t flat_index = K2_L_FLAT_WI_ID + step * K2_L_FLAT_NUM_WI;
      const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_R_1);
      const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_R_1;
      K2_P3_L_1_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_P3_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
    }
    if (K2_L_FLAT_WI_ID < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= K2_L_NUM_FU_L_1) * (K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > K2_L_NUM_FU_L_1) * K2_L_NUM_FU_L_1))) % K2_L_FLAT_NUM_WI) {
      const size_t flat_index = K2_L_FLAT_WI_ID + (((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= K2_L_NUM_FU_L_1) * (K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > K2_L_NUM_FU_L_1) * K2_L_NUM_FU_L_1))) / K2_L_FLAT_NUM_WI) * K2_L_FLAT_NUM_WI;
      const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_R_1);
      const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_R_1;
      K2_P3_L_1_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_P3_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    #endif
    // ---------- end of L caching -------------
    
    
    // parent level in phase 3: process extra private elements in dimension L_1 with a subset of all FUs
    if (K2_L_FU_ID_L_1 < K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) {
      size_t p_step_l_1 = 0;
      // phase 1: process private cached iterations in dimension R_1 with all FUs
      #if K2_P_NUM_STEPS_R_1 > 0
      size_t p_step_r_1 = 0;
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
        #if CACHE_L_CB != 0
        const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
        const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        #else
        const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
        #endif
        K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
      }
      #endif
      #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI > 0
      if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
        const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
        #if CACHE_L_CB != 0
        const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
        const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        #else
        const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
        const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
        #endif
        K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      {
        size_t p_iteration_l_1 = 0;
        size_t p_iteration_r_1 = 0;
        // process one mda element
        K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
        
        #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
        for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
          // process one mda element
          K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
        
        }
        #endif
        
      
      }
      #if K2_P_NUM_STEPS_R_1 > 1
      for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_R_1; ++p_step_r_1) {
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        {
          size_t p_iteration_l_1 = 0;
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }
      } // end of "p_step_r_1"-loop
      #endif
      #endif
      // phase 2: process extra private iterations in dimension R_1 with all FUs
      #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0
      {  
        const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1;  
        #if K2_P_NUM_STEPS_R_1 == 0
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        {
          size_t p_iteration_l_1 = 0;
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }  
        #else
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
          #endif
          K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        {
          size_t p_iteration_l_1 = 0;
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }  
        #endif
      }
      #endif
      // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
      #if K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
      if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
        // set step variable for access to res buffer
        const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0);  
        #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_PP3_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_PP3_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        {
          size_t p_iteration_l_1 = 0;
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_L_1_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
        
        }  
        #else
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_PP3_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
          #else
          const size_t p_index_l_1 = flat_index / (1);
          const size_t p_index_r_1 = flat_index % 1;
          #endif
          K2_PP3_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        // ---------- end of P caching -------------
        
        {
          size_t p_iteration_l_1 = 0;
          {
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }  
        #endif
      }
      #endif
      
      // ---------- reduction --------------------
      // will never be necessary on this level
      // because K2_P_NUM_FU_R == 1 for OpenCL
      // ---------- end of reduction -------------
      
      // move results upwards in memory hierarchy
      #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
      #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
      if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
      #endif
      {
        
        {
          size_t p_iteration_l_1 = 0;
          K2_P3_L_1_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
      }
      #endif
    
    }
    
    
    
    // wait for all WIs to finish computation on local cache block
    #if CACHE_L_CB != 0
    barrier(CLK_LOCAL_MEM_FENCE);
    #endif
    #if K2_L_NUM_STEPS_R_1 > 1
    for (l_step_r_1 = 1; l_step_r_1 < K2_L_NUM_STEPS_R_1; ++l_step_r_1) {
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      for (size_t step = 0; step < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= K2_L_NUM_FU_L_1) * (K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > K2_L_NUM_FU_L_1) * K2_L_NUM_FU_L_1))) / K2_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_L_FLAT_WI_ID + step * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_R_1);
        const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_R_1;
        K2_P3_L_1_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_P3_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      if (K2_L_FLAT_WI_ID < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= K2_L_NUM_FU_L_1) * (K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > K2_L_NUM_FU_L_1) * K2_L_NUM_FU_L_1))) % K2_L_FLAT_NUM_WI) {
        const size_t flat_index = K2_L_FLAT_WI_ID + (((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= K2_L_NUM_FU_L_1) * (K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > K2_L_NUM_FU_L_1) * K2_L_NUM_FU_L_1))) / K2_L_FLAT_NUM_WI) * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_R_1);
        const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_R_1;
        K2_P3_L_1_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_P3_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      
      // parent level in phase 3: process extra private elements in dimension L_1 with a subset of all FUs
      if (K2_L_FU_ID_L_1 < K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) {
        size_t p_step_l_1 = 0;
        // phase 1: process private cached iterations in dimension R_1 with all FUs
        #if K2_P_NUM_STEPS_R_1 > 0
        size_t p_step_r_1 = 0;
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
        {
          size_t p_iteration_l_1 = 0;
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }
        #else
        {
          size_t p_iteration_l_1 = 0;
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }
        #endif
        #if K2_P_NUM_STEPS_R_1 > 1
        for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_R_1; ++p_step_r_1) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
        } // end of "p_step_r_1"-loop
        #endif
        #endif
        // phase 2: process extra private iterations in dimension R_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0
        {  
          const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1;  
          #if K2_P_NUM_STEPS_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1 > 1
            for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
            #endif
            
          
          }
          #else
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_L_CB_R_1;
            #endif
            K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_L_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
          // set step variable for access to res buffer
          const size_t p_step_r_1 = K2_P_NUM_STEPS_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0);  
          #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_L_1_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
          #else
          {
            size_t p_iteration_l_1 = 0;
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_L_1_P3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        
        // ---------- reduction --------------------
        // will never be necessary on this level
        // because K2_P_NUM_FU_R == 1 for OpenCL
        // ---------- end of reduction -------------
        
        // move results upwards in memory hierarchy
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
        #if K2_P_NUM_STEPS_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
        #endif
        {
          
          {
            size_t p_iteration_l_1 = 0;
            K2_P3_L_1_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
          }
        }
        #endif
      
      }
      
      
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
    } // end of "l_step_r_1"-loop
    #endif
    #endif
    // phase 2: process extra local iterations in dimension R_1 with all FUs
    #if K2_L_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0
    {  
      const size_t l_step_r_1 = K2_L_NUM_STEPS_R_1;  
      #if K2_L_NUM_STEPS_R_1 == 0
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      for (size_t step = 0; step < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= K2_L_NUM_FU_L_1) * (K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > K2_L_NUM_FU_L_1) * K2_L_NUM_FU_L_1)) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_L_FLAT_WI_ID + step * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
        const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
        K2_P3_L_1_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_P3_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      if (K2_L_FLAT_WI_ID < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= K2_L_NUM_FU_L_1) * (K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > K2_L_NUM_FU_L_1) * K2_L_NUM_FU_L_1)) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_L_FLAT_NUM_WI) {
        const size_t flat_index = K2_L_FLAT_WI_ID + (((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= K2_L_NUM_FU_L_1) * (K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > K2_L_NUM_FU_L_1) * K2_L_NUM_FU_L_1)) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_L_FLAT_NUM_WI) * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
        const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
        K2_P3_L_1_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_P3_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      // parent level in phase 3: process extra private elements in dimension L_1 with a subset of all FUs
      if (K2_L_FU_ID_L_1 < K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) {
        size_t p_step_l_1 = 0;
        // phase 1: process private cached iterations in dimension R_1 with all FUs
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 0
        size_t p_step_r_1 = 0;
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        {
          size_t p_iteration_l_1 = 0;
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 1
        for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1; ++p_step_r_1) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
        } // end of "p_step_r_1"-loop
        #endif
        #endif
        // phase 2: process extra private iterations in dimension R_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0
        {  
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1;  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 1
            for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
            #endif
            
          
          }  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
          // set step variable for access to res buffer
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0);  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        
        // ---------- reduction --------------------
        // will never be necessary on this level
        // because K2_P_NUM_FU_R == 1 for OpenCL
        // ---------- end of reduction -------------
        
        // move results upwards in memory hierarchy
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
        #endif
        {
          
          {
            size_t p_iteration_l_1 = 0;
            K2_P3_L_1_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
          }
        }
        #endif
      
      }
      
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif  
      #else
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      for (size_t step = 0; step < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= K2_L_NUM_FU_L_1) * (K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > K2_L_NUM_FU_L_1) * K2_L_NUM_FU_L_1)) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_L_FLAT_WI_ID + step * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
        const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
        K2_P3_L_1_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_P3_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      if (K2_L_FLAT_WI_ID < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= K2_L_NUM_FU_L_1) * (K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > K2_L_NUM_FU_L_1) * K2_L_NUM_FU_L_1)) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_L_FLAT_NUM_WI) {
        const size_t flat_index = K2_L_FLAT_WI_ID + (((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= K2_L_NUM_FU_L_1) * (K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > K2_L_NUM_FU_L_1) * K2_L_NUM_FU_L_1)) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_L_FLAT_NUM_WI) * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
        const size_t l_index_r_1 = flat_index % K2_L_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
        K2_P3_L_1_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_P3_L_TO_G_INDEX_L_1(l_index_l_1), K2_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      // parent level in phase 3: process extra private elements in dimension L_1 with a subset of all FUs
      if (K2_L_FU_ID_L_1 < K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) {
        size_t p_step_l_1 = 0;
        // phase 1: process private cached iterations in dimension R_1 with all FUs
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 0
        size_t p_step_r_1 = 0;
        // ---------- P caching --------------------
        #if CACHE_P_CB != 0
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI > 0
        for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
          const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI > 0
        if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
          const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
          #if CACHE_L_CB != 0
          const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
          const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          #else
          const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
          const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
          #endif
          K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
        }
        #endif
        #endif
        // ---------- end of P caching -------------
        
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
        {
          size_t p_iteration_l_1 = 0;
          size_t p_iteration_r_1 = 0;
          // process one mda element
          K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          #if K2_P_NUM_CACHED_ITERATIONS_R_1 > 1
          for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
          #endif
          
        
        }
        #else
        {
          size_t p_iteration_l_1 = 0;
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
            // process one mda element
            K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
          
          }
        }
        #endif
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 > 1
        for (p_step_r_1 = 1; p_step_r_1 < K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1; ++p_step_r_1) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_R_1;
            #endif
            K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
        } // end of "p_step_r_1"-loop
        #endif
        #endif
        // phase 2: process extra private iterations in dimension R_1 with all FUs
        #if K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0
        {  
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1;  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            #if K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 1
            for (p_iteration_r_1 = 1; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
            #endif
            
          
          }
          #else
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            #else
            const size_t p_index_l_1 = flat_index / (K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1);
            const size_t p_index_r_1 = flat_index % K2_P_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_G_CB_R_1;
            #endif
            K2_PP3_L_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < K2_P_NUM_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1; ++p_iteration_r_1) {
              // process one mda element
              K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        // phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        #if K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1) {  
          // set step variable for access to res buffer
          const size_t p_step_r_1 = K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 + (K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 > 0);  
          #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
          #else
          {
            size_t p_iteration_l_1 = 0;
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif  
          #else
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_P3_P_TO_L_INDEX_IN_COMPLETE_G_CB_R_1(p_index_r_1));
          }
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P3_IN_COMPLETE_G_CB_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }  
          #endif
        }
        #endif
        
        // ---------- reduction --------------------
        // will never be necessary on this level
        // because K2_P_NUM_FU_R == 1 for OpenCL
        // ---------- end of reduction -------------
        
        // move results upwards in memory hierarchy
        #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
        #if K2_P_NUM_STEPS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_G_CB_R_1 == 0 && K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 > 0
        if (K2_L_FU_ID_R_1 < (K2_P_NUM_EXTRA_ELEMENTS_IN_COMPLETE_G_CB_R_1 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
        #endif
        {
          
          {
            size_t p_iteration_l_1 = 0;
            K2_P3_L_1_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
          }
        }
        #endif
      
      }
      
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif  
      #endif
    }
    #endif
    // phase 3: process extra local elements in dimension R_1 with a subset of all FUs
    #if K2_L_NUM_EXTRA_ELEMENTS_R_1 > 0
    if (K2_G_FU_ID_R_1 < (K2_L_NUM_EXTRA_ELEMENTS_R_1 + K2_L_NUM_FU_R_1 - 1) / K2_L_NUM_FU_R_1) {  
      // set step variable for access to res buffer
      const size_t l_step_r_1 = K2_L_NUM_STEPS_R_1 + (K2_L_NUM_EXTRA_CACHED_ITERATIONS_R_1 > 0);  
      #if K2_L_NUM_STEPS_R_1 == 0 && K2_L_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      for (size_t step = 0; step < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= K2_L_NUM_FU_L_1) * (K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > K2_L_NUM_FU_L_1) * K2_L_NUM_FU_L_1)) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1))) / K2_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_L_FLAT_WI_ID + step * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1)));
        const size_t l_index_r_1 = flat_index % ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1));
        K2_P3_L_1_P3_R_1_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_P3_L_TO_G_INDEX_L_1(l_index_l_1), K2_P3_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      if (K2_L_FLAT_WI_ID < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= K2_L_NUM_FU_L_1) * (K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > K2_L_NUM_FU_L_1) * K2_L_NUM_FU_L_1)) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1))) % K2_L_FLAT_NUM_WI) {
        const size_t flat_index = K2_L_FLAT_WI_ID + (((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= K2_L_NUM_FU_L_1) * (K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > K2_L_NUM_FU_L_1) * K2_L_NUM_FU_L_1)) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1))) / K2_L_FLAT_NUM_WI) * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1)));
        const size_t l_index_r_1 = flat_index % ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1));
        K2_P3_L_1_P3_R_1_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_P3_L_TO_G_INDEX_L_1(l_index_l_1), K2_P3_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      // parent level in phase 3: process extra private elements in dimension L_1 with a subset of all FUs
      if (K2_L_FU_ID_L_1 < K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) {
        size_t p_step_l_1 = 0;
        // parent level in phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        if (K2_L_FU_ID_R_1 < K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) {
          size_t p_step_r_1 = 0;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_L_1_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_P3_R_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_L_1_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_P3_R_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_L_1_PP3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
        
          // ---------- reduction --------------------
          // will never be necessary on this level
          // because K2_P_NUM_FU_R == 1 for OpenCL
          // ---------- end of reduction -------------
        
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
          #if 1 == 0 && 0 == 0 && 0 > 0
          if (K2_L_FU_ID_R_1 < (0 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
          #endif
          {
            
            {
              size_t p_iteration_l_1 = 0;
              K2_P3_L_1_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
            }
          }
          #endif
        
        }
      
      }
      
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif  
      #else
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      for (size_t step = 0; step < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= K2_L_NUM_FU_L_1) * (K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > K2_L_NUM_FU_L_1) * K2_L_NUM_FU_L_1)) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1))) / K2_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_L_FLAT_WI_ID + step * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1)));
        const size_t l_index_r_1 = flat_index % ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1));
        K2_P3_L_1_P3_R_1_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_P3_L_TO_G_INDEX_L_1(l_index_l_1), K2_P3_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      if (K2_L_FLAT_WI_ID < ((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= K2_L_NUM_FU_L_1) * (K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > K2_L_NUM_FU_L_1) * K2_L_NUM_FU_L_1)) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1))) % K2_L_FLAT_NUM_WI) {
        const size_t flat_index = K2_L_FLAT_WI_ID + (((K2_L_NUM_PROCESSED_ELEMENTS_L_1 * K2_L_NUM_PROCESSED_ELEMENTS_R_1) / K2_L_NUM_PROCESSED_ELEMENTS_L_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= K2_L_NUM_FU_L_1) * (K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > K2_L_NUM_FU_L_1) * K2_L_NUM_FU_L_1)) / K2_L_NUM_PROCESSED_ELEMENTS_R_1 * ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1))) / K2_L_FLAT_NUM_WI) * K2_L_FLAT_NUM_WI;
        const size_t l_index_l_1 = flat_index / (((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1)));
        const size_t l_index_r_1 = flat_index % ((((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) <= K2_L_NUM_FU_R_1) * (K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) > K2_L_NUM_FU_R_1) * K2_L_NUM_FU_R_1));
        K2_P3_L_1_P3_R_1_L_MEM_INT_RES(l_index_l_1, l_index_r_1) = K2_G_MEM_INT_RES(K2_P3_L_TO_G_INDEX_L_1(l_index_l_1), K2_P3_L_TO_G_INDEX_R_1(l_index_r_1));
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      // parent level in phase 3: process extra private elements in dimension L_1 with a subset of all FUs
      if (K2_L_FU_ID_L_1 < K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) {
        size_t p_step_l_1 = 0;
        // parent level in phase 3: process extra private elements in dimension R_1 with a subset of all FUs
        if (K2_L_FU_ID_R_1 < K2_L_NUM_EXTRA_ELEMENTS_R_1 - K2_G_FU_ID_R_1 * K2_L_NUM_FU_R_1) {
          size_t p_step_r_1 = 0;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI; ++step) {
            const size_t flat_index = K2_P_FLAT_WI_ID + step * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_L_1_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_P3_R_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #if ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI > 0
          if (K2_P_FLAT_WI_ID < ((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) % K2_P_FLAT_NUM_WI) {
            const size_t flat_index = K2_P_FLAT_WI_ID + (((K2_P_NUM_PROCESSED_ELEMENTS_L_1 * K2_P_NUM_PROCESSED_ELEMENTS_R_1) / K2_P_NUM_PROCESSED_ELEMENTS_L_1 * 1 / K2_P_NUM_PROCESSED_ELEMENTS_R_1 * 1) / K2_P_FLAT_NUM_WI) * K2_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_INT_RES_INDEX_1(p_index_l_1, p_index_r_1) = flat_index / (BUFFER_INT_RES_INDEX_0(1, 1));
            const size_t BUFFER_INT_RES_INDEX_0(p_index_l_1, p_index_r_1) = flat_index % BUFFER_INT_RES_INDEX_0(1, 1);
            #else
            const size_t p_index_l_1 = flat_index / (1);
            const size_t p_index_r_1 = flat_index % 1;
            #endif
            K2_PP3_L_1_PP3_R_1_P_MEM_INT_RES(p_index_l_1, p_index_r_1) = K2_P3_L_1_P3_R_1_L_MEM_INT_RES(K2_PP3_P_TO_L_INDEX_L_1(p_index_l_1), K2_PP3_P_TO_L_INDEX_R_1(p_index_r_1));
          }
          #endif
          #endif
          // ---------- end of P caching -------------
          
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          {
            size_t p_iteration_l_1 = 0;
            size_t p_iteration_r_1 = 0;
            // process one mda element
            K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_PP3_L_1_PP3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
          
          }
          #else
          {
            size_t p_iteration_l_1 = 0;
            {
              size_t p_iteration_r_1 = 0;
              // process one mda element
              K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_PP3_R_1_P_MEM_INT_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1), K2_P_ITERATION_TO_P_INDEX_R_1(p_iteration_r_1));
            
            }
          }
          #endif
        
          // ---------- reduction --------------------
          // will never be necessary on this level
          // because K2_P_NUM_FU_R == 1 for OpenCL
          // ---------- end of reduction -------------
        
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL || K2_P_NUM_FU_R_1 > 1
          #if 1 == 0 && 0 == 0 && 0 > 0
          if (K2_L_FU_ID_R_1 < (0 + K2_P_NUM_FU_R_1 - 1) / K2_P_NUM_FU_R_1)
          #endif
          {
            
            {
              size_t p_iteration_l_1 = 0;
              K2_P3_L_1_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) += K2_PP3_L_1_P_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
            }
          }
          #endif
        
        }
      
      }
      
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif  
      #endif
    }
    #endif
    
    #if K2_L_NUM_FU_R_1 > 1
    #if K2_L_NUM_STEPS_R_1 == 0 && K2_L_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0 && K2_L_NUM_EXTRA_ELEMENTS_R_1 > 0
    if (K2_G_FU_ID_R_1 < (K2_L_NUM_EXTRA_ELEMENTS_R_1 + K2_L_NUM_FU_R_1 - 1) / K2_L_NUM_FU_R_1)
    #endif
    {
      // wait for all FUs to finish computation
      #if   L_CB_RES_DEST_LEVEL == LOCAL && CACHE_L_CB == 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #if L_CB_RES_DEST_LEVEL < LOCAL
      // parent level in phase 3: process extra private elements in dimension L_1 with a subset of all FUs
      if (K2_L_FU_ID_L_1 < K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) {
        size_t p_step_l_1 = 0;
        {
          size_t p_iteration_l_1 = 0;
          K2_L_REDUCTION_MEM(K2_PP3_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1) = K2_P3_L_1_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
        }
      }
      
      // wait for all values to be copied into reduction memory
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      #if (K2_L_NUM_FU_R_1 & (K2_L_NUM_FU_R_1 - 1)) != 0
      // pre processing: reduce number of values to largest possible power of 2
      size_t stride = pow(2.0f, floor(log2((float) K2_L_NUM_FU_R_1)));
      if (K2_L_FU_ID_R_1 < stride && K2_L_FU_ID_R_1 + stride < K2_L_NUM_FU_R_1) {
        // parent level in phase 3: process extra private elements in dimension L_1 with a subset of all FUs
        if (K2_L_FU_ID_L_1 < K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) {
          size_t p_step_l_1 = 0;
          {
            size_t p_iteration_l_1 = 0;
            #if L_CB_RES_DEST_LEVEL < LOCAL
            // reduction in separate memory location
            K2_L_REDUCTION_MEM(K2_PP3_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1) += K2_L_REDUCTION_MEM(K2_PP3_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride);
            #elif L_CB_RES_DEST_LEVEL == LOCAL
            K2_RES_L_BUFFER(K2_PP3_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_L_BUFFER(K2_PP3_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
            #elif L_CB_RES_DEST_LEVEL == GLOBAL
            K2_RES_G_BUFFER(K2_P3_L_TO_G_INDEX_L_1(K2_PP3_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_G_BUFFER(K2_P3_L_TO_G_INDEX_L_1(K2_PP3_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
            #endif
          
          }
        }
      
      }
      stride /= 2;
      #if L_CB_RES_DEST_LEVEL <= LOCAL
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #else
      size_t stride = K2_L_NUM_FU_R_1 / 2;
      #endif
      // perform reduction
      for (; stride > 0; stride /= 2) {
        if (K2_L_FU_ID_R_1 < stride) {
          // parent level in phase 3: process extra private elements in dimension L_1 with a subset of all FUs
          if (K2_L_FU_ID_L_1 < K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) {
            size_t p_step_l_1 = 0;
            {
              size_t p_iteration_l_1 = 0;
              #if L_CB_RES_DEST_LEVEL < LOCAL
              // reduction in separate memory location
              K2_L_REDUCTION_MEM(K2_PP3_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1) += K2_L_REDUCTION_MEM(K2_PP3_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride);
              #elif L_CB_RES_DEST_LEVEL == LOCAL
              K2_RES_L_BUFFER(K2_PP3_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_L_BUFFER(K2_PP3_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)), K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
              #elif L_CB_RES_DEST_LEVEL == GLOBAL
              K2_RES_G_BUFFER(K2_P3_L_TO_G_INDEX_L_1(K2_PP3_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1, K2_P_FU_ID_R_1) += K2_RES_G_BUFFER(K2_P3_L_TO_G_INDEX_L_1(K2_PP3_P_TO_L_INDEX_L_1(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1))), K2_G_FU_ID_R_1, K2_L_FU_ID_R_1 + stride, K2_P_FU_ID_R_1);
              #endif
            
            }
          }
      
        }
        #if L_CB_RES_DEST_LEVEL <= LOCAL
        barrier(CLK_LOCAL_MEM_FENCE);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
        barrier(CLK_GLOBAL_MEM_FENCE);
        #endif
      }
      #if L_CB_RES_DEST_LEVEL <= LOCAL
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
    }
    #endif
    
    // move results upwards in memory hierarchy
    #if G_CB_RES_DEST_LEVEL > L_CB_RES_DEST_LEVEL || K2_L_NUM_FU_R_1 > 1
    #if K2_L_NUM_STEPS_R_1 == 0 && K2_L_NUM_EXTRA_CACHED_ITERATIONS_R_1 == 0 && K2_L_NUM_EXTRA_ELEMENTS_R_1 > 0
    if (K2_G_FU_ID_R_1 < (K2_L_NUM_EXTRA_ELEMENTS_R_1 + K2_L_NUM_FU_R_1 - 1) / K2_L_NUM_FU_R_1)
    #endif
    {
      
      #if L_CB_RES_DEST_LEVEL < LOCAL && K2_L_NUM_FU_R_1 == 1
      if (K2_L_FU_ID_R_1 == 0) {
        // parent level in phase 3: process extra private elements in dimension L_1 with a subset of all FUs
        if (K2_L_FU_ID_L_1 < K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) {
          size_t p_step_l_1 = 0;
          {
            size_t p_iteration_l_1 = 0;
            K2_PP3_L_1_P_KERNEL_RES(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1)) = K2_P3_L_1_L_CB_RES_DEST(K2_P_ITERATION_TO_P_INDEX_L_1(p_iteration_l_1));
          }
        }
      
      }
      #else
      #if K2_L_NUM_FU_R_1 == 1
        #if L_CB_RES_DEST_LEVEL <= LOCAL && CACHE_L_CB == 0
        barrier(CLK_LOCAL_MEM_FENCE);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
        barrier(CLK_GLOBAL_MEM_FENCE);
        #endif
      #endif
      for (size_t step = 0; step < (((((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= K2_L_NUM_FU_L_1) * (K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > K2_L_NUM_FU_L_1) * K2_L_NUM_FU_L_1))) / K2_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K2_L_FLAT_WI_ID + step * K2_L_FLAT_NUM_WI;
        #if K2_G_NUM_FU_R_1 == 1
        const size_t l_index_l_1 = flat_index;
        #else
        const size_t DESCENDING_L_DIMS_0(l_index_l_1) = flat_index;
        #endif
        K2_P3_L_1_L_KERNEL_RES(l_index_l_1) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K2_L_REDUCTION_MEM(l_index_l_1, 0);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K2_RES_L_BUFFER(l_index_l_1, 0, 0);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K2_RES_G_BUFFER(K2_P3_L_TO_G_INDEX_L_1(l_index_l_1), i_wg_r_1, 0, 0);
        #endif
      }
      if (K2_L_FLAT_WI_ID < (((((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= K2_L_NUM_FU_L_1) * (K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > K2_L_NUM_FU_L_1) * K2_L_NUM_FU_L_1))) % K2_L_FLAT_NUM_WI) {
        const size_t flat_index = K2_L_FLAT_WI_ID + ((((((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= 0) * 0) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > 0) * ((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) <= K2_L_NUM_FU_L_1) * (K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1)) + (((K2_L_NUM_EXTRA_ELEMENTS_L_1 - K2_G_FU_ID_L_1 * K2_L_NUM_FU_L_1) > K2_L_NUM_FU_L_1) * K2_L_NUM_FU_L_1))) / K2_L_FLAT_NUM_WI) * K2_L_FLAT_NUM_WI;
        #if K2_G_NUM_FU_R_1 == 1
        const size_t l_index_l_1 = flat_index;
        #else
        const size_t DESCENDING_L_DIMS_0(l_index_l_1) = flat_index;
        #endif
        K2_P3_L_1_L_KERNEL_RES(l_index_l_1) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K2_L_REDUCTION_MEM(l_index_l_1, 0);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K2_RES_L_BUFFER(l_index_l_1, 0, 0);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K2_RES_G_BUFFER(K2_P3_L_TO_G_INDEX_L_1(l_index_l_1), i_wg_r_1, 0, 0);
        #endif
      }
      #if L_CB_RES_DEST_LEVEL <= LOCAL
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #endif
    }
    #endif
  
  }
  #endif
}
// =============== end of kernel 2 ============================================