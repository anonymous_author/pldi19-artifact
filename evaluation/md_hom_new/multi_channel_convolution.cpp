#include <iostream>
#include <cstdlib>
#include <argparse.hpp>
#include <atf.h>
#include <json.hpp>

#include "include/process_wrapper_helper.hpp"
using atf::cf::WEIGHTS_33;

void tune(size_t platform_id, size_t device_id, atf::cf::device_info &device_info, const std::string &device_type,
          size_t N, size_t K, size_t P, size_t Q, size_t C,
          const std::vector<float> &in, const std::vector<struct WEIGHTS_33> &weights, const std::vector<float> &out) {
    // get device information
    std::vector<size_t> max_wi_size;
    size_t max_wg_size = 0;
    device_info.get_device_limits(&max_wi_size, &max_wg_size);
    size_t combined_max_wi_size = 0;
    for (int i = 0; i < 3; ++i) {
        combined_max_wi_size = std::max(combined_max_wi_size, max_wi_size[i]);
    }
    
    // define search space
    auto CACHE_L_CB          = atf::tp("CACHE_L_CB",          {0, 1});
    auto CACHE_P_CB          = atf::tp("CACHE_P_CB",          {0, 1});
    auto G_CB_RES_DEST_LEVEL = atf::tp("G_CB_RES_DEST_LEVEL", {2});
    auto L_CB_RES_DEST_LEVEL = atf::tp("L_CB_RES_DEST_LEVEL", {2, 1, 0}, atf::less_than_or_eq(G_CB_RES_DEST_LEVEL));
    auto P_CB_RES_DEST_LEVEL = atf::tp("P_CB_RES_DEST_LEVEL", {2, 1, 0}, atf::less_than_or_eq(L_CB_RES_DEST_LEVEL));

    auto INPUT_SIZE_L_1      = atf::tp("INPUT_SIZE_L_1",      {N});
    auto L_CB_SIZE_L_1       = atf::tp("L_CB_SIZE_L_1",       atf::interval<int>(1, N), atf::divides(INPUT_SIZE_L_1));
    auto P_CB_SIZE_L_1       = atf::tp("P_CB_SIZE_L_1",       atf::interval<int>(1, N), atf::divides(L_CB_SIZE_L_1));
    auto OCL_DIM_L_1         = atf::tp("OCL_DIM_L_1",         {0, 1, 2, 3, 4});
    auto NUM_WG_L_1          = atf::tp("NUM_WG_L_1",          atf::interval<int>(1, N), atf::divides(INPUT_SIZE_L_1 / L_CB_SIZE_L_1));
    auto NUM_WI_L_1          = atf::tp("NUM_WI_L_1",          atf::interval<int>(1, N), atf::divides(L_CB_SIZE_L_1 / P_CB_SIZE_L_1) && atf::less_than_or_eq(INPUT_SIZE_L_1 + NUM_WG_L_1 - 1 / NUM_WG_L_1));

    auto INPUT_SIZE_L_2      = atf::tp("INPUT_SIZE_L_2",      {K});
    auto L_CB_SIZE_L_2       = atf::tp("L_CB_SIZE_L_2",       atf::interval<int>(1, K), atf::less_than_or_eq(INPUT_SIZE_L_2));
    auto P_CB_SIZE_L_2       = atf::tp("P_CB_SIZE_L_2",       atf::interval<int>(1, K), atf::less_than_or_eq(L_CB_SIZE_L_2));
    auto OCL_DIM_L_2         = atf::tp("OCL_DIM_L_2",         {0, 1, 2, 3, 4}, atf::unequal(OCL_DIM_L_1));
    auto NUM_WG_L_2          = atf::tp("NUM_WG_L_2",          atf::interval<int>(1, K));
    auto NUM_WI_L_2          = atf::tp("NUM_WI_L_2",          atf::interval<int>(1, K), atf::less_than_or_eq(L_CB_SIZE_L_2) && atf::less_than_or_eq(INPUT_SIZE_L_2 + NUM_WG_L_2 - 1 / NUM_WG_L_2));

    auto INPUT_SIZE_L_3      = atf::tp("INPUT_SIZE_L_3",      {P});
    auto L_CB_SIZE_L_3       = atf::tp("L_CB_SIZE_L_3",       atf::interval<int>(1, P), atf::less_than_or_eq(INPUT_SIZE_L_3));
    auto P_CB_SIZE_L_3       = atf::tp("P_CB_SIZE_L_3",       atf::interval<int>(1, P), atf::less_than_or_eq(L_CB_SIZE_L_3));
    auto OCL_DIM_L_3         = atf::tp("OCL_DIM_L_3",         {0, 1, 2, 3, 4}, atf::unequal(OCL_DIM_L_1) && atf::unequal(OCL_DIM_L_2));
    auto NUM_WG_L_3          = atf::tp("NUM_WG_L_3",          atf::interval<int>(1, P));
    auto NUM_WI_L_3          = atf::tp("NUM_WI_L_3",          atf::interval<int>(1, P), atf::less_than_or_eq(L_CB_SIZE_L_3) && atf::less_than_or_eq(INPUT_SIZE_L_3 + NUM_WG_L_3 - 1 / NUM_WG_L_3));

    auto INPUT_SIZE_L_4      = atf::tp("INPUT_SIZE_L_4",      {Q});
    auto L_CB_SIZE_L_4       = atf::tp("L_CB_SIZE_L_4",       atf::interval<int>(1, Q), atf::less_than_or_eq(INPUT_SIZE_L_4));
    auto P_CB_SIZE_L_4       = atf::tp("P_CB_SIZE_L_4",       atf::interval<int>(1, Q), atf::less_than_or_eq(L_CB_SIZE_L_4));
    auto OCL_DIM_L_4         = atf::tp("OCL_DIM_L_4",         {0, 1, 2, 3, 4}, atf::unequal(OCL_DIM_L_1) && atf::unequal(OCL_DIM_L_2) && atf::unequal(OCL_DIM_L_3));
    auto NUM_WG_L_4          = atf::tp("NUM_WG_L_4",          atf::interval<int>(1, Q));
    auto NUM_WI_L_4          = atf::tp("NUM_WI_L_4",          atf::interval<int>(1, Q), atf::less_than_or_eq(L_CB_SIZE_L_4) && atf::less_than_or_eq(INPUT_SIZE_L_4 + NUM_WG_L_4 - 1 / NUM_WG_L_4));

    auto INPUT_SIZE_R_1      = atf::tp("INPUT_SIZE_R_1",      {C});
    auto L_CB_SIZE_R_1       = atf::tp("L_CB_SIZE_R_1",       atf::interval<int>(1, C), atf::less_than_or_eq(INPUT_SIZE_R_1));
    auto P_CB_SIZE_R_1       = atf::tp("P_CB_SIZE_R_1",       atf::interval<int>(1, C), atf::less_than_or_eq(L_CB_SIZE_R_1));
    auto OCL_DIM_R_1         = atf::tp("OCL_DIM_R_1",         {0, 1, 2, 3, 4}, atf::unequal(OCL_DIM_L_1) && atf::unequal(OCL_DIM_L_2) && atf::unequal(OCL_DIM_L_3) && atf::unequal(OCL_DIM_L_4));
    auto NUM_WG_R_1          = atf::tp("NUM_WG_R_1",          atf::interval<int>(1, C));
    auto NUM_WI_R_1          = atf::tp("NUM_WI_R_1",          atf::interval<int>(1, C), atf::less_than_or_eq(L_CB_SIZE_R_1) && atf::less_than_or_eq(INPUT_SIZE_R_1 + NUM_WG_R_1 - 1 / NUM_WG_R_1));

    // prepare tuner
    auto tuner = atf::open_tuner(new atf::cond::duration<std::chrono::seconds>(18000));
    tuner(CACHE_L_CB)(CACHE_P_CB)(G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL)
            (OCL_DIM_L_1, OCL_DIM_L_2, OCL_DIM_L_3, OCL_DIM_L_4, OCL_DIM_R_1)
            (INPUT_SIZE_L_1, L_CB_SIZE_L_1, P_CB_SIZE_L_1, NUM_WG_L_1, NUM_WI_L_1)
            (INPUT_SIZE_L_2, L_CB_SIZE_L_2, P_CB_SIZE_L_2, NUM_WG_L_2, NUM_WI_L_2)
            (INPUT_SIZE_L_3, L_CB_SIZE_L_3, P_CB_SIZE_L_3, NUM_WG_L_3, NUM_WI_L_3)
            (INPUT_SIZE_L_4, L_CB_SIZE_L_4, P_CB_SIZE_L_4, NUM_WG_L_4, NUM_WI_L_4)
            (INPUT_SIZE_R_1, L_CB_SIZE_R_1, P_CB_SIZE_R_1, NUM_WG_R_1, NUM_WI_R_1);


    // prepare kernel
    auto is_valid = [&](atf::configuration &config) -> bool {
        size_t num_wi[] = {1, 1, 1};
        if (config[OCL_DIM_L_1.name()].value().int_val() == 0) {
            num_wi[0] *= config[NUM_WI_L_1.name()].value().int_val();
        }
        if (config[OCL_DIM_L_2.name()].value().int_val() == 0) {
            num_wi[0] *= config[NUM_WI_L_2.name()].value().int_val();
        }
        if (config[OCL_DIM_L_3.name()].value().int_val() == 0) {
            num_wi[0] *= config[NUM_WI_L_3.name()].value().int_val();
        }
        if (config[OCL_DIM_L_4.name()].value().int_val() == 0) {
            num_wi[0] *= config[NUM_WI_L_4.name()].value().int_val();
        }
        if (config[OCL_DIM_R_1.name()].value().int_val() == 0) {
            num_wi[0] *= config[NUM_WI_R_1.name()].value().int_val();
        }
        if (config[OCL_DIM_L_1.name()].value().int_val() == 1) {
            num_wi[1] *= config[NUM_WI_L_1.name()].value().int_val();
        }
        if (config[OCL_DIM_L_2.name()].value().int_val() == 1) {
            num_wi[1] *= config[NUM_WI_L_2.name()].value().int_val();
        }
        if (config[OCL_DIM_L_3.name()].value().int_val() == 1) {
            num_wi[1] *= config[NUM_WI_L_3.name()].value().int_val();
        }
        if (config[OCL_DIM_L_4.name()].value().int_val() == 1) {
            num_wi[1] *= config[NUM_WI_L_4.name()].value().int_val();
        }
        if (config[OCL_DIM_R_1.name()].value().int_val() == 1) {
            num_wi[1] *= config[NUM_WI_R_1.name()].value().int_val();
        }
        if (config[OCL_DIM_L_1.name()].value().int_val() >= 2) {
            num_wi[2] *= config[NUM_WI_L_1.name()].value().int_val();
        }
        if (config[OCL_DIM_L_2.name()].value().int_val() >= 2) {
            num_wi[2] *= config[NUM_WI_L_2.name()].value().int_val();
        }
        if (config[OCL_DIM_L_3.name()].value().int_val() >= 2) {
            num_wi[2] *= config[NUM_WI_L_3.name()].value().int_val();
        }
        if (config[OCL_DIM_L_4.name()].value().int_val() >= 2) {
            num_wi[2] *= config[NUM_WI_L_4.name()].value().int_val();
        }
        if (config[OCL_DIM_R_1.name()].value().int_val() >= 2) {
            num_wi[2] *= config[NUM_WI_R_1.name()].value().int_val();
        }
        return num_wi[0] <= max_wi_size[0] && num_wi[1] <= max_wi_size[1] && num_wi[2] <= max_wi_size[2] && num_wi[0] * num_wi[1] * num_wi[2] <= max_wg_size;
    };
    size_t res_g_l_size = N * K * P * Q * sizeof(float);
    std::vector<float> int_res(res_g_l_size / sizeof(float)); for (int i = 0; i < int_res.size(); ++i) int_res[i] = 0;
    auto res_g_size =
            [&](unsigned int kernel, atf::configuration &config) -> size_t {
                size_t size = res_g_l_size;
                if (kernel == 1) {
                    if (config[G_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= config[NUM_WG_R_1.name()].value().int_val();
                    }
                    if (config[L_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= config[NUM_WI_R_1.name()].value().int_val();
                    }
                } else {
                    if (config[L_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= config[NUM_WI_R_1.name()].value().int_val();
                    }
                }
                if (config[P_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                    size *= 1;
                }
                return size;
            };
    auto int_res_size =
            [&](atf::configuration &config) -> size_t {
                size_t size = res_g_l_size * config[NUM_WG_R_1.name()].value().int_val();
                return size;
            };
    auto needs_second_kernel =
            [&](atf::configuration &config) -> bool {
                return config[NUM_WG_R_1.name()].value().int_val() > 1;
            };
    auto gs_1 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 0) * (NUM_WG_R_1) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 1) * (NUM_WG_R_1) * (NUM_WI_R_1),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WG_L_1) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WG_L_2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WG_L_3) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WG_L_4) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_R_1 >= 2) * (NUM_WG_R_1) * (NUM_WI_R_1), 1)
    );
    auto ls_1 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_R_1 >= 2) * (NUM_WI_R_1), 1)
    );
    auto gs_2 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WG_L_1) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WG_L_2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WG_L_3) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WG_L_4) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_R_1 >= 2) * (NUM_WI_R_1), 1)
    );
    auto ls_2 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_R_1 >= 2) * (NUM_WI_R_1), 1)
    );
    atf::cf::process_wrapper_info process_wrapper = {
            atf::cf::LOCAL,
            "../../extern/ATF/ocl_md_hom_process_wrapper_float",
            "none",
            atf::cf::CHECK_NONE,
            "wrap"
    };
    atf::cf::timeout warm_up_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    atf::cf::timeout evaluation_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    auto kernel = atf::cf::ocl_md_hom(
            device_info,
            {atf::cf::kernel_info::FILENAME, "kernel/multi_channel_convolution_1.cl", "multi_channel_convolution_1", " -DTYPE_T=float -DTYPE_TS=float"},
            atf::inputs(atf::buffer(in), atf::buffer(weights)),
            gs_1, ls_1,
            {atf::cf::kernel_info::FILENAME, "kernel/multi_channel_convolution_2.cl", "multi_channel_convolution_2"},
            atf::inputs(atf::buffer(out)),
            gs_2, ls_2,
            res_g_size,
            atf::buffer(int_res),
            int_res_size,
            needs_second_kernel,
            is_valid,
            5, 3,
            false,
            process_wrapper,
            warm_up_timeout,
            evaluation_timeout
    );

    // tune
    auto best_configuration = tuner(kernel);

    // store best found configuration if valid
    if (tuner.number_of_valid_evaluated_configs() > 0) {
        std::map<std::string, int> plain_config;
        for (const auto &tp : best_configuration) {
            plain_config[tp.first] = tp.second.value().int_val();
        }
        std::string config_file_dir = std::getenv("ARTIFACT_ROOT");
        config_file_dir.append("/results/")
                .append(device_type).append("/")
                .append(std::to_string(platform_id)).append("/")
                .append(std::to_string(device_id)).append("/md_hom_new/");
        std::string config_file_name = config_file_dir;
        config_file_name.append("multi_channel_convolution_");
        if (N == 16 && K == 64 && P == 12 && Q == 120 && C == 32) {
            config_file_name.append("small");
        } else if (N == 8 && K == 64 && P == 54 && Q == 54 && C == 64) {
            config_file_name.append("large");
        } else {
            config_file_name.append(std::to_string(N)).append("x")
                    .append(std::to_string(K)).append("x")
                    .append(std::to_string(P)).append("x")
                    .append(std::to_string(Q)).append("x")
                    .append(std::to_string(C));
        }
        config_file_name.append("_config.json");
        int ret = system(std::string("mkdir -p ").append(config_file_dir).c_str());
        if (ret != 0) {
            std::cerr << "Failed to create results directory." << std::endl;
            exit(EXIT_FAILURE);
        }
        std::ofstream config_file(config_file_name, std::ios::out | std::ios::trunc);
        config_file << std::setw(4) << nlohmann::json(plain_config);
        config_file.close();
    } else {
        std::cerr << "Tuning failed." << std::endl;
        exit(EXIT_FAILURE);
    }
}

void bench(size_t platform_id, size_t device_id, atf::cf::device_info &device_info, const std::string &device_type,
           size_t N, size_t K, size_t P, size_t Q, size_t C,
           const std::vector<float> &in, const std::vector<struct WEIGHTS_33> &weights, const std::vector<float> &out) {
    // read configuration
    std::string config_file_dir = std::getenv("ARTIFACT_ROOT");
    config_file_dir.append("/results/")
            .append(device_type).append("/")
            .append(std::to_string(platform_id)).append("/")
            .append(std::to_string(device_id)).append("/md_hom_new/");
    std::string config_file_name = config_file_dir;
    config_file_name.append("multi_channel_convolution_");
    if (N == 16 && K == 64 && P == 12 && Q == 120 && C == 32) {
        config_file_name.append("small");
    } else if (N == 8 && K == 64 && P == 54 && Q == 54 && C == 64) {
        config_file_name.append("large");
    } else {
        config_file_name.append(std::to_string(N)).append("x")
                .append(std::to_string(K)).append("x")
                .append(std::to_string(P)).append("x")
                .append(std::to_string(Q)).append("x")
                .append(std::to_string(C));
    }
    config_file_name.append("_config.json");
    std::ifstream config_file(config_file_name, std::ios::in);
    if (config_file.fail()) {
        std::cerr << "Unable to open configuration file. Has md_hom not yet been tuned for this input size?" << std::endl;
        exit(EXIT_FAILURE);
    }
    auto plain_config = nlohmann::json::parse(config_file);

    // set configuration for kernel execution
    auto CACHE_L_CB          = atf::tp("CACHE_L_CB",          {plain_config.at("CACHE_L_CB").get<int>()});
    auto CACHE_P_CB          = atf::tp("CACHE_P_CB",          {plain_config.at("CACHE_P_CB").get<int>()});
    auto G_CB_RES_DEST_LEVEL = atf::tp("G_CB_RES_DEST_LEVEL", {plain_config.at("G_CB_RES_DEST_LEVEL").get<int>()});
    auto L_CB_RES_DEST_LEVEL = atf::tp("L_CB_RES_DEST_LEVEL", {plain_config.at("L_CB_RES_DEST_LEVEL").get<int>()});
    auto P_CB_RES_DEST_LEVEL = atf::tp("P_CB_RES_DEST_LEVEL", {plain_config.at("P_CB_RES_DEST_LEVEL").get<int>()});
    auto INPUT_SIZE_L_1      = atf::tp("INPUT_SIZE_L_1",      {plain_config.at("INPUT_SIZE_L_1").get<int>()});
    auto L_CB_SIZE_L_1       = atf::tp("L_CB_SIZE_L_1",       {plain_config.at("L_CB_SIZE_L_1").get<int>()});
    auto P_CB_SIZE_L_1       = atf::tp("P_CB_SIZE_L_1",       {plain_config.at("P_CB_SIZE_L_1").get<int>()});
    auto OCL_DIM_L_1         = atf::tp("OCL_DIM_L_1",         {plain_config.at("OCL_DIM_L_1").get<int>()});
    auto NUM_WG_L_1          = atf::tp("NUM_WG_L_1",          {plain_config.at("NUM_WG_L_1").get<int>()});
    auto NUM_WI_L_1          = atf::tp("NUM_WI_L_1",          {plain_config.at("NUM_WI_L_1").get<int>()});
    auto INPUT_SIZE_L_2      = atf::tp("INPUT_SIZE_L_2",      {plain_config.at("INPUT_SIZE_L_2").get<int>()});
    auto L_CB_SIZE_L_2       = atf::tp("L_CB_SIZE_L_2",       {plain_config.at("L_CB_SIZE_L_2").get<int>()});
    auto P_CB_SIZE_L_2       = atf::tp("P_CB_SIZE_L_2",       {plain_config.at("P_CB_SIZE_L_2").get<int>()});
    auto OCL_DIM_L_2         = atf::tp("OCL_DIM_L_2",         {plain_config.at("OCL_DIM_L_2").get<int>()});
    auto NUM_WG_L_2          = atf::tp("NUM_WG_L_2",          {plain_config.at("NUM_WG_L_2").get<int>()});
    auto NUM_WI_L_2          = atf::tp("NUM_WI_L_2",          {plain_config.at("NUM_WI_L_2").get<int>()});
    auto INPUT_SIZE_L_3      = atf::tp("INPUT_SIZE_L_3",      {plain_config.at("INPUT_SIZE_L_3").get<int>()});
    auto L_CB_SIZE_L_3       = atf::tp("L_CB_SIZE_L_3",       {plain_config.at("L_CB_SIZE_L_3").get<int>()});
    auto P_CB_SIZE_L_3       = atf::tp("P_CB_SIZE_L_3",       {plain_config.at("P_CB_SIZE_L_3").get<int>()});
    auto OCL_DIM_L_3         = atf::tp("OCL_DIM_L_3",         {plain_config.at("OCL_DIM_L_3").get<int>()});
    auto NUM_WG_L_3          = atf::tp("NUM_WG_L_3",          {plain_config.at("NUM_WG_L_3").get<int>()});
    auto NUM_WI_L_3          = atf::tp("NUM_WI_L_3",          {plain_config.at("NUM_WI_L_3").get<int>()});
    auto INPUT_SIZE_L_4      = atf::tp("INPUT_SIZE_L_4",      {plain_config.at("INPUT_SIZE_L_4").get<int>()});
    auto L_CB_SIZE_L_4       = atf::tp("L_CB_SIZE_L_4",       {plain_config.at("L_CB_SIZE_L_4").get<int>()});
    auto P_CB_SIZE_L_4       = atf::tp("P_CB_SIZE_L_4",       {plain_config.at("P_CB_SIZE_L_4").get<int>()});
    auto OCL_DIM_L_4         = atf::tp("OCL_DIM_L_4",         {plain_config.at("OCL_DIM_L_4").get<int>()});
    auto NUM_WG_L_4          = atf::tp("NUM_WG_L_4",          {plain_config.at("NUM_WG_L_4").get<int>()});
    auto NUM_WI_L_4          = atf::tp("NUM_WI_L_4",          {plain_config.at("NUM_WI_L_4").get<int>()});
    auto INPUT_SIZE_R_1      = atf::tp("INPUT_SIZE_R_1",      {plain_config.at("INPUT_SIZE_R_1").get<int>()});
    auto L_CB_SIZE_R_1       = atf::tp("L_CB_SIZE_R_1",       {plain_config.at("L_CB_SIZE_R_1").get<int>()});
    auto P_CB_SIZE_R_1       = atf::tp("P_CB_SIZE_R_1",       {plain_config.at("P_CB_SIZE_R_1").get<int>()});
    auto OCL_DIM_R_1         = atf::tp("OCL_DIM_R_1",         {plain_config.at("OCL_DIM_R_1").get<int>()});
    auto NUM_WG_R_1          = atf::tp("NUM_WG_R_1",          {plain_config.at("NUM_WG_R_1").get<int>()});
    auto NUM_WI_R_1          = atf::tp("NUM_WI_R_1",          {plain_config.at("NUM_WI_R_1").get<int>()});
    auto tuner = atf::exhaustive();
    tuner(CACHE_L_CB)(CACHE_P_CB)(G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL)
            (OCL_DIM_L_1, OCL_DIM_L_2, OCL_DIM_L_3, OCL_DIM_L_4, OCL_DIM_R_1)
            (INPUT_SIZE_L_1, L_CB_SIZE_L_1, P_CB_SIZE_L_1, NUM_WG_L_1, NUM_WI_L_1)
            (INPUT_SIZE_L_2, L_CB_SIZE_L_2, P_CB_SIZE_L_2, NUM_WG_L_2, NUM_WI_L_2)
            (INPUT_SIZE_L_3, L_CB_SIZE_L_3, P_CB_SIZE_L_3, NUM_WG_L_3, NUM_WI_L_3)
            (INPUT_SIZE_L_4, L_CB_SIZE_L_4, P_CB_SIZE_L_4, NUM_WG_L_4, NUM_WI_L_4)
            (INPUT_SIZE_R_1, L_CB_SIZE_R_1, P_CB_SIZE_R_1, NUM_WG_R_1, NUM_WI_R_1);

    // prepare kernel
    auto is_valid = [&](atf::configuration &config) -> bool {
        return true;
    };
    size_t res_g_l_size = N * K * P * Q * sizeof(float);
    std::vector<float> int_res(res_g_l_size / sizeof(float)); for (int i = 0; i < int_res.size(); ++i) int_res[i] = 0;
    auto res_g_size =
            [&](unsigned int kernel, atf::configuration &config) -> size_t {
                size_t size = res_g_l_size;
                if (kernel == 1) {
                    if (config[G_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= config[NUM_WG_R_1.name()].value().int_val();
                    }
                    if (config[L_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= config[NUM_WI_R_1.name()].value().int_val();
                    }
                } else {
                    if (config[L_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= config[NUM_WI_R_1.name()].value().int_val();
                    }
                }
                if (config[P_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                    size *= 1;
                }
                return size;
            };
    auto int_res_size =
            [&](atf::configuration &config) -> size_t {
                size_t size = res_g_l_size * config[NUM_WG_R_1.name()].value().int_val();
                return size;
            };
    auto needs_second_kernel =
            [&](atf::configuration &config) -> bool {
                return config[NUM_WG_R_1.name()].value().int_val() > 1;
            };
    auto gs_1 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 0) * (NUM_WG_R_1) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 1) * (NUM_WG_R_1) * (NUM_WI_R_1),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WG_L_1) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WG_L_2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WG_L_3) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WG_L_4) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_R_1 >= 2) * (NUM_WG_R_1) * (NUM_WI_R_1), 1)
    );
    auto ls_1 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_R_1 >= 2) * (NUM_WI_R_1), 1)
    );
    auto gs_2 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WG_L_1) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WG_L_2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WG_L_3) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WG_L_4) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_R_1 >= 2) * (NUM_WI_R_1), 1)
    );
    auto ls_2 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_R_1 >= 2) * (NUM_WI_R_1), 1)
    );
    atf::cf::process_wrapper_info process_wrapper = {
            atf::cf::NONE,
            "none",
            "none",
            atf::cf::CHECK_NONE,
            "wrap"
    };
    atf::cf::timeout warm_up_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    atf::cf::timeout evaluation_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    auto kernel = atf::cf::ocl_md_hom(
            device_info,
            {atf::cf::kernel_info::FILENAME, "kernel/multi_channel_convolution_1.cl", "multi_channel_convolution_1", " -DTYPE_T=float -DTYPE_TS=float"},
            atf::inputs(atf::buffer(in), atf::buffer(weights)),
            gs_1, ls_1,
            {atf::cf::kernel_info::FILENAME, "kernel/multi_channel_convolution_2.cl", "multi_channel_convolution_2"},
            atf::inputs(atf::buffer(out)),
            gs_2, ls_2,
            res_g_size,
            atf::buffer(int_res),
            int_res_size,
            needs_second_kernel,
            is_valid,
            1, 0, // 0 warm ups and 1 evaluation because proper measurement is done below
            true,
            process_wrapper,
            warm_up_timeout,
            evaluation_timeout
    );

    // measure runtime
    std::cout << "measuring runtime... " << std::endl;
    std::cout.setstate(std::ios_base::failbit);
    auto best_config = tuner(kernel);
    std::cout.clear();
    kernel.warm_ups(10);
    kernel.evaluations(200);
    std::vector<std::vector<unsigned long long>> runtimes;
    kernel(best_config, &runtimes);

    // write runtime to file
    unsigned long long runtime = 0;
    for (const auto &times : runtimes) {
        if (!times.empty())
            runtime += *std::min_element(times.begin(), times.end());
    }
    std::string runtime_file_name = config_file_dir;
    runtime_file_name.append("multi_channel_convolution_");
    if (N == 16 && K == 64 && P == 12 && Q == 120 && C == 32) {
        runtime_file_name.append("small");
    } else if (N == 8 && K == 64 && P == 54 && Q == 54 && C == 64) {
        runtime_file_name.append("large");
    } else {
        runtime_file_name.append(std::to_string(N)).append("x")
                .append(std::to_string(K)).append("x")
                .append(std::to_string(P)).append("x")
                .append(std::to_string(Q)).append("x")
                .append(std::to_string(C));
    }
    runtime_file_name.append("_runtime");
    std::ofstream runtime_file(runtime_file_name, std::ios::out | std::ios::trunc);
    runtime_file << (runtime / 1000000.0f);
    runtime_file.close();
}

int main(int argc, const char **argv) {
    // define and parse arguments
    ArgumentParser args;
    args.addArgument("--mode",        1, false);
    args.addArgument("--platform-id", 1, false);
    args.addArgument("--device-id",   1, false);
    args.addArgument("--input-size",  5, false);
    args.parse(static_cast<size_t>(argc), argv);
    std::string         mode        = args.retrieve_string("mode");
    size_t              platform_id = args.retrieve_size_t("platform-id");
    size_t              device_id   = args.retrieve_size_t("device-id");
    std::vector<size_t> input_size  = args.retrieve_size_t_vector("input-size");
    const size_t N = input_size[0], K = input_size[1], P = input_size[2], Q = input_size[3], C = input_size[4];

    // get device
    atf::cf::device_info device_info(static_cast<const int &>(platform_id), static_cast<const int &>(device_id));
    device_info.initialize();
    std::string device_type;
    cl_device_type type;
    atf::cf::check_error(device_info.device().getInfo(CL_DEVICE_TYPE, &type));
    switch (type) {
        case CL_DEVICE_TYPE_CPU:
            device_type = "cpu";
            break;
        case CL_DEVICE_TYPE_GPU:
            device_type = "gpu";
            break;
        case CL_DEVICE_TYPE_ACCELERATOR:
            device_type = "accelerator";
            break;
        case CL_DEVICE_TYPE_CUSTOM:
            device_type = "custom";
            break;
        default:
            std::cerr << "unknown device type." << std::endl;
            exit(EXIT_FAILURE);
    }

    // prepare inputs
    std::vector<float> in(N * C * (P + 2) * (Q + 2)); for (int i = 0; i < in.size(); ++i) in[i] = (i % 100) + 1;
    std::vector<struct WEIGHTS_33> weights(K * C * 3 * 3);
    int i = 1;
    for (int k = 0; k < K; ++k) {
        for (int c = 0; c < C; ++c) {
            for (int r = 0; r < 3; ++r) {
                for (int s = 0; s < 3; ++s) {
                    weights[k * C + c].values[r][s] = i++;
                    if (i == 100) i = 1;
                }
            }
        }
    }
    std::vector<float> out(N * K * P * Q); for (int i = 0; i < out.size(); ++i) out[i] = 0;

    if (mode == "tune") {
        tune(platform_id, device_id, device_info, device_type, N, K, P, Q, C, in, weights, out);
    } else if (mode == "bench") {
        bench(platform_id, device_id, device_info, device_type, N, K, P, Q, C, in, weights, out);
    } else {
        std::cerr << "unknown mode! possible values: tune, bench" << std::endl;
        exit(EXIT_FAILURE);
    }
}