// This file is part of the Auto Tuning Framework (ATF).


// (c) Dominique Bönninghoff 2016
// This file was neither created nor modified during the PJS, and thus retains
// its original license.

// MIT License, Copyright 2016 Dominique Bönninghoff

#include "platform.hxx"

#ifdef ATFC_IS_POSIX

#include "console_color.hxx"
#include <stdexcept>
#include <type_traits>
#include <unistd.h>

namespace atfc {
namespace internal {
void set_style(std::ostream &p_stream, console_style p_style) {
  // The style value is directly encoded in the enumeration.
  p_stream << "\033[" << static_cast<int>(p_style) << "m";
}

void set_color(std::ostream &p_stream, console_color p_clr,
               bool p_isBack = false) {
  static const char *map[17][2]{/* RED */ {"\033[31m", "\033[41m"},
                                /* BRED */ {"\033[31;1m", "\033[41;1m"},

                                /* GREEN */ {"\033[32m", "\033[42m"},
                                /* BGREEN */ {"\033[32;1m", "\033[42;1m"},

                                /* YELLOW */ {"\033[33m", "\033[43m"},
                                /* BYELLOW */ {"\033[33;1m", "\033[43;1m"},

                                /* BLUE */ {"\033[34m", "\033[44m"},
                                /* BBLUE */ {"\033[34;1m", "\033[44;1m"},

                                /* MAGENTA */ {"\033[35m", "\033[45m"},
                                /* BMAGENTA */ {"\033[35;1m", "\033[45;1m"},

                                /* CYAN */ {"\033[36m", "\033[46m"},
                                /* BCYAN */ {"\033[36;1m", "\033[46;1m"},

                                /* WHITE */ {"\033[37m", "\033[47m"},
                                /* BWHITE */ {"\033[37;1m", "\033[47;1m"},

                                /* BLACK */ {"\033[30m", "\033[40m"},
                                /* BBLACK */ {"\033[30;1m", "\033[40;1m"},

                                /* RESET */ {"\033[00m", "\033[00m"}};

  // Everything is BRIGHT for now
  // p_stream << "\033[1m";

  p_stream << map[static_cast<int>(p_clr)][p_isBack ? 1 : 0];
}

bool is_terminal(std::ostream &p_stream) {
  // TODO Dirty hack
  FILE *str;

  if (&p_stream == &std::cout)
    str = stdout;
  else if (&p_stream == &std::cerr)
    str = stderr;
  else
    return false;

  return isatty(fileno(str)) != 0;
}
} // namespace internal

color_manipulator foreground(console_color p_clr) {
  return color_manipulator(p_clr, false);
}

color_manipulator background(console_color p_clr) {
  return color_manipulator(p_clr, true);
}

style_manipulator style(console_style p_style) {
  return style_manipulator(p_style);
}

std::ostream &operator<<(std::ostream &p_stream,
                         const style_manipulator &p_manip) {
  if (internal::is_terminal(p_stream)) {
    internal::set_style(p_stream, p_manip.style());
  }
  return p_stream;
}

std::ostream &operator<<(std::ostream &p_stream,
                         const color_manipulator &p_manip) {
  if (internal::is_terminal(p_stream)) {
    internal::set_color(p_stream, p_manip.color(), p_manip.is_background());
  }
  return p_stream;
}

std::ostream &reset_color(std::ostream &p_stream) {
  if (internal::is_terminal(p_stream)) {
    internal::set_color(p_stream, console_color::reset);
  }
  return p_stream;
}
} // namespace atfc

#endif
