// This file is part of the Auto Tuning Framework (ATF).

#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <ostream>
#include <string>
#include <tuple>
#include <vector>

#include <code_generator.hxx>
#include <diagnostics.hxx>
#include <grouping.hxx>

namespace atfc {
code_generator::code_generator(const job &p_job) : m_Job{p_job} {
  this->m_NoConstraints = ::std::all_of(
      this->m_Job.m_TuningParams.begin(), this->m_Job.m_TuningParams.end(),
      [](const auto &t_tp) -> bool { return t_tp.m_Constraints.empty(); });
}

void code_generator::generate_code(::std::ostream &p_stream) {
  this->insert_header(p_stream);
  this->insert_vars(p_stream);
  this->insert_tps(p_stream);
  this->insert_cf(p_stream);
  this->insert_tune(p_stream);
  this->insert_source_gen(p_stream);
  this->insert_json_gen(p_stream);
  this->insert_footer(p_stream);
}

void code_generator::generate_code(const ::std::string &p_path) {
  ::std::ofstream t_file{};
  t_file.open(p_path, ::std::ofstream::out | ::std::ofstream::trunc);

  this->generate_code(t_file);
}

bool code_generator::is_ocl_mode() const {
  return this->m_Job.m_Mode == mode::ocl;
}

bool code_generator::is_cuda_mode() const {
  return this->m_Job.m_Mode == mode::cuda;
}

void code_generator::insert_header(::std::ostream &p_stream) {
  p_stream << "#include <cstdlib>" << ::std::endl
           << "#include <cmath>" << ::std::endl
           << "#include <sstream>" << ::std::endl
           << "#include <string>" << ::std::endl
           << "#include <fstream>" << ::std::endl
           << "#include <thirdparty/json.hpp>" << ::std::endl
           << "#include <atf.h>" << ::std::endl
           << "using namespace nlohmann;" << ::std::endl;

  this->insert_convert_func(p_stream);
  this->insert_check_func(p_stream);

  if (this->is_ocl_mode() || this->is_cuda_mode())
    this->insert_kernel_read_func(p_stream);

  if (this->m_Job.m_OutputJson)
    this->insert_json_func(p_stream);

  p_stream << "int main(int argc, char** argv){" << ::std::endl;
}

void code_generator::insert_kernel_read_func(::std::ostream &p_stream) {
  p_stream << "::std::string read_kernel(const ::std::string& p_path) {"
           << ::std::endl
           << "\t::std::ifstream t_file{ p_path }; ::std::string t_kernel; "
              "bool t_isMultiLineDirective = false;"
           << ::std::endl
           << "\tfor(::std::string t_line; ::std::getline(t_file, t_line);) {"
           << ::std::endl
           << "\t\tif(t_isMultiLineDirective) {" << ::std::endl
           << "\t\t\tif(*(::std::prev(t_line.end(), 1)) == '\\\\') continue;"
           << ::std::endl
           << "\t\t\telse { t_isMultiLineDirective = false; continue; }"
           << ::std::endl
           << "\t\t}" << ::std::endl
           << "\t\tif(t_line.find(\"#atf::\") != 0) {" << ::std::endl
           << "\t\t\tt_kernel.append(t_line); t_kernel.append(\"\\n\");"
           << ::std::endl
           << "\t\t}" << ::std::endl
           << "\t\telse {" << ::std::endl
           << "\t\t\tif(*(::std::prev(t_line.end(), 1)) == '\\\\') { "
              "t_isMultiLineDirective = true; }"
           << ::std::endl
           << "\t\t}" << ::std::endl
           << "\t}" << ::std::endl
           << "\treturn t_kernel;" << ::std::endl
           << "}" << ::std::endl;
}

void code_generator::insert_json_func(::std::ostream &p_stream) {
  p_stream << "void emit_json(const atf::configuration& p_cfg, const "
              "::std::string& p_path){"
           << ::std::endl
           << "\tjson t_obj; for(const auto& t_tp: p_cfg){ t_obj[t_tp.first] = "
              "t_tp.second.value(); }"
           << ::std::endl
           << "\t::std::ofstream t_fs{p_path}; t_fs << t_obj.dump(4);"
           << ::std::endl
           << "}" << ::std::endl;
}

void code_generator::insert_check_func(::std::ostream &p_stream) {
  p_stream << "void check(int p_argc, int p_pos){" << ::std::endl
           << "\tif(p_pos >= p_argc) {" << ::std::endl
           << "\t\t::std::cout << \"tuner: error: Tried to access non-existing "
              "command line argument argv[\" << p_pos << \"]\" << ::std::endl;"
           << ::std::endl
           << "\t\t::std::exit(EXIT_FAILURE);" << ::std::endl
           << "\t}" << ::std::endl
           << "}" << ::std::endl;
}

void code_generator::insert_convert_func(::std::ostream &p_stream) {
  p_stream << "template<typename T> T convert(const ::std::string& p_str) {"
           << ::std::endl
           << "\t::std::stringstream t_ss; t_ss << p_str; T t_val{}; t_ss >> "
              "t_val; return t_val;"
           << ::std::endl
           << "}" << ::std::endl;
}

void code_generator::insert_convert_call(::std::ostream &p_stream,
                                         const var_type &p_var) {
  p_stream << "const auto " << p_var.m_Name << " = convert<" << p_var.m_Type
           << ">(";

  if (p_var.m_UsesArgV)
    p_stream << "argv[" << p_var.m_ArgPos << "]";
  else
    p_stream << ::std::quoted(p_var.m_Value);

  p_stream << ");" << ::std::endl;
}

void code_generator::insert_vars(::std::ostream &p_stream) {
  for (const auto &t_var : this->m_Job.m_Variables)
    this->insert_var(p_stream, t_var);
}

void code_generator::insert_var(::std::ostream &p_stream,
                                const var_type &p_var) {
  // Check if we need to acces command line parameters
  if (p_var.m_UsesArgV) {
    this->insert_argv_check(p_stream, p_var);
  }

  this->insert_convert_call(p_stream, p_var);
}

void code_generator::insert_argv_check(::std::ostream &p_stream,
                                       const var_type &p_var) {
  p_stream << "check(argc, " << p_var.m_ArgPos << ");" << ::std::endl;
}

void code_generator::insert_footer(::std::ostream &p_stream) {
  p_stream << "}" << std::endl;
}

void code_generator::insert_source_gen(::std::ostream &p_stream) {
  if (this->m_Job.m_OutputSrc) {
    if (this->m_Job.m_OutSrcPath.empty()) {
      post_diagnostic(
          message_type::error, "atfc",
          "File path for best configuration source file cannot be empty");
      ::std::exit(EXIT_FAILURE);
    }

    p_stream << "cf.write_source(best, \"" << this->m_Job.m_OutSrcPath << "\");"
             << ::std::endl;
  }
}

void code_generator::insert_tps(::std::ostream &p_stream) {
  for (const auto &t_tp : this->m_Job.m_TuningParams) {
    this->insert_tp(p_stream, t_tp);
  }
}

void code_generator::insert_tp(::std::ostream &p_stream, const tp_type &p_tp) {
  if (p_tp.m_Name.empty()) {
    post_diagnostic(message_type::error, "atfc",
                    "Tuning parameter name cannot be empty");
    ::std::exit(EXIT_FAILURE);
  }

  if (p_tp.m_Range.empty()) {
    post_diagnostic(message_type::error, "atfc",
                    "Tuning parameter range cannot be empty");
    ::std::exit(EXIT_FAILURE);
  }

  p_stream << "auto " << p_tp.m_Name << " = atf::tp(\"" << p_tp.m_Name << "\", "
           << p_tp.m_Range;

  if (!p_tp.m_Constraints.empty()) {
    p_stream << ", " << p_tp.m_Constraints;
  }

  p_stream << ");" << ::std::endl;
}

void code_generator::insert_cf_base(::std::ostream &p_stream) {
  if (this->m_Job.m_SourceFiles.size() == 0) {
    post_diagnostic(message_type::error, "atfc", "No source files supplied");
    ::std::exit(EXIT_FAILURE);
  }

  if (this->m_Job.m_RunScript.empty()) {
    post_diagnostic(message_type::error, "atfc", "No run script supplied");
    ::std::exit(EXIT_FAILURE);
  }

  p_stream << "auto cf = atf::cf::ccfg(\"" << this->m_Job.m_SourceFiles[0]
           << "\", ";

  // Check if we need to use a compilescript
  if (!this->m_Job.m_CompileScript.empty()) {
    p_stream << " ::std::make_pair(\"" << this->m_Job.m_CompileScript
             << "\", \"" << this->m_Job.m_RunScript << "\"),";
  } else {
    p_stream << "\"" << this->m_Job.m_RunScript << "\",";
  }

  // Check if should use a costfile
  if (!this->m_Job.m_CostFile.empty()) {
    p_stream << "true, \"" << this->m_Job.m_CostFile << "\"";
  } else {
    p_stream << "false";
  }

  p_stream << ");" << ::std::endl;
}

void code_generator::insert_cf(::std::ostream &p_stream) {
  switch (this->m_Job.m_Mode) {
  case mode::base: {
    this->insert_cf_base(p_stream);
    break;
  }
  case mode::ocl: {
    this->insert_cf_ocl(p_stream);
    break;
  }
  case mode::cuda: {
    this->insert_cf_cuda(p_stream);
    break;
  }
  default: { post_ice("atfc", "code_generator::insert_cf: not implemented"); }
  }
}

void code_generator::insert_cf_ocl(::std::ostream &p_stream) {
  const auto &t_data = this->m_Job.m_OclData;

  const auto t_inputCheck = [&t_data](const ::std::string &p_str,
                                      const ::std::string &p_msg) -> void {
    if (p_str.empty()) {
      post_diagnostic(message_type::error, "atfc", {p_msg});
      ::std::exit(EXIT_FAILURE);
    }
  };

  t_inputCheck(t_data.m_KernelName, "No kernel name specified");
  t_inputCheck(t_data.m_Gs, "No global size specified");
  t_inputCheck(t_data.m_Ls, "No local size specified");

  if (!t_data.m_DeviceInfo.m_UseIds) {
    t_inputCheck(t_data.m_DeviceInfo.m_Vendor, "No device vendor specified");
    t_inputCheck(t_data.m_DeviceInfo.m_Type, "No device type specified");
  }

  if (t_data.m_Inputs.empty()) {
    post_diagnostic(message_type::error, "atfc",
                    "OpenCL kernel inputs cannot be empty");
    ::std::exit(EXIT_FAILURE);
  }

  if (this->m_Job.m_SourceFiles.empty()) {
    post_diagnostic(message_type::error, "atfc", "No source files supplied");
    ::std::exit(EXIT_FAILURE);
  }

  p_stream << "auto cf = atf::cf::ocl(" << ::std::endl;

  if (t_data.m_DeviceInfo.m_UseIds) {
    p_stream << "\t\t{ " << t_data.m_DeviceInfo.m_PlatformId << ", "
             << t_data.m_DeviceInfo.m_DeviceId << " }," << ::std::endl;
  } else {
    p_stream << "\t\t{\"" << t_data.m_DeviceInfo.m_Vendor << "\","
             << ::std::endl
             << "\t\t atf::cf::device_info::" << t_data.m_DeviceInfo.m_Type
             << "," << ::std::endl
             << "\t\t " << t_data.m_DeviceInfo.m_Id << "}," << ::std::endl;
  }

  p_stream << "\t\t{read_kernel(\"" << this->m_Job.m_SourceFiles.at(0)
           << "\"), \"" << t_data.m_KernelName << "\"}," << ::std::endl
           << "\t\tinputs(";

  bool t_first{true};
  for (const auto &t_input : t_data.m_Inputs) {
    if (t_first)
      t_first = false;
    else {
      p_stream << ", ";
    }

    p_stream << t_input;
  }

  p_stream << ")," << ::std::endl
           << "\t\tatf::cf::GS(" << t_data.m_Gs << ")," << ::std::endl
           << "\t\tatf::cf::LS(" << t_data.m_Ls << ")" << ::std::endl
           << ");" << ::std::endl;
}

void code_generator::insert_cf_cuda(::std::ostream &p_stream) {
  const auto &t_data = this->m_Job.m_CudaData;

  const auto t_inputCheck = [&t_data](const ::std::string &p_str,
                                      const ::std::string &p_msg) -> void {
    if (p_str.empty()) {
      post_diagnostic(message_type::error, "atfc", {p_msg});
      ::std::exit(EXIT_FAILURE);
    }
  };

  t_inputCheck(t_data.m_KernelName, "No kernel name specified");
  t_inputCheck(t_data.m_Gd, "No grid dimensions specified");
  t_inputCheck(t_data.m_Bd, "No block size specified");

  if (t_data.m_Inputs.empty()) {
    post_diagnostic(message_type::error, "atfc",
                    "CUDA kernel inputs cannot be empty");
    ::std::exit(EXIT_FAILURE);
  }

  if (this->m_Job.m_SourceFiles.empty()) {
    post_diagnostic(message_type::error, "atfc", "No source files supplied");
    ::std::exit(EXIT_FAILURE);
  }

  p_stream << "auto cf = atf::cf::cuda(" << ::std::endl
           << "\t\t atf::cf::cuda_device_id{" << t_data.m_Id << "},"
           << ::std::endl
           << "\t\t{read_kernel(\"" << this->m_Job.m_SourceFiles.at(0)
           << "\"), \"" << t_data.m_KernelName << "\"}," << ::std::endl
           << "\t\tinputs(";

  bool t_first{true};
  for (const auto &t_input : t_data.m_Inputs) {
    if (t_first)
      t_first = false;
    else {
      p_stream << ", ";
    }

    p_stream << t_input;
  }

  p_stream << ")," << ::std::endl
           << "\t\tatf::cf::grid_dim(" << t_data.m_Gd << ")," << ::std::endl
           << "\t\tatf::cf::block_dim(" << t_data.m_Bd << ")" << ::std::endl
           << ");" << ::std::endl;
}

void code_generator::insert_json_gen(::std::ostream &p_stream) {
  if (this->m_Job.m_OutputJson) {
    if (this->m_Job.m_OutJsonPath.empty()) {
      post_diagnostic(
          message_type::error, "atfc",
          "File path for best configuration JSON file cannot be empty");
      ::std::exit(EXIT_FAILURE);
    }

    p_stream << "emit_json(best, \"" << this->m_Job.m_OutJsonPath << "\");"
             << ::std::endl;
  }
}

void code_generator::insert_tune(::std::ostream &p_stream) {
  if (this->m_Job.m_SearchStrategy.empty()) {
    post_diagnostic(message_type::error, "atfc",
                    "No search strategy specified");
    ::std::exit(EXIT_FAILURE);
  }

  p_stream << "auto best = " << this->m_Job.m_SearchStrategy;

  // If none of the tuning parameters have any constraints, we can use the
  // NO_CONSTRAINTS hint
  if (this->m_NoConstraints) {
    p_stream << "<NO_CONSTRAINTS>";
  }

  p_stream << "(" << this->m_Job.m_AbortCondition << ")(";

  // If we are using a tuner without constraints, the grouping function cannot
  // be used.
  if (this->m_NoConstraints) {
    bool t_first{true};

    for (const auto &t_tp : this->m_Job.m_TuningParams) {
      if (t_first)
        t_first = false;
      else
        p_stream << ", ";

      p_stream << t_tp.m_Name;
    }
  } else {
    auto t_groups = group(this->m_Job);

    bool t_firstOuter{true};
    for (const auto &t_group : t_groups) {
      if (t_firstOuter)
        t_firstOuter = false;
      else
        p_stream << ", ";

      p_stream << "G(";

      bool t_firstInner{true};
      for (const auto &t_tp : t_group) {
        if (t_firstInner)
          t_firstInner = false;
        else
          p_stream << ", ";

        p_stream << t_tp;
      }

      p_stream << ")";
    }
  }

  p_stream << ")(cf);" << ::std::endl;
}
} // namespace atfc
