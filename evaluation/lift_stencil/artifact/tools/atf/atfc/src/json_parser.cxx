// This file is part of the Auto Tuning Framework (ATF).


#include <boost/filesystem.hpp>
#include <diagnostics.hxx>
#include <job.hxx>
#include <json_parser.hxx>
#include <stdexcept>
#include <thirdparty/json.hpp>
#include <unordered_set>

using namespace ::std::literals::string_literals;
using namespace nlohmann;
namespace fs = boost::filesystem;

namespace atfc {
namespace detail {
template <typename T> auto load_req(const json &, const ::std::string &) -> T {
  static_assert(sizeof(T) == 0, "Not implemented");
  return {};
}

template <>
auto load_req<::std::string>(const json &p_json, const ::std::string &p_key)
    -> ::std::string {
  // Try to retrieve object
  try {
    ::std::string t_val = p_json.at(p_key);

    if (t_val.empty())
      throw ::std::runtime_error("value \""s + p_key +
                                 "\" is not allowed to be empty");
    else
      return t_val;
  } catch (...) {
    const auto t_msg =
        ::std::string{"missing required value \""} + p_key + "\"";
    throw ::std::runtime_error(t_msg);
  }
}

template <>
auto load_req<int>(const json &p_json, const ::std::string &p_key) -> int {
  // Try to retrieve object
  try {
    return p_json.at(p_key);
  } catch (...) {
    const auto t_msg =
        ::std::string{"missing required value \""} + p_key + "\"";
    throw ::std::runtime_error(t_msg);
  }
}

auto load_config(job &p_job, const json &p_json) -> void {
  // TODO remove load_req, just use at()
  // Required entries
  const ::std::string t_src = load_req<::std::string>(p_json, "source_file");
  p_job.m_SourceFiles.push_back(t_src);
  p_job.m_SearchStrategy = load_req<::std::string>(p_json, "search_strategy");

  // Optional entries
  p_job.m_CompileScript = p_json.value("compile_script", ""s);
  p_job.m_AbortCondition = p_json.value("abort_condition", ""s);
  p_job.m_RunScript = p_json.value("run_script", ""s);
  p_job.m_CostFile = p_json.value("cost_file", ""s);
}

auto load_tp(const json &p_json) -> job::tp_type {
  tp t_tp{};

  // Name is required
  t_tp.m_Name = load_req<::std::string>(p_json, "name");

  // Handle constraint
  if (p_json.find("divides") != p_json.end()) {
    const ::std::string t_val = p_json.at("divides");
    t_tp.m_Constraints = "atf::divides("s + t_val + ")";
  }

  if (p_json.find("constraints") != p_json.end()) {
    const ::std::string t_val = p_json.at("constraints");
    t_tp.m_Constraints += " && ";
    t_tp.m_Constraints += t_val;
  }

  // Handle interval
  if (p_json.find("interval") == p_json.end())
    throw ::std::runtime_error("no range given for tuning parameter \""s +
                               t_tp.m_Name + "\"");

  {
    const auto t_interval = p_json.at("interval");

    // All members are required
    t_tp.m_Type = load_req<::std::string>(t_interval, "type");
    const auto t_from = load_req<::std::string>(t_interval, "from");
    const auto t_to = load_req<::std::string>(t_interval, "to");

    t_tp.m_Range =
        "atf::interval<"s + t_tp.m_Type + ">(" + t_from + ", " + t_to + ")";
  }

  return t_tp;
}

auto load_tps(job &p_job, const json &p_json) -> void {
  // If there is no child entry, no TPs are supplied
  if (p_json.find("tuning_parameters") == p_json.end())
    return;

  // Retrieve array entry
  auto t_array = p_json.at("tuning_parameters");

  // Check that it is indeed an array
  if (!t_array.is_array())
    throw ::std::runtime_error("tuning parameter subobject is not an array");

  // Iterate through all entries
  for (const auto &t_entry : t_array) {
    if (!t_entry.is_object())
      throw ::std::runtime_error("expected array of tuning parameter objects");

    p_job.m_TuningParams.push_back(load_tp(t_entry));
  }
}

auto load_var(const json &p_json) -> job::var_type {
  static ::std::unordered_set<::std::string> t_types{"int", "size_t"};

  job::var_type t_var{};

  t_var.m_Name = load_req<::std::string>(p_json, "name");

  t_var.m_Type = load_req<::std::string>(p_json, "type");

  if (t_types.count(t_var.m_Type) == 0) {
    ::std::ostringstream t_ss{};
    t_ss << "given variable type " << ::std::quoted(t_var.m_Type)
         << " is not allowed";

    throw ::std::runtime_error(t_ss.str());
  }

  // Check if there is an value entry
  if (p_json.find("value") != p_json.end()) {
    const auto t_valueNode = p_json.at("value");

    if (t_valueNode.is_string())
      t_var.m_Value = t_valueNode;
    else
      throw ::std::runtime_error(
          "variable value node needs to be a string value");
  }

  // Check if there is an argv index entry
  if (p_json.find("argv") != p_json.end()) {
    const auto t_argvNode = p_json.at("argv");

    if (t_argvNode.is_number_unsigned()) {
      t_var.m_ArgPos = t_argvNode;
      t_var.m_UsesArgV = true;

      if (t_var.m_ArgPos == 0)
        throw ::std::runtime_error("variable argv index can't be 0");
    } else
      throw ::std::runtime_error(
          "variable argv node needs to be a unsigned integer value");
  }

  if (t_var.m_Value.empty() && !t_var.m_UsesArgV)
    throw ::std::runtime_error("variable has neither value nor argv index");

  if (!t_var.m_Value.empty() && t_var.m_UsesArgV)
    throw ::std::runtime_error("variable has both value and argv index");

  return t_var;
}

auto load_vars(job &p_job, const json &p_json) -> void {
  // If there is no child entry, no vars are supplied
  if (p_json.find("variables") == p_json.end())
    return;

  // Retrieve it
  const auto t_array = p_json.at("variables");

  // It needs to be an array
  if (!t_array.is_array())
    throw ::std::runtime_error("variable subobject is not an array");

  // Iterate through all entries
  for (const auto &t_entry : t_array) {
    if (!t_entry.is_object())
      throw ::std::runtime_error("expected array of variable objects");

    p_job.m_Variables.push_back(load_var(t_entry));
  }
}

auto load_ocl(job &p_job, const json &p_json) -> void {
  // Needs to be object
  if (!p_json.is_object())
    throw ::std::runtime_error("expected opencl entry to be an object");

  ocl_data t_ocl{};

  t_ocl.m_KernelName = load_req<::std::string>(p_json, "kernel_name");
  t_ocl.m_Gs = load_req<::std::string>(p_json, "gs");
  t_ocl.m_Ls = load_req<::std::string>(p_json, "ls");

  // Try to parse device info
  if (p_json.find("device_info") == p_json.end())
    throw ::std::runtime_error("expected device_info structure");

  const auto t_dientry = p_json.at("device_info");

  if (!t_dientry.is_object())
    throw ::std::runtime_error("device_info entry needs to be an object");

  ocl_device_info t_di{};
  t_di.m_Vendor = load_req<::std::string>(t_dientry, "vendor");
  t_di.m_Type = load_req<::std::string>(t_dientry, "type");
  t_di.m_Id = load_req<int>(t_dientry, "id");
  t_ocl.m_DeviceInfo = t_di;

  // Parse input
  if (p_json.find("inputs") == p_json.end())
    throw ::std::runtime_error("expected input array");

  const auto t_inputs = p_json.at("inputs");

  if (!t_inputs.is_array())
    throw ::std::runtime_error("expected input array");

  for (const auto &t_input : t_inputs) {
    if (!t_input.is_string())
      throw ::std::runtime_error("Inputs have to be strings");

    ::std::string t_ip = t_input;
    t_ocl.m_Inputs.push_back(t_ip);
  }

  p_job.m_OclData = t_ocl;
  p_job.m_Mode = mode::ocl;
}
} // namespace detail

auto parse_json(const ::std::string &p_path) -> job {
  // Extract file name from (possible) path for diagnostics
  const auto t_file = fs::path{p_path}.filename().string();

  // Job object to be filled
  job t_job{};

  try {
    // Load json object
    json t_json{};
    ::std::ifstream t_file{p_path};
    t_file >> t_json;

    // Try to read atf configuration
    detail::load_config(t_job, t_json);

    // Try to retrieve tuning parameter array.
    // If it is not there, there are no tps!
    detail::load_tps(t_job, t_json);

    // Try to parse variables
    detail::load_vars(t_job, t_json);

    // Check if there is openCl info
    if (t_json.find("opencl") != t_json.end())
      detail::load_ocl(t_job, t_json.at("opencl"));
  } catch (const ::std::exception &p_ex) {
    post_diagnostic(message_type::error, {t_file}, {p_ex.what()});
    throw ::std::runtime_error("");
  }

  return t_job;
}
} // namespace atfc
