// This file is part of the Auto Tuning Framework (ATF).


// console_color: Support for colored output to stdout/cout

// (c) Dominique Bönninghoff 2016
// This file was neither created nor modified during the PJS, and thus retains
// its original license.

// MIT License, Copyright 2016 Dominique Bönninghoff

#pragma once

#include <iostream>

namespace atfc {
// Console color enumeration
enum class console_color {
  red,
  bright_red,

  green,
  bright_green,

  yellow,
  bright_yellow,

  blue,
  bright_blue,

  magenta,
  bright_magenta,

  cyan,
  bright_cyan,

  white,
  bright_white,

  black,
  bright_black,

  reset
};

enum class console_style {
  bold = 1,
  dim,
  italic,
  underline,
  blink,
  reversed,
  conceal,
  crossed
};

class color_manipulator {
public:
  color_manipulator(console_color p_clr, bool p_isBackground)
      : m_Clr(p_clr), m_IsBackground(p_isBackground) {}

public:
  console_color color() const { return m_Clr; }

  bool is_background() const { return m_IsBackground; }

private:
  console_color m_Clr;
  bool m_IsBackground;
};

class style_manipulator {
public:
  style_manipulator(console_style p_sty) : m_Style(p_sty) {}

public:
  console_style style() const { return m_Style; }

private:
  console_style m_Style;
};

color_manipulator foreground(console_color p_clr);

color_manipulator background(console_color p_clr);

style_manipulator style(console_style p_style);

::std::ostream &operator<<(::std::ostream &p_stream,
                           const color_manipulator &p_manip);
::std::ostream &operator<<(::std::ostream &p_stream,
                           const style_manipulator &p_manip);

::std::ostream &reset_color(::std::ostream &p_stream);
} // namespace atfc
