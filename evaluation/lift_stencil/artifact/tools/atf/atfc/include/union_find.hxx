// This file is part of the Auto Tuning Framework (ATF).


#pragma once
#include <algorithm>
#include <functional>
#include <iterator>
#include <limits>
#include <unordered_map>
#include <utility>
#include <vector>

#include <iostream>
#include <tuple>
#include <utility>

namespace atfc {
template <typename T, typename THash = ::std::hash<T>> class union_find;

namespace internal {
template <typename T, typename THash = ::std::hash<T>> class union_find_it;

template <typename T, typename THash = ::std::hash<T>>
class const_union_find_it;

template <typename T, typename THash>
bool operator==(const union_find_it<T, THash> &,
                const union_find_it<T, THash> &);

template <typename T, typename THash>
bool operator==(const const_union_find_it<T, THash> &,
                const const_union_find_it<T, THash> &);

template <typename T, typename THash>
bool operator!=(const union_find_it<T, THash> &,
                const union_find_it<T, THash> &);

template <typename T, typename THash>
bool operator!=(const const_union_find_it<T, THash> &,
                const const_union_find_it<T, THash> &);

template <typename T, typename THash> class union_find_it {
private:
  // private typedefs
  using parent_type = atfc::union_find<T, THash>;
  using macro_it_type = typename parent_type::bucket_container_type::iterator;
  using micro_it_type = typename parent_type::bucket_type::iterator;
  using bucket_id_type = typename parent_type::bucket_id_type;

  static constexpr const bucket_id_type macro_npos =
      ::std::numeric_limits<bucket_id_type>::max();

  // members
  bucket_id_type m_bucket_id;
  macro_it_type m_macro_it;
  micro_it_type m_micro_it;
  macro_it_type m_macro_end;
  macro_it_type m_macro_begin;

public:
  // public typedefs
  using value_type = typename micro_it_type::value_type;
  using reference = typename micro_it_type::reference;
  using pointer = typename micro_it_type::pointer;
  using TThis = union_find_it<T, THash>;
  using iterator_category = ::std::bidirectional_iterator_tag;
  using size_type = typename parent_type::size_type;
  using difference_type = typename micro_it_type::difference_type;

  // constr/destr
  union_find_it() = default;
  union_find_it(const TThis &) = default;
  union_find_it(TThis &&) = default;
  TThis &operator=(const TThis &) = default;
  TThis &operator=(TThis &&) = default;

  // construct iterator spanning given bucket range, where id of first bucket is
  // equal to third parameter.
  union_find_it(macro_it_type, macro_it_type, bucket_id_type);

  // interface
  reference operator*();
  micro_it_type operator->();
  TThis &operator++();
  TThis &operator++(int);
  TThis &operator--();
  TThis &operator--(int);

  bucket_id_type bucket_id() const;
  void swap(TThis &);

  // friends
  friend bool operator!=<T, THash>(const TThis &, const TThis &);
  friend bool operator==<T, THash>(const TThis &, const TThis &);
  // friend class union_find <T, THash>;
}; // class union_find_it

/*
        union_find_it impl begin
   -----------------------------------------------------------------------------
*/
template <typename T, typename THash>
union_find_it<T, THash>::union_find_it(macro_it_type p_macro_begin,
                                       macro_it_type p_macro_end,
                                       bucket_id_type p_bucket_id)
    : m_macro_begin(p_macro_begin), m_macro_end(p_macro_end),
      m_bucket_id(p_bucket_id),
      m_macro_it(::std::next(p_macro_begin, p_bucket_id)) {
  m_micro_it =
      (m_macro_it == m_macro_end) ? micro_it_type{} : m_macro_it->begin();
}

template <typename T, typename THash>
auto union_find_it<T, THash>::operator*() -> reference {
  return *m_micro_it;
}

template <typename T, typename THash>
auto union_find_it<T, THash>::operator-> () -> micro_it_type {
  return m_micro_it;
}

template <typename T, typename THash>
auto union_find_it<T, THash>::operator++() -> TThis & {
  ++m_micro_it;
  // skip to next bucket if necessary
  while (m_micro_it == m_macro_it->end()) {
    if (m_macro_it == m_macro_end) {
      m_micro_it = micro_it_type{};
      return *this;
    }
    ++m_macro_it;
    ++m_bucket_id;
    m_micro_it = m_macro_it->begin();
  }
  return *this;
}

template <typename T, typename THash>
auto union_find_it<T, THash>::operator++(int) -> TThis & {
  auto copy = *this;
  ++m_micro_it;
  // skip to next bucket if necessary
  while (m_micro_it == m_macro_it->end()) {
    if (m_macro_it == m_macro_end) {
      m_micro_it = micro_it_type{};
      return copy;
    }
    ++m_macro_it;
    ++m_bucket_id;
    m_micro_it = m_macro_it->begin();
  }
  return copy;
}

template <typename T, typename THash>
auto union_find_it<T, THash>::operator--() -> TThis & {
  // in case we're off-the-end
  if (m_macro_it == m_macro_end || m_micro_it == m_macro_it->begin()) {
    do {
      --m_macro_it;
      --m_bucket_id;
    } // decrementing in empty container is not meaningful, but should not crash
      // either
    while (m_macro_it != m_macro_begin && m_macro_it->empty());
    m_micro_it = ::std::prev(m_macro_it->end());
    return *this;
  } else {
    --m_micro_it;
    return *this;
  }
}

template <typename T, typename THash>
auto union_find_it<T, THash>::operator--(int) -> TThis & {
  auto copy = *this;
  // return to first bucket with elements
  // in case we're off-the-end
  if (m_macro_it == m_macro_end || m_micro_it == m_macro_it->begin()) {
    do {
      --m_macro_it;
      --m_bucket_id;
    } // decrementing in empty container is not meaningful, but should not crash
      // either
    while (m_macro_it != m_macro_begin && m_macro_it->empty());
    m_micro_it = ::std::prev(m_macro_it->end());
    return copy;
  } else {
    --m_micro_it;
    return copy;
  }
}

template <typename T, typename THash>
auto union_find_it<T, THash>::bucket_id() const -> bucket_id_type {
  return (m_macro_it == m_macro_end) ? macro_npos : m_bucket_id;
}

template <typename T, typename THash>
auto union_find_it<T, THash>::swap(union_find_it<T, THash> &p_other) -> void {
  using ::std::swap;
  swap(m_bucket_id, p_other.m_bucket_id);
  swap(m_macro_begin, p_other.m_macro_begin);
  swap(m_macro_end, p_other.m_macro_end);
  swap(m_micro_it, p_other.m_micro_it);
  swap(m_macro_it, p_other.m_macro_it);
}

// non-member operators
template <typename T, typename THash>
auto operator==(const union_find_it<T, THash> &p_lhs,
                const union_find_it<T, THash> &p_rhs) -> bool {
  if (p_lhs.bucket_id() == union_find_it<T>::macro_npos &&
      p_rhs.bucket_id() == union_find_it<T>::macro_npos) {
    return true;
  } else {
    return (p_lhs.m_macro_it == p_rhs.m_macro_it) &&
           (p_lhs.m_micro_it == p_rhs.m_micro_it) &&
           (p_lhs.m_bucket_id == p_rhs.m_bucket_id);
  }
}

template <typename T, typename THash>
auto operator!=(const union_find_it<T, THash> &p_lhs,
                const union_find_it<T, THash> &p_rhs) -> bool {
  if (p_lhs.bucket_id() == union_find_it<T>::macro_npos &&
      p_rhs.bucket_id() == union_find_it<T>::macro_npos) {
    return false;
  }
  return (p_lhs.m_macro_it != p_rhs.m_macro_it) ||
         (p_lhs.m_micro_it != p_rhs.m_micro_it) ||
         (p_lhs.m_bucket_id != p_rhs.m_bucket_id);
}
/*
        union_find_it impl end
   -------------------------------------------------------------------
*/

template <typename T, typename THash> class const_union_find_it {
private:
  // private typedefs
  using parent_type = atfc::union_find<T, THash>;
  using macro_it_type =
      typename parent_type::bucket_container_type::const_iterator;
  using micro_it_type = typename parent_type::bucket_type::const_iterator;
  using bucket_id_type = typename parent_type::bucket_id_type;

  static constexpr const bucket_id_type macro_npos =
      ::std::numeric_limits<bucket_id_type>::max();

  // members
  bucket_id_type m_bucket_id;
  macro_it_type m_macro_it;
  micro_it_type m_micro_it;
  macro_it_type m_macro_end;
  macro_it_type m_macro_begin;

public:
  // public typedefs
  using value_type = typename micro_it_type::value_type;
  using reference = typename micro_it_type::reference;
  using pointer = typename micro_it_type::pointer;
  using TThis = const_union_find_it<T, THash>;
  using iterator_category = ::std::bidirectional_iterator_tag;
  using size_type = typename parent_type::size_type;
  using difference_type = typename micro_it_type::difference_type;

  // constr/destr
  const_union_find_it() = default;
  const_union_find_it(const TThis &) = default;
  const_union_find_it(TThis &&) = default;
  TThis &operator=(const TThis &) = default;
  TThis &operator=(TThis &&) = default;

  // construct iterator spanning given bucket range, where id of first bucket is
  // equal to third parameter.
  const_union_find_it(macro_it_type, macro_it_type, bucket_id_type);

  // interface
  reference operator*();
  micro_it_type operator->();
  TThis &operator++();
  TThis &operator++(int);
  TThis &operator--();
  TThis &operator--(int);

  bucket_id_type bucket_id() const;
  void swap(TThis &);

  // friends
  friend bool operator!=<T, THash>(const TThis &, const TThis &);
  friend bool operator==<T, THash>(const TThis &, const TThis &);
}; // class const_union_find_it

/*
        const_union_find_it impl begin
   ----------------------------------------------------------------------
*/
template <typename T, typename THash>
const_union_find_it<T, THash>::const_union_find_it(macro_it_type p_macro_begin,
                                                   macro_it_type p_macro_end,
                                                   bucket_id_type p_bucket_id)
    : m_macro_begin(p_macro_begin), m_macro_end(p_macro_end),
      m_bucket_id(p_bucket_id),
      m_macro_it(::std::next(p_macro_begin, p_bucket_id)) {
  m_micro_it =
      (m_macro_it == m_macro_end) ? micro_it_type{} : m_macro_it->begin();
}

template <typename T, typename THash>
auto const_union_find_it<T, THash>::operator*() -> reference {
  return *m_micro_it;
}

template <typename T, typename THash>
auto const_union_find_it<T, THash>::operator-> () -> micro_it_type {
  return m_micro_it;
}

template <typename T, typename THash>
auto const_union_find_it<T, THash>::operator++() -> TThis & {
  ++m_micro_it;
  // skip to next bucket if necessary
  while (m_micro_it == m_macro_it->end()) {
    if (m_macro_it == m_macro_end) {
      m_micro_it = micro_it_type{};
      return *this;
    }
    ++m_macro_it;
    ++m_bucket_id;
    m_micro_it = m_macro_it->begin();
  }
  return *this;
}

template <typename T, typename THash>
auto const_union_find_it<T, THash>::operator++(int) -> TThis & {
  auto copy = *this;
  ++m_micro_it;
  // skip to next bucket if necessary
  while (m_micro_it == m_macro_it->end()) {
    if (m_macro_it == m_macro_end) {
      m_micro_it = micro_it_type{};
      return copy;
    }
    ++m_macro_it;
    ++m_bucket_id;
    m_micro_it = m_macro_it->begin();
  }
  return copy;
}

template <typename T, typename THash>
auto const_union_find_it<T, THash>::operator--() -> TThis & {
  // in case we're off-the-end
  if (m_macro_it == m_macro_end || m_micro_it == m_macro_it->begin()) {
    do {
      --m_macro_it;
      --m_bucket_id;
    } // decrementing in empty container is not meaningful, but should not crash
      // either
    while (m_macro_it != m_macro_begin && m_macro_it->empty());
    m_micro_it = ::std::prev(m_macro_it->end());
    return *this;
  } else {
    --m_micro_it;
    return *this;
  }
}

template <typename T, typename THash>
auto const_union_find_it<T, THash>::operator--(int) -> TThis & {
  auto copy = *this;
  // return to first bucket with elements
  // in case we're off-the-end
  if (m_macro_it == m_macro_end || m_micro_it == m_macro_it->begin()) {
    do {
      --m_macro_it;
      --m_bucket_id;
    } // decrementing in empty container is not meaningful, but should not crash
      // either
    while (m_macro_it != m_macro_begin && m_macro_it->empty());
    m_micro_it = ::std::prev(m_macro_it->end());
    return copy;
  } else {
    --m_micro_it;
    return copy;
  }
}

template <typename T, typename THash>
auto const_union_find_it<T, THash>::bucket_id() const -> bucket_id_type {
  return (m_macro_it == m_macro_end) ? macro_npos : m_bucket_id;
}

template <typename T, typename THash>
auto const_union_find_it<T, THash>::swap(const_union_find_it<T, THash> &p_other)
    -> void {
  using ::std::swap;
  swap(m_bucket_id, p_other.m_bucket_id);
  swap(m_macro_begin, p_other.m_macro_begin);
  swap(m_macro_end, p_other.m_macro_end);
  swap(m_micro_it, p_other.m_micro_it);
  swap(m_macro_it, p_other.m_macro_it);
}

// non-member operators
template <typename T, typename THash>
auto operator==(const const_union_find_it<T, THash> &p_lhs,
                const const_union_find_it<T, THash> &p_rhs) -> bool {
  if (p_lhs.block_id() == const_union_find_it<T, THash>::macro_npos &&
      p_rhs.block_id() == const_union_find_it<T, THash>::macro_npos) {
    return true;
  } else {
    return (p_lhs.m_macro_it == p_rhs.m_macro_it) &&
           (p_lhs.m_micro_it == p_rhs.m_micro_it) &&
           (p_lhs.m_bucket_id == p_rhs.m_bucket_id);
  }
}

template <typename T, typename THash>
auto operator!=(const const_union_find_it<T, THash> &p_lhs,
                const const_union_find_it<T, THash> &p_rhs) -> bool {
  if (p_lhs.block_id() == const_union_find_it<T, THash>::macro_npos &&
      p_rhs.block_id() == const_union_find_it<T, THash>::macro_npos) {
    return false;
  }
  return (p_lhs.m_macro_it != p_rhs.m_macro_it) ||
         (p_lhs.m_micro_it != p_rhs.m_micro_it) ||
         (p_lhs.m_bucket_id != p_rhs.m_bucket_id);
}

/*
        const_union_find_it impl end
   -------------------------------------------------------------
*/
} // namespace internal

template <typename T, typename THash> class union_find {
public:
  // public typedefs
  using value_type = T;
  using TThis = union_find<value_type, THash>;
  using reference = value_type &;
  using pointer = value_type *;
  using const_reference = const value_type &;
  using const_pointer = const value_type *;
  using iterator = internal::union_find_it<value_type>;
  using const_iterator = internal::const_union_find_it<value_type>;

  // private:
  // private typedefs
  using bucket_type = ::std::vector<value_type>;
  using bucket_container_type = ::std::vector<bucket_type>;

public:
  // public typedefs part deux
  using bucket_id_type = typename bucket_container_type::size_type;
  using size_type = typename bucket_type::size_type;
  using map_type = ::std::unordered_map<value_type, bucket_id_type, THash>;

private:
  // members
  bucket_container_type m_buckets;
  map_type m_map;

public:
  // constants
  // TODO change back to proper typedefs
  static constexpr const bucket_id_type npos =
      ::std::numeric_limits<bucket_id_type>::max();

  // constr/destr
  union_find() = default;
  union_find(const TThis &) = default;
  union_find(TThis &&) = default;
  TThis &operator=(const TThis &) = default;
  TThis &operator=(TThis &&) = default;
  ~union_find() = default;

  // interface
  // inserts element into bucket with specified ID. ID must refer to a valid
  // bucket.
  void insert(bucket_id_type, const_reference);
  // adds a new bucket at the end of this container and inserts the specified
  // element into it.  returns the ID of the bucket inserted into.
  bucket_id_type push_back(const_reference);
  // merges two buckets with specified IDs. IDs must refer to valid buckets.
  bucket_id_type merge(bucket_id_type, bucket_id_type);
  // merges all buckets whose IDs are found within the given iterator span.
  template <typename It> bucket_id_type merge(It, It);
  // searches this structure for the specified element, returning both an
  // iterator to it and its bucket's ID.  If the element is not found, iterator is
  // equal to .end() and ID equal to npos.
  ::std::pair<iterator, bucket_id_type> find(const_reference);
  ::std::pair<const_iterator, bucket_id_type> find(const_reference) const;
  // deletes all empty buckets from the container. Invalidates all iterators,
  // references and IDs.  CURRENTLY BROKEN - DO NOT USE
  void trim();

  // iterators
  iterator begin();
  iterator end();
  const_iterator begin() const;
  const_iterator end() const;
  const_iterator cbegin() const;
  const_iterator cend() const;

private:
  // helper
  // merges two buckets without reserving extra storage; Must be ensured
  // externally.  always merges into first bucket.
  void internal_merge(bucket_id_type, bucket_id_type);

}; // class union_find
// union_find impl start -----
template <typename T, typename THash>
auto union_find<T, THash>::insert(bucket_id_type p_id,
                                  const_reference p_element) -> void {
  m_map[p_element] = p_id;
  m_buckets[p_id].push_back(p_element);
}

template <typename T, typename THash>
auto union_find<T, THash>::push_back(const_reference p_element)
    -> bucket_id_type {
  m_buckets.push_back(bucket_type{p_element});
  m_map[p_element] = m_buckets.size() - 1;

  return m_buckets.size() - 1;
}

template <typename T, typename THash>
auto union_find<T, THash>::merge(bucket_id_type p_first,
                                 bucket_id_type p_second) -> bucket_id_type {
  if (p_first == p_second) {
    return p_first;
  }
  auto &fst_bucket = m_buckets[p_first];
  auto &snd_bucket = m_buckets[p_second];
  // merge into larger bucket to reduce number of moves.
  if (fst_bucket.size() <= snd_bucket.size()) {
    snd_bucket.reserve(snd_bucket.size() + fst_bucket.size());
    internal_merge(p_second, p_first);
    return p_second;
  } else {
    fst_bucket.reserve(snd_bucket.size() + fst_bucket.size());
    internal_merge(p_first, p_second);
    return p_first;
  }
}

template <typename T, typename THash>
template <typename It>
auto union_find<T, THash>::merge(It p_beg, It p_end) -> bucket_id_type {
  if (p_beg == p_end) {
    return npos;
  }

  auto count = m_buckets[*p_beg].size();
  auto max_id = *p_beg;
  // find largest bucket; dirty but saves a max_elem afterwards
  ::std::for_each(::std::next(p_beg), p_end,
                  [this, &count, &max_id](auto p_id) -> void {
                    if (m_buckets[p_id].size() > m_buckets[max_id].size()) {
                      max_id = p_id;
                    }
                    count += m_buckets[p_id].size();
                  });
  // ensure required capacity
  m_buckets[max_id].reserve(count);
  // merge everything into largest bucket

  ::std::for_each(p_beg, p_end, [this, max_id](auto p_id) -> void {
    if (p_id != max_id) {
      this->internal_merge(max_id, p_id);
    }
  });
  return max_id;
}

template <typename T, typename THash>
auto union_find<T, THash>::find(const_reference p_elem)
    -> std::pair<iterator, bucket_id_type> {

  auto it = m_map.find(p_elem);
  if (it == m_map.end()) {
    return ::std::make_pair(end(), npos);
  } else {
    iterator ret_it(m_buckets.begin(), m_buckets.end(), it->second);

    while (*ret_it != p_elem) {
      ++ret_it;
    }

    return ::std::make_pair(ret_it, ret_it.bucket_id());
  }
}

template <typename T, typename THash>
auto union_find<T, THash>::find(const_reference p_elem) const
    -> std::pair<const_iterator, bucket_id_type> {
  auto it = m_map.find(p_elem);
  if (it == m_map.end()) {
    return ::std::make_pair(end(), npos);
  } else {
    const_iterator ret_it(m_buckets.begin(), m_buckets.end(), it->second);
    while (*ret_it != p_elem) {
      ++ret_it;
    }
    return ::std::make_pair(ret_it, ret_it.bucket_id());
  }
}

// CURRENTLY BROKEN - DO NOT USE
template <typename T, typename THash>
auto union_find<T, THash>::trim() -> void {
  auto remove_lambda = [](const bucket_type &p_bucket) -> bool {
    return p_bucket.empty();
  };
  // erase-remove empty buckets
  m_buckets.erase(::std::remove_if(begin(), end(), remove_lambda), end());
}

template <typename T, typename THash>
auto union_find<T, THash>::internal_merge(bucket_id_type p_fst,
                                          bucket_id_type p_snd) -> void {
  ::std::for_each(m_buckets[p_snd].begin(), m_buckets[p_snd].end(),
                  [this, p_fst](auto &p_elem) {
                    m_map[p_elem] = p_fst;
                    m_buckets[p_fst].push_back(::std::move(p_elem));
                  });
  m_buckets[p_snd].clear();
}

template <typename T, typename THash>
auto union_find<T, THash>::begin() -> iterator {
  bucket_id_type id = 0;
  for (auto macro = m_buckets.begin(); macro != m_buckets.end();
       ++macro, ++id) {
    if (!(macro->empty())) {
      return iterator(macro, m_buckets.end(), id);
    }
  }
  // empty structure -> return off-the-end
  return end();
}

template <typename T, typename THash>
auto union_find<T, THash>::end() -> iterator {
  return iterator(m_buckets.end(), m_buckets.end(), npos);
}

template <typename T, typename THash>
auto union_find<T, THash>::begin() const -> const_iterator {
  bucket_id_type id = 0;
  for (auto macro = m_buckets.begin(); macro != m_buckets.end();
       ++macro, ++id) {
    if (!(macro->empty())) {
      return const_iterator(macro, m_buckets.end(), id);
    }
  }
  // empty structure -> return off-the-end
  return end();
}

template <typename T, typename THash>
auto union_find<T, THash>::end() const -> const_iterator {
  return const_iterator(m_buckets.end(), m_buckets.end(), npos);
}

template <typename T, typename THash>
auto union_find<T, THash>::cbegin() const -> const_iterator {
  bucket_id_type id = 0;
  for (auto macro = m_buckets.begin(); macro != m_buckets.end();
       ++macro, ++id) {
    if (!(macro->empty())) {
      return const_iterator(macro, m_buckets.end(), id);
    }
  }
  // empty structure -> return off-the-end
  return cend();
}

template <typename T, typename THash>
auto union_find<T, THash>::cend() const -> const_iterator {
  return const_iterator(m_buckets.end(), m_buckets.end(), npos);
}
// union_find impl end -----

template <typename T, typename THash>
const typename union_find<T, THash>::bucket_id_type union_find<T, THash>::npos;

} // namespace atfc

namespace std {
template <typename T, typename THash>
void swap(::atfc::internal::union_find_it<T, THash> &p_rhs,
          ::atfc::internal::union_find_it<T, THash> &p_lhs) {
  p_lhs.swap(p_rhs);
}

template <typename T, typename THash>
void swap(::atfc::internal::const_union_find_it<T, THash> &p_rhs,
          ::atfc::internal::const_union_find_it<T, THash> &p_lhs) {
  p_lhs.swap(p_rhs);
}
} // namespace std
