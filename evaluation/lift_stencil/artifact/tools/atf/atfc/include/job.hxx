// This file is part of the Auto Tuning Framework (ATF).


//===-- job.hxx - Job message objects ---------------------------*- C++ -*-===//
//
//                             ATF Code Generator
//
// This file is part of the WWU PJS of the work group PVS in SoSe 17.
// For thirdparty code, see LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains all objects used as messager objects between compiler
// phase. They carry information from the parser stages to the code generation
// phase.
//
// Note that most (if not all) of the following objects are implemented as
// PODs without any constructors or data encapsulation of any sorts. This is
// done because they are only used internally, and big constructors and many
// getter/setters are worse than having none (in this case). Most of these
// structs are initialized by aggregate initialization anyways.
//
//===----------------------------------------------------------------------===//
// TODO:
//  - Put all utility structs in a "detail" namespace
//  - Use enumeration for `ocl_device_info::m_Type`
//
//===----------------------------------------------------------------------===//

#pragma once

#include <string>
#include <tuple>
#include <vector>

namespace atfc {
//===------------------------------------------------------------------===//
/// mode - Enumeration of all modes the program can be in
///
/// This enumeration is used to distinguish between the different modes of
/// operations supported by the atfc. This is needed because the code
/// generator has to take different actions depending on the requested
/// type of tuner to generate.
///
enum class mode {
  base, //< Base mode, used for tuning using the atf::ccfg
  ocl,  //< Tune OpenCL kernel
  cuda  //< Tune CUDA kernel
};
//===------------------------------------------------------------------===//

//===------------------------------------------------------------------===//
/// tp - Struct containing information about a tuning parameter
///
struct tp {
  ::std::string m_Name;        //< Identifier used by this tp.
  ::std::string m_Type;        //< Type. Currently unused.
  ::std::string m_Range;       //< Range declaration, e.g. `interval<T>`
  ::std::string m_Constraints; //< Constraints declaration
};
//===------------------------------------------------------------------===//

//===------------------------------------------------------------------===//
/// var_type - Enumeration for all possible types of ATF-variables UNUSED
///
enum class var_type {
  integer, //< `int`
  size     //< `::std::size_t`
};
//===------------------------------------------------------------------===//

//===------------------------------------------------------------------===//
/// var - Struct containing information about a ATF-variable
///
struct var {
  ::std::string m_Name;   //< C++ name of variable. Needs to be ident.
  ::std::string m_Type;   //< C++ type of variable
  ::std::string m_Value;  //< Value of variable.
  bool m_UsesArgV{false}; //< Get value from tuner command line
  ::std::size_t m_ArgPos; //< Index of command line argument to use
};
//===------------------------------------------------------------------===//

//===------------------------------------------------------------------===//
/// ocl_device_info - Struct containing information about a OpenCL device
///
/// Note that the member `m_Type` can only be CPU, GPU or ACC.
/// This will later be enforced by the use of an enumeration.
///
struct ocl_device_info {
  bool m_UseIds{false};
  ::std::string m_Vendor;
  ::std::string m_Type;
  ::std::size_t m_Id{0};
  ::std::size_t m_DeviceId{0};
  ::std::size_t m_PlatformId{0};
};
//===------------------------------------------------------------------===//

//===------------------------------------------------------------------===//
/// ocl_data - Struct containing information for OpenCL tuners
///
struct ocl_data {
  ::std::string m_KernelName;
  ::std::string m_Gs;
  ::std::string m_Ls;
  ocl_device_info m_DeviceInfo;
  ::std::vector<::std::string> m_Inputs;
};
//===------------------------------------------------------------------===//

struct cuda_data {
  ::std::string m_KernelName;
  ::std::string m_Bd;
  ::std::string m_Gd;
  ::std::size_t m_Id{0};
  ::std::vector<::std::string> m_Inputs;
};

struct job {
  // name, type, range, constraints
  using tp_type = tp;
  using var_type = var;

  ::std::vector<::std::string> m_SourceFiles;
  ::std::vector<tp_type> m_TuningParams;
  ::std::vector<var_type> m_Variables;
  ::std::string m_RunScript;
  ::std::string m_CompileScript;
  ::std::string m_SearchStrategy;
  ::std::string m_AbortCondition;
  ::std::string m_CostFile;
  bool m_OutputSrc{false}; //< Whether code should be generated that will write
                           //best config out as source file
  ::std::string m_OutSrcPath;
  bool m_OutputJson{
      false}; // Whether best configuration should be exported as json
  ::std::string m_OutJsonPath;

  mode m_Mode{mode::base};

  // Is only populated if mode is mode::ocl
  ocl_data m_OclData;
  cuda_data m_CudaData;
};
} // namespace atfc
