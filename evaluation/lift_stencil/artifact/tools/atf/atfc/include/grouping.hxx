// This file is part of the Auto Tuning Framework (ATF).


#pragma once

#include <string>
#include <vector>

#include "job.hxx"

namespace atfc {
// group tps in given job by name
::std::vector<::std::vector<::std::string>> group(const job &);
} // namespace atfc