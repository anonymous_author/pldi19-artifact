// This file is part of the Auto Tuning Framework (ATF).


#pragma once

#include "../diagnostics.hxx"
#include "../string_view.hxx"
#include "meta.hxx"
#include <pegtl.hh>
#include <string>
#include <type_traits>

using namespace pegtl;

// TODO: Im Moment kann der Parser im Falle "#atf::tp nme" nur ein Caret unter
// "nme" setzen und sagen: "hier sollte
//		 eigentlich name stehen", kann das aber nicht unterschlängeln.
//		 Besser: Statt "kw_name" da zu erwarten, einen identifier erwarten
//und bei der Action dann prüfen ob es "name" ist. 		 Wenn nicht kann
//genau der Bereich unterschlängelt werden.

// TODO: error on multiple definition => maybe in job_action?

namespace atfc {
struct line_comment : seq<string<'/', '/'>,
                          sor<until<eol, ascii::any>, until<eof, ascii::any>>> {
};

struct comment
    : seq<string<'/', '*'>, until<string<'*', '/'>, sor<comment, ascii::any>>> {
};

struct ws : sor<blank, comment> {};

struct integral : plus<ascii::digit> {};

struct opt_ws : star<ws> {};

struct req_ws : plus<ws> {};

struct dcl_preamble : string<'#', 'a', 't', 'f', ':', ':'> {};

struct cuda_dcl_preamble : string<'c', 'u', 'd', 'a', ':', ':'> {};

struct ocl_dcl_preamble : string<'o', 'c', 'l', ':', ':'> {};

struct kw_source : string<'s', 'o', 'u', 'r', 'c', 'e'> {};

struct kw_run_script
    : string<'r', 'u', 'n', '_', 's', 'c', 'r', 'i', 'p', 't'> {};

struct kw_compile_script : string<'c', 'o', 'm', 'p', 'i', 'l', 'e', '_', 's',
                                  'c', 'r', 'i', 'p', 't'> {};

struct kw_search_technique : string<'s', 'e', 'a', 'r', 'c', 'h', '_', 't', 'e',
                                    'c', 'h', 'n', 'i', 'q', 'u', 'e'> {};

struct kw_abort_condition : string<'a', 'b', 'o', 'r', 't', '_', 'c', 'o', 'n',
                                   'd', 'i', 't', 'i', 'o', 'n'> {};

struct kw_cost_file : string<'c', 'o', 's', 't', '_', 'f', 'i', 'l', 'e'> {};

struct kw_tp : string<'t', 'p'> {};

struct kw_name : string<'n', 'a', 'm', 'e'> {};

struct kw_type : string<'t', 'y', 'p', 'e'> {};

struct kw_range : string<'r', 'a', 'n', 'g', 'e'> {};

struct kw_constraint
    : string<'c', 'o', 'n', 's', 't', 'r', 'a', 'i', 'n', 't'> {};

struct kw_type_int : string<'i', 'n', 't'> {};

struct kw_type_size_t : string<'s', 'i', 'z', 'e', '_', 't'> {};

struct kw_type_float : string<'f', 'l', 'o', 'a', 't'> {};

struct kw_type_double : string<'d', 'o', 'u', 'b', 'l', 'e'> {};

struct kw_type_bool : string<'b', 'o', 'o', 'l'> {};

struct kw_ignore_begin
    : string<'i', 'g', 'n', 'o', 'r', 'e', '_', 'b', 'e', 'g', 'i', 'n'> {};

struct kw_ignore_end
    : string<'i', 'g', 'n', 'o', 'r', 'e', '_', 'e', 'n', 'd'> {};

struct kw_cuda_input : string<'i', 'n', 'p', 'u', 't'> {};

struct kw_cuda_gd : string<'g', 'r', 'i', 'd', '_', 'd', 'i', 'm'> {};

struct kw_cuda_di : string<'d', 'e', 'v', 'i', 'c', 'e', '_', 'i', 'd'> {};

struct kw_cuda_bd : string<'b', 'l', 'o', 'c', 'k', '_', 'd', 'i', 'm'> {};

struct kw_cuda_kernel_name
    : string<'k', 'e', 'r', 'n', 'e', 'l', '_', 'n', 'a', 'm', 'e'> {};

struct kw_ocl_input : string<'i', 'n', 'p', 'u', 't'> {};

struct kw_ocl_gs : string<'g', 's'> {};

struct kw_ocl_ls : string<'l', 's'> {};

struct kw_ocl_kernel_name
    : string<'k', 'e', 'r', 'n', 'e', 'l', '_', 'n', 'a', 'm', 'e'> {};

struct kw_ocl_di
    : string<'d', 'e', 'v', 'i', 'c', 'e', '_', 'i', 'n', 'f', 'o'> {};

struct kw_ocl_di_platform_id
    : string<'p', 'l', 'a', 't', 'f', 'o', 'r', 'm', '_', 'i', 'd'> {};

struct kw_ocl_di_device_id
    : string<'d', 'e', 'v', 'i', 'c', 'e', '_', 'i', 'd'> {};

struct kw_ocl_di_vendor : string<'v', 'e', 'n', 'd', 'o', 'r'> {};

struct kw_ocl_di_id : string<'i', 'd'> {};

struct kw_ocl_di_type : string<'t', 'y', 'p', 'e'> {};

struct ocl_di_type : sor<string<'G', 'P', 'U'>, string<'C', 'P', 'U'>> {};

struct ocl_di_id : integral {};

struct ocl_di_did : integral {};

struct ocl_di_pid : integral {};

struct cuda_id : integral {};

struct kw_var_type : sor<kw_type_int, kw_type_size_t, kw_type_float,
                         kw_type_double, kw_type_bool> {};

struct kw_var : string<'v', 'a', 'r'> {};

struct open_quote : one<'"'> {};

struct close_quote : one<'"'> {};

struct dcl_string
    : seq<star<seq<not_at<sor<close_quote, eol, eof>>, ascii::any>>> {};

// For tp declaration. Will use different action.
struct dcl_tp_string : dcl_string {};

struct ocl_di_vendor : dcl_string {};

struct end_dcl : seq<opt_ws, sor<line_comment, eof, eol>> {};

struct open_var_type : one<'<'> {};

struct close_var_type : one<'>'> {};

struct equals_sign : one<'='> {};

struct kw_argv : string<'a', 'r', 'g', 'v'> // one<'$'>//string<'a','r','g','v'>
{};

struct bracket_open : one<'['> {};

struct bracket_close : one<']'> {};

struct inline_eol_marker : one<'\\'> {};

struct argv_dcl_old
    : seq<kw_argv, must<bracket_open>, must<integral>, must<bracket_close>> {};

struct argv_dcl : seq<one<'$'>, must<integral>> {};

struct var_value_dcl : sor<sor<argv_dcl, argv_dcl_old>, must<dcl_string>> {};

template <bool Inline>
struct ignore_dcl
    : seq<::std::conditional_t<Inline, seq<dcl_preamble>,
                               seq<must<meta_fis_guard>, must<dcl_preamble>>>,
          must<meta_kw_guard>, sor<kw_ignore_begin, kw_ignore_end>,
          must<end_dcl>> {};

template <bool Inline>
struct var_dcl
    : seq<
          // If we are in Inline mode, we have to NOT cause a global failure
          // when dcl_preamble (and the leading fis) are not encountered.
          // We thus loose the debugging capabilities here.
          // TODO maybe detect if it starts with "atf::" to still give the
          // '#' hint => redo meta_fis_guard
          ::std::conditional_t<Inline, seq<dcl_preamble>,
                               seq<must<meta_fis_guard>, must<dcl_preamble>>>,
          must<meta_kw_guard>, kw_var, must<open_var_type>, must<kw_var_type>,
          must<close_var_type>, must<req_ws>, must<ascii::identifier>,
          must<req_ws>, must<equals_sign>, must<req_ws>, must<var_value_dcl>,
          must<end_dcl>> {};

template <bool Inline, typename T_KW>
struct base_dcl
    : seq<
          // If we are in Inline mode, we have to NOT cause a global failure
          // when dcl_preamble (and the leading fis) are not encountered.
          // We thus loose the debugging capabilities here.
          // TODO maybe detect if it starts with "atf::" to still give the
          // '#' hint => redo meta_fis_guard
          ::std::conditional_t<Inline, seq<dcl_preamble>,
                               seq<must<meta_fis_guard>, must<dcl_preamble>>>,
          must<meta_kw_guard>, T_KW, must<req_ws>, must<open_quote>,
          must<dcl_string>, must<close_quote>, opt_ws, must<end_dcl>> {};

template <bool Inline, typename T_KW>
struct base_ocl_dcl
    : seq<::std::conditional_t<Inline, seq<dcl_preamble>,
                               seq<must<meta_fis_guard>, must<dcl_preamble>>>,
          ocl_dcl_preamble, must<meta_ocl_kw_guard>, T_KW, must<req_ws>,
          must<open_quote>, must<dcl_string>, must<close_quote>, opt_ws,
          must<end_dcl>> {};

template <bool Inline, typename T_KW>
struct base_cuda_dcl
    : seq<::std::conditional_t<Inline, seq<dcl_preamble>,
                               seq<must<meta_fis_guard>, must<dcl_preamble>>>,
          cuda_dcl_preamble, must<meta_cuda_kw_guard>, T_KW, must<req_ws>,
          must<open_quote>, must<dcl_string>, must<close_quote>, opt_ws,
          must<end_dcl>> {};

template <bool Inline>
struct multiline_dcl_helper
    : seq<opt_ws,
          ::std::conditional_t<
              Inline,
              seq<must<not_at<eol>>, inline_eol_marker, opt_ws, must<eol>>,
              seq<must<not_at<inline_eol_marker>>, opt_ws, eol>>,
          opt_ws> {};

template <bool Inline>
struct ocl_di_by_name
    : seq<kw_ocl_di_vendor, must<req_ws>, must<open_quote>, must<ocl_di_vendor>,
          must<close_quote>, sor<multiline_dcl_helper<Inline>, must<req_ws>>,

          must<kw_ocl_di_type>, must<req_ws>, must<open_quote>,
          must<ocl_di_type>, must<close_quote>,
          sor<multiline_dcl_helper<Inline>, must<req_ws>>,

          must<kw_ocl_di_id>, must<req_ws>, must<ocl_di_id>, must<end_dcl>> {};

// todo control of all must internals
template <bool Inline>
struct ocl_di_by_id : seq<kw_ocl_di_platform_id, must<req_ws>, must<ocl_di_pid>,
                          sor<multiline_dcl_helper<Inline>, must<req_ws>>,
                          must<kw_ocl_di_device_id>, must<req_ws>,
                          must<ocl_di_did>, must<end_dcl>> {};

// todo control
template <bool Inline>
struct ocl_di_body : sor<ocl_di_by_name<Inline>, ocl_di_by_id<Inline>> {};

template <bool Inline>
struct ocl_di_dcl
    : seq<::std::conditional_t<Inline, seq<dcl_preamble>,
                               seq<must<meta_fis_guard>, must<dcl_preamble>>>,
          ocl_dcl_preamble, must<meta_ocl_kw_guard>, kw_ocl_di, must<req_ws>,
          must<ocl_di_body<Inline>>> {};

template <bool Inline>
struct cuda_di_dcl
    : seq<::std::conditional_t<Inline, seq<dcl_preamble>,
                               seq<must<meta_fis_guard>, must<dcl_preamble>>>,
          cuda_dcl_preamble, must<meta_cuda_kw_guard>, kw_cuda_di, must<req_ws>,
          must<cuda_id>, must<end_dcl>> {};

struct tp_dcl_helper : seq<opt_ws, eol, opt_ws> {};

template <bool Inline>
struct tp_dcl
    : seq<::std::conditional_t<Inline, seq<dcl_preamble>,
                               seq<must<meta_fis_guard>, must<dcl_preamble>>>,
          must<meta_kw_guard>, kw_tp, must<req_ws>,

          must<kw_name>, must<req_ws>, must<open_quote>, must<dcl_tp_string>,
          must<close_quote>, sor<multiline_dcl_helper<Inline>, must<req_ws>>,

          must<kw_type>, must<req_ws>, must<open_quote>, must<dcl_tp_string>,
          must<close_quote>, sor<multiline_dcl_helper<Inline>, must<req_ws>>,

          must<kw_range>, must<req_ws>, must<open_quote>, must<dcl_tp_string>,
          must<close_quote>,

          sor<seq<::std::conditional_t<
                      Inline,
                      sor<at<seq<opt_ws, opt<inline_eol_marker>, opt_ws, eol,
                                 opt_ws, kw_constraint>>,
                          at<seq<req_ws, kw_constraint>>>,
                      success>,

                  sor<multiline_dcl_helper<Inline>, req_ws>, kw_constraint,
                  must<req_ws>, must<open_quote>, must<dcl_tp_string>,
                  must<close_quote>, must<end_dcl>>,
              must<end_dcl>>> {};

template <bool Inline> using source_dcl = base_dcl<Inline, kw_source>;

template <bool Inline> using run_script_dcl = base_dcl<Inline, kw_run_script>;

template <bool Inline>
using compile_script_dcl = base_dcl<Inline, kw_compile_script>;

template <bool Inline>
using search_technique_dcl = base_dcl<Inline, kw_search_technique>;

template <bool Inline>
using abort_condition_dcl = base_dcl<Inline, kw_abort_condition>;

template <bool Inline> using cost_file_dcl = base_dcl<Inline, kw_cost_file>;

template <bool Inline> using ocl_input_dcl = base_ocl_dcl<Inline, kw_ocl_input>;

template <bool Inline>
using ocl_kernel_name_dcl = base_ocl_dcl<Inline, kw_ocl_kernel_name>;

template <bool Inline> using ocl_gs_dcl = base_ocl_dcl<Inline, kw_ocl_gs>;

template <bool Inline> using ocl_ls_dcl = base_ocl_dcl<Inline, kw_ocl_ls>;

template <bool Inline>
using cuda_input_dcl = base_cuda_dcl<Inline, kw_cuda_input>;

template <bool Inline> using cuda_gd_dcl = base_cuda_dcl<Inline, kw_cuda_gd>;

template <bool Inline> using cuda_bd_dcl = base_cuda_dcl<Inline, kw_cuda_bd>;

template <bool Inline>
using cuda_kernel_name_dcl = base_cuda_dcl<Inline, kw_cuda_kernel_name>;

template <bool Inline>
struct declaration
    : sor<cuda_di_dcl<Inline>, ocl_input_dcl<Inline>,
          ocl_kernel_name_dcl<Inline>, ocl_gs_dcl<Inline>, ocl_ls_dcl<Inline>,
          ocl_di_dcl<Inline>, cuda_kernel_name_dcl<Inline>,
          cuda_input_dcl<Inline>, cuda_gd_dcl<Inline>, cuda_bd_dcl<Inline>,
          ignore_dcl<Inline>, var_dcl<Inline>, compile_script_dcl<Inline>,
          source_dcl<Inline>, run_script_dcl<Inline>,
          search_technique_dcl<Inline>, abort_condition_dcl<Inline>,
          cost_file_dcl<Inline>, tp_dcl<Inline>> {};

template <bool Inline>
struct any_if_inline
    : ::std::conditional_t<Inline, until<sor<eol, eof>, ascii::any>, failure> {
};

template <bool Inline = false>
struct start : seq<plus<sor< // TODO until eol or eof, WS
                   seq<opt_ws, sor<eol, eof>>, line_comment,
                   declaration<Inline>, any_if_inline<Inline>>>> {};
} // namespace atfc
