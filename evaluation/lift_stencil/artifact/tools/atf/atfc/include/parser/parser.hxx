// This file is part of the Auto Tuning Framework (ATF).


#pragma once

#include "../job.hxx"
#include "../string_view.hxx"

namespace atfc {
// Parse given text for configuration information.
// Will throw on parser error.
auto parse(string_view p_src, string_view p_name, bool p_inline) -> job;
} // namespace atfc
