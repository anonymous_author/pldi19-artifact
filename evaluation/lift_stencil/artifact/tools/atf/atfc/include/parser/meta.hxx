// This file is part of the Auto Tuning Framework (ATF).


#pragma once

#include "../diagnostics.hxx"
#include "../string_view.hxx"
#include <pegtl.hh>
#include <string>
#include <type_traits>
#include <unordered_set>

using namespace pegtl;

namespace atfc {
struct meta_fis_guard {
  using analyze_t = analysis::generic<analysis::rule_type::ANY>;

  template <pegtl::apply_mode A, template <typename...> class Action,
            template <typename...> class Control, typename... States>
  static bool match(pegtl::input &p_input, string_view p_source, States &...) {
    string_view t_view{&*p_input.begin(),
                       static_cast<::std::size_t>(
                           ::std::distance(p_input.begin(), p_input.end()))};

    // We just want to throw an error if we REALLY know that # was forgotten
    if (t_view.length() == 0)
      return true;

    if (t_view[0] != '#') {
      // Check if it is atf::
      if (t_view.find(string_view{"atf::"}) == 0) {
        post_diagnostic(message_type::error, p_input.source(),
                        {p_input.line(), p_input.column()},
                        {p_input.column(), 0}, p_source, "#",
                        "Missing '#' at beginning of declaration");

        return false;
      }
    }

    return true;
  }
};

struct meta_kw_guard {
  using analyze_t = analysis::generic<analysis::rule_type::ANY>;

  template <pegtl::apply_mode A, template <typename...> class Action,
            template <typename...> class Control, typename... States>
  static bool match(pegtl::input &p_input, string_view p_source, States &...) {
    // Retrieve keyword string
    auto t_it = p_input.begin();

    while (t_it != p_input.end() &&
           (::std::isalnum(*t_it) || (*t_it == '_'))) // TODO other WS
      ++t_it;

    ::std::string t_kw(p_input.begin(), t_it);

    // Check it
    if (m_KeyWords.count(t_kw) != 0)
      return true;
    else {
      // TODO when formatted diagnostics are available: Unknown keyword "bla"
      post_diagnostic(message_type::error, p_input.source(),
                      {p_input.line(), p_input.column()},
                      {p_input.column(), t_kw.length()}, p_source,
                      "Unknown keyword");

      // Is there a close alternative?
      ::std::string t_altkw{};

      if (has_alt(t_kw, t_altkw)) {
        post_diagnostic(message_type::note, p_input.source(),
                        {p_input.line(), p_input.column()},
                        {p_input.column(), t_kw.length()}, p_source, {t_altkw},
                        "Did you mean:");
      }

      return false;
    }
  }

  static bool has_alt(const ::std::string &p_kw, ::std::string &p_out) {
    // Calculate all distances to p_kw
    ::std::vector<::std::pair<::std::string, ::std::size_t>> t_distances;

    for (const auto &t_kw : m_KeyWords) {
      const auto t_dist = levenshtein_distance(p_kw, t_kw);

      t_distances.push_back(::std::make_pair(t_kw, t_dist));
    }

    // Find lowest distance
    const auto &t_lowestPair =
        *::std::min_element(t_distances.begin(), t_distances.end(),
                            [](const auto &x, const auto &y) -> bool {
                              return x.second < y.second;
                            });

    /*constexpr auto t_threshold = 4;*/
    constexpr auto t_threshold = 5;

    if (t_lowestPair.second <= t_threshold) {
      p_out = t_lowestPair.first;
      return true;
    } else
      return false;
  }

  static unsigned int levenshtein_distance(const std::string &s1,
                                           const std::string &s2) {
    const std::size_t len1 = s1.size(), len2 = s2.size();
    std::vector<unsigned int> col(len2 + 1), prevCol(len2 + 1);

    for (unsigned int i = 0; i < prevCol.size(); i++)
      prevCol[i] = i;
    for (unsigned int i = 0; i < len1; i++) {
      col[0] = i + 1;
      for (unsigned int j = 0; j < len2; j++)
        col[j + 1] = std::min({prevCol[1 + j] + 1, col[j] + 1,
                               prevCol[j] + (s1[i] == s2[j] ? 0 : 1)});
      col.swap(prevCol);
    }
    return prevCol[len2];
  }

  static const ::std::unordered_set<::std::string> m_KeyWords;
};

struct meta_ocl_kw_guard {
  using analyze_t = analysis::generic<analysis::rule_type::ANY>;

  template <pegtl::apply_mode A, template <typename...> class Action,
            template <typename...> class Control, typename... States>
  static bool match(pegtl::input &p_input, string_view p_source, States &...) {
    // Retrieve keyword string
    auto t_it = p_input.begin();

    while (t_it != p_input.end() &&
           (::std::isalnum(*t_it) || (*t_it == '_'))) // TODO other WS
      ++t_it;

    ::std::string t_kw(p_input.begin(), t_it);

    // Check it
    if (m_KeyWords.count(t_kw) != 0)
      return true;
    else {
      // TODO when formatted diagnostics are available: Unknown keyword "bla"
      post_diagnostic(message_type::error, p_input.source(),
                      {p_input.line(), p_input.column()},
                      {p_input.column(), t_kw.length()}, p_source,
                      "Unknown keyword");

      // Is there a close alternative?
      ::std::string t_altkw{};

      if (has_alt(t_kw, t_altkw)) {
        post_diagnostic(message_type::note, p_input.source(),
                        {p_input.line(), p_input.column()},
                        {p_input.column(), t_kw.length()}, p_source, {t_altkw},
                        "Did you mean:");
      }

      return false;
    }
  }

  static bool has_alt(const ::std::string &p_kw, ::std::string &p_out) {
    // Calculate all distances to p_kw
    ::std::vector<::std::pair<::std::string, ::std::size_t>> t_distances;

    for (const auto &t_kw : m_KeyWords) {
      const auto t_dist = levenshtein_distance(p_kw, t_kw);

      t_distances.push_back(::std::make_pair(t_kw, t_dist));
    }

    // Find lowest distance
    const auto &t_lowestPair =
        *::std::min_element(t_distances.begin(), t_distances.end(),
                            [](const auto &x, const auto &y) -> bool {
                              return x.second < y.second;
                            });

    /*constexpr auto t_threshold = 4;*/
    constexpr auto t_threshold = 5;

    if (t_lowestPair.second <= t_threshold) {
      p_out = t_lowestPair.first;
      return true;
    } else
      return false;
  }

  static unsigned int levenshtein_distance(const std::string &s1,
                                           const std::string &s2) {
    const std::size_t len1 = s1.size(), len2 = s2.size();
    std::vector<unsigned int> col(len2 + 1), prevCol(len2 + 1);

    for (unsigned int i = 0; i < prevCol.size(); i++)
      prevCol[i] = i;
    for (unsigned int i = 0; i < len1; i++) {
      col[0] = i + 1;
      for (unsigned int j = 0; j < len2; j++)
        col[j + 1] = std::min({prevCol[1 + j] + 1, col[j] + 1,
                               prevCol[j] + (s1[i] == s2[j] ? 0 : 1)});
      col.swap(prevCol);
    }
    return prevCol[len2];
  }

  static const ::std::unordered_set<::std::string> m_KeyWords;
};

struct meta_cuda_kw_guard {
  using analyze_t = analysis::generic<analysis::rule_type::ANY>;

  template <pegtl::apply_mode A, template <typename...> class Action,
            template <typename...> class Control, typename... States>
  static bool match(pegtl::input &p_input, string_view p_source, States &...) {
    // Retrieve keyword string
    auto t_it = p_input.begin();

    while (t_it != p_input.end() &&
           (::std::isalnum(*t_it) || (*t_it == '_'))) // TODO other WS
      ++t_it;

    ::std::string t_kw(p_input.begin(), t_it);

    // Check it
    if (m_KeyWords.count(t_kw) != 0)
      return true;
    else {
      // TODO when formatted diagnostics are available: Unknown keyword "bla"
      post_diagnostic(message_type::error, p_input.source(),
                      {p_input.line(), p_input.column()},
                      {p_input.column(), t_kw.length()}, p_source,
                      "Unknown keyword");

      // Is there a close alternative?
      ::std::string t_altkw{};

      if (has_alt(t_kw, t_altkw)) {
        post_diagnostic(message_type::note, p_input.source(),
                        {p_input.line(), p_input.column()},
                        {p_input.column(), t_kw.length()}, p_source, {t_altkw},
                        "Did you mean:");
      }

      return false;
    }
  }

  static bool has_alt(const ::std::string &p_kw, ::std::string &p_out) {
    // Calculate all distances to p_kw
    ::std::vector<::std::pair<::std::string, ::std::size_t>> t_distances;

    for (const auto &t_kw : m_KeyWords) {
      const auto t_dist = levenshtein_distance(p_kw, t_kw);

      t_distances.push_back(::std::make_pair(t_kw, t_dist));
    }

    // Find lowest distance
    const auto &t_lowestPair =
        *::std::min_element(t_distances.begin(), t_distances.end(),
                            [](const auto &x, const auto &y) -> bool {
                              return x.second < y.second;
                            });

    /*constexpr auto t_threshold = 4;*/
    constexpr auto t_threshold = 5;

    if (t_lowestPair.second <= t_threshold) {
      p_out = t_lowestPair.first;
      return true;
    } else
      return false;
  }

  static unsigned int levenshtein_distance(const std::string &s1,
                                           const std::string &s2) {
    const std::size_t len1 = s1.size(), len2 = s2.size();
    std::vector<unsigned int> col(len2 + 1), prevCol(len2 + 1);

    for (unsigned int i = 0; i < prevCol.size(); i++)
      prevCol[i] = i;
    for (unsigned int i = 0; i < len1; i++) {
      col[0] = i + 1;
      for (unsigned int j = 0; j < len2; j++)
        col[j + 1] = std::min({prevCol[1 + j] + 1, col[j] + 1,
                               prevCol[j] + (s1[i] == s2[j] ? 0 : 1)});
      col.swap(prevCol);
    }
    return prevCol[len2];
  }

  static const ::std::unordered_set<::std::string> m_KeyWords;
};
} // namespace atfc
