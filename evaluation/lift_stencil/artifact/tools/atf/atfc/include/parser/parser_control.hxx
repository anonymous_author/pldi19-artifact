// This file is part of the Auto Tuning Framework (ATF).


#pragma once

#include "../diagnostics.hxx"
#include "grammar.hxx"

namespace atfc {
template <typename T> struct parser_control : normal<T> {};

template <> struct parser_control<var_value_dcl> : normal<var_value_dcl> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "Expected value or \"argv[i]\"");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<ocl_di_id> : normal<ocl_di_id> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "Expected positive integral device id");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<ocl_di_pid> : normal<ocl_di_pid> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "Expected positive integral platform id");

    throw ::std::runtime_error("");
  }
};

template <bool Inline>
struct parser_control<ocl_di_body<Inline>> : normal<ocl_di_body<Inline>> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "Expected either \"platform_id\" or \"vendor\"");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<ocl_di_did> : normal<ocl_di_did> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "Expected positive integral device id");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<ocl_di_type> : normal<ocl_di_type> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "Expected either \"GPU\" or \"CPU\"");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<kw_ocl_di_vendor> : normal<kw_ocl_di_vendor> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "vendor", "Expected keyword \"vendor\"");

    throw ::std::runtime_error("");
  }
};

template <>
struct parser_control<kw_ocl_di_platform_id> : normal<kw_ocl_di_platform_id> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "platform_id",
                    "Expected keyword \"platform_id\"");

    throw ::std::runtime_error("");
  }
};

template <>
struct parser_control<kw_ocl_di_device_id> : normal<kw_ocl_di_device_id> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "device_id", "Expected keyword \"device_id\"");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<kw_ocl_di_id> : normal<kw_ocl_di_id> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "id", "Expected keyword \"id\"");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<eol> : normal<eol> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "\\n", "Expected end-of-line");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<not_at<eol>> : normal<not_at<eol>> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "\\", "Expected '\\' here in inline mode");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<kw_ocl_di_type> : normal<kw_ocl_di_type> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "type", "Expected keyword \"type\"");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<integral> : normal<integral> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "Expected positive integral value");

    throw ::std::runtime_error("");
  }
};

template <>
struct parser_control<not_at<inline_eol_marker>>
    : normal<not_at<inline_eol_marker>> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "'\\' is only allowed in inline mode");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<bracket_open> : normal<bracket_open> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "[", "Expected '['");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<equals_sign> : normal<equals_sign> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "=", "Expected '='");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<bracket_close> : normal<bracket_close> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "]", "Expected ']'");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<identifier> : normal<identifier> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "Expected identifier");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<open_var_type> : normal<open_var_type> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "<", "Expected '<'");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<close_var_type> : normal<close_var_type> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, ">", "Expected '>'");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<kw_var_type> : normal<kw_var_type> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "Expected type");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<dcl_preamble> : normal<dcl_preamble> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "Expected begin of declaration");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<req_ws> : normal<req_ws> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "Expected white space");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<end_dcl> : normal<end_dcl> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "Expected new line or end of file");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<open_quote> : normal<open_quote> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "Expected quoted string literal");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<dcl_string> : normal<dcl_string> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "Expected quoted string literal");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<close_quote> : normal<close_quote> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "\"", "Expected closing quote");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<kw_range> : normal<kw_range> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "range", "Expected keyword \"range\"");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<kw_name> : normal<kw_name> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "name", "Expected keyword \"name\"");

    throw ::std::runtime_error("");
  }
};

template <> struct parser_control<kw_type> : normal<kw_type> {
  template <typename Input, typename... States>
  static void raise(const Input &p_input, string_view p_source, States &&...) {
    post_diagnostic(message_type::error, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, "type", "Expected keyword \"type\"");

    throw ::std::runtime_error("");
  }
};
} // namespace atfc
