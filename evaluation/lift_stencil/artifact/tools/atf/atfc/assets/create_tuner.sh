#!/bin/sh

check_rc(){
	rc=$?
	if [[ $rc != 0 ]]
	then
		exit $rc
	fi
}

exec_cmd(){
	eval "$1"
	check_rc
}



exec > create_tuner.log
exec 2>&1

exec_cmd "mv ./tuner.cpp ./code/src/tuner.cpp"
exec_cmd "cd ./code/bld"

if [ "$1" == "1" ]
then
	exec_cmd "cmake -DCMAKE_BUILD_TYPE=Release -DLIBATF_DISABLE_WARNINGS=On -DLIBATF_DISABLE_CUDA=Off ./.."
else
	exec_cmd "cmake -DCMAKE_BUILD_TYPE=Release -DLIBATF_DISABLE_WARNINGS=On ./.."
fi

exec_cmd "make"
exec_cmd "cd ../.."
exec_cmd "cp ./code/bld/tuner ./tuner"
