// This file is part of the Auto Tuning Framework (ATF).


// string_view: Constant view of string

// (c) Dominique Bönninghoff 2016
// This file was neither created nor modified during the PJS, and thus retains
// its original license.

// MIT License, Copyright 2016 Dominique Bönninghoff

#ifndef string_view_h
#define string_view_h

#include <algorithm>
#include <functional>
#include <iterator>
#include <limits>
#include <ostream>
#include <stdexcept>
#include <string>
#include <utility>

namespace atf {
namespace detail {
namespace internal {
template <typename TRandAccessIt>
constexpr auto distance_impl(TRandAccessIt p_first, TRandAccessIt p_last,
                             ::std::random_access_iterator_tag) ->
    typename ::std::iterator_traits<TRandAccessIt>::difference_type {
  return p_last - p_first;
}

template <typename TInputIt>
constexpr auto distance_impl(TInputIt p_first, TInputIt p_last,
                             ::std::input_iterator_tag) ->
    typename ::std::iterator_traits<TInputIt>::difference_type {
  using TDist = typename ::std::iterator_traits<TInputIt>::difference_type;

  TDist t_cnt{};

  for (; p_first != p_last; ++p_first, ++t_cnt)
    ;

  return t_cnt;
}

template <typename TIterator>
constexpr auto distance(TIterator p_first, TIterator p_last) ->
    typename ::std::iterator_traits<TIterator>::difference_type {
  using TIteratorTag =
      typename ::std::iterator_traits<TIterator>::iterator_category;

  return distance_impl(p_first, p_last, TIteratorTag{});
}

// InputIterator: Linear
template <typename TInputIt, typename TDist>
constexpr auto advance_impl(TInputIt &p_it, TDist p_n,
                            ::std::input_iterator_tag) -> void {
  while (p_n--)
    ++p_it;
}

// BidirectionalIterator: Allows negative distance.
template <typename TBidirectIt, typename TDist>
constexpr auto advance_impl(TBidirectIt &p_it, TDist p_n,
                            ::std::bidirectional_iterator_tag) -> void {
  if (p_n < 0) {
    while (p_n++)
      --p_it;
  } else {
    while (p_n--)
      ++p_it;
  }
}

// RandomAccessIterator: Direct use of +=
template <typename TRandAccessIt, typename TDist>
constexpr auto advance_impl(TRandAccessIt &p_it, TDist p_n,
                            ::std::random_access_iterator_tag) -> void {
  p_it += p_n;
}

// advance and next being constexpr is a C++17 feature
template <typename TIterator, typename TDist>
constexpr auto advance(TIterator &p_it, TDist p_n) -> void {
  using TIteratorTag =
      typename ::std::iterator_traits<TIterator>::iterator_category;

  advance_impl(p_it, p_n, TIteratorTag{});
}

template <typename TFwdIt>
constexpr auto
next(TFwdIt p_it,
     typename ::std::iterator_traits<TFwdIt>::difference_type p_n = 1)
    -> TFwdIt {
  advance(p_it, p_n);
  return p_it;
}

// Algorithms that aren't constexpr in the STL but are needed by string_view
template <typename TInputIt, typename TForwardIt, typename TFunc>
constexpr auto find_first_of(TInputIt p_first, TInputIt p_last,
                             TForwardIt p_sfirst, TForwardIt p_slast,
                             TFunc p_func) -> TInputIt {
  for (; p_first != p_last; ++p_first) {
    for (TForwardIt t_it = p_sfirst; t_it != p_slast; ++t_it) {
      if (p_func(*p_first, *t_it))
        return p_first;
    }
  }
  return p_last;
}

template <typename TFwdIt1, typename TFwdIt2, typename TPred>
constexpr auto search(TFwdIt1 p_first, TFwdIt1 p_last, TFwdIt2 p_sfirst,
                      TFwdIt2 p_slast, TPred p_pred) -> TFwdIt1 {
  for (;; ++p_first) {
    TFwdIt1 t_it = p_first;

    for (TFwdIt2 t_sit = p_sfirst;; ++t_it, ++t_sit) {
      if (t_sit == p_slast)
        return p_first;

      if (t_it == p_last)
        return p_last;

      if (!p_pred(*t_it, *t_sit))
        break;
    }
  }
}

// Output formatted character sequence to output stream
template <typename TChar, typename TTraits>
auto put_character_sequence(::std::basic_ostream<TChar, TTraits> &p_os,
                            const TChar *p_data, ::std::size_t p_len) -> void {
  using TSentry = typename ::std::basic_ostream<TChar, TTraits>::sentry;

  // Construct sentry object
  TSentry t_sentry{p_os};

  // Only output data if stream is in good state
  if (t_sentry) {
    // Query stream format state
    auto t_fill = p_os.fill();
    auto t_width = p_os.width();
    bool t_left = p_os.flags() & ::std::ios::left;
    bool t_right = p_os.flags() & ::std::ios::right;
    bool t_fixed = p_os.flags() & ::std::ios::fixed;

    // Padding function
    auto t_pad = [&](::std::streamsize p_cnt) {
      for (::std::streamsize i = 0; i < p_cnt; ++i)
        p_os.put(t_fill);
    };

    // If data length is shorter than set width, padding is required
    if (p_len < t_width) {
      // Determine length of padding
      auto t_padlen = t_width - p_len;

      // Pad and output data
      if (t_left)
        t_pad(t_padlen);
      p_os.write(p_data, p_len);
      if (t_right)
        t_pad(t_padlen);
    } else // Write everything, or if fixed is set a maximum of width characters
    {
      p_os.write(p_data, t_fixed ? t_width : p_len);
    }
  }

  // Reset stream width setting
  p_os.width(0);
}
} // namespace internal
namespace internal {
template <typename T = void> struct identity { using type = T; };

template <> struct identity<void>;

template <typename T> using identity_t = typename identity<T>::type;
} // namespace internal

// TEMPLATE CLASS basic_string_view_iter
// Random access iterator for basic_string_view.
template <typename TChar> class basic_string_view_iter {
private:
  using TThis = basic_string_view_iter<TChar>;

public:
  using iterator_category = ::std::random_access_iterator_tag;
  using value_type = TChar;
  using difference_type = ::std::ptrdiff_t;
  using pointer = const TChar *;
  using reference = const TChar &;
  using size_type = ::std::size_t;

public:
  constexpr basic_string_view_iter(const TChar *p_data) : m_Data{p_data} {}

  constexpr basic_string_view_iter() : m_Data{nullptr} {}

  constexpr basic_string_view_iter(const TThis &) = default;
  TThis &operator=(const TThis &) = default;

public:
  constexpr auto operator*() const -> reference { return *m_Data; }

  constexpr auto operator-> () const -> const pointer { return m_Data; }

  constexpr auto operator++() -> TThis & {
    ++m_Data;
    return *this;
  }

  constexpr auto operator++(int) -> TThis {
    TThis x{*this};
    this->operator++();
    return x;
  }

  constexpr auto operator--() -> TThis & {
    --m_Data;
    return *this;
  }

  constexpr auto operator--(int) -> TThis {
    TThis x{*this};
    this->operator--();
    return x;
  }

  constexpr auto operator+=(size_type p_sz) -> TThis & {
    m_Data += p_sz;
    return *this;
  }

  constexpr auto operator-=(size_type p_sz) -> TThis & {
    m_Data -= p_sz;
    return *this;
  }

  constexpr auto operator[](size_type p_sz) -> reference {
    return m_Data[p_sz];
  }

public:
  constexpr auto data() const noexcept -> pointer { return m_Data; }

public:
  constexpr void swap(TThis &p_rhs) { std::swap(m_Data, p_rhs.m_Data); }

private:
  const TChar *m_Data;
};
//

template <typename TChar>
constexpr void swap(basic_string_view_iter<TChar> &p_lhs,
                    basic_string_view_iter<TChar> &p_rhs) noexcept {
  p_lhs.swap(p_rhs);
}

template <typename TChar>
constexpr auto operator-(const basic_string_view_iter<TChar> &p_lhs,
                         const basic_string_view_iter<TChar> &p_rhs) ->
    typename basic_string_view_iter<TChar>::difference_type {
  return static_cast<typename basic_string_view_iter<TChar>::difference_type>(
      p_lhs.data() - p_rhs.data());
}

template <typename TChar>
constexpr auto operator-(const basic_string_view_iter<TChar> &p_lhs,
                         typename basic_string_view_iter<TChar>::size_type p_sz)
    -> basic_string_view_iter<TChar> {
  return basic_string_view_iter<TChar>{p_lhs.data() - p_sz};
}

template <typename TChar>
constexpr auto operator+(const basic_string_view_iter<TChar> &p_lhs,
                         typename basic_string_view_iter<TChar>::size_type p_sz)
    -> basic_string_view_iter<TChar> {
  return basic_string_view_iter<TChar>{p_lhs.data() + p_sz};
}

template <typename TChar>
constexpr auto operator+(typename basic_string_view_iter<TChar>::size_type p_sz,
                         const basic_string_view_iter<TChar> &p_rhs)
    -> basic_string_view_iter<TChar> {
  return basic_string_view_iter<TChar>{p_rhs.data() + p_sz};
}

template <typename TChar>
constexpr auto operator==(const basic_string_view_iter<TChar> &p_lhs,
                          const basic_string_view_iter<TChar> &p_rhs) -> bool {
  return (p_lhs.data() == p_rhs.data());
}

template <typename TChar>
constexpr auto operator!=(const basic_string_view_iter<TChar> &p_lhs,
                          const basic_string_view_iter<TChar> &p_rhs) -> bool {
  return !(p_lhs == p_rhs);
}

template <typename TChar>
constexpr auto operator<(const basic_string_view_iter<TChar> &p_lhs,
                         const basic_string_view_iter<TChar> &p_rhs) -> bool {
  return (p_rhs - p_lhs) > 0;
}

template <typename TChar>
constexpr auto operator>(const basic_string_view_iter<TChar> &p_lhs,
                         const basic_string_view_iter<TChar> &p_rhs) -> bool {
  return (p_rhs < p_lhs);
}

template <typename TChar>
constexpr auto operator>=(const basic_string_view_iter<TChar> &p_lhs,
                          const basic_string_view_iter<TChar> &p_rhs) -> bool {
  return !(p_lhs < p_rhs);
}

template <typename TChar>
constexpr auto operator<=(const basic_string_view_iter<TChar> &p_lhs,
                          const basic_string_view_iter<TChar> &p_rhs) -> bool {
  return !(p_lhs > p_rhs);
}

// TEMPLATE CLASS basic_string_view
template <typename TChar, typename TTraits = ::std::char_traits<TChar>>
class basic_string_view {
private:
  using TThis = basic_string_view<TChar, TTraits>;

public:
  using traits_type = TTraits;
  using value_type = TChar;
  using pointer = TChar *;
  using const_pointer = const TChar *;
  using reference = TChar &;
  using const_reference = const TChar &;
  using const_iterator = basic_string_view_iter<TChar>;
  using iterator = const_iterator;
  using const_reverse_iterator = ::std::reverse_iterator<const_iterator>;
  using reverse_iterator = const_reverse_iterator;
  using size_type = ::std::size_t;
  using difference_type = ::std::ptrdiff_t;

public:
  static constexpr size_type npos = ::std::numeric_limits<size_type>::max();

public:
  constexpr basic_string_view() noexcept : m_Data(nullptr), m_Length{} {}

  template <typename TAlloc>
  basic_string_view(
      const ::std::basic_string<TChar, TTraits, TAlloc> &p_str) noexcept
      : m_Data(p_str.data()), m_Length(p_str.size()) {}

  constexpr basic_string_view(const_pointer p_str, size_type p_len)
      : m_Data(p_str), m_Length(p_len) {}

  basic_string_view(const TChar *p_str)
      : m_Data(p_str), m_Length(p_str == nullptr ? 0 : TTraits::length(p_str)) {
  }

  constexpr basic_string_view(const TThis &) noexcept = default;

public:
  TThis &operator=(const TThis &) noexcept = default;

public:
  constexpr auto begin() const -> const_iterator {
    return const_iterator{m_Data};
  }

  constexpr auto cbegin() const -> const_iterator { return begin(); }

  constexpr auto end() const -> const_iterator {
    return const_iterator{m_Data + m_Length};
  }

  constexpr auto cend() const -> const_iterator { return end(); }

  auto rbegin() const -> const_reverse_iterator {
    return const_reverse_iterator{end()};
  }

  auto crbegin() const -> const_reverse_iterator { return rbegin(); }

  auto rend() const -> const_reverse_iterator {
    return const_reverse_iterator{begin()};
  }

  auto crend() const -> const_reverse_iterator { return rend(); }

public:
  constexpr auto operator[](size_type p_pos) const -> const_reference {
    return m_Data[p_pos];
  }

  constexpr auto at(size_type p_pos) const -> const_reference {
    if (p_pos < m_Length)
      return m_Data[p_pos];
    else
      throw ::std::out_of_range("");
  }

  constexpr auto front() const -> const_reference { return operator[](0); }

  constexpr auto back() const -> const_reference {
    return operator[](m_Length - 1);
  }

  constexpr auto data() const noexcept -> const_pointer { return m_Data; }

public:
  constexpr auto size() const noexcept -> size_type { return m_Length; }

  constexpr auto length() const noexcept -> size_type { return size(); }

  constexpr auto max_size() const noexcept -> size_type {
    return ::std::numeric_limits<size_type>::max() - 1;
  }

  constexpr auto empty() const noexcept -> bool { return size() == 0; }

public:
  constexpr auto remove_prefix(size_type p_n) -> void {
    m_Data += p_n;
    m_Length -= p_n;
  }

  constexpr auto remove_suffix(size_type p_n) -> void { m_Length -= p_n; }

public:
  constexpr auto swap(TThis &p_rhs) noexcept -> void {
    ::std::swap(m_Data, p_rhs.m_Data);
    ::std::swap(m_Length, p_rhs.m_Length);
  }

public:
  template <typename TAlloc = std::allocator<TChar>>
  auto to_string(const TAlloc &p_alloc = TAlloc()) const
      -> ::std::basic_string<TChar, TTraits, TAlloc> {
    return ::std::basic_string<TChar, TTraits, TAlloc>(data(), size(), p_alloc);
  }

  template <typename TAlloc>
  explicit operator ::std::basic_string<TChar, TTraits, TAlloc>() const {
    return to_string();
  }

public:
  auto copy(pointer p_dst, size_type p_count, size_type p_pos = 0) const
      -> size_type {
    if (p_pos < size()) {
      size_type t_tmp = size() - p_pos;
      size_type rcount = ::std::min(t_tmp, p_count);

      ::std::copy_n(begin(), rcount, p_dst);

      return rcount;
    } else
      throw std::out_of_range("");
  }

  constexpr auto substr(size_type p_pos = 0, size_type p_count = npos) const
      -> TThis {
    if (p_pos < size()) {
      size_type t_tmp = size() - p_pos;
      size_type rcount = std::min(t_tmp, p_count);

      return TThis(data() + p_pos, rcount);
    } else
      throw ::std::out_of_range("");
  }

  constexpr auto compare(TThis p_other) const noexcept -> int {
    size_type rlen = ::std::min(size(), p_other.size());

    auto result = TTraits::compare(data(), p_other.data(), rlen);

    if (result == 0) {
      return size() - p_other.size();
    } else
      return result;
  }

  constexpr auto compare(size_type p_pos1, size_type p_count1, TThis p_v) const
      -> int {
    return substr(p_pos1, p_count1).compare(p_v);
  }

  constexpr auto compare(size_type p_pos1, size_type p_count1, TThis p_v,
                         size_type p_pos2, size_type p_count2) const -> int {
    return substr(p_pos1, p_count1).compare(p_v.substr(p_pos2, p_count2));
  }

  constexpr auto compare(const_pointer p_str) const -> int {
    return compare(TThis(p_str));
  }

  constexpr auto compare(size_type p_pos1, size_type p_count1,
                         const_pointer p_str) const -> int {
    return substr(p_pos1, p_count1).compare(TThis(p_str));
  }

  constexpr auto compare(size_type p_pos1, size_type p_count1,
                         const_pointer p_str, size_type p_count2) const -> int {
    return substr(p_pos1, p_count1).compare(TThis(p_str, p_count2));
  }

  constexpr auto find(TThis p_v, size_type p_pos = 0) const noexcept
      -> size_type {
    if (p_pos >= size() || p_v.empty())
      return npos;

    auto t_it = internal::search(internal::next(begin(), p_pos), end(),
                                 p_v.begin(), p_v.end(), &TTraits::eq);

    if (t_it != end())
      return internal::distance(begin(), t_it);
    else
      return npos;
  }

  constexpr auto find(TChar p_c, size_type p_pos = 0) const noexcept
      -> size_type {
    return find(TThis(&p_c, 1), p_pos);
  }

  constexpr auto find(const_pointer p_str, size_type p_pos,
                      size_type p_count) const -> size_type {
    return find(TThis(p_str, p_count), p_pos);
  }

  constexpr auto find(const_pointer p_str, size_type p_pos = 0) const
      -> size_type {
    return find(TThis(p_str), p_pos);
  }

  constexpr auto rfind(TThis p_v, size_type pos = npos) const noexcept
      -> size_type {
    size_type t_tmp = size() - 1;
    size_type rpos = std::min(t_tmp, pos);

    if (p_v.empty())
      return npos;

    auto t_it = internal::search(internal::next(rbegin(), t_tmp - rpos), rend(),
                                 p_v.rbegin(), p_v.rend(), &TTraits::eq);

    if (t_it != rend())
      return size() - internal::distance(rbegin(), t_it) - p_v.length();
    else
      return npos;
  }

  constexpr auto rfind(TChar p_c, size_type p_pos = npos) const noexcept
      -> size_type {
    return rfind(TThis(&p_c, 1), p_pos);
  }

  constexpr auto rfind(const_pointer p_str, size_type p_pos,
                       size_type p_count) const -> size_type {
    return rfind(TThis(p_str, p_count), p_pos);
  }

  constexpr auto rfind(const_pointer p_str, size_type p_pos = npos) const
      -> size_type {
    return rfind(TThis(p_str), p_pos);
  }

  constexpr auto find_first_of(TThis p_v, size_type p_pos = 0) const noexcept
      -> size_type {
    if (p_pos >= size() || p_v.empty())
      return npos;

    auto t_it = internal::find_first_of(internal::next(begin(), p_pos), end(),
                                        p_v.begin(), p_v.end(), &TTraits::eq);

    if (t_it != end())
      return internal::distance(begin(), t_it);
    else
      return npos;
  }

  constexpr auto find_first_of(TChar p_c, size_type p_pos = 0) const noexcept
      -> size_type {
    return find_first_of(TThis(&p_c, 1), p_pos);
  }

  constexpr auto find_first_of(const_pointer p_str, size_type p_pos,
                               size_type p_count) const -> size_type {
    return find_first_of(TThis(p_str, p_count), p_pos);
  }

  constexpr auto find_first_of(const_pointer p_str, size_type p_pos = 0) const
      -> size_type {
    return find_first_of(TThis(p_str), p_pos);
  }

  constexpr auto find_last_of(TThis p_v, size_type p_pos = npos) const noexcept
      -> size_type {
    size_type t_tmp = size() - 1;
    size_type rpos = std::min(t_tmp, p_pos);

    if (p_v.empty())
      return npos;

    auto t_it =
        internal::find_first_of(internal::next(rbegin(), t_tmp - rpos), rend(),
                                p_v.begin(), p_v.end(), &TTraits::eq);

    if (t_it != rend())
      return size() - internal::distance(rbegin(), t_it) - 1;
    else
      return npos;
  }

  constexpr auto find_last_of(TChar p_c, size_type p_pos = npos) const noexcept
      -> size_type {
    return find_last_of(TThis(&p_c, 1), p_pos);
  }

  constexpr auto find_last_of(const_pointer p_str, size_type p_pos,
                              size_type p_count) const -> size_type {
    return find_last_of(TThis(p_str, p_count), p_pos);
  }

  constexpr auto find_last_of(const_pointer p_str, size_type p_pos = npos) const
      -> size_type {
    return find_last_of(TThis(p_str), p_pos);
  }

  constexpr auto find_first_not_of(TThis p_v, size_type p_pos = 0) const
      noexcept -> size_type {
    if (p_pos >= size())
      return npos;

    if (p_v.empty())
      return p_pos;

    auto t_begin = internal::next(cbegin(), p_pos);

    for (; t_begin != cend(); ++t_begin) {
      if (p_v.find(*t_begin) == npos)
        break;
    }

    if (t_begin != cend())
      return internal::distance(cbegin(), t_begin);
    else
      return npos;
  }

  constexpr auto find_first_not_of(TChar p_c, size_type p_pos = 0) const
      noexcept -> size_type {
    return find_first_not_of(TThis(&p_c, 1), p_pos);
  }

  constexpr auto find_first_not_of(const_pointer p_str, size_type p_pos,
                                   size_type p_cnt) const -> size_type {
    return find_first_not_of(TThis(p_str, p_cnt), p_pos);
  }

  constexpr auto find_first_not_of(const_pointer p_str,
                                   size_type p_pos = 0) const -> size_type {
    return find_first_not_of(TThis(p_str), p_pos);
  }

  constexpr auto find_last_not_of(TThis p_v, size_type p_pos = npos) const
      noexcept -> size_type {
    auto t_rpos = (p_pos < size()) ? p_pos : size();

    if (p_v.empty())
      return t_rpos;

    auto t_begin = internal::next(crbegin(), size() - t_rpos);

    for (; t_begin != crend(); ++t_begin) {
      if (p_v.find(*t_begin) == npos)
        break;
    }

    if (t_begin != crend())
      return size() - internal::distance(crbegin(), t_begin) - 1;
    else
      return npos;
  }

  constexpr auto find_last_not_of(TChar p_c, size_type p_pos = npos) const
      noexcept -> size_type {
    return find_last_not_of(TThis(&p_c, 1), p_pos);
  }

  constexpr auto find_last_not_of(const_pointer p_str, size_type p_pos,
                                  size_type p_cnt) const -> size_type {
    return find_last_not_of(TThis(p_str, p_cnt), p_pos);
  }

  constexpr auto find_last_not_of(const_pointer p_str, size_type p_pos = npos)
      -> size_type {
    return find_last_not_of(TThis(p_str), p_pos);
  }

private:
  const_pointer m_Data;
  size_type m_Length;
};
//

using string_view = basic_string_view<char>;
using wstring_view = basic_string_view<wchar_t>;
using u16string_view = basic_string_view<char16_t>;
using u32string_view = basic_string_view<char32_t>;

template <typename TChar, typename TTraits>
auto operator<<(::std::basic_ostream<TChar, TTraits> &p_os,
                basic_string_view<TChar, TTraits> p_v)
    -> ::std::basic_ostream<TChar, TTraits> & {
  internal::put_character_sequence(p_os, p_v.data(), p_v.length());
  return p_os;
}

template <typename TChar, typename TTraits>
constexpr auto _cmp_base(basic_string_view<TChar, TTraits> p_lhs,
                         basic_string_view<TChar, TTraits> p_rhs) -> int {
  return p_lhs.compare(p_rhs);
}

template <typename TChar, typename TTraits>
constexpr auto operator==(basic_string_view<TChar, TTraits> p_lhs,
                          basic_string_view<TChar, TTraits> p_rhs) -> bool {
  return _cmp_base(p_lhs, p_rhs) == 0;
}

template <typename TChar, typename TTraits>
constexpr auto
operator==(internal::identity_t<basic_string_view<TChar, TTraits>> p_lhs,
           basic_string_view<TChar, TTraits> p_rhs) -> bool {
  return _cmp_base(p_lhs, p_rhs) == 0;
}

template <typename TChar, typename TTraits>
constexpr auto
operator==(basic_string_view<TChar, TTraits> p_lhs,
           internal::identity_t<basic_string_view<TChar, TTraits>> p_rhs)
    -> bool {
  return _cmp_base(p_lhs, p_rhs) == 0;
}

template <typename TChar, typename TTraits>
constexpr auto operator!=(basic_string_view<TChar, TTraits> p_lhs,
                          basic_string_view<TChar, TTraits> p_rhs) -> bool {
  return !(p_lhs == p_rhs);
}

template <typename TChar, typename TTraits>
constexpr auto
operator!=(internal::identity_t<basic_string_view<TChar, TTraits>> p_lhs,
           basic_string_view<TChar, TTraits> p_rhs) -> bool {
  return !(p_lhs == p_rhs);
}

template <typename TChar, typename TTraits>
constexpr auto
operator!=(basic_string_view<TChar, TTraits> p_lhs,
           internal::identity_t<basic_string_view<TChar, TTraits>> p_rhs)
    -> bool {
  return !(p_lhs == p_rhs);
}

template <typename TChar, typename TTraits>
constexpr auto operator<(basic_string_view<TChar, TTraits> p_lhs,
                         basic_string_view<TChar, TTraits> p_rhs) -> bool {
  return _cmp_base(p_lhs, p_rhs) < 0;
}

template <typename TChar, typename TTraits>
constexpr auto
operator<(internal::identity_t<basic_string_view<TChar, TTraits>> p_lhs,
          basic_string_view<TChar, TTraits> p_rhs) -> bool {
  return _cmp_base(p_lhs, p_rhs) < 0;
}

template <typename TChar, typename TTraits>
constexpr auto
operator<(basic_string_view<TChar, TTraits> p_lhs,
          internal::identity_t<basic_string_view<TChar, TTraits>> p_rhs)
    -> bool {
  return _cmp_base(p_lhs, p_rhs) < 0;
}

template <typename TChar, typename TTraits>
constexpr auto operator>(basic_string_view<TChar, TTraits> p_lhs,
                         basic_string_view<TChar, TTraits> p_rhs) -> bool {
  return _cmp_base(p_lhs, p_rhs) > 0;
}

template <typename TChar, typename TTraits>
constexpr auto
operator>(internal::identity_t<basic_string_view<TChar, TTraits>> p_lhs,
          basic_string_view<TChar, TTraits> p_rhs) -> bool {
  return _cmp_base(p_lhs, p_rhs) > 0;
}

template <typename TChar, typename TTraits>
constexpr auto
operator>(basic_string_view<TChar, TTraits> p_lhs,
          internal::identity_t<basic_string_view<TChar, TTraits>> p_rhs)
    -> bool {
  return _cmp_base(p_lhs, p_rhs) > 0;
}

template <typename TChar, typename TTraits>
constexpr auto operator>=(basic_string_view<TChar, TTraits> p_lhs,
                          basic_string_view<TChar, TTraits> p_rhs) -> bool {
  return !(p_lhs < p_rhs);
}

template <typename TChar, typename TTraits>
constexpr auto
operator>=(internal::identity_t<basic_string_view<TChar, TTraits>> p_lhs,
           basic_string_view<TChar, TTraits> p_rhs) -> bool {
  return !(p_lhs < p_rhs);
}

template <typename TChar, typename TTraits>
constexpr auto
operator>=(basic_string_view<TChar, TTraits> p_lhs,
           internal::identity_t<basic_string_view<TChar, TTraits>> p_rhs)
    -> bool {
  return !(p_lhs < p_rhs);
}

template <typename TChar, typename TTraits>
constexpr auto operator<=(basic_string_view<TChar, TTraits> p_lhs,
                          basic_string_view<TChar, TTraits> p_rhs) -> bool {
  return !(p_lhs > p_rhs);
}

template <typename TChar, typename TTraits>
constexpr auto
operator<=(internal::identity_t<basic_string_view<TChar, TTraits>> p_lhs,
           basic_string_view<TChar, TTraits> p_rhs) -> bool {
  return !(p_lhs > p_rhs);
}

template <typename TChar, typename TTraits>
constexpr auto
operator<=(basic_string_view<TChar, TTraits> p_lhs,
           internal::identity_t<basic_string_view<TChar, TTraits>> p_rhs)
    -> bool {
  return !(p_lhs > p_rhs);
}

// --- FNV1a hashing for byte sequences ---

// -- FNV1a prime number --
template <int> constexpr ::std::size_t fnv1a_prime_();

// 32bit hash size
template <> constexpr ::std::size_t fnv1a_prime_<4>() { return 16777619U; }

// 64bit hash size
template <> constexpr ::std::size_t fnv1a_prime_<8>() {
  return 1099511628211ULL;
}
// --

// -- FNV1a offset --
template <int> constexpr ::std::size_t fnv1a_offset_();

// 32bit hash size
template <> constexpr ::std::size_t fnv1a_offset_<4>() { return 2166136261U; }

// 64bit hash size
template <> constexpr ::std::size_t fnv1a_offset_<8>() {
  return 14695981039346656037ULL;
}
// --

inline auto hash_seq_(const unsigned char *p_arr, ::std::size_t p_len)
    -> ::std::size_t {
  constexpr ::std::size_t t_prime = fnv1a_prime_<sizeof(::std::size_t)>();
  constexpr ::std::size_t t_offset = fnv1a_offset_<sizeof(::std::size_t)>();

  // Set up hash
  ::std::size_t t_hash = t_offset;

  // Create hash by folding in bytes
  while (p_len--) {
    t_hash ^= *(p_arr++);
    t_hash *= t_prime;
  }

  return t_hash;
}
} // namespace detail
} // namespace atf

namespace std {
template <typename TChar, typename TTraits>
struct hash<::atf::detail::basic_string_view<TChar, TTraits>> {
  using argument_type = ::atf::detail::basic_string_view<TChar, TTraits>;
  using result_type = ::std::size_t;

  auto operator()(const argument_type &p_v) const noexcept -> result_type {
    return ::atf::detail::hash_seq_((const unsigned char *)p_v.data(),
                                    p_v.length() * sizeof(TChar));
  }
};
} // namespace std

#endif
