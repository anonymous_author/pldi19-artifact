# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/bastian/repos/atf/pvs-pjs-ss17/atf/examples/ccfg/src/main.cpp" "/home/bastian/repos/atf/pvs-pjs-ss17/atf/bld/examples/ccfg/CMakeFiles/ccfg.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "LIBATF_DISABLE_CUDA=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "/usr/include/python2.7"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/bastian/repos/atf/pvs-pjs-ss17/atf/bld/CMakeFiles/atf.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
