// clang-format off
/*
 * j3d7-sp.sdsl.c: This file is part of the SDSLC project.
 *
 * SDSLC: A compiler for high performance stencil computations
 *
 * Copyright (C) 2011-2013 Ohio State University
 *
 * This program can be redistributed and/or modified under the terms
 * of the license specified in the LICENSE.txt file at the root of the
 * project.
 *
 * Contact: P Sadayappan <saday@cse.ohio-state.edu>
 */

/*
 * @file: j3d7-sp.sdsl.c
 * @author: Tom Henretty <henretty@cse.ohio-state.edu>
 */
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <math.h>
#include <assert.h>

const float ONESEVENTH = (0.142857143f);

// Problem parameters
#ifndef D
#define D (256)
#endif

#ifndef H
#define H (256)
#endif

#ifndef W
#define W (256)
#endif
                                                                    
#ifndef T
//#define T (4)
#define T (1)
#endif

  
float aref[2][D][H][W];
float a[2][D][H][W];


/** Main program */
int main(int argc, char* argv[]) {
  int i, j, k, ii, jj, kk, t, tt;
  double refElapsed, sdslElapsed;
  double refGFLOPS, sdslGFLOPS;
  float (*inref)[H][W], (*outref)[H][W];
  
  // Initialize arrays
  for (i = 0; i < D; i++) {
    for (j = 0; j < H; j++) {
      for (k = 0; k < W; k++) {
        aref[0][i][j][k] = aref[1][i][j][k] = sin(i)*cos(j)*tan(k);
        a[0][i][j][k] = a[1][i][j][k] = sin(i)*cos(j)*tan(k);
      }
    }
  }

  // Compute reference
  #ifndef NOREF
  for (t = 0; t < T; t++) {
    inref  = aref[t & 1];
    outref = aref[(t + 1) & 1];
    for (i = 1; i < D - 1; i++) {
      for (j = 1; j < H - 1; j++) {
        #pragma ivdep
        #pragma vector always
        for (k = 1; k < W - 1; k++) {
          outref[i][j][k] = 0.161f * inref[i][j][k+1] + 0.162f * inref[i][j][k-1] +
          					0.163f * inref[i][j+1][k] + 0.164f * inref[i][j-1][k] +
          					0.165f * inref[i+1][j][k] + 0.166f * inref[i-1][j][k] -
          					1.67f * inref[i][j][k];
        }
      }
    }
  }
  #endif
 
  // Compute ppcg 
  #ifndef NOREF
  for (t = 0; t < T; t++) {
    inref  = a[t & 1];
    outref = a[(t + 1) & 1];
#pragma scop
    for (i = 1; i < D - 1; i++) {
      for (j = 1; j < H - 1; j++) {
        for (k = 1; k < W - 1; k++) {
          outref[i][j][k] = 0.161f * inref[i][j][k+1] + 0.162f * inref[i][j][k-1] +
          					0.163f * inref[i][j+1][k] + 0.164f * inref[i][j-1][k] +
          					0.165f * inref[i+1][j][k] + 0.166f * inref[i-1][j][k] -
          					1.67f * inref[i][j][k];
        }
      }
    }
#pragma endscop
  }
  #endif
  
  // Check correctness
  inref = a[T & 1]; outref = aref[T & 1];float maxDiff = 0.0, sumDiff = 0.0, diff, meanDiff;
  int numDiff = 0;
  for (i = 4; i < D-4; i++) {
    for (j = 4; j < H-4; j++) {
      for (k = 4; k < W-4; k++) {
        diff = fabs(outref[i][j][k] - inref[i][j][k]);
        if (diff > 0.0001f) {
          numDiff++;
          sumDiff += diff;
        }
        if (diff > maxDiff) {
          maxDiff = diff;
        }
      }
    }
  }
  meanDiff = sumDiff/((float)(H*W));
  
  printf("Num diff  = %d\n",numDiff);
  printf("Sum diff  = %.8f\n",sumDiff);
  printf("Mean diff = %.8f\n",meanDiff);
  printf("Max diff  = %.8f\n",maxDiff);
  
  return 0;
}
