#!/bin/bash
### README ##############################################################################################
# This script needs to be executed in $ROOTDIR/benchmarks/figure8/workflow2
# It tunes all generted lift kernels using ATF
#
# Requirements:
# * generate_lift_kernels_for_ppcg_benchmarks.sh was executed before
# * ATF build directory (containing atfc) in $ATF env var
#########################################################################################################
#copy tune script to atf directory
KERNELS=$(find $PWD -name "*Cl")
N="$(echo $KERNELS | wc -w)"
echo -e "\e[31m[WARNING] \e[97mThis script tunes $N kernel-sets each for a max duration of 3h"
echo -e "\e[31m[WARNING] \e[97mDepending on the machine, the tuning time for one set might also be << 3h"
echo
echo -e "\e[31m[WARNING] \e[97mDo you want to continue? (y/n)\e[0m"

read -p "" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    # do dangerous stuff
    cp $ROOTDIR/scripts/do_you_even_tune.sh $ATF
    
    
    pushd $ATF > /dev/null
    for k in $KERNELS ; do
        echo $k
        do_you_even_tune.sh $k
    done
    popd > /dev/null
fi

