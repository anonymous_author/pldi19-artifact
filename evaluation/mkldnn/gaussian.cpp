#include <iostream>
#include <cstdlib>
#include <argparse.hpp>
#include <fstream>
#include <chrono>
#include "../../extern/mkl-dnn/include/mkldnn.hpp"

int main(int argc, const char **argv) {
    // define and parse arguments
    ArgumentParser args;
    args.addArgument("--input-size",  2, false);
    args.parse(static_cast<size_t>(argc), argv);
    std::vector<int> input_size  = args.retrieve_int_vector("input-size");
    const int H = input_size[0], W = input_size[1];

    // prepare inputs
    std::vector<float> input((H + 4) * (W + 4)); for (int i = 0; i < input.size(); ++i) input[i] = (i % 100) + 1;
    std::vector<float> weights(5 * 5);
    float kernel_template[5][5] = {
            {2,  4,  5,  4, 2},
            {4,  9, 12,  9, 4},
            {5, 12, 15, 12, 5},
            {4,  9, 12,  9, 4},
            {2,  4,  5,  4, 2}
    };
    for (int row = 0; row < 5; ++row) {
        for (int column = 0; column < 5; ++column) {
            weights[row * 5 + column] = kernel_template[row][column] / 159.0f;
        }
    }
    std::vector<float> output(H * W); for (int i = 0; i < output.size(); ++i) output[i] = 0;

    // create MKL-DNN engine
    auto engine = mkldnn::engine(mkldnn::engine::cpu, 0);

    // create MKL-DNN tensors
    mkldnn::memory::dims input_dims  = {1, 1, (H + 4), (W + 4)};
    mkldnn::memory input_memory({{{input_dims},
                                  mkldnn::memory::data_type::f32, mkldnn::memory::format::nchw},
                                 engine},
                                input.data());
    mkldnn::memory::dims weight_dims = {1, 1, 5, 5};
    mkldnn::memory weights_memory({{{weight_dims},
                                    mkldnn::memory::data_type::f32, mkldnn::memory::format::oihw},
                                   engine},
                                  weights.data());
    mkldnn::memory::dims output_dims = {1, 1, H, W};
    mkldnn::memory output_memory({{{output_dims},
                                   mkldnn::memory::data_type::f32,
                                   mkldnn::memory::format::nchw},
                                  engine},
                                 output.data());
    mkldnn::memory::dims strides = {1, 1};
    auto padding = {0, 0};

    // create memory descriptors
    auto input_mem_desc = mkldnn::memory::desc({input_dims},
                                               mkldnn::memory::data_type::f32,
                                               mkldnn::memory::format::nchw);
    auto weights_mem_desc = mkldnn::memory::desc({weight_dims},
                                                 mkldnn::memory::data_type::f32,
                                                 mkldnn::memory::format::oihw);
    auto output_mem_desc = mkldnn::memory::desc({output_dims},
                                                mkldnn::memory::data_type::f32,
                                                mkldnn::memory::format::nchw);

    // create convolution descriptor
    auto conv_descriptor = mkldnn::convolution_forward::desc(
            mkldnn::prop_kind::forward,
            mkldnn::convolution_direct,
            input_mem_desc,
            weights_mem_desc,
            output_mem_desc,
            strides,
            padding,
            padding,
            mkldnn::padding_kind::zero
    );
    auto conv_primitive_descriptor = mkldnn::convolution_forward::primitive_desc(conv_descriptor, engine);

    // create primitives
    std::vector<mkldnn::primitive> primitives;
    mkldnn::memory i_input_memory(input_memory);
    if (mkldnn::memory::primitive_desc(conv_primitive_descriptor.src_primitive_desc()) != input_memory.get_primitive_desc()) {
        // add reorder primitive if necessary
        i_input_memory = mkldnn::memory(conv_primitive_descriptor.src_primitive_desc());
        primitives.push_back(mkldnn::reorder(input_memory, i_input_memory));
    }
    mkldnn::memory i_weights_memory(weights_memory);
    if (mkldnn::memory::primitive_desc(conv_primitive_descriptor.weights_primitive_desc()) != weights_memory.get_primitive_desc()) {
        // add reorder primitive if necessary
        i_weights_memory = mkldnn::memory(conv_primitive_descriptor.weights_primitive_desc());
        primitives.push_back(mkldnn::reorder(weights_memory, i_weights_memory));
    }
    mkldnn::memory i_output_memory(conv_primitive_descriptor.dst_primitive_desc());
    primitives.push_back(mkldnn::convolution_forward(conv_primitive_descriptor,
                                                     i_input_memory,
                                                     i_weights_memory,
                                                     i_output_memory));
    // reorder output if necessary
    if (i_output_memory != output_memory) {
        primitives.push_back(mkldnn::reorder(i_output_memory, output_memory));
    }

    // warm ups
    std::cout << std::endl << "benchmarking Intel MKL-DNN..." << std::endl;
    long runtime = std::numeric_limits<long>::max();
    for (int i = 0; i < 10; ++i) {
        // create stream for each execution (as done in the MKL-DNN examples)
        auto stream = mkldnn::stream(mkldnn::stream::kind::eager);
        // time warm_ups to prevent compiler optimization
        auto start = std::chrono::high_resolution_clock::now();
        stream.submit(primitives); stream.wait();
        auto end = std::chrono::high_resolution_clock::now();
        runtime = std::min(runtime, std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count());
    }

    // evaluations
    runtime = std::numeric_limits<long>::max();
    for (int i = 0; i < 200; ++i) {
        // create stream for each execution (as done in the MKL-DNN examples)
        auto stream = mkldnn::stream(mkldnn::stream::kind::eager);
        auto start = std::chrono::high_resolution_clock::now();
        stream.submit(primitives); stream.wait();
        auto end = std::chrono::high_resolution_clock::now();
        runtime = std::min(runtime, std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count());
    }

    // write runtime to file
    std::string runtime_file_name = std::getenv("ARTIFACT_ROOT");
    runtime_file_name.append("/results/cpu/mkldnn/");
    runtime_file_name.append("gaussian_");
    if (H == 220 && W == 220) {
        runtime_file_name.append("small");
    } else if (H == 4092 && W == 4092) {
        runtime_file_name.append("large");
    } else {
        runtime_file_name.append(std::to_string(H + 4)).append("x")
                .append(std::to_string(W + 4));
    }
    runtime_file_name.append("_runtime");
    std::ofstream runtime_file(runtime_file_name, std::ios::out | std::ios::trunc);
    runtime_file << (runtime / 1000000.0f);
    runtime_file.close();
}