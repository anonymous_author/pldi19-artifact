#include <iostream>
#include <cstdlib>
#include <argparse.hpp>
#include <mkl.h>
#include <fstream>

int main(int argc, const char **argv) {
    // define and parse arguments
    ArgumentParser args;
    args.addArgument("--input-size", 2, false);
    args.parse(static_cast<size_t>(argc), argv);
    std::vector<size_t> input_size  = args.retrieve_size_t_vector("input-size");
    const size_t M = input_size[0], N = input_size[1];

    // prepare inputs
    auto *a = (float *) mkl_malloc(M * N * sizeof(float), 64); for (int i = 0; i < M * N; ++i) a[i] = (i % 100) + 1;
    auto *b = (float *) mkl_malloc(    N * sizeof(float), 64); for (int i = 0; i <     N; ++i) b[i] = (i % 100) + 1;
    auto *c = (float *) mkl_malloc(M     * sizeof(float), 64); for (int i = 0; i < M    ; ++i) c[i] = 0;

    // warm ups
    std::cout << std::endl << "benchmarking Intel MKL..." << std::endl;
    double runtime = std::numeric_limits<double>::max();
    for (int i = 0; i < 10; ++i) {
        // time warm_ups to prevent compiler optimization
        auto start = dsecnd();
        cblas_sgemv(CblasRowMajor, CblasNoTrans,
                    M, N,
                    1,
                    a, N,
                    b, 1,
                    0,
                    c, 1);
        auto end = dsecnd();
        runtime = std::min(runtime, (end - start) * 1000);
    }

    // evaluations
    runtime = std::numeric_limits<double>::max();
    for (int i = 0; i < 200; ++i) {
        auto start = dsecnd();
        cblas_sgemv(CblasRowMajor, CblasNoTrans,
                    M, N,
                    1,
                    a, N,
                    b, 1,
                    0,
                    c, 1);
        auto end = dsecnd();
        runtime = std::min(runtime, (end - start) * 1000);
    }
    mkl_free(a);
    mkl_free(b);
    mkl_free(c);

    // write runtime to file
    std::string runtime_file_name = std::getenv("ARTIFACT_ROOT");
    runtime_file_name.append("/results/cpu/mkl/");
    runtime_file_name.append("gemv_");
    if (M == 4096 && N == 4096) {
        runtime_file_name.append("small");
    } else if (M == 8192 && N == 8192) {
        runtime_file_name.append("large");
    } else {
        runtime_file_name.append(std::to_string(M)).append("x")
                .append(std::to_string(N));
    }
    runtime_file_name.append("_runtime");
    std::ofstream runtime_file(runtime_file_name, std::ios::out | std::ios::trunc);
    runtime_file << runtime;
    runtime_file.close();
}