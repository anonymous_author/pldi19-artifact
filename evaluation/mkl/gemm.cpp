#include <iostream>
#include <cstdlib>
#include <argparse.hpp>
#include <mkl.h>
#include <fstream>

int main(int argc, const char **argv) {
    // define and parse arguments
    ArgumentParser args;
    args.addArgument("--input-size",  3, false);
    args.parse(static_cast<size_t>(argc), argv);
    std::vector<size_t> input_size  = args.retrieve_size_t_vector("input-size");
    const size_t M = input_size[0], N = input_size[1], K = input_size[2];

    // prepare inputs
    auto *a = (float *) mkl_malloc(M * K * sizeof(float), 64); for (int i = 0; i < M * K; ++i) a[i] = (i % 100) + 1;
    auto *b = (float *) mkl_malloc(K * N * sizeof(float), 64); for (int i = 0; i < K * N; ++i) b[i] = (i % 100) + 1;
    auto *c = (float *) mkl_malloc(M * N * sizeof(float), 64); for (int i = 0; i < M * N; ++i) c[i] = 0;

    // warm ups
    std::cout << std::endl << "benchmarking Intel MKL..." << std::endl;
    double runtime = std::numeric_limits<double>::max();
    for (int i = 0; i < 10; ++i) {
        // time warm_ups to prevent compiler optimization
        auto start = dsecnd();
        cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
                    M, N, K,
                    1,
                    a, K,
                    b, N,
                    0,
                    c, N);
        auto end = dsecnd();
        runtime = std::min(runtime, (end - start) * 1000);
    }

    // evaluations
    runtime = std::numeric_limits<double>::max();
    for (int i = 0; i < 200; ++i) {
        auto start = dsecnd();
        cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
                    M, N, K,
                    1,
                    a, K,
                    b, N,
                    0,
                    c, N);
        auto end = dsecnd();
        runtime = std::min(runtime, (end - start) * 1000);
    }
    mkl_free(a);
    mkl_free(b);
    mkl_free(c);

    // write runtime to file
    std::string runtime_file_name = std::getenv("ARTIFACT_ROOT");
    runtime_file_name.append("/results/cpu/mkl/");
    runtime_file_name.append("gemm_");
    if (M == 10 && N == 500 && K == 64) {
        runtime_file_name.append("small");
    } else if (M == 1024 && N == 1024 && K == 1024) {
        runtime_file_name.append("large");
    } else {
        runtime_file_name.append(std::to_string(M)).append("x")
                .append(std::to_string(N)).append("x")
                .append(std::to_string(K));
    }
    runtime_file_name.append("_runtime");
    std::ofstream runtime_file(runtime_file_name, std::ios::out | std::ios::trunc);
    runtime_file << runtime;
    runtime_file.close();
}