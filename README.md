# Portable Parallel Performance via Multi-Dimensional Homomorphisms

This preliminary artifact contains the workflow to reproduce the results shown in the paper *Portable Parallel Performance via Multi-Dimensional Homomorphisms*. The reviewer is invited to perform the steps described below. In case of **any problems**, please feel free to **open an issue** in order to get in contact with the authors.

In addition to the application examples discussed in the paper (BLAS routines and stencil computations), this repository also contains our preliminary implementations for several other applications, which confirm the wide applicability of our approach: Ensemble of Classifier Chains (ECC), Multi-Layer Perceptron (MLP), Support Vector Machines (SVM), and Record Linkage (RL). The source code for these examples can be found in the `preliminary` folder.

## Software Requirements

- an OpenCL driver and runtime environment
- OpenGL (libmesa)
- CMake 3.8 or higher
- a compiler supporting C++14 or higher
- Boost 1.56 or higher
- OpenSSL
- finger
- Python 2.7 (not Python 3.x)
- tabulate Python package
- OpenTuner 0.8.0
- Java 8 SDK
- Intel MKL
- NVIDIA cuBLAS
- NVIDIA cuDNN

## Workflow

The workflow of this artifact is divided into three main steps: **installation**, **tuning**, and **benchmarking**. Note that the tuning step may take up to 130h per device, because for each routine and input size both md_hom and Lift are tuned for 5h. To reduce the overall runtime of the artifact workflow, the tuning step can be omitted and the tuning results found on the system described in the paper are used instead. **Be aware that - in case the artifact is executed on devices different from the ones listed in the paper - the tuning step has to be exectued in order to achieve the best, and thus portable, performance. Omitting the tuning step and using our provided parameter values may cause suboptimal performance on a different device and, therefore, prevent the reviewer from reproducing the results shown in the paper.**

All experiments are compiled with the `-O3` flag. Additionally, for the Intel MKL experiments, we use the following flags, as advised by the [Intel Math Kernel Library Link Line Advisor](https://software.intel.com/en-us/articles/intel-mkl-link-line-advisor):

`-Wl,--no-as-needed -lmkl_intel_ilp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm -ldl`

### Step 1: Installation

Before installing the artifact, the following dependencies have to be installed:

- **OpenCL driver and runtime:**
  
  Download and install the OpenCL driver and runtime from the vendor website of the utilized hardware.
  
- **OpenGL (libmesa)**:
  
  `sudo apt-get install libgl1-mesa-dev`
  
- **CMake 3.8 or higher**:

  Download CMake from the developer website:
  
  `wget https://cmake.org/files/v3.13/cmake-3.13.0-rc3-Linux-x86_64.sh`
  
  Install CMake locally. If asked if you want to include the subdirectory in the installation path, type `y`:
  
  `/bin/bash cmake-3.13.0-rc3-Linux-x86_64.sh`
  
  Make CMake available:
  
  ``export PATH=`pwd`/cmake-3.13.0-rc3-Linux-x86_64/bin:$PATH``
  
- **A compiler supporting C++14 or higher**:
  
  `sudo apt-get install gcc g++`

- **Boost 1.56 or higher**:

  `sudo apt-get install libboost-all-dev`
  
- **OpenSSL**:

  `sudo apt-get install libssl-dev`
  
- **finger**:

  `sudo apt-get install finger`

- **Python 2.7 (not Python 3.x)**:

  `sudo apt-get install python-dev python-pip`
  
- **tabulate Python package**:

  `sudo pip install tabulate`

- **OpenTuner 0.8.0**:

  OpenTuner has dependencies itself. The OpenTuner dependencies can be installed with:
  
  `sudo apt-get install sqlite3 libsqlite3-dev`
  
  Afterwards, install OpenTuner by executing:
  
  `sudo pip install opentuner`
  
- **Java 8 SDK**:

  The Oracle Java SDK can be installed as follows:
  
  ```
  sudo su
  echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | tee /etc/apt/sources.list.d/webupd8team-java.list
  echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | tee -a /etc/apt/sources.list.d/webupd8team-java.list
  apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886
  apt-get update
  apt-get install oracle-java8-installer
  exit
  ```
  
  After installing Java, please restart your terminal session.
  
- **Intel MKL**:

  Intel MKL can be downloaded at [https://software.intel.com/en-us/mkl/choose-download/linux](https://software.intel.com/en-us/mkl/choose-download/linux). After registering, the Intel MKL library can be downloaded and installed using the provided installation scripts.


- **NVIDIA cuBLAS**:

  NVIDIA cuBLAS is bundled into NVIDIA CUDA Toolkit. The CUDA Toolkit can be downloaded at [https://developer.nvidia.com/cuda-downloads](https://developer.nvidia.com/cuda-downloads). Choose your preferred download type and follow the instructions to install NVIDIA CUDA Toolkit.

- **NVIDIA cuDNN**:

  NVIDIA cuDNN has to be manually installed into an existing NVIDIA CUDA Toolkit installation. The required steps can be found at [https://docs.nvidia.com/deeplearning/sdk/cudnn-install/index.html](https://docs.nvidia.com/deeplearning/sdk/cudnn-install/index.html).
  
- **Installing the artifact**:
  
  Clone the artifact repository:
  
  `git clone https://gitlab.com/anonymous_author/pldi19-artifact.git`
  
  Change into the artifact directory:
  
  `cd pldi19-artifact`
  
  Edit the `environment.env` configuration file. Enter the OpenCL platform and device ids of the CPU and GPU device you wish to evaluate on. In case the system is not equipped with a GPU or you do not wish to evaluate on the CPU or GPU, remove the comment symbol (`#`) on the corresponding line:
  
  ```
  # CPU Evaluation
  # uncomment if you do not want to evaluate on a CPU
  #export DISABLE_CPU=1
  # platform and device id of the OpenCL cpu to evaluate on
  export OCL_CPU_PLATFORM_ID=0
  export OCL_CPU_DEVICE_ID=0
  
  # GPU Evaluation
  # uncomment if you do not want to evaluate on a GPU
  #export DISABLE_GPU=1
  # platform and device id of the OpenCL gpu to evaluate on
  export OCL_GPU_PLATFORM_ID=0
  export OCL_GPU_DEVICE_ID=0
  export CUDA_GPU_DEVICE_ID=0
  
  export ARTIFACT_ROOT=`pwd`
  ``` 


### Step 2: Tuning OR Using Default Values

The scripts for tuning or using default values are provided in two variants: 1) the ones for tuning and using default values on the cpu (containing `cpu` in their names), and 2) the equivalents on the gpu (containing `gpu` in their names). The reviewer is free to choose for wich device type to evaluate by executing only the scripts for the desired device type.

#### Step 2a: Tuning

Note that this step may take up to 130h per device, because for each routine and input size both md_hom and Lift are tuned for 5h. To reduce the overall runtime of the artifact workflow, the tuning step can be omitted and the tuning results found on the system described in the paper are used instead. Continue with step 2b in case you want to omit the tuning step. **Be aware that - in case the artifact is executed on devices different from the ones listed in the paper - the tuning step has to be exectued in order to achieve the best, and thus portable, performance. Omitting the tuning step and using our provided parameter values may cause suboptimal performance on a different device and, therefore, prevent the reviewer from reproducing the results shown in the paper.**

- Tune md_hom:

  `scripts/tune_cpu_md_hom.sh`
  
  `scripts/tune_gpu_md_hom.sh`

- Tune references:

  `scripts/tune_cpu_references.sh`
  
  `scripts/tune_gpu_references.sh`

#### Step 2b: Use Default Values

- Execute the script to use the default values on CPU and/or GPU:

  `scripts/use_cpu_defaults.sh`
  
  `scripts/use_gpu_defaults.sh`

### Step 3: Benchmarking

- Execute md_hom:
  
  `scripts/run_cpu_md_hom.sh`
  
  `scripts/run_gpu_md_hom.sh`

- Execute references:
  
  `scripts/run_cpu_references.sh`
  
  `scripts/run_gpu_references.sh`

- Print results:
  
  `scripts/print_results.sh`