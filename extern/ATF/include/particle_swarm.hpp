/**
 * \file particle_swarm.hpp
 * \author
 * \date 05/08/2018
 */

#ifndef particle_swarm_h
#define particle_swarm_h

#include <random>
#include <vector>

#include "coordinate_space.hpp"
#include "detail/pso/multivariate_distribution.hpp"
#include "detail/pso/particle.hpp"
#include "detail/pso/swarm.hpp"
#include "detail/pso/xs.hpp"

#ifdef EVAL
#include <cctof.hpp>
#endif

namespace atf {

  /**
   * \brief Implementation of public ATF interface for particle swarm search technique.
   *
   * \tparam N      number of swarms
   * \tparam M      number of particles per swarm
   * \tparam XFunc  type of crossover functor
   * \tparam T      type of tuner to inherit from
   */
  template<std::size_t N, std::size_t M, typename XFunc, typename T = tuner_with_constraints>
  class particle_swarm_class : public T {
  public:

    using x_func = XFunc;
    using distribution_type = typename XFunc::distribution_type; 

    /**
     * \brief Constructs new particle swarm technique.
     *
     * \tparam Ts       types of forward-arguments for tuner base class
     *
     * \param params    forward-arguments for tuner base class
     */
    template<typename... Ts>
    particle_swarm_class(Ts... params)
      : T(params...)
      , _dist {0.0, XFunc::phi_one}
    {
#ifdef EVAL
        _os << "Time (sec), costs, best costs" << std::endl;
#endif
    }

    /**
     * \brief Initialises the search technique on specific search space.
     *
     * Rather than using the default ATF search space, the particle swarm
     * search technique will make use of the coordinate space to move in the
     * search space.
     *
     * \param search_space  search space to initialise the particle swarm on
     */
    void initialize(const search_space &search_space)
    {
      _search_space = coordinate_space {search_space};

      /*
       * Reserve memory for the N swarms and then use `emplace_back`
       * to make this as fast as possible for many swarms without
       * unnecessary default initialization as before with std::array
       */
      _swarms.reserve(N);
      for (std::size_t i = 0; i < N; ++i) {
        _swarms.emplace_back(_search_space.dimension());
      }
    }

    /**
     * \brief Provides next found configuration.
     *
     * Therefore will iterate over all particles in all swarms that are used and returns that
     * particle's configuration. When all particles have been looked at exactly once, this function
     * will automatically call the swarms' `move()` function before providing the next
     * configuration.
     *
     * This means that this function has to be called `M * N` times before the particles are moved.
     */
    configuration get_next_config()
    {
      ++_pos;
      if (_pos == M * N) {
        _pos = 0;

        for (auto &sw : _swarms) {
          sw.move(_dist, x_func{});
        }
      }

      /* Get current particle */
      detail::pso::particle& p = _swarms[_pos % N][_pos % M];
      /* Check if current particle has valid position. If not, use fmod to map its position to the coordinate_space. */
      if (!_search_space.is_valid_point(p.position())) {
        p.set_position(_search_space.convert_to_valid_point_mod(p.position()));
      }

      return _search_space.get_config(p.position());
    }

    /**
     * \brief Callback to report the result of the last provided configuration.
     *
     * The determined fitness is forwarded to the swarm the last configuration was delivered
     * from. The swarm and particle themselves will decide how to act on the determined
     * fitness value.
     *
     * \param result    determined runtime costs of the last found configuration
     */
    void report_result(const unsigned long long &result)
    {
      _swarms[_pos % N].report_fitness(result, _pos % M);

#ifdef EVAL
      auto n = std::chrono::system_clock::now();
      _os << std::chrono::duration_cast<std::chrono::seconds>(n - this->tuning_start_time()).count()
          << ", " << result << ", " << this->best_measured_result()
          << std::endl;
#endif
    }

    /**
     * \brief Finalizes the particle swarm search technique.
     *
     * Empty. All member have automatic storage duration.
     */
    void finalize()
    {

    }

      std::string display_string() const {
          return "Particle Swarm Optimization";
      }

  private:
    /** coordinate search space */
    coordinate_space _search_space;
    /** vector of swarms to use */
    std::vector<detail::pso::swarm<M>> _swarms;
    /** probability distribution to use for crossover functor */
    distribution_type _dist;
    /** index of current particle */
    std::size_t _pos {0};

#ifdef EVAL
    /** output stream in case of evaluation output */
    std::ofstream _os {"pso_eval_data.csv", std::ios::app};
#endif
  };

  /**
   * \brief Factory function constructing a new instance of the particle swarm search technique.
   *
   * \tparam N      number of swarms to use
   * \tparam M      number of particles per swarm
   * \tparam XFunc  type of crossover functor
   * \tparam Ts     types to forward to tuner base class
   *
   * \param args    arguments to forward to tuner base class
   *
   * \return        new instance of the particle swarm search technique
   */
  template<std::size_t N = 1, std::size_t M = 30, typename XFunc = detail::pso::xs::def, typename... Ts>
  auto particle_swarm(Ts... args)
  {
    return particle_swarm_class<N, M, XFunc>{args...};
  }

  /**
   * \brief Factory function construction a new instance of the particle swarm search technique,
   *        derived from specified tuner.
   *
   * \tparam T      tuner base class to inherit from
   * \tparam N      number of swarms to use
   * \tparam M      number of particles per swarm
   * \tparam XFunc  type of crossover functor
   * \tparam Ts     types to forward to tuner base class
   *
   * \param args    arguments to forward to tuner base class
   *
   * \return        new instance of the particle swarm search technique
   */
  template <typename T, std::size_t N = 1, std::size_t M = 30, typename XFunc = detail::pso::xs::def, typename... Ts>
  auto particle_swarm(Ts... args)
  {
    return particle_swarm_class<N, M, XFunc, T> {args...};
  }

} /* namespace atf */

#endif
