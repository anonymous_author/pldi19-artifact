//
//  tuner_with_constraints_with_constraints.cpp
//  new_atf_lib
//
//  Created by   on 21/03/2017.
//  Copyright © 2017  . All rights reserved.
//

#include <stdio.h>

#include "../include/tuner_with_constraints.hpp"
#include "../include/tp_value_node.hpp"


namespace atf
{
  
tuner_with_constraints::tuner_with_constraints( const bool& abort_on_error )
  : _logging_file(""), _search_space(), _abort_on_error( abort_on_error ), _number_of_evaluated_configs(), _number_of_invalid_configs(), _evaluations_required_to_find_best_found_result(), _history()
{
  _abort_condition = NULL; // new abort_condition_t( abort_condition );
  
  _history.emplace_back( std::chrono::high_resolution_clock::now(),
                         configuration{},
                         std::numeric_limits<unsigned long long>::max()
                       );
}
  

tuner_with_constraints::tuner_with_constraints()
  : _logging_file(""), _search_space(), _abort_on_error( false ), _number_of_evaluated_configs(), _number_of_invalid_configs(), _evaluations_required_to_find_best_found_result(), _history()
{
  _abort_condition = NULL; // new abort_condition_t( abort_condition );
  
  _history.emplace_back( std::chrono::high_resolution_clock::now(),
                         configuration{},
                         std::numeric_limits<unsigned long long>::max()
                       );
}
  
  
tuner_with_constraints::~tuner_with_constraints()
{
//  std::cout << "\nNumber of Tree nodes: " << tp_value_node::number_of_nodes() << std::endl;
}


std::chrono::high_resolution_clock::time_point tuner_with_constraints::tuning_start_time() const
{
  return std::get<0>( _history.front() );
}


size_t tuner_with_constraints::number_of_evaluated_configs() const
{
  return _number_of_evaluated_configs;
}


size_t tuner_with_constraints::number_of_valid_evaluated_configs() const
{
  return _number_of_evaluated_configs - _number_of_invalid_configs;
}


unsigned long long tuner_with_constraints::best_measured_result() const
{
  return std::get<2>( _history.back() );
}

configuration tuner_with_constraints::best_configuration() const
{
  return std::get<1>( _history.back() );
}

const std::vector<tuner_with_constraints::history_entry>& tuner_with_constraints::history() const {
  return _history;
}

void tuner_with_constraints::insert_tp_names_in_search_space()
{}

void tuner_with_constraints::insert_tp_names_of_one_tree_in_search_space()
{}



// loeschen
void tuner_with_constraints::print_path()
{
  std::cout << std::endl;
}

std::string tuner_with_constraints::display_string_with_abort_condition() const {
    return display_string() + " (" + _abort_condition->display_string() + ")";
}


} // namespace "atf"

