R"(

import opentuner
from opentuner.search.manipulator import ConfigurationManipulator
from opentuner.measurement.interface import DefaultMeasurementInterface
from opentuner.api import TuningRunManager
from opentuner.resultsdb.models import Result
from opentuner.search.manipulator import IntegerParameter
import argparse
import time

def get_next_desired_result():
    global desired_result
    desired_result = None
    start_time = int(time.time())
    while desired_result is None:
        current_time = int(time.time())
        elapsed = (current_time - start_time)
        if elapsed >= 300:
            print "[ATF] OpenTuner timeout"
            desired_result = None
            return desired_result
        desired_result = api.get_next_desired_result()
    return desired_result.configuration.data

def report_result(runtime):
    if not desired_result is None:
        api.report_result(desired_result, Result(time=runtime))

def finish():
    api.finish()

parser = argparse.ArgumentParser(parents=opentuner.argparsers())
args = parser.parse_args()

manipulator = ConfigurationManipulator()
:::parameters:::
interface = DefaultMeasurementInterface(args=args,
                                        manipulator=manipulator,
                                        project_name='atf_library',
                                        program_name='atf_library',
                                        program_version='0.1')


api = TuningRunManager(interface, args)

)"
