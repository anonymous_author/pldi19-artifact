#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."
	exit 1
fi
: ${OCL_GPU_PLATFORM_ID?"Please set the environment variable OCL_GPU_PLATFORM_ID."}
if [ -z "$OCL_GPU_PLATFORM_ID" ]
then
    	echo "Please set the environment variable OCL_GPU_PLATFORM_ID."
	exit 1
fi
: ${OCL_GPU_DEVICE_ID?"Please set the environment variable OCL_GPU_DEVICE_ID."}
if [ -z "$OCL_GPU_DEVICE_ID" ]
then
    	echo "Please set the environment variable OCL_GPU_DEVICE_ID."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."; exit 1; }
{
  cd build/evaluation/lift_stencil/artifact/ &&
  source environment.env &&
  export OCL_PLATFORM_ID=$OCL_GPU_PLATFORM_ID &&
  export OCL_DEVICE_ID=$OCL_GPU_DEVICE_ID &&
  {
    rm ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gaussian_small_runtime &> /dev/null
    ./scripts/run_workflow2_figure8.sh nvidia gaussian small &&
    mkdir -p ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
    cp benchmarks/figure8/workflow2/gaussian/small/kepler/gaussianCl/best.cl ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gaussian_small_kernel.cl &&
    hash=`grep -oP "(?<=// Low-level hash: ).*" benchmarks/figure8/workflow2/gaussian/small/kepler/gaussianCl/best.cl` &&
    tmp=`grep -oP "(?<=$hash.cl -> )\\d+" benchmarks/figure8/workflow2/gaussian/small/kepler/gaussianCl/summary.txt` &&
    gs_0=`grep -oP "(?<=$hash.cl -> $tmp \\()\\d+" benchmarks/figure8/workflow2/gaussian/small/kepler/gaussianCl/summary.txt` &&
    gs_1=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;)\\d+" benchmarks/figure8/workflow2/gaussian/small/kepler/gaussianCl/summary.txt` &&
    ls_0=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;)\\d+" benchmarks/figure8/workflow2/gaussian/small/kepler/gaussianCl/summary.txt` &&
    ls_1=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;$ls_0;)\\d+" benchmarks/figure8/workflow2/gaussian/small/kepler/gaussianCl/summary.txt` &&
    printf "{\n    \"GLOBAL_SIZE_0\": $gs_0,\n    \"GLOBAL_SIZE_1\": $gs_1,\n    \"LOCAL_SIZE_0\": $ls_0,\n    \"LOCAL_SIZE_1\": $ls_1\n}" > ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gaussian_small_config.json
  }
  {
    rm ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gaussian_large_runtime &> /dev/null
    ./scripts/run_workflow2_figure8.sh nvidia gaussian large &&
    mkdir -p ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
    cp benchmarks/figure8/workflow2/gaussian/big/kepler/gaussianCl/best.cl ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gaussian_large_kernel.cl &&
    hash=`grep -oP "(?<=// Low-level hash: ).*" benchmarks/figure8/workflow2/gaussian/big/kepler/gaussianCl/best.cl` &&
    tmp=`grep -oP "(?<=$hash.cl -> )\\d+" benchmarks/figure8/workflow2/gaussian/big/kepler/gaussianCl/summary.txt` &&
    gs_0=`grep -oP "(?<=$hash.cl -> $tmp \\()\\d+" benchmarks/figure8/workflow2/gaussian/big/kepler/gaussianCl/summary.txt` &&
    gs_1=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;)\\d+" benchmarks/figure8/workflow2/gaussian/big/kepler/gaussianCl/summary.txt` &&
    ls_0=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;)\\d+" benchmarks/figure8/workflow2/gaussian/big/kepler/gaussianCl/summary.txt` &&
    ls_1=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;$ls_0;)\\d+" benchmarks/figure8/workflow2/gaussian/big/kepler/gaussianCl/summary.txt` &&
    printf "{\n    \"GLOBAL_SIZE_0\": $gs_0,\n    \"GLOBAL_SIZE_1\": $gs_1,\n    \"LOCAL_SIZE_0\": $ls_0,\n    \"LOCAL_SIZE_1\": $ls_1\n}" > ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gaussian_large_config.json
  }
  {
    rm ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/j3d7pt_small_runtime &> /dev/null
    ./scripts/run_workflow2_figure8.sh nvidia j3d7pt small &&
    mkdir -p ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
    cp benchmarks/figure8/workflow2/j3d7pt/small/kepler/j3d7ptCl/best.cl ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/j3d7pt_small_kernel.cl &&
    hash=`grep -oP "(?<=// Low-level hash: ).*" benchmarks/figure8/workflow2/j3d7pt/small/kepler/j3d7ptCl/best.cl` &&
    tmp=`grep -oP "(?<=$hash.cl -> )\\d+" benchmarks/figure8/workflow2/j3d7pt/small/kepler/j3d7ptCl/summary.txt` &&
    gs_0=`grep -oP "(?<=$hash.cl -> $tmp \\()\\d+" benchmarks/figure8/workflow2/j3d7pt/small/kepler/j3d7ptCl/summary.txt` &&
    gs_1=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;)\\d+" benchmarks/figure8/workflow2/j3d7pt/small/kepler/j3d7ptCl/summary.txt` &&
    gs_2=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;)\\d+" benchmarks/figure8/workflow2/j3d7pt/small/kepler/j3d7ptCl/summary.txt` &&
    ls_0=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;$gs_2;)\\d+" benchmarks/figure8/workflow2/j3d7pt/small/kepler/j3d7ptCl/summary.txt` &&
    ls_1=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;$gs_2;$ls_0;)\\d+" benchmarks/figure8/workflow2/j3d7pt/small/kepler/j3d7ptCl/summary.txt` &&
    ls_2=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;$gs_2;$ls_0;$ls_1;)\\d+" benchmarks/figure8/workflow2/j3d7pt/small/kepler/j3d7ptCl/summary.txt` &&
    printf "{\n    \"GLOBAL_SIZE_0\": $gs_0,\n    \"GLOBAL_SIZE_1\": $gs_1,\n    \"GLOBAL_SIZE_2\": $gs_2,\n    \"LOCAL_SIZE_0\": $ls_0,\n    \"LOCAL_SIZE_1\": $ls_1,\n    \"LOCAL_SIZE_2\": $ls_2\n}" > ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/j3d7pt_small_config.json
  }
  {
    rm ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/j3d7pt_large_runtime &> /dev/null
    ./scripts/run_workflow2_figure8.sh nvidia j3d7pt large &&
    mkdir -p ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
    cp benchmarks/figure8/workflow2/j3d7pt/big/kepler/j3d7ptCl/best.cl ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/j3d7pt_large_kernel.cl &&
    hash=`grep -oP "(?<=// Low-level hash: ).*" benchmarks/figure8/workflow2/j3d7pt/big/kepler/j3d7ptCl/best.cl` &&
    tmp=`grep -oP "(?<=$hash.cl -> )\\d+" benchmarks/figure8/workflow2/j3d7pt/big/kepler/j3d7ptCl/summary.txt` &&
    gs_0=`grep -oP "(?<=$hash.cl -> $tmp \\()\\d+" benchmarks/figure8/workflow2/j3d7pt/big/kepler/j3d7ptCl/summary.txt` &&
    gs_1=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;)\\d+" benchmarks/figure8/workflow2/j3d7pt/big/kepler/j3d7ptCl/summary.txt` &&
    gs_2=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;)\\d+" benchmarks/figure8/workflow2/j3d7pt/big/kepler/j3d7ptCl/summary.txt` &&
    ls_0=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;$gs_2;)\\d+" benchmarks/figure8/workflow2/j3d7pt/big/kepler/j3d7ptCl/summary.txt` &&
    ls_1=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;$gs_2;$ls_0;)\\d+" benchmarks/figure8/workflow2/j3d7pt/big/kepler/j3d7ptCl/summary.txt` &&
    ls_2=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;$gs_2;$ls_0;$ls_1;)\\d+" benchmarks/figure8/workflow2/j3d7pt/big/kepler/j3d7ptCl/summary.txt` &&
    printf "{\n    \"GLOBAL_SIZE_0\": $gs_0,\n    \"GLOBAL_SIZE_1\": $gs_1,\n    \"GLOBAL_SIZE_2\": $gs_2,\n    \"LOCAL_SIZE_0\": $ls_0,\n    \"LOCAL_SIZE_1\": $ls_1,\n    \"LOCAL_SIZE_2\": $ls_2\n}" > ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/j3d7pt_large_config.json
  }

  # md_hom_initial
  cd ${ARTIFACT_ROOT}/build/evaluation/md_hom_initial
  mkdir -p $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/gemm_small_runtime &> /dev/null
  ./md_hom_initial_gemm --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 10 500 64

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/gemm_large_runtime &> /dev/null
  ./md_hom_initial_gemm --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 1024 1024 1024

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/gemv_small_runtime &> /dev/null
  ./md_hom_initial_gemv --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 4096 4096

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/gemv_large_runtime &> /dev/null
  ./md_hom_initial_gemv --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 8192 8192

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/gaussian_static_small_runtime &> /dev/null
  ./md_hom_initial_gaussian_static --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 220 220

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/gaussian_static_large_runtime &> /dev/null
  ./md_hom_initial_gaussian_static --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 4092 4092

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/gaussian_dynamic_small_runtime &> /dev/null
  ./md_hom_initial_gaussian_dynamic --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 220 220

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/gaussian_dynamic_large_runtime &> /dev/null
  ./md_hom_initial_gaussian_dynamic --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 4092 4092

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/j3d7pt_small_runtime &> /dev/null
  ./md_hom_initial_j3d7pt --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 254 254 254

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/j3d7pt_large_runtime &> /dev/null
  ./md_hom_initial_j3d7pt --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 510 510 510

  printf "\n\nReference tuning successful!\n"
} || {
  printf "\n\nReference tuning failed!\n"
  exit 1
}