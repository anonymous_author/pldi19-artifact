#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."
	exit 1
fi
: ${OCL_GPU_PLATFORM_ID?"Please set the environment variable OCL_GPU_PLATFORM_ID."}
if [ -z "$OCL_GPU_PLATFORM_ID" ]
then
    	echo "Please set the environment variable OCL_GPU_PLATFORM_ID."
	exit 1
fi
: ${OCL_GPU_DEVICE_ID?"Please set the environment variable OCL_GPU_DEVICE_ID."}
if [ -z "$OCL_GPU_DEVICE_ID" ]
then
    	echo "Please set the environment variable OCL_GPU_DEVICE_ID."
	exit 1
fi
: ${CUDA_GPU_DEVICE_ID?"Please set the environment variable CUDA_GPU_DEVICE_ID."}
if [ -z "$CUDA_GPU_DEVICE_ID" ]
then
    	echo "Please set the environment variable CUDA_GPU_DEVICE_ID."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."; exit 1; }
{
  # NVIDIA cuBLAS
  cd $ARTIFACT_ROOT/build/evaluation/cublas &&
  mkdir -p ${ARTIFACT_ROOT}/results/gpu/cublas
  ./cublas_gemm --device-id $CUDA_GPU_DEVICE_ID --input-size 10 500 64
  ./cublas_gemm --device-id $CUDA_GPU_DEVICE_ID --input-size 1024 1024 1024
  ./cublas_gemv --device-id $CUDA_GPU_DEVICE_ID --input-size 4096 4096
  ./cublas_gemv --device-id $CUDA_GPU_DEVICE_ID --input-size 8192 8192

  # Lift BLAS
  cd $ARTIFACT_ROOT/build/evaluation/lift_blas &&
  (
    export PARENT_ARTIFACT_ROOT=$ARTIFACT_ROOT
    export ARTIFACT_ROOT=`pwd`
    {
      lift/scripts/MatrixVector --iterations 210 --variant 3 --ig --il -s 4096 -s 4096 -l 64 -g 4096 --loadKernel generated_kernels/gemv/gemv_N.cl -p $OCL_GPU_PLATFORM_ID -d $OCL_GPU_DEVICE_ID > gemv_lift_small.log &&
      mkdir -p ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
      grep -oP "(?<=MIN: )\\d+\\.\\d+" gemv_lift_small.log > ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gemv_small_runtime
    }
    {
      lift/scripts/MatrixVector --iterations 210 --variant 3 --ig --il -s 8192 -s 8192 -l 64 -g 8192 --loadKernel generated_kernels/gemv/gemv_N.cl -p $OCL_GPU_PLATFORM_ID -d $OCL_GPU_DEVICE_ID > gemv_lift_large.log &&
      mkdir -p ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
      grep -oP "(?<=MIN: )\\d+\\.\\d+" gemv_lift_large.log > ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gemv_large_runtime
    }
    {
      lift/scripts/MatrixMultiplication --iterations 210 --loadKernel generated_kernels/mm_nvidia/mm_nvidia.cl -p $OCL_GPU_PLATFORM_ID -d $OCL_GPU_DEVICE_ID -l 32 -l 8 -g 256 -g 128 -s 10 -s 500 -s 64 > gemm_lift_small.log &&
      mkdir -p ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
      grep -oP "(?<=MIN: )\\d+\\.\\d+" gemm_lift_small.log > ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gemm_small_runtime
    }
    {
      lift/scripts/MatrixMultiplication --iterations 210 --loadKernel generated_kernels/mm_nvidia/mm_nvidia.cl -p $OCL_GPU_PLATFORM_ID -d $OCL_GPU_DEVICE_ID -l 32 -l 8 -g 256 -g 128 -s 1024 -s 1024 -s 1024 > gemm_lift_large.log &&
      mkdir -p ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
      grep -oP "(?<=MIN: )\\d+\\.\\d+" gemm_lift_large.log > ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gemm_large_runtime
    }
  )

  # NVIDIA cuDNN
  cd $ARTIFACT_ROOT/build/evaluation/cudnn &&
  mkdir -p ${ARTIFACT_ROOT}/results/gpu/cudnn
  ./cudnn_gaussian --input-size 220 220
  ./cudnn_gaussian --input-size 4092 4092
  ./cudnn_multi_channel_convolution --input-size 16 64 12 120 32
  ./cudnn_multi_channel_convolution --input-size 8 64 54 54 64

  # Lift stencil
  cd $ARTIFACT_ROOT/build/evaluation/lift_stencil &&
  (
    ./bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --application gaussian --input-size small
    ./bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --application gaussian --input-size large
    ./bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --application j3d7pt --input-size small
    ./bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --application j3d7pt --input-size large
  )

  # md_hom_initial
  cd $ARTIFACT_ROOT/build/evaluation/md_hom_initial &&
  mkdir -p $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/

  ./md_hom_initial_gemm --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 10 500 64
  ./md_hom_initial_gemm --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 1024 1024 1024
  ./md_hom_initial_gemv --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 4096 4096
  ./md_hom_initial_gemv --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 8192 8192
  ./md_hom_initial_gaussian_static --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 220 220
  ./md_hom_initial_gaussian_static --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 4092 4092
  ./md_hom_initial_gaussian_dynamic --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 220 220
  ./md_hom_initial_gaussian_dynamic --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 4092 4092
  ./md_hom_initial_j3d7pt --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 254 254 254
  ./md_hom_initial_j3d7pt --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 510 510 510

  printf "\n\nReference execution successful!\n"
} || {
  printf "\n\nReference execution failed!\n"
  exit 1
}