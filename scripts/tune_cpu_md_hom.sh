#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."
	exit 1
fi
: ${OCL_CPU_PLATFORM_ID?"Please set the environment variable OCL_CPU_PLATFORM_ID."}
if [ -z "$OCL_CPU_PLATFORM_ID" ]
then
    	echo "Please set the environment variable OCL_CPU_PLATFORM_ID."
	exit 1
fi
: ${OCL_CPU_DEVICE_ID?"Please set the environment variable OCL_CPU_DEVICE_ID."}
if [ -z "$OCL_CPU_DEVICE_ID" ]
then
    	echo "Please set the environment variable OCL_CPU_DEVICE_ID."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."; exit 1; }
{
  cd build/evaluation/md_hom_new &&
  mkdir -p $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_new/

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_new/gemm_small_runtime &> /dev/null
  ./md_hom_new_gemm --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 10 500 64

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_new/gemm_large_runtime &> /dev/null
  ./md_hom_new_gemm --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 1024 1024 1024

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_new/gemv_small_runtime &> /dev/null
  ./md_hom_new_gemv --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 4096 4096

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_new/gemv_large_runtime &> /dev/null
  ./md_hom_new_gemv --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 8192 8192

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_new/gaussian_static_small_runtime &> /dev/null
  ./md_hom_new_gaussian_static --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 220 220

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_new/gaussian_static_large_runtime &> /dev/null
  ./md_hom_new_gaussian_static --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 4092 4092

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_new/gaussian_dynamic_small_runtime &> /dev/null
  ./md_hom_new_gaussian_dynamic --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 220 220

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_new/gaussian_dynamic_large_runtime &> /dev/null
  ./md_hom_new_gaussian_dynamic --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 4092 4092

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_new/j3d7pt_small_runtime &> /dev/null
  ./md_hom_new_j3d7pt --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 254 254 254

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_new/j3d7pt_large_runtime &> /dev/null
  ./md_hom_new_j3d7pt --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 510 510 510

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_new/multi_channel_convolution_small_runtime &> /dev/null
  ./md_hom_new_multi_channel_convolution --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 16 64 12 120 32

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_new/multi_channel_convolution_large_runtime &> /dev/null
  ./md_hom_new_multi_channel_convolution --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 8 64 54 54 64

  printf "\n\nmd_hom tuning successful!\n"
} || {
  printf "\n\nmd_hom tuning failed!\n"
  exit 1
}