#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."; exit 1; }
mkdir build &> /dev/null || { rm -rf build; mkdir build; }
{
  # download MKL-DNN bundled MKL
  if [[ -z "$DISABLE_CPU" ]]; then
    cd $ARTIFACT_ROOT/extern/mkl-dnn/scripts &&
    ./prepare_mkl.sh
  fi &&

  # initialize CMake project
  cd $ARTIFACT_ROOT/build &&
  cmake -DCMAKE_BUILD_TYPE="Release" ${@:1} .. &&
  make -j `nproc` &&

  # md_hom_new
  cp -r $ARTIFACT_ROOT/evaluation/md_hom_new/kernel $ARTIFACT_ROOT/build/evaluation/md_hom_new/ &&

  # md_hom_initial
  cp -r $ARTIFACT_ROOT/evaluation/md_hom_initial/kernel $ARTIFACT_ROOT/build/evaluation/md_hom_initial/ &&

  # Lift stencils
  cp -r $ARTIFACT_ROOT/evaluation/lift_stencil/artifact/ $ARTIFACT_ROOT/build/evaluation/lift_stencil/ &&
  cd $ARTIFACT_ROOT/build/evaluation/lift_stencil/artifact &&
  source environment.env &&
  ./scripts/installation/build_sbt.sh &&
  ./scripts/installation/build_lift.sh &&
  ./scripts/installation/build_atf.sh &&
  ./scripts/utils/updateTimeout.sh 18000 &&
  ./scripts/run_workflow2_kernel_generation.sh &&

  # Lift BLAS
  cp -r $ARTIFACT_ROOT/evaluation/lift_blas $ARTIFACT_ROOT/build/evaluation/ &&
  cd $ARTIFACT_ROOT/build/evaluation/lift_blas &&
  cd lift &&
  sbt compile &&
  cd .. &&
  ARTIFACT_ROOT=`pwd` ./lift/scripts/GenerateGemv &&
  ARTIFACT_ROOT=`pwd` APART_VECTOR_CAST=1 ./lift/scripts/GenerateMMNvidia &&

  printf "\n\nArtifact installation successful!\n"
} || {
  printf "\n\nArtifact installation failed!\n"
  exit 1
}