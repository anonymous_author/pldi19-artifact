#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."
	exit 1
fi
: ${OCL_GPU_PLATFORM_ID?"Please set the environment variable OCL_GPU_PLATFORM_ID."}
if [ -z "$OCL_GPU_PLATFORM_ID" ]
then
    	echo "Please set the environment variable OCL_GPU_PLATFORM_ID."
	exit 1
fi
: ${OCL_GPU_DEVICE_ID?"Please set the environment variable OCL_GPU_DEVICE_ID."}
if [ -z "$OCL_GPU_DEVICE_ID" ]
then
    	echo "Please set the environment variable OCL_GPU_DEVICE_ID."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."; exit 1; }
{
  cd build/evaluation/md_hom_new &&
  mkdir -p $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/

  ./md_hom_new_gemm --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 10 500 64
  ./md_hom_new_gemm --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 1024 1024 1024
  ./md_hom_new_gemv --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 4096 4096
  ./md_hom_new_gemv --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 8192 8192
  ./md_hom_new_gaussian_static --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 220 220
  ./md_hom_new_gaussian_static --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 4092 4092
  ./md_hom_new_gaussian_dynamic --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 220 220
  ./md_hom_new_gaussian_dynamic --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 4092 4092
  ./md_hom_new_j3d7pt --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 254 254 254
  ./md_hom_new_j3d7pt --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 510 510 510
  ./md_hom_new_multi_channel_convolution --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 16 64 12 120 32
  ./md_hom_new_multi_channel_convolution --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 8 64 54 54 64

  printf "\n\nmd_hom execution successful!\n"
} || {
  printf "\n\nmd_hom execution failed!\n"
  exit 1
}